\select@language {russian}
\contentsline {section}{Введение}{5}{Item.14}
\contentsline {section}{\numberline {1}Обзор работ по тематике исследования}{9}{section.1}
\contentsline {section}{\numberline {2}Формальные обозначения и постановка задачи}{11}{section.2}
\contentsline {subsection}{\numberline {2.1}Формальные обозначения}{11}{subsection.2.1}
\contentsline {subsection}{\numberline {2.2}Последовательный алгоритм UCR-DTW}{12}{subsection.2.2}
\contentsline {section}{\numberline {3}Параллельный алгоритм phiBestMatch}{16}{section.3}
\contentsline {subsection}{\numberline {3.1}Модуль Block'n'Pad}{18}{subsection.3.1}
\contentsline {subsection}{\numberline {3.2}Модуль CalculateLB}{18}{subsection.3.2}
\contentsline {subsection}{\numberline {3.3}Модуль LowerBounding}{19}{subsection.3.3}
\contentsline {subsection}{\numberline {3.4}Модуль FillMatrixDTW}{20}{subsection.3.4}
\contentsline {subsection}{\numberline {3.5}Модуль CalculateDTW}{21}{subsection.3.5}
\contentsline {section}{\numberline {4}Вычислительные эксперименты}{22}{section.4}
\contentsline {subsection}{\numberline {4.1}Аппаратная платформа}{22}{subsection.4.1}
\contentsline {section}{Заключение}{23}{table.caption.1}
\contentsline {section}{\uppercase {Литература}}{24}{section*.2}
