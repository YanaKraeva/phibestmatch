\begin{thebibliography}{10}
\def\selectlanguageifdefined#1{
\expandafter\ifx\csname date#1\endcsname\relax
\else\language\csname l@#1\endcsname\fi}
\providecommand*{\href}[2]{{\small #2}}
\providecommand*{\url}[1]{{\small #1}}
\providecommand*{\BibUrl}[1]{#1}
\providecommand{\BibAnnote}[1]{}
\providecommand*{\BibEmph}[1]{#1}
\makeatletter
\def\@biblabel#1{#1.}
\makeatother
\providecommand*{\cyrdash}{\hbox to.8em{--\hss--}}
\providecommand*{\BibDash}{ -- }

\bibitem{AthitsosPPKG08}
\selectlanguageifdefined{english}
\BibEmph{Athitsos~V.}
  \href{http://dx.doi.org/10.1145/1376616.1376656}{Approximate embedding-based
  subsequence matching of time series}~/ V.~Athitsos, P.~Papapetrou,
  M.~Potamias, G.~Kollios, D.~Gunopulos~// Proceedings of the {ACM} {SIGMOD}
  International Conference on Management of Data, {SIGMOD} 2008, Vancouver, BC,
  Canada, June 10-12, 2008. \BibDash
\newblock 2008. \BibDash
\newblock P.~365--378. \BibDash
\newblock URL: \BibUrl{http://doi.acm.org/10.1145/1376616.1376656}.

\bibitem{Chrysos2012}
\selectlanguageifdefined{english}
\BibEmph{Chrysos~G.}
  \href{http://dx.doi.org/10.1109/HOTCHIPS.2012.7476487}{{Intel{\textregistered}
  Xeon Phi} coprocessor (codename {Knights Corner})}~// 2012 {IEEE Hot Chips
  24th Symposium (HCS)}, Cupertino, CA, USA, August 27--29, 2012. \BibDash
\newblock 2012. \BibDash
\newblock P.~1--31.

\bibitem{Fu2008}
\selectlanguageifdefined{english}
\BibEmph{Fu~A.W.} Scaling and time warping in time series querying~/ A.W.~Fu,
  E.J.~Keogh, L.Y.H.~Lau, C.A.~Ratanamahatana, R.C.~Wong~//
  \href{http://dx.doi.org/10.1007/s00778-006-0040-z}{\BibEmph{{VLDB} J.}}
  \BibDash
\newblock 2008. \BibDash
\newblock Vol.~17. \BibDash
\newblock No.~4. \BibDash
\newblock P.~899--921. \BibDash
\newblock URL: \BibUrl{https://doi.org/10.1007/s00778-006-0040-z}.

\bibitem{HuangDSWWY13}
\selectlanguageifdefined{english}
\BibEmph{Huang~S.}
  \href{http://dx.doi.org/10.1109/HPCC.and.EUC.2013.149}{DTW-Based Subsequence
  Similarity Search on {AMD} Heterogeneous Computing Platform}~/ S.~Huang,
  G.~Dai, Y.~Sun, Z.~Wang, Y.~Wang et~al.~// 10th {IEEE} International
  Conference on High Performance Computing and Communications {\&} 2013 {IEEE}
  International Conference on Embedded and Ubiquitous Computing, {HPCC/EUC}
  2013, Zhangjiajie, China, November 13-15, 2013. \BibDash
\newblock 2013. \BibDash
\newblock P.~1054--1063. \BibDash
\newblock URL: \BibUrl{https://doi.org/10.1109/HPCC.and.EUC.2013.149}.

\bibitem{Ding2008}
\selectlanguageifdefined{english}
\BibEmph{Hui~D.} Querying and Mining of Time Series Data: Experimental
  Comparison of Representations and Distance Measures~/ D.~Hui, T.~Goce,
  S.~Peter, W.~Xiaoyue, K.~Eamonn~//
  \href{http://dx.doi.org/10.14778/1454159.1454226}{\BibEmph{Proceedings of the
  VLDB Endowment}}. \BibDash
\newblock 2008. \BibDash
\newblock Vol.~1. \BibDash
\newblock No.~2. \BibDash
\newblock P.~1542--1552. \BibDash
\newblock URL: \BibUrl{http://dx.doi.org/10.14778/1454159.1454226}.

\bibitem{Jeffers2013}
\selectlanguageifdefined{english}
\BibEmph{Jeffers~J., Reinders~J.} Intel Xeon Phi Coprocessor High Performance
  Programming. 1st {ed.} \BibDash
\newblock San Francisco, CA, USA~: Morgan Kaufmann Publishers Inc., 2013.

\bibitem{KeoghLin2005}
\selectlanguageifdefined{english}
\BibEmph{Keogh~E., Lin~J.} Clustering of time-series subsequences is
  meaningless: implications for previous and future research~//
  \href{http://dx.doi.org/10.1007/s10115-004-0172-7}{\BibEmph{Knowledge and
  Information Systems}}. \BibDash
\newblock 2005. \BibDash Aug. \BibDash
\newblock Vol.~8. \BibDash
\newblock No.~2. \BibDash
\newblock P.~154--177. \BibDash
\newblock URL: \BibUrl{https://doi.org/10.1007/s10115-004-0172-7}.

\bibitem{Keogh2005}
\selectlanguageifdefined{english}
\BibEmph{Keogh~E.J., Ratanamahatana~C.A.} Exact indexing of dynamic time
  warping~// \BibEmph{Knowl. Inf. Syst.} \BibDash
\newblock 2005. \BibDash
\newblock Vol.~7. \BibDash
\newblock No.~3. \BibDash
\newblock P.~358--386. \BibDash
\newblock URL:
  \BibUrl{http://www.springerlink.com/index/10.1007/s10115-004-0154-9}.

\bibitem{Kim2001}
\selectlanguageifdefined{english}
\BibEmph{Kim~S., Park~S., Chu~W.W.}
  \href{http://dx.doi.org/10.1109/ICDE.2001.914875}{An Index-Based Approach for
  Similarity Search Supporting Time Warping in Large Sequence Databases}~//
  Proceedings of the 17th International Conference on Data Engineering, April
  2-6, 2001, Heidelberg, Germany. \BibDash
\newblock 2001. \BibDash
\newblock P.~607--614. \BibDash
\newblock URL: \BibUrl{https://doi.org/10.1109/ICDE.2001.914875}.

\bibitem{Lemire2009}
\selectlanguageifdefined{english}
\BibEmph{Lemire~D.} Faster Retrieval with a Two-pass Dynamic-time-warping Lower
  Bound~//
  \href{http://dx.doi.org/10.1016/j.patcog.2008.11.030}{\BibEmph{Pattern
  Recogn.}} \BibDash
\newblock 2009. \BibDash
\newblock Vol.~42. \BibDash
\newblock No.~9. \BibDash
\newblock P.~2169--2180. \BibDash
\newblock URL: \BibUrl{http://dx.doi.org/10.1016/j.patcog.2008.11.030}.

\bibitem{Movchan2015}
\selectlanguageifdefined{english}
\BibEmph{Movchan~A., Zymbler~M.L.}
  \href{http://dx.doi.org/10.1007/978-3-319-25087-8\_28}{Time Series
  Subsequence Similarity Search Under Dynamic Time Warping Distance on the
  Intel Many-core Accelerators}~// Similarity Search and Applications - 8th
  International Conference, {SISAP} 2015, Glasgow, UK, October 12-14, 2015,
  Proceedings. \BibDash
\newblock 2015. \BibDash
\newblock P.~295--306. \BibDash
\newblock URL: \BibUrl{https://doi.org/10.1007/978-3-319-25087-8\_28}.

\bibitem{MueenKS09}
\selectlanguageifdefined{english}
\BibEmph{Mueen~A., Keogh~E.J., Shamlo~N.B.}
  \href{http://dx.doi.org/10.1109/ICDM.2009.15}{Finding Time Series Motifs in
  Disk-Resident Data}~// {ICDM} 2009, The Ninth {IEEE} International Conference
  on Data Mining, Miami, Florida, USA, 6-9 December 2009. \BibDash
\newblock 2009. \BibDash
\newblock P.~367--376. \BibDash
\newblock URL: \BibUrl{https://doi.org/10.1109/ICDM.2009.15}.

\bibitem{Rakthanmanon2012}
\selectlanguageifdefined{english}
\BibEmph{Rakthanmanon~T.}
  \href{http://dx.doi.org/10.1145/2339530.2339576}{Searching and mining
  trillions of time series subsequences under dynamic time warping}~/
  T.~Rakthanmanon, B.J.L.~Campana, A.~Mueen, G.E.A.P.A.~Batista, M.B.~Westover
  et~al.~// The 18th {ACM} {SIGKDD} International Conference on Knowledge
  Discovery and Data Mining, {KDD} '12, Beijing, China, August 12-16, 2012.
  \BibDash
\newblock 2012. \BibDash
\newblock P.~262--270. \BibDash
\newblock URL: \BibUrl{http://doi.acm.org/10.1145/2339530.2339576}.

\bibitem{Sakoe1990}
\selectlanguageifdefined{english}
\BibEmph{Sakoe~H., Chiba~S.} Readings in Speech Recognition~/ Ed.\ by\
  A.~Waibel, K.F.~Lee. \BibDash
\newblock San Francisco, CA, USA~: Morgan Kaufmann Publishers Inc., 1990.
  \BibDash
\newblock P.~159--165. \BibDash
\newblock URL: \BibUrl{http://dl.acm.org/citation.cfm?id=108235.108244}.

\bibitem{Sart2010}
\selectlanguageifdefined{english}
\BibEmph{Sart~D.} \href{http://dx.doi.org/10.1109/ICDM.2010.21}{Accelerating
  Dynamic Time Warping Subsequence Search with GPUs and FPGAs}~/ D.~Sart,
  A.~Mueen, W.A.~Najjar, E.J.~Keogh, V.~Niennattrakul~// {ICDM} 2010, The 10th
  {IEEE} International Conference on Data Mining, Sydney, Australia, 14-17
  December 2010. \BibDash
\newblock 2010. \BibDash
\newblock P.~1001--1006. \BibDash
\newblock URL: \BibUrl{https://doi.org/10.1109/ICDM.2010.21}.

\bibitem{Shabib2015}
\selectlanguageifdefined{english}
\BibEmph{Shabib~A.}
  \href{http://dx.doi.org/10.1109/ICACCI.2015.7275633}{Parallelization of
  searching and mining time series data using Dynamic Time Warping}~/
  A.~Shabib, A.~Narang, C.P.~Niddodi, M.~Das, R.~Pradeep et~al.~// 2015
  International Conference on Advances in Computing, Communications and
  Informatics, {ICACCI} 2015, Kochi, India, August 10-13, 2015. \BibDash
\newblock 2015. \BibDash
\newblock P.~343--348. \BibDash
\newblock URL: \BibUrl{https://doi.org/10.1109/ICACCI.2015.7275633}.

\bibitem{Sodani2015}
\selectlanguageifdefined{english}
\BibEmph{Sodani~A.} {Knights Landing (KNL):} 2nd Generation
  {Intel{\textregistered} Xeon Phi} processor~// 2015 {IEEE Hot Chips 27th
  Symposium (HCS)}, Cupertino, CA, USA, August 22--25, 2015. \BibDash
\newblock 2015. \BibDash
\newblock P.~1--24.

\bibitem{Srikanthan2011}
\selectlanguageifdefined{english}
\BibEmph{Srikanthan~S., Kumar~A., Gupta~R.}
  \href{http://dx.doi.org/10.1109/ICCCT.2011.6075111}{Implementing the dynamic
  time warping algorithm in multithreaded environments for real time and
  unsupervised pattern discovery}~// 2011 2nd International Conference on
  Computer and Communication Technology, {ICCCT} 2011, Allahabad, India,
  September 15-17, 2011. \BibDash
\newblock 2011. \BibDash
\newblock P.~394--398. \BibDash
\newblock URL: \BibUrl{https://doi.org/10.1109/ICCCT.2011.6075111}.

\bibitem{StaffordInsect}
\selectlanguageifdefined{english}
\BibEmph{Stafford~C.A., Walker~G.P.} Characterization and correlation of DC
  electrical penetration graph waveforms with feeding behavior of beet
  leafhopper, Circulifer tenellus~//
  \href{http://dx.doi.org/10.1111/j.1570-7458.2008.00812.x}{\BibEmph{Entomologia
  Experimentalis et Applicata}}. \BibDash
\newblock Vol. 130. \BibDash
\newblock No.~2. \BibDash
\newblock P.~113--129. \BibDash
\newblock URL: \BibUrl{https://doi.org/10.1111/j.1570-7458.2008.00812.x}.

\bibitem{Takahashi2009}
\selectlanguageifdefined{english}
\BibEmph{Takahashi~N.} \href{http://dx.doi.org/10.1109/CISIS.2009.77}{A
  Parallelized Data Stream Processing System Using Dynamic Time Warping
  Distance}~/ N.~Takahashi, T.~Yoshihisa, Y.~Sakurai, M.~Kanazawa~// 2009
  International Conference on Complex, Intelligent and Software Intensive
  Systems, {CISIS} 2009, Fukuoka, Japan, March 16-19, 2009. \BibDash
\newblock 2009. \BibDash
\newblock P.~1100--1105. \BibDash
\newblock URL: \BibUrl{https://doi.org/10.1109/CISIS.2009.77}.

\bibitem{Tarango2013}
\selectlanguageifdefined{english}
\BibEmph{Tarango~J., Keogh~E.J., Brisk~P.}
  \href{http://dx.doi.org/10.1109/CODES-ISSS.2013.6659005}{Instruction set
  extensions for Dynamic Time Warping}~// Proceedings of the International
  Conference on Hardware/Software Codesign and System Synthesis, {CODES+ISSS}
  2013, Montreal, QC, Canada, September 29 - October 4, 2013. \BibDash
\newblock 2013. \BibDash
\newblock P.~18:1--18:10. \BibDash
\newblock URL: \BibUrl{https://doi.org/10.1109/CODES-ISSS.2013.6659005}.

\bibitem{Wang2013}
\selectlanguageifdefined{english}
\BibEmph{Wang~Z.} \href{http://dx.doi.org/10.1145/2435264.2435277}{Accelerating
  subsequence similarity search based on dynamic time warping distance with
  {FPGA}}~/ Z.~Wang, S.~Huang, L.~Wang, H.~Li, Y.~Wang et~al.~// The 2013
  {ACM/SIGDA} International Symposium on Field Programmable Gate Arrays, {FPGA}
  '13, Monterey, CA, USA, February 11-13, 2013. \BibDash
\newblock 2013. \BibDash
\newblock P.~53--62. \BibDash
\newblock URL: \BibUrl{http://doi.acm.org/10.1145/2435264.2435277}.

\bibitem{XiKSWR06}
\selectlanguageifdefined{english}
\BibEmph{Xi~X.} \href{http://dx.doi.org/10.1145/1143844.1143974}{Fast time
  series classification using numerosity reduction}~/ X.~Xi, E.J.~Keogh,
  C.R.~Shelton, L.~Wei, C.A.~Ratanamahatana~// Machine Learning, Proceedings of
  the Twenty-Third International Conference {(ICML} 2006), Pittsburgh,
  Pennsylvania, USA, June 25-29, 2006. \BibDash
\newblock 2006. \BibDash
\newblock P.~1033--1040. \BibDash
\newblock URL: \BibUrl{http://doi.acm.org/10.1145/1143844.1143974}.

\bibitem{Zhang2012}
\selectlanguageifdefined{english}
\BibEmph{Zhang~Y., Adl~K., Glass~J.R.}
  \href{http://dx.doi.org/10.1109/ICASSP.2012.6289085}{Fast spoken query
  detection using lower-bound Dynamic Time Warping on Graphical Processing
  Units}~// 2012 {IEEE} International Conference on Acoustics, Speech and
  Signal Processing, {ICASSP} 2012, Kyoto, Japan, March 25-30, 2012. \BibDash
\newblock 2012. \BibDash
\newblock P.~5173--5176. \BibDash
\newblock URL: \BibUrl{https://doi.org/10.1109/ICASSP.2012.6289085}.

\bibitem{Zymbler2015}
\selectlanguageifdefined{english}
\BibEmph{Zymbler~M.L.}
  \href{http://dx.doi.org/10.1007/978-3-319-23135-8\_19}{Best-Match Time Series
  Subsequence Search on the Intel Many Integrated Core Architecture}~//
  Advances in Databases and Information Systems - 19th East European
  Conference, {ADBIS} 2015, Poitiers, France, September 8-11, 2015,
  Proceedings. \BibDash
\newblock 2015. \BibDash
\newblock P.~275--286. \BibDash
\newblock URL: \BibUrl{https://doi.org/10.1007/978-3-319-23135-8\_19}.

\end{thebibliography}
\setcounter{bibitems}{25}
