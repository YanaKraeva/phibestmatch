#ifndef ENVELOP_H
#define ENVELOP_H

#include "reader.h"
#include "params.h"

void new_envelope_query(int lenquery, int r, float matrix[368][221]);

void new_envelope_subsequence(int lenquery, int r, int t);

void envelope_query(int lenquery, int r);

void envelope(int lenquery, int r, int t);

void init_matrix(int lenquery);

extern float** UPPER; //double UPPER[COUNTSUBS][128];
extern float** LOWER; //double LOWER[COUNTSUBS][128];
extern float* LOWER_q;  //double LOWER_q[128];
extern float* UPPER_q; //double UPPER_q[128];
#endif