#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "system.h"
#include <malloc.h>
#include "envelope_lb.h"
//#include "envelope.h"

#define min(x,y) ((x)<(y)?(x):(y))
#define max(x,y) ((x)>(y)?(x):(y))
#define dist(x,y) ((x-y)*(x-y))

float** UPPER; 
float** LOWER; 
float* LOWER_q; 
float* UPPER_q; 

void new_envelope_query(int lenquery, float matrix[128][13], int r)
{

    for (int i = 0; i < r; i++)
	for (int j = 0; j < (r - i); j++){
	    matrix[i][j] = QUERY[0];
	}
	    
    for (int i = lenquery-r; i < lenquery; i++)
	for (int j = 0; j < r -(lenquery - i - 1); j++)
	    matrix[i][j] = QUERY[lenquery-1];
	    
 
    for (int i = 0; i < lenquery - r; i++)
	for (int j = max(0, r-i); j <= 2*r; j++){
	    matrix[i][j] = QUERY[i + j - r];
	    }

    for (int i = 0; i < r; i++)
	for (int j = 2*r; j > i; j--)
	    matrix[lenquery - r + i][j] = QUERY[lenquery - j + i];
	   
    for (int i = 0; i < lenquery; i++) {
	float max_value = matrix[i][0];
	float min_value = matrix[i][0];
	for (int j = 0; j <= 2*r; j++) {
	    min_value = min(min_value, matrix[i][j]);
	    max_value = max(max_value, matrix[i][j]);
	}
	UPPER_q[i] = max_value;
	LOWER_q[i] = min_value;
    }
}


#pragma omp declare simd linear(lb_keogh)
void new_envelope_subsequence(int lenquery, float matrix[128][13], int r, int t, float &lb_keogh)
{
    
    //float matrix[128][13] __attribute__((aligned(64)));
    
    float d = 0;//, lb_keogh = 0;

    for (int i = 0; i < r; i++)
	for (int j = 0; j < (r - i); j++)
	    matrix[i][j] = SUBSMATRIX[t][0];
	    
	    
    for (int i = lenquery-r; i < lenquery; i++)
	for (int j = 0; j < r - (lenquery - i - 1); j++)
	    matrix[i][j] = SUBSMATRIX[t][lenquery-1];
	    
    for (int i = 0; i < lenquery - r; i++)
	for (int j = max(0, r-i); j <= 2*r; j++)
	    matrix[i][j] = SUBSMATRIX[t][i + j - r];
	    
    for (int i = 0; i < r; i++)
	for (int j = 2*r; j > i; j--)
	    matrix[lenquery - r + i][j] = SUBSMATRIX[t][lenquery - j + i];

#pragma vector aligned	    
    for (int i = 0; i < lenquery; i++) {
	float  max_value = matrix[i][0];
	float  min_value = matrix[i][0];
	for (int j = 0; j <= 2*r; j++) {
	    min_value = min(min_value, matrix[i][j]);
	    max_value = max(max_value, matrix[i][j]);
	}
	UPPER[t][i] = max_value;
	LOWER[t][i] = min_value;
	d = (QUERY[i] > UPPER[t][i]) ? dist(QUERY[i], UPPER[t][i]) : dist(QUERY[i], LOWER[t][i]);
	d = ((QUERY[i] >= LOWER[t][i]) && (QUERY[i] <= UPPER[t][i])) ? 0 : d;
	lb_keogh += d;
    }

    //return lb_keogh;
}







void init_matrix(int lenquery) {
	UPPER_q = (float*)_align_malloc(lenquery * sizeof(float));
	LOWER_q = (float*)_align_malloc(lenquery * sizeof(float));
	UPPER = (float**)_align_malloc(COUNTSUBS * sizeof(float*));
	LOWER = (float**)_align_malloc(COUNTSUBS * sizeof(float*));
	for (int i = 0; i < COUNTSUBS; i++) {
	    UPPER[i] = (float*)_align_malloc(lenquery * sizeof(float));
	    LOWER[i] = (float*)_align_malloc(lenquery * sizeof(float));
	}
}	
