#ifndef NORM_LB_H
#define NORM_LB_H

#include "reader.h"
#include "params.h"
#include "envelope_lb.h"

void normalize_query(int lenquery);
#pragma omp declare simd
float normalize(int lenquery, int t);

#endif