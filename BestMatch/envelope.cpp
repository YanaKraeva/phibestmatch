#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "system.h"
#include <malloc.h>
#include "envelope.h"

#define min(x,y) ((x)<(y)?(x):(y))
#define max(x,y) ((x)>(y)?(x):(y))


float** UPPER; //UPPER[COUNTSUBS][128];
float** LOWER; //[COUNTSUBS][128];
float* LOWER_q; //[128];
float* UPPER_q; //[128];


void new_envelope_query(int lenquery, int r, float matrix[368][221])
{
    /*for (int i = 0; i < lenquery; i++)
	for (int j = 0; j < max(r-i, i-r-1); j++)
	    matrix[i][j] = (i > r) ? QUERY[lenquery] : QUERY[0];*/

    //float matrix[128][13] __attribute__((aligned(64)));


    for (int i = 0; i < r; i++)
	for (int j = 0; j < (r - i); j++)
	    matrix[i][j] = QUERY[0];

	    
    for (int i = lenquery-r; i < lenquery; i++)
	for (int j = 0; j < r -(lenquery - i - 1); j++)
	    matrix[i][j] = QUERY[lenquery-1];
	    
    for (int i = 0; i < lenquery - r; i++)
	for (int j = max(0, r-i); j <= 2*r; j++)
	    matrix[i][j] = QUERY[i + j - r];

    for (int i = 0; i < r; i++)
	for (int j = 2*r; j > i; j--)
	    matrix[lenquery - r + i][j] = QUERY[lenquery - j + i];
	   
    for (int i = 0; i < lenquery; i++) {
	float max_value = matrix[i][0];
	float min_value = matrix[i][0];
	for (int j = 0; j <= 2*r; j++) {
	    min_value = min(min_value, matrix[i][j]);
	    max_value = max(max_value, matrix[i][j]);
	}
	UPPER_q[i] = max_value;
	LOWER_q[i] = min_value;
    }
}


void new_envelope_subsequence(int lenquery, int r, int t)
{
    /*for (int i = 0; i < lenquery; i++)
	for (int j = 0; j < max(r-i, i-r-1); j++)
	    matrix[i][j] = (i > r) ? QUERY[lenquery] : QUERY[0];*/
    
    
    float matrix[368][221] __attribute__((aligned(64)));

    for (int i = 0; i < r; i++)
	for (int j = 0; j < (r - i); j++)
	    matrix[i][j] = SUBSMATRIX[t][0];
	     
    for (int i = lenquery-r; i < lenquery; i++)
	for (int j = 0; j < r - (lenquery - i - 1); j++)
	    matrix[i][j] = SUBSMATRIX[t][lenquery-1];
	    
    for (int i = 0; i < lenquery - r; i++)
	for (int j = max(0, r-i); j <= 2*r; j++)
	    matrix[i][j] = SUBSMATRIX[t][i + j - r];
	    
    for (int i = 0; i < r; i++)
	for (int j = 2*r; j > i; j--)
	    matrix[lenquery - r + i][j] = SUBSMATRIX[t][lenquery - j + i];
	    
    for (int i = 0; i < lenquery; i++) {
	float  max_value = matrix[i][0];
	float  min_value = matrix[i][0];
	for (int j = 0; j <= 2*r; j++) {
	    min_value = min(min_value, matrix[i][j]);
	    max_value = max(max_value, matrix[i][j]);
	}
	UPPER[t][i] = max_value;
	LOWER[t][i] = min_value;
    }
}









// Data structure (circular array) for finding minimum and maximum for LB_Keogh envolop
struct deque
{   int *dq;
    int size, capacity;
    int f, r;
};

// Initial the queue at the begining step of envelop calculation
void init(deque *d, int capacity)
{
    d->capacity = capacity;
    d->size = 0;
    d->dq = (int*)malloc(sizeof(int)*d->capacity);
    d->f = 0;
    d->r = d->capacity - 1;
}

// Destroy the queue
void destroy(deque *d)
{
    free(d->dq);
}

// Insert to the queue at the back
void push_back(struct deque *d, int v)
{
    d->dq[d->r] = v;
    d->r--;
    if (d->r < 0)
        d->r = d->capacity - 1;
    d->size++;
}

// Delete the current (front) element from queue
void pop_front(struct deque *d)
{
    d->f--;
    if (d->f < 0)
        d->f = d->capacity - 1;
    d->size--;
}

// Delete the last element from queue
void pop_back(struct deque *d)
{
    d->r = (d->r + 1)%d->capacity;
    d->size--;
}

// Get the value at the current position of the circular queue
int front(struct deque *d)
{
    int aux = d->f - 1;

    if (aux < 0)
        aux = d->capacity - 1;
    return d->dq[aux];
}

// Get the value at the last position of the circular queueint back(struct deque *d)
int back(struct deque *d)
{
    int aux = (d->r + 1)%d->capacity;
    return d->dq[aux];
}

// Check whether or not the queue is empty
int empty(struct deque *d)
{
    return d->size == 0;
}

// Finding the envelop of min and max value for LB_Keogh
// Implementation idea is intoruduced by Danial Lemire in his paper
// "Faster Retrieval with a Two-Pass Dynamic-Time-Warping Lower Bound", Pattern Recognition 42(9), 2009.

void envelope(int lenquery, int r, int t)
{
	
    struct deque du, dl;

    init(&du, 2 * r + 2);
    init(&dl, 2 * r + 2);

    push_back(&du, 0);
    push_back(&dl, 0);

    for (int i = 1; i < lenquery; i++)
    {
        if (i > r)
        {
            UPPER[t][i - r - 1] = SUBSMATRIX[t][front(&du)];
            LOWER[t][i - r - 1] = SUBSMATRIX[t][front(&dl)];
        }
        if (SUBSMATRIX[t][i] > SUBSMATRIX[t][i-1])
        {
            pop_back(&du);
            while (!empty(&du) && SUBSMATRIX[t][i] > SUBSMATRIX[t][back(&du)])
                pop_back(&du);
        }
        else
        {
            pop_back(&dl);
            while (!empty(&dl) && SUBSMATRIX[t][i] < SUBSMATRIX[t][back(&dl)])
                pop_back(&dl);
        }
        push_back(&du, i);
        push_back(&dl, i);
        if (i == 2 * r + 1 + front(&du))
            pop_front(&du);
        else if (i == 2 * r + 1 + front(&dl))
            pop_front(&dl);
    }
    for (int i = lenquery; i < lenquery + r + 1; i++)
    {
        UPPER[t][i - r - 1] = SUBSMATRIX[t][front(&du)];
        LOWER[t][i - r - 1] = SUBSMATRIX[t][front(&dl)];
        if (i-front(&du) >= 2 * r + 1)
            pop_front(&du);
        if (i-front(&dl) >= 2 * r + 1)
            pop_front(&dl);
    }
    destroy(&du);
    destroy(&dl);
}


void envelope_query(int lenquery, int r)
{
    struct deque du, dl;

    init(&du, 2 * r + 2);
    init(&dl, 2 * r + 2);

    push_back(&du, 0);
    push_back(&dl, 0);

    for (int i = 1; i < lenquery; i++)
    {
        if (i > r)
        {
            UPPER_q[i - r - 1] = QUERY[front(&du)];
            LOWER_q[i - r - 1] = QUERY[front(&dl)];
        }
        if (QUERY[i] > QUERY[i-1])
        {
            pop_back(&du);
            while (!empty(&du) && QUERY[i] > QUERY[back(&du)])
                pop_back(&du);
        }
        else
        {
            pop_back(&dl);
            while (!empty(&dl) && QUERY[i] < QUERY[back(&dl)])
                pop_back(&dl);
        }
        push_back(&du, i);
        push_back(&dl, i);
        if (i == 2 * r + 1 + front(&du))
            pop_front(&du);
        else if (i == 2 * r + 1 + front(&dl))
            pop_front(&dl);
    }
    for (int i = lenquery; i < lenquery + r + 1; i++)
    {
        UPPER_q[i - r - 1] = QUERY[front(&du)];
        LOWER_q[i - r - 1] = QUERY[front(&dl)];
        if (i-front(&du) >= 2 * r + 1)
            pop_front(&du);
        if (i-front(&dl) >= 2 * r + 1)
            pop_front(&dl);
    }
    destroy(&du);
    destroy(&dl);
}


void init_matrix(int lenquery) {
	UPPER_q = (float*)_align_malloc(lenquery * sizeof(float));
	LOWER_q = (float*)_align_malloc(lenquery * sizeof(float));
	UPPER = (float**)_align_malloc(COUNTSUBS * sizeof(float*));
	LOWER = (float**)_align_malloc(COUNTSUBS * sizeof(float*));
	for (int i = 0; i < COUNTSUBS; i++) {
	    UPPER[i] = (float*)_align_malloc(lenquery * sizeof(float));
	    LOWER[i] = (float*)_align_malloc(lenquery * sizeof(float));
	}
	
}	
