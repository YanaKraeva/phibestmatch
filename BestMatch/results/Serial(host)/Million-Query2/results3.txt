number of a  series of experiments = 0
count of threads = 1

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bsf = 3.79070
position = 430264
DTW Calculation = 3.64%
Time for calculation lower bounding = 17.22%
Time for initializing arrays with start and end segment values = 0.000%
Time for finding the best subsequence = 82.74%
Runtime of reading 0.43
Runtime of finding the best subsequence 30.92


number of a  series of experiments = 0
count of threads = 3

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bsf = 3.79070
position = 430264
DTW Calculation = 3.74%
Time for calculation lower bounding = 37.12%
Time for initializing arrays with start and end segment values = 0.000%
Time for finding the best subsequence = 62.79%
Runtime of reading 0.43
Runtime of finding the best subsequence 14.44


number of a  series of experiments = 0
count of threads = 6

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bsf = 3.79070
position = 430264
DTW Calculation = 3.94%
Time for calculation lower bounding = 51.32%
Time for initializing arrays with start and end segment values = 0.000%
Time for finding the best subsequence = 48.56%
Runtime of reading 0.43
Runtime of finding the best subsequence 10.38


number of a  series of experiments = 0
count of threads = 12

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bsf = 3.79070
position = 430264
DTW Calculation = 3.74%
Time for calculation lower bounding = 68.24%
Time for initializing arrays with start and end segment values = 0.000%
Time for finding the best subsequence = 31.60%
Runtime of reading 0.43
Runtime of finding the best subsequence 7.89


number of a  series of experiments = 0
count of threads = 18

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bsf = 3.79070
position = 430264
DTW Calculation = 3.90%
Time for calculation lower bounding = 76.02%
Time for initializing arrays with start and end segment values = 0.000%
Time for finding the best subsequence = 23.81%
Runtime of reading 0.43
Runtime of finding the best subsequence 7.18


number of a  series of experiments = 0
count of threads = 24

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bsf = 3.79070
position = 430264
DTW Calculation = 3.78%
Time for calculation lower bounding = 79.57%
Time for initializing arrays with start and end segment values = 0.000%
Time for finding the best subsequence = 20.24%
Runtime of reading 0.43
Runtime of finding the best subsequence 6.73


number of a  series of experiments = 1
count of threads = 1

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bsf = 3.79070
position = 430264
DTW Calculation = 3.64%
Time for calculation lower bounding = 17.50%
Time for initializing arrays with start and end segment values = 0.000%
Time for finding the best subsequence = 82.46%
Runtime of reading 0.43
Runtime of finding the best subsequence 30.42


number of a  series of experiments = 1
count of threads = 3

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bsf = 3.79070
position = 430264
DTW Calculation = 3.74%
Time for calculation lower bounding = 36.98%
Time for initializing arrays with start and end segment values = 0.000%
Time for finding the best subsequence = 62.93%
Runtime of reading 0.43
Runtime of finding the best subsequence 14.49


number of a  series of experiments = 1
count of threads = 6

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bsf = 3.79070
position = 430264
DTW Calculation = 3.94%
Time for calculation lower bounding = 52.08%
Time for initializing arrays with start and end segment values = 0.000%
Time for finding the best subsequence = 47.79%
Runtime of reading 0.43
Runtime of finding the best subsequence 10.27


number of a  series of experiments = 1
count of threads = 12

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bsf = 3.79070
position = 430264
DTW Calculation = 3.74%
Time for calculation lower bounding = 68.40%
Time for initializing arrays with start and end segment values = 0.000%
Time for finding the best subsequence = 31.44%
Runtime of reading 0.43
Runtime of finding the best subsequence 7.83


number of a  series of experiments = 1
count of threads = 18

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bsf = 3.79070
position = 430264
DTW Calculation = 3.90%
Time for calculation lower bounding = 75.84%
Time for initializing arrays with start and end segment values = 0.000%
Time for finding the best subsequence = 23.98%
Runtime of reading 0.43
Runtime of finding the best subsequence 7.03


number of a  series of experiments = 1
count of threads = 24

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bsf = 3.79070
position = 430264
DTW Calculation = 3.78%
Time for calculation lower bounding = 79.67%
Time for initializing arrays with start and end segment values = 0.000%
Time for finding the best subsequence = 20.14%
Runtime of reading 0.43
Runtime of finding the best subsequence 6.72


number of a  series of experiments = 2
count of threads = 1

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bsf = 3.79070
position = 430264
DTW Calculation = 3.64%
Time for calculation lower bounding = 17.11%
Time for initializing arrays with start and end segment values = 0.000%
Time for finding the best subsequence = 82.85%
Runtime of reading 0.43
Runtime of finding the best subsequence 31.25


number of a  series of experiments = 2
count of threads = 3

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bsf = 3.79070
position = 430264
DTW Calculation = 3.74%
Time for calculation lower bounding = 37.48%
Time for initializing arrays with start and end segment values = 0.000%
Time for finding the best subsequence = 62.43%
Runtime of reading 0.43
Runtime of finding the best subsequence 14.29


number of a  series of experiments = 2
count of threads = 6

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bsf = 3.79070
position = 430264
DTW Calculation = 3.94%
Time for calculation lower bounding = 52.29%
Time for initializing arrays with start and end segment values = 0.000%
Time for finding the best subsequence = 47.58%
Runtime of reading 0.43
Runtime of finding the best subsequence 10.24


number of a  series of experiments = 2
count of threads = 12

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bsf = 3.79070
position = 430264
DTW Calculation = 3.74%
Time for calculation lower bounding = 68.36%
Time for initializing arrays with start and end segment values = 0.000%
Time for finding the best subsequence = 31.48%
Runtime of reading 0.43
Runtime of finding the best subsequence 7.81


number of a  series of experiments = 2
count of threads = 18

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bsf = 3.79070
position = 430264
DTW Calculation = 3.90%
Time for calculation lower bounding = 75.37%
Time for initializing arrays with start and end segment values = 0.000%
Time for finding the best subsequence = 24.45%
Runtime of reading 0.43
Runtime of finding the best subsequence 7.08


number of a  series of experiments = 2
count of threads = 24

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bsf = 3.79070
position = 430264
DTW Calculation = 3.78%
Time for calculation lower bounding = 79.55%
Time for initializing arrays with start and end segment values = 0.000%
Time for finding the best subsequence = 20.26%
Runtime of reading 0.43
Runtime of finding the best subsequence 6.74


