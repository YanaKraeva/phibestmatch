number of a  series of experiments = 0
count of threads = 1

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bsf = 3.79070
position = 430264
DTW Calculation = 3.64%
Time for calculation lower bounding = 12.00%
Time for initializing arrays with start and end segment values = 0.000%
Time for finding the best subsequence = 87.97%
Runtime of reading 0.43
Runtime of finding the best subsequence 44.68


number of a  series of experiments = 0
count of threads = 3

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bsf = 3.79070
position = 430264
DTW Calculation = 3.74%
Time for calculation lower bounding = 28.78%
Time for initializing arrays with start and end segment values = 0.000%
Time for finding the best subsequence = 71.15%
Runtime of reading 0.43
Runtime of finding the best subsequence 18.58


number of a  series of experiments = 0
count of threads = 6

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bsf = 3.79070
position = 430264
DTW Calculation = 3.94%
Time for calculation lower bounding = 42.57%
Time for initializing arrays with start and end segment values = 0.000%
Time for finding the best subsequence = 57.33%
Runtime of reading 0.43
Runtime of finding the best subsequence 12.56


number of a  series of experiments = 0
count of threads = 12

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bsf = 3.79070
position = 430264
DTW Calculation = 3.74%
Time for calculation lower bounding = 59.58%
Time for initializing arrays with start and end segment values = 0.000%
Time for finding the best subsequence = 40.27%
Runtime of reading 0.43
Runtime of finding the best subsequence 8.99


number of a  series of experiments = 0
count of threads = 18

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bsf = 3.79070
position = 430264
DTW Calculation = 3.89%
Time for calculation lower bounding = 68.66%
Time for initializing arrays with start and end segment values = 0.000%
Time for finding the best subsequence = 31.18%
Runtime of reading 0.43
Runtime of finding the best subsequence 7.81


number of a  series of experiments = 0
count of threads = 24

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bsf = 3.79070
position = 430264
DTW Calculation = 3.78%
Time for calculation lower bounding = 73.66%
Time for initializing arrays with start and end segment values = 0.000%
Time for finding the best subsequence = 26.16%
Runtime of reading 0.43
Runtime of finding the best subsequence 7.27


number of a  series of experiments = 1
count of threads = 1

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bsf = 3.79070
position = 430264
DTW Calculation = 3.64%
Time for calculation lower bounding = 12.19%
Time for initializing arrays with start and end segment values = 0.000%
Time for finding the best subsequence = 87.78%
Runtime of reading 0.43
Runtime of finding the best subsequence 43.68


number of a  series of experiments = 1
count of threads = 3

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bsf = 3.79070
position = 430264
DTW Calculation = 3.74%
Time for calculation lower bounding = 28.44%
Time for initializing arrays with start and end segment values = 0.000%
Time for finding the best subsequence = 71.49%
Runtime of reading 0.43
Runtime of finding the best subsequence 18.81


number of a  series of experiments = 1
count of threads = 6

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bsf = 3.79070
position = 430264
DTW Calculation = 3.94%
Time for calculation lower bounding = 42.23%
Time for initializing arrays with start and end segment values = 0.000%
Time for finding the best subsequence = 57.67%
Runtime of reading 0.43
Runtime of finding the best subsequence 12.61


number of a  series of experiments = 1
count of threads = 12

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bsf = 3.79070
position = 430264
DTW Calculation = 3.74%
Time for calculation lower bounding = 60.19%
Time for initializing arrays with start and end segment values = 0.000%
Time for finding the best subsequence = 39.67%
Runtime of reading 0.43
Runtime of finding the best subsequence 8.88


number of a  series of experiments = 1
count of threads = 18

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bsf = 3.79070
position = 430264
DTW Calculation = 3.89%
Time for calculation lower bounding = 68.33%
Time for initializing arrays with start and end segment values = 0.000%
Time for finding the best subsequence = 31.51%
Runtime of reading 0.43
Runtime of finding the best subsequence 7.83


number of a  series of experiments = 1
count of threads = 24

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bsf = 3.79070
position = 430264
DTW Calculation = 3.78%
Time for calculation lower bounding = 73.26%
Time for initializing arrays with start and end segment values = 0.000%
Time for finding the best subsequence = 26.57%
Runtime of reading 0.43
Runtime of finding the best subsequence 7.30


number of a  series of experiments = 2
count of threads = 1

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bsf = 3.79070
position = 430264
DTW Calculation = 3.64%
Time for calculation lower bounding = 12.02%
Time for initializing arrays with start and end segment values = 0.000%
Time for finding the best subsequence = 87.95%
Runtime of reading 0.43
Runtime of finding the best subsequence 44.55


number of a  series of experiments = 2
count of threads = 3

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bsf = 3.79070
position = 430264
DTW Calculation = 3.74%
Time for calculation lower bounding = 28.57%
Time for initializing arrays with start and end segment values = 0.000%
Time for finding the best subsequence = 71.36%
Runtime of reading 0.43
Runtime of finding the best subsequence 18.64


number of a  series of experiments = 2
count of threads = 6

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bsf = 3.79070
position = 430264
DTW Calculation = 3.94%
Time for calculation lower bounding = 42.16%
Time for initializing arrays with start and end segment values = 0.000%
Time for finding the best subsequence = 57.74%
Runtime of reading 0.43
Runtime of finding the best subsequence 12.69


number of a  series of experiments = 2
count of threads = 12

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bsf = 3.79070
position = 430264
DTW Calculation = 3.74%
Time for calculation lower bounding = 59.91%
Time for initializing arrays with start and end segment values = 0.000%
Time for finding the best subsequence = 39.94%
Runtime of reading 0.43
Runtime of finding the best subsequence 8.91


number of a  series of experiments = 2
count of threads = 18

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bsf = 3.79070
position = 430264
DTW Calculation = 3.89%
Time for calculation lower bounding = 68.08%
Time for initializing arrays with start and end segment values = 0.000%
Time for finding the best subsequence = 31.76%
Runtime of reading 0.43
Runtime of finding the best subsequence 7.84


number of a  series of experiments = 2
count of threads = 24

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bsf = 3.79070
position = 430264
DTW Calculation = 3.78%
Time for calculation lower bounding = 73.19%
Time for initializing arrays with start and end segment values = 0.000%
Time for finding the best subsequence = 26.63%
Runtime of reading 0.43
Runtime of finding the best subsequence 7.32


