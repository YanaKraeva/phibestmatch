#ifndef DTW_H
#define DTW_H

#include "reader.h"
#include "envelope.h"
#include "params.h"

float 			// Returns: DTW distance
dtw(			// Input:
	int t,
	int r,		// size of Sakoe-chiba warping band
	int lenquery,
	float bsf,
	bool big_lb);

#endif