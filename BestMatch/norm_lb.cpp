#include "reader.h"
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "envelope_lb.h"

#define dist(x,y) ((x-y)*(x-y))

void normalize_query(int lenquery)
{
	float ex = 0, ex2 = 0;
	double  mean = 0, std = 0;

	for (int i = 0; i < lenquery; i++)
	{
		ex += QUERY[i];
		ex2 += QUERY[i] * QUERY[i];
	}

	mean = ex/lenquery;
	std = ex2/lenquery;
	std = sqrt(std - mean * mean);

	for (int i = 0; i < lenquery; i++)
		QUERY[i] = float((QUERY[i] - mean) / std);

}
#pragma omp declare simd
float normalize(int lenquery, int t)
{	
	float d = 0, lb_keogh = 0;
	float ex = 0, ex2 = 0;
	double  mean = 0, std = 0;
#pragma vector aligned
	for (int i = 0; i < lenquery; i++)
	{
		ex += SUBSMATRIX[t][i];
		ex2 += SUBSMATRIX[t][i] * SUBSMATRIX[t][i];
	}

	mean = ex/lenquery;
	std = ex2/lenquery;
	std = sqrt(std - mean * mean);

#pragma vector aligned
	for (int i = 0; i < lenquery; i++) {
		SUBSMATRIX[t][i] = float((SUBSMATRIX[t][i] - mean) / std);
		d = (SUBSMATRIX[t][i] > UPPER_q[i]) ? dist(SUBSMATRIX[t][i], UPPER_q[i]) : dist(SUBSMATRIX[t][i], LOWER_q[i]);
		d = ((SUBSMATRIX[t][i] >= LOWER_q[i]) && (SUBSMATRIX[t][i] <= UPPER_q[i])) ? 0 : d;
		lb_keogh += d;
	}

	return lb_keogh;
}