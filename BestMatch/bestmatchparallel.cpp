#include "bestmatchparallel.h"
#include "system.h"
#include "params.h"
#include "dtw.h"
#include "lb.h"
#include "reader.h"
#include "norm.h"
#include "envelope.h"
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <float.h>
#include <omp.h>
#include <malloc.h>
#include <math.h>
#include <iostream>
#include "profiler.h"


using namespace std;

#define min(x,y) ((x)<(y)?(x):(y))
#define PROFILE

#define eps 0.001

double bestmatch(int lenquery, int num_of_threads, int r, int k) {

	float bsf = FLT_MAX, local_bsf = 0;
	bool full_DTW_matrix = false, subseqProcessing = true;
	int current_size_DTW_matrix = 0, num_of_segments = num_of_threads, handleSubseq = 0, result_position = 0, current_segment = 0, countSubsInSegment = 0, num_calc_DTW = 0;
	init_matrix(lenquery);

	float* lb1 = (float*)_align_malloc(COUNTSUBS * sizeof(float)); //array for LB_Kim
	float* lb2 = (float*)_align_malloc(COUNTSUBS * sizeof(float)); //array for LB_Keogh
	float* lb3 = (float*)_align_malloc(COUNTSUBS * sizeof(float)); //array for LB_Keogh_EC
    	int* bitmap_lb1 = (int*)_align_malloc(COUNTSUBS * sizeof(int)); //bitmap for LB_Kim
	int* bitmap_lb2 = (int*)_align_malloc(COUNTSUBS * sizeof(int)); //bitmap for LB_Keogh
	int* bitmap_lb3 = (int*)_align_malloc(COUNTSUBS * sizeof(int)); //bitmap for LB_Keogh_EC
	int* result_bitmap = (int*)_align_malloc(COUNTSUBS * sizeof(int));

	int* endSegment = (int*)_align_malloc(num_of_threads * sizeof(int));
	int* positionInSegment = (int*)_align_malloc(num_of_threads * sizeof(int));
	int* position = (int*)_align_malloc(k * num_of_threads * sizeof(int));
	//float** DTW_matrix = (float**)_align_malloc(k * num_of_threads * sizeof(float*)); //matrix of subsequences for DTW calculation
	
	//init_matrix(lenquery);
	
	float envelope_matrix[368][221] __attribute__((aligned(64)));
	
	
	PRF_INIT;

	double start = omp_get_wtime();
	normalize_query(lenquery); // do z-normalize the query
	//envelope_query(lenquery, r);
	new_envelope_query(lenquery, r, envelope_matrix);


//start1
PRF_START(start1)
#pragma omp parallel for num_threads(num_of_threads)
	for (int i = 0; i < COUNTSUBS; i++) {
		normalize(lenquery, i);	//do z-normalize the data 
		lb1[i] = LB_Kim(lenquery, i);
		lb2[i] = LB_Keogh(lenquery, i);
		new_envelope_subsequence(lenquery, r, i);
		lb3[i] = LB_Keogh_EC(lenquery, i);
	}
PRF_FINISH(finish1); 
//end1


PRF_START(start4);
#pragma omp parallel for num_threads(num_of_threads)
    //#pragma omp parallel for collapse(2)
	for (int i = 0; i < COUNTSUBS; i++) {
#pragma vector aligned	
#pragma ivdep
		for (int j = 0; j < lenquery; j++) {
		    UPPER[i][j] = (lb2[i] > lb3[i]) ? UPPER_q[j] : UPPER[i][j];
		    LOWER[i][j] = (lb2[i] > lb3[i]) ? LOWER_q[j] : LOWER[i][j];
		}
	}
PRF_FINISH(finish4);


	bsf = dtw(0, r, lenquery, bsf, lb2[0] < lb3[0]);

	countSubsInSegment = (int)ceilf((float)COUNTSUBS / num_of_segments); // length of segment

//start2
PRF_START(start2);
#pragma omp parallel for num_threads(num_of_threads) 
	for (int i = 0; i < num_of_segments; i++) {
		positionInSegment[i] = i * countSubsInSegment;
		endSegment[i] = (i + 1) * countSubsInSegment;
	}	
	endSegment[num_of_segments - 1] = COUNTSUBS;
PRF_FINISH(finish2);
//end2

int pruned_subs = 0;

//start3
PRF_START(start3);
	while (subseqProcessing) {
		full_DTW_matrix = false;
		pruned_subs = 0;

		for (int i = 0; i < num_of_threads; i++)
		{
			printf("%d   %d   %d\n", i, positionInSegment[i], endSegment[i]);
		}

#pragma omp parallel num_threads(num_of_threads) reduction(+:pruned_subs)
	{	__ASSUME_ALIGNED(bitmap_lb1, ALIGN_SIZE);
		__ASSUME_ALIGNED(bitmap_lb2, ALIGN_SIZE);
		__ASSUME_ALIGNED(bitmap_lb3, ALIGN_SIZE);
		__ASSUME_ALIGNED(lb1, ALIGN_SIZE);
		__ASSUME_ALIGNED(lb2, ALIGN_SIZE);
		__ASSUME_ALIGNED(lb3, ALIGN_SIZE);
		__ASSUME_ALIGNED(result_bitmap, ALIGN_SIZE);
		int current_thread = omp_get_thread_num();
		assert(positionInSegment[current_thread] <= endSegment[current_thread]);
		for (int i = positionInSegment[current_thread]; i < endSegment[current_thread]; i++) {
			bitmap_lb1[i] = (lb1[i] < bsf);
			bitmap_lb2[i] = (lb2[i] < bsf);
			bitmap_lb3[i] = (lb3[i] < bsf);
			result_bitmap[i] = bitmap_lb1[i] && bitmap_lb2[i] && bitmap_lb3[i];
			if (!result_bitmap[i])
				pruned_subs++;
		}
	}


		printf("pruned_subs = %d\n", pruned_subs);		

		current_size_DTW_matrix = 0;
		while (!full_DTW_matrix) {
			if (positionInSegment[current_segment] < endSegment[current_segment]) {
				if (result_bitmap[positionInSegment[current_segment]]) {
					current_size_DTW_matrix++;
					//DTW_matrix[current_size_DTW_matrix - 1] = SUBSMATRIX[positionInSegment[current_segment]];
					position[current_size_DTW_matrix - 1] = positionInSegment[current_segment];
				}

				positionInSegment[current_segment]++;
				handleSubseq++;
			}

			current_segment++;
			if (current_segment == num_of_segments) 
				current_segment = 0;

			if (current_size_DTW_matrix == num_of_threads * k)
				break;
			if (handleSubseq == COUNTSUBS) {
				subseqProcessing = false;
				break;
			}
		}
		
	for (int i = 0; i < current_size_DTW_matrix; i++)
		{
			printf("i = %d  position[i] = %d\n", i, position[i]);
		}

#pragma omp parallel for num_threads(num_of_threads) shared(bsf, result_position) private(local_bsf)
		for (int i = 0; i < current_size_DTW_matrix; i++) {
			local_bsf = dtw(position[i],  r, lenquery, bsf, lb2[position[i]] < lb3[position[i]]);	
			#pragma omp critical 
			{
				if (bsf > local_bsf) {
					bsf = local_bsf;	//remember position and distance of the best subsequence
					result_position = position[i];
					printf("bsf_local = %f, position = %d\n", bsf, result_position);
				}
			}
		}
		num_calc_DTW += current_size_DTW_matrix;
	}
PRF_FINISH(finish3);
//end3	
	
	double stop = omp_get_wtime();

	//output result
	printf("bsf = %.5lf\n", sqrt(bsf));
	printf("position = %d\n", result_position);
	printf("DTW Calculation = %.2lf%%\n", ((double)num_calc_DTW / (double)COUNTSUBS) * 100);
	printf("Count of DTW = %d\n", num_calc_DTW);



#ifndef NPROFILE
	printf("Time for calculation lower bounding = %.2lf%%\nTime for initializing arrays with start and end segment values = %.3lf%%\n", (finish1 - start1) / (stop - start) * 100,  (finish2 - start2) / (stop - start) * 100);
	printf("Time for finding the best subsequence = %.2lf%%\n", (finish3 - start3) / (stop - start) * 100);
#endif

//	printf("Time for update UPPER and LOWER matrixes = %.2lf\n", (finish4 - start4));
	return stop - start;
}