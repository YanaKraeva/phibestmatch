#ifndef ENVELOPE_LB_H
#define ENVELOPE_LB_H
#include "reader.h"
#include "params.h"

void new_envelope_query(int lenquery, float matrix[128][13], int r);

#pragma omp declare simd linear(lb_keogh)
void new_envelope_subsequence(int lenquery, float matrix[128][13], int r, int t, float & lb_keogh);

void init_matrix(int lenquery);

extern float** UPPER;
extern float** LOWER;
extern float* LOWER_q;
extern float* UPPER_q;
#endif