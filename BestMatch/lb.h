#ifndef LB_H
#define LB_H

#include "reader.h"
#include "envelope.h"
#include "params.h"

#pragma omp declare simd
float			// Returns: LB_Kim
LB_Kim(			// Input: // data
	int lenquery,
	int i);	
						// Returns: LB_Keogh
float LB_Keogh(					// Input:
	int lenquery,
	int t);

float						// Returns: LB_Keogh, LB_KeoghEC
LB_Keogh_EC(					// Input:				
	int lenquery,
	int t);

#endif