#ifndef READER_H
#define READER_H

#include "params.h"

double						// Returns: time series
read_timeseries(			// Input:
	char * filename, int lenquery);		// filename - name of text file to read time series(in one row - one value of time series)

double						// Returns: query
read_query(					// Input:
	 char* filename, int lenquery);		// filename - name of text file to read query(in one row - one value of time series)

extern float* QUERY;		// Query
extern float** SUBSMATRIX; // matrix of subsequences

#endif