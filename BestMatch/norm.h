#ifndef NORM_H
#define NORM_H

#include "reader.h"
#include "params.h"

void normalize_query(int lenquery);

void normalize(int lenquery, int t);

#endif