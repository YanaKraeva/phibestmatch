#include "params.h"
#include "debugger.h"
#include "system.h"
#include "bestmatchparallel.h"
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <iostream>
#include "reader.h"
#include <cmath>

using namespace std;

#define RUNTIME 

//#define namefileTS "//profile01/FRW7/kraevayaa/Desktop/BestMatchParallel/Executable/data.txt"
//#define namefileQ "//profile01/FRW7/kraevayaa/Desktop/BestMatchParallel/Executable/query.txt"

// Parameters of execution
char* timeseries_fname; // filename to read time series
char* query_fname;		 // filename to read query
int r;					 // size of Sakoe-chiba warping band
int lenquery;			 // length of subsequence	
int num_of_threads;		 // number of threads to run the algorithm
int k;

int main(int argc, char * argv[])
{
	timeseries_fname = argv[1];
	assert(timeseries_fname != NULL);
	query_fname = argv[2];
	assert(query_fname != NULL);
	lenquery = atoi(argv[3]);
	double R = atof(argv[4]);

    	if (R <= 1)
		r = (int)floor(R*lenquery);
    	else
        	r = (int)floor(R);
        
    
	k = atoi(argv[5]);

	num_of_threads = (argc == 7 ? atoi(argv[6]) : omp_get_max_threads());
	assert(num_of_threads >= 1 && num_of_threads <= omp_get_max_threads());


	/*timeseries_fname = namefileTS;
	assert(timeseries_fname != NULL);

	query_fname = namefileQ;
	assert(query_fname != NULL);

	scanf("%d\n", &lenquery); //length of subsequence
	
	double R;
	scanf("%lf\n", &R); //Sakoe-chiba band
	 if (R <= 1)
		r = (int)floor(R*lenquery);
    else
        r = (int)floor(R);

	scanf("%d", &k);

	scanf("%d", &num_of_threads); //number of threads
	assert(num_of_threads >= 1 && num_of_threads <= omp_get_max_threads());*/

	double runtime_readQuery = read_query(query_fname, lenquery);

	double runtime_readTS = read_timeseries(timeseries_fname, lenquery);
	double runtime_read = runtime_readTS + runtime_readQuery;
	
	double runtime = bestmatch(lenquery, num_of_threads, r, k);
	

#ifdef RUNTIME
	printf("Runtime of reading %.2lf\n", runtime_read);
	printf("Total Runtime of finding the best subsequence %.2lf\n", runtime);
#endif
	printf("\n\n");
	
	return 0;
}