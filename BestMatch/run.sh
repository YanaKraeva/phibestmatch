#!/bin/sh
# Run algorithm for experiments

DATA=$1
QUERY=$2
M=$3
R=$4
K=$5

for i in 0 1 2
do
	echo "number of a  series of experiments = $i"
	echo "count of threads = 1"
	./bestmatchparallel.phi $DATA $QUERY $M $R $K 1
	echo "count of threads = 10"
	./bestmatchparallel.phi $DATA $QUERY $M $R $K 10
	echo "count of threads = 30"
	./bestmatchparallel.phi $DATA $QUERY $M $R $K 30
	echo "count of threads = 60"
	./bestmatchparallel.phi $DATA $QUERY $M $R $K 60
	echo "count of threads = 120"
	./bestmatchparallel.phi $DATA $QUERY $M $R $K 120
	echo "count of threads = 180"
	./bestmatchparallel.phi $DATA $QUERY $M $R $K 180
	echo "count of threads = 240"
	./bestmatchparallel.phi $DATA $QUERY $M $R $K 240
done