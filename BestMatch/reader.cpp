#include "reader.h"
#include "debugger.h"
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <malloc.h>
#include "system.h"
#include <omp.h>

float* QUERY;		// Query
float** SUBSMATRIX; //matrix of subsequences


double										    // Returns: matrix of subsequences
read_timeseries(char* filename, int lenquery) // Input: filename - name of text file to read time series(in the first row - lenght of time series, in next rows - one value of time series)
{	
	FILE * file = fopen(filename, "rt");
	assert(file != NULL);
	START("Read time series and fill matrix of subsequences");
	SUBSMATRIX = (float**)_align_malloc(COUNTSUBS * sizeof(float*));
	for (int i = 0; i < COUNTSUBS; i++)
		SUBSMATRIX[i] = (float*)_align_malloc(lenquery * sizeof(float));
	int i = 0;
	double start = omp_get_wtime();

	for (int j = 0; j < lenquery; j++)
		fscanf(file, "%f\n", &SUBSMATRIX[i][j]);
	i++;

	while (!feof(file)) 
	{
		for (int j = 0; j < lenquery-1; j++)
			SUBSMATRIX[i][j] = SUBSMATRIX[i-1][j+1];
	
		fscanf(file, "%f\n", &SUBSMATRIX[i][lenquery-1]);
		i++;
	}

	double stop = omp_get_wtime();

	fclose(file);
	FINISH("Read time series and fill matrix of subsequences");
	return stop - start;
}


double										// Returns: query, lenght of query
read_query(char* filename, int lenquery)	// Input: filename - name of text file to read query(in one row - one value of time series))		
{
	FILE * file = fopen(filename, "rt");
	assert(file != NULL);
	START("Read query");

	QUERY = (float*)_align_malloc(lenquery * sizeof(float));

	double start = omp_get_wtime();
 
	for (int i = 0; i < lenquery; i++)
		fscanf(file, "%f\n", &QUERY[i]);

	double stop = omp_get_wtime();

	fclose(file);
	FINISH("Read query");

	return stop - start;
}