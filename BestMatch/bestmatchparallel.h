#ifndef BESTMATCH_H
#define BESTMATCH_H

#include "reader.h"
#include "params.h"

double bestmatch(int lenquery, int num_of_threads, int r, int k);	// Returns: runtime of the algorithm 

#endif