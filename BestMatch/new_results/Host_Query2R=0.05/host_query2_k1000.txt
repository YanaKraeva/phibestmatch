number of a  series of experiments = 0
count of threads = 1

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bfs_local = 331.023254, position = 8
bfs_local = 318.669739, position = 9
bfs_local = 296.215729, position = 10
bfs_local = 287.683807, position = 11
bfs_local = 280.313782, position = 12
bfs_local = 273.292236, position = 13
bfs_local = 266.512299, position = 14
bfs_local = 263.441864, position = 15
bfs_local = 260.405457, position = 16
bfs_local = 260.237671, position = 17
bfs_local = 257.753296, position = 20
bfs_local = 251.134399, position = 21
bfs_local = 242.582504, position = 22
bfs_local = 233.340271, position = 23
bfs_local = 225.882736, position = 24
bfs_local = 218.148422, position = 25
bfs_local = 213.393127, position = 26
bfs_local = 212.282486, position = 27
bfs_local = 210.792252, position = 28
bfs_local = 207.359833, position = 29
bfs_local = 204.954147, position = 30
bfs_local = 202.519257, position = 31
bfs_local = 200.540253, position = 72
bfs_local = 196.008636, position = 73
bfs_local = 189.615891, position = 74
bfs_local = 181.814896, position = 75
bfs_local = 173.498856, position = 76
bfs_local = 167.110184, position = 77
bfs_local = 163.472794, position = 78
bfs_local = 162.806366, position = 79
bfs_local = 161.465118, position = 80
bfs_local = 158.023849, position = 81
bfs_local = 154.501572, position = 82
bfs_local = 149.160400, position = 83
bfs_local = 141.522964, position = 84
bfs_local = 138.315399, position = 85
bfs_local = 137.348206, position = 86
bfs_local = 132.668030, position = 123
bfs_local = 118.871078, position = 124
bfs_local = 106.985115, position = 125
bfs_local = 94.707817, position = 126
bfs_local = 86.262077, position = 127
bfs_local = 80.716881, position = 128
bfs_local = 76.177689, position = 129
bfs_local = 73.051994, position = 130
bfs_local = 71.865570, position = 131
bfs_local = 71.614456, position = 132
bfs_local = 69.846733, position = 133
bfs_local = 63.599754, position = 134
bfs_local = 58.314983, position = 135
bfs_local = 56.493637, position = 136
bfs_local = 56.004280, position = 138
bfs_local = 54.256195, position = 262
bfs_local = 51.119732, position = 263
bfs_local = 46.568924, position = 264
bfs_local = 42.756683, position = 265
bfs_local = 41.278423, position = 266
bfs_local = 38.495296, position = 267
bfs_local = 38.044487, position = 655
bfs_local = 37.672665, position = 1805
bfs_local = 33.796528, position = 1806
bfs_local = 31.685881, position = 1807
bfs_local = 30.068129, position = 1808
bfs_local = 29.133039, position = 2373
bfs_local = 27.872950, position = 2374
bfs_local = 25.931755, position = 2375
bfs_local = 23.420702, position = 25605
bfs_local = 23.140800, position = 38645
bfs_local = 23.113501, position = 38648
bfs_local = 19.419260, position = 48114
bfs_local = 18.486671, position = 48115
bfs_local = 17.464245, position = 48116
bfs_local = 16.939854, position = 48117
bfs_local = 16.353062, position = 223370
bsf = 4.04389
position = 223370
DTW Calculation = 0.67%
Count of DTW = 6729
Time for calculation lower bounding = 98.21%
Time for initializing arrays with start and end segment values = 0.000%
Time for finding the best subsequence = 1.51%
Runtime of reading 0.33
Total Runtime of finding the best subsequence 2.23


number of a  series of experiments = 0
count of threads = 3

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bfs_local = 120.641106, position = 333624
bfs_local = 118.337769, position = 666915
bfs_local = 60.380657, position = 667
bfs_local = 58.020573, position = 666609
bfs_local = 55.308708, position = 666610
bfs_local = 53.240074, position = 666611
bfs_local = 50.745861, position = 666612
bfs_local = 44.466137, position = 667337
bfs_local = 41.149200, position = 667338
bfs_local = 37.575169, position = 667340
bfs_local = 37.158833, position = 667342
bfs_local = 31.491907, position = 333570
bfs_local = 28.767593, position = 333571
bfs_local = 28.072241, position = 333572
bfs_local = 27.896215, position = 333573
bfs_local = 27.872950, position = 2374
bfs_local = 25.931755, position = 2375
bfs_local = 24.583559, position = 33340
bfs_local = 23.140800, position = 38645
bfs_local = 23.113501, position = 38648
bfs_local = 20.539707, position = 430268
bfs_local = 19.419260, position = 48114
bfs_local = 18.486671, position = 48115
bfs_local = 17.464245, position = 48116
bfs_local = 16.939854, position = 48117
bfs_local = 16.353062, position = 223370
bsf = 4.04389
position = 223370
DTW Calculation = 1.18%
Count of DTW = 11815
Time for calculation lower bounding = 97.47%
Time for initializing arrays with start and end segment values = 0.001%
Time for finding the best subsequence = 2.06%
Runtime of reading 0.33
Total Runtime of finding the best subsequence 0.78


number of a  series of experiments = 0
count of threads = 6

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bfs_local = 202.132446, position = 166646
bfs_local = 110.530159, position = 333625
bfs_local = 88.031967, position = 500771
bfs_local = 60.380657, position = 667
bfs_local = 59.029716, position = 500105
bfs_local = 54.218662, position = 500106
bfs_local = 49.332451, position = 500107
bfs_local = 41.522850, position = 500108
bfs_local = 35.000397, position = 500109
bfs_local = 32.391541, position = 500110
bfs_local = 30.698460, position = 500111
bfs_local = 30.681719, position = 833758
bfs_local = 30.068380, position = 833759
bfs_local = 28.800150, position = 833760
bfs_local = 26.926502, position = 167237
bfs_local = 26.025536, position = 167238
bfs_local = 25.445486, position = 48112
bfs_local = 21.121153, position = 519777
bfs_local = 19.419260, position = 48114
bfs_local = 18.486671, position = 48115
bfs_local = 17.464245, position = 48116
bfs_local = 16.939854, position = 48117
bfs_local = 16.353062, position = 223370
bsf = 4.04389
position = 223370
DTW Calculation = 1.50%
Count of DTW = 15003
Time for calculation lower bounding = 96.58%
Time for initializing arrays with start and end segment values = 0.001%
Time for finding the best subsequence = 2.70%
Runtime of reading 0.33
Total Runtime of finding the best subsequence 0.40


number of a  series of experiments = 0
count of threads = 12

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bfs_local = 127.181213, position = 83323
bfs_local = 89.782211, position = 416615
bfs_local = 67.646683, position = 833813
bfs_local = 48.458794, position = 417448
bfs_local = 46.717445, position = 417450
bfs_local = 45.174480, position = 167233
bfs_local = 40.519955, position = 167234
bfs_local = 35.343723, position = 167235
bfs_local = 29.206373, position = 167236
bfs_local = 26.926502, position = 167237
bfs_local = 26.025536, position = 167238
bfs_local = 24.116642, position = 133751
bfs_local = 23.246679, position = 213653
bfs_local = 21.566925, position = 213654
bfs_local = 21.535198, position = 963653
bfs_local = 21.121153, position = 519777
bfs_local = 20.979221, position = 607762
bfs_local = 19.838844, position = 607763
bfs_local = 18.648207, position = 607764
bfs_local = 18.486671, position = 48115
bfs_local = 17.464245, position = 48116
bfs_local = 16.939854, position = 48117
bfs_local = 16.353062, position = 223370
bsf = 4.04389
position = 223370
DTW Calculation = 2.55%
Count of DTW = 25546
Time for calculation lower bounding = 95.66%
Time for initializing arrays with start and end segment values = 0.002%
Time for finding the best subsequence = 3.42%
Runtime of reading 0.33
Total Runtime of finding the best subsequence 0.36


number of a  series of experiments = 0
count of threads = 18

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bfs_local = 142.473938, position = 555545
bfs_local = 107.095947, position = 611094
bfs_local = 99.843033, position = 722192
bfs_local = 88.214615, position = 777741
bfs_local = 74.724007, position = 111209
bfs_local = 60.352810, position = 889561
bfs_local = 53.769878, position = 111487
bfs_local = 49.668869, position = 833957
bfs_local = 49.295986, position = 944499
bfs_local = 28.072241, position = 333572
bfs_local = 27.896215, position = 333573
bfs_local = 26.926502, position = 167237
bfs_local = 26.025536, position = 167238
bfs_local = 24.901159, position = 932726
bfs_local = 23.381083, position = 932727
bfs_local = 22.647320, position = 985129
bfs_local = 19.482222, position = 223368
bfs_local = 17.483942, position = 223369
bfs_local = 16.353062, position = 223370
bsf = 4.04389
position = 223370
DTW Calculation = 3.59%
Count of DTW = 35922
Time for calculation lower bounding = 92.22%
Time for initializing arrays with start and end segment values = 0.003%
Time for finding the best subsequence = 6.63%
Runtime of reading 0.33
Total Runtime of finding the best subsequence 0.25


number of a  series of experiments = 0
count of threads = 24

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bfs_local = 231.727020, position = 334004
bfs_local = 157.921616, position = 500
bfs_local = 95.859253, position = 41662
bfs_local = 52.682213, position = 417453
bfs_local = 45.465416, position = 208310
bfs_local = 44.522770, position = 208477
bfs_local = 44.414856, position = 250764
bfs_local = 37.158833, position = 667342
bfs_local = 30.698460, position = 500111
bfs_local = 29.206373, position = 167236
bfs_local = 26.926502, position = 167237
bfs_local = 26.025536, position = 167238
bfs_local = 23.148270, position = 48113
bfs_local = 19.419260, position = 48114
bfs_local = 18.486671, position = 48115
bfs_local = 17.464245, position = 48116
bfs_local = 16.939854, position = 48117
bfs_local = 16.353062, position = 223370
bsf = 4.04389
position = 223370
DTW Calculation = 4.19%
Count of DTW = 41907
Time for calculation lower bounding = 93.87%
Time for initializing arrays with start and end segment values = 0.003%
Time for finding the best subsequence = 5.23%
Runtime of reading 0.33
Total Runtime of finding the best subsequence 0.31


number of a  series of experiments = 1
count of threads = 1

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bfs_local = 331.023254, position = 8
bfs_local = 318.669739, position = 9
bfs_local = 296.215729, position = 10
bfs_local = 287.683807, position = 11
bfs_local = 280.313782, position = 12
bfs_local = 273.292236, position = 13
bfs_local = 266.512299, position = 14
bfs_local = 263.441864, position = 15
bfs_local = 260.405457, position = 16
bfs_local = 260.237671, position = 17
bfs_local = 257.753296, position = 20
bfs_local = 251.134399, position = 21
bfs_local = 242.582504, position = 22
bfs_local = 233.340271, position = 23
bfs_local = 225.882736, position = 24
bfs_local = 218.148422, position = 25
bfs_local = 213.393127, position = 26
bfs_local = 212.282486, position = 27
bfs_local = 210.792252, position = 28
bfs_local = 207.359833, position = 29
bfs_local = 204.954147, position = 30
bfs_local = 202.519257, position = 31
bfs_local = 200.540253, position = 72
bfs_local = 196.008636, position = 73
bfs_local = 189.615891, position = 74
bfs_local = 181.814896, position = 75
bfs_local = 173.498856, position = 76
bfs_local = 167.110184, position = 77
bfs_local = 163.472794, position = 78
bfs_local = 162.806366, position = 79
bfs_local = 161.465118, position = 80
bfs_local = 158.023849, position = 81
bfs_local = 154.501572, position = 82
bfs_local = 149.160400, position = 83
bfs_local = 141.522964, position = 84
bfs_local = 138.315399, position = 85
bfs_local = 137.348206, position = 86
bfs_local = 132.668030, position = 123
bfs_local = 118.871078, position = 124
bfs_local = 106.985115, position = 125
bfs_local = 94.707817, position = 126
bfs_local = 86.262077, position = 127
bfs_local = 80.716881, position = 128
bfs_local = 76.177689, position = 129
bfs_local = 73.051994, position = 130
bfs_local = 71.865570, position = 131
bfs_local = 71.614456, position = 132
bfs_local = 69.846733, position = 133
bfs_local = 63.599754, position = 134
bfs_local = 58.314983, position = 135
bfs_local = 56.493637, position = 136
bfs_local = 56.004280, position = 138
bfs_local = 54.256195, position = 262
bfs_local = 51.119732, position = 263
bfs_local = 46.568924, position = 264
bfs_local = 42.756683, position = 265
bfs_local = 41.278423, position = 266
bfs_local = 38.495296, position = 267
bfs_local = 38.044487, position = 655
bfs_local = 37.672665, position = 1805
bfs_local = 33.796528, position = 1806
bfs_local = 31.685881, position = 1807
bfs_local = 30.068129, position = 1808
bfs_local = 29.133039, position = 2373
bfs_local = 27.872950, position = 2374
bfs_local = 25.931755, position = 2375
bfs_local = 23.420702, position = 25605
bfs_local = 23.140800, position = 38645
bfs_local = 23.113501, position = 38648
bfs_local = 19.419260, position = 48114
bfs_local = 18.486671, position = 48115
bfs_local = 17.464245, position = 48116
bfs_local = 16.939854, position = 48117
bfs_local = 16.353062, position = 223370
bsf = 4.04389
position = 223370
DTW Calculation = 0.67%
Count of DTW = 6729
Time for calculation lower bounding = 98.22%
Time for initializing arrays with start and end segment values = 0.000%
Time for finding the best subsequence = 1.51%
Runtime of reading 0.33
Total Runtime of finding the best subsequence 2.24


number of a  series of experiments = 1
count of threads = 3

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bfs_local = 180.492981, position = 667248
bfs_local = 158.788956, position = 333291
bfs_local = 120.641106, position = 333624
bfs_local = 60.380657, position = 667
bfs_local = 58.020573, position = 666609
bfs_local = 55.308708, position = 666610
bfs_local = 53.240074, position = 666611
bfs_local = 50.745861, position = 666612
bfs_local = 44.466137, position = 667337
bfs_local = 41.149200, position = 667338
bfs_local = 37.575169, position = 667340
bfs_local = 37.158833, position = 667342
bfs_local = 31.491907, position = 333570
bfs_local = 28.767593, position = 333571
bfs_local = 28.072241, position = 333572
bfs_local = 27.896215, position = 333573
bfs_local = 27.872950, position = 2374
bfs_local = 25.931755, position = 2375
bfs_local = 24.583559, position = 33340
bfs_local = 23.140800, position = 38645
bfs_local = 23.113501, position = 38648
bfs_local = 20.539707, position = 430268
bfs_local = 19.419260, position = 48114
bfs_local = 18.486671, position = 48115
bfs_local = 17.464245, position = 48116
bfs_local = 16.939854, position = 48117
bfs_local = 16.353062, position = 223370
bsf = 4.04389
position = 223370
DTW Calculation = 1.18%
Count of DTW = 11815
Time for calculation lower bounding = 97.26%
Time for initializing arrays with start and end segment values = 0.001%
Time for finding the best subsequence = 2.22%
Runtime of reading 0.33
Total Runtime of finding the best subsequence 0.78


number of a  series of experiments = 1
count of threads = 6

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bfs_local = 202.132446, position = 166646
bfs_local = 151.484543, position = 666750
bfs_local = 110.530159, position = 333625
bfs_local = 60.380657, position = 667
bfs_local = 59.029716, position = 500105
bfs_local = 54.218662, position = 500106
bfs_local = 49.332451, position = 500107
bfs_local = 41.522850, position = 500108
bfs_local = 35.000397, position = 500109
bfs_local = 32.391541, position = 500110
bfs_local = 30.698460, position = 500111
bfs_local = 30.681719, position = 833758
bfs_local = 30.068380, position = 833759
bfs_local = 28.800150, position = 833760
bfs_local = 26.926502, position = 167237
bfs_local = 26.025536, position = 167238
bfs_local = 25.445486, position = 48112
bfs_local = 21.121153, position = 519777
bfs_local = 19.419260, position = 48114
bfs_local = 18.486671, position = 48115
bfs_local = 17.464245, position = 48116
bfs_local = 16.939854, position = 48117
bfs_local = 16.353062, position = 223370
bsf = 4.04389
position = 223370
DTW Calculation = 1.50%
Count of DTW = 15003
Time for calculation lower bounding = 96.56%
Time for initializing arrays with start and end segment values = 0.001%
Time for finding the best subsequence = 2.74%
Runtime of reading 0.33
Total Runtime of finding the best subsequence 0.40


number of a  series of experiments = 1
count of threads = 12

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bfs_local = 128.949112, position = 750
bfs_local = 89.301880, position = 167396
bfs_local = 79.348068, position = 750073
bfs_local = 70.774361, position = 583594
bfs_local = 48.458794, position = 417448
bfs_local = 46.717445, position = 417450
bfs_local = 45.174480, position = 167233
bfs_local = 44.466137, position = 667337
bfs_local = 41.149200, position = 667338
bfs_local = 40.519955, position = 167234
bfs_local = 35.343723, position = 167235
bfs_local = 29.206373, position = 167236
bfs_local = 26.926502, position = 167237
bfs_local = 26.025536, position = 167238
bfs_local = 26.000038, position = 213652
bfs_local = 23.246679, position = 213653
bfs_local = 21.566925, position = 213654
bfs_local = 21.535198, position = 963653
bfs_local = 21.121153, position = 519777
bfs_local = 20.979221, position = 607762
bfs_local = 19.838844, position = 607763
bfs_local = 18.648207, position = 607764
bfs_local = 18.486671, position = 48115
bfs_local = 17.464245, position = 48116
bfs_local = 16.939854, position = 48117
bfs_local = 16.353062, position = 223370
bsf = 4.04389
position = 223370
DTW Calculation = 2.55%
Count of DTW = 25546
Time for calculation lower bounding = 95.47%
Time for initializing arrays with start and end segment values = 0.002%
Time for finding the best subsequence = 3.70%
Runtime of reading 0.33
Total Runtime of finding the best subsequence 0.35


number of a  series of experiments = 1
count of threads = 18

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bfs_local = 221.527237, position = 777963
bfs_local = 187.008728, position = 833512
bfs_local = 115.153267, position = 889061
bfs_local = 98.013000, position = 166925
bfs_local = 74.724007, position = 111209
bfs_local = 28.072241, position = 333572
bfs_local = 27.896215, position = 333573
bfs_local = 26.926502, position = 167237
bfs_local = 26.025536, position = 167238
bfs_local = 24.901159, position = 932726
bfs_local = 23.381083, position = 932727
bfs_local = 23.246679, position = 213653
bfs_local = 19.482222, position = 223368
bfs_local = 17.483942, position = 223369
bfs_local = 16.353062, position = 223370
bsf = 4.04389
position = 223370
DTW Calculation = 3.59%
Count of DTW = 35922
Time for calculation lower bounding = 92.88%
Time for initializing arrays with start and end segment values = 0.003%
Time for finding the best subsequence = 5.94%
Runtime of reading 0.33
Total Runtime of finding the best subsequence 0.26


number of a  series of experiments = 1
count of threads = 24

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bfs_local = 209.666458, position = 666758
bfs_local = 82.662376, position = 666883
bfs_local = 62.963726, position = 458740
bfs_local = 52.682213, position = 417453
bfs_local = 47.460110, position = 833948
bfs_local = 37.158833, position = 667342
bfs_local = 30.698460, position = 500111
bfs_local = 29.206373, position = 167236
bfs_local = 28.767593, position = 333571
bfs_local = 27.353354, position = 250703
bfs_local = 26.926502, position = 167237
bfs_local = 26.025536, position = 167238
bfs_local = 23.148270, position = 48113
bfs_local = 19.419260, position = 48114
bfs_local = 18.486671, position = 48115
bfs_local = 17.464245, position = 48116
bfs_local = 16.939854, position = 48117
bfs_local = 16.353062, position = 223370
bsf = 4.04389
position = 223370
DTW Calculation = 4.19%
Count of DTW = 41907
Time for calculation lower bounding = 92.93%
Time for initializing arrays with start and end segment values = 0.006%
Time for finding the best subsequence = 6.19%
Runtime of reading 0.33
Total Runtime of finding the best subsequence 0.32


number of a  series of experiments = 2
count of threads = 1

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bfs_local = 331.023254, position = 8
bfs_local = 318.669739, position = 9
bfs_local = 296.215729, position = 10
bfs_local = 287.683807, position = 11
bfs_local = 280.313782, position = 12
bfs_local = 273.292236, position = 13
bfs_local = 266.512299, position = 14
bfs_local = 263.441864, position = 15
bfs_local = 260.405457, position = 16
bfs_local = 260.237671, position = 17
bfs_local = 257.753296, position = 20
bfs_local = 251.134399, position = 21
bfs_local = 242.582504, position = 22
bfs_local = 233.340271, position = 23
bfs_local = 225.882736, position = 24
bfs_local = 218.148422, position = 25
bfs_local = 213.393127, position = 26
bfs_local = 212.282486, position = 27
bfs_local = 210.792252, position = 28
bfs_local = 207.359833, position = 29
bfs_local = 204.954147, position = 30
bfs_local = 202.519257, position = 31
bfs_local = 200.540253, position = 72
bfs_local = 196.008636, position = 73
bfs_local = 189.615891, position = 74
bfs_local = 181.814896, position = 75
bfs_local = 173.498856, position = 76
bfs_local = 167.110184, position = 77
bfs_local = 163.472794, position = 78
bfs_local = 162.806366, position = 79
bfs_local = 161.465118, position = 80
bfs_local = 158.023849, position = 81
bfs_local = 154.501572, position = 82
bfs_local = 149.160400, position = 83
bfs_local = 141.522964, position = 84
bfs_local = 138.315399, position = 85
bfs_local = 137.348206, position = 86
bfs_local = 132.668030, position = 123
bfs_local = 118.871078, position = 124
bfs_local = 106.985115, position = 125
bfs_local = 94.707817, position = 126
bfs_local = 86.262077, position = 127
bfs_local = 80.716881, position = 128
bfs_local = 76.177689, position = 129
bfs_local = 73.051994, position = 130
bfs_local = 71.865570, position = 131
bfs_local = 71.614456, position = 132
bfs_local = 69.846733, position = 133
bfs_local = 63.599754, position = 134
bfs_local = 58.314983, position = 135
bfs_local = 56.493637, position = 136
bfs_local = 56.004280, position = 138
bfs_local = 54.256195, position = 262
bfs_local = 51.119732, position = 263
bfs_local = 46.568924, position = 264
bfs_local = 42.756683, position = 265
bfs_local = 41.278423, position = 266
bfs_local = 38.495296, position = 267
bfs_local = 38.044487, position = 655
bfs_local = 37.672665, position = 1805
bfs_local = 33.796528, position = 1806
bfs_local = 31.685881, position = 1807
bfs_local = 30.068129, position = 1808
bfs_local = 29.133039, position = 2373
bfs_local = 27.872950, position = 2374
bfs_local = 25.931755, position = 2375
bfs_local = 23.420702, position = 25605
bfs_local = 23.140800, position = 38645
bfs_local = 23.113501, position = 38648
bfs_local = 19.419260, position = 48114
bfs_local = 18.486671, position = 48115
bfs_local = 17.464245, position = 48116
bfs_local = 16.939854, position = 48117
bfs_local = 16.353062, position = 223370
bsf = 4.04389
position = 223370
DTW Calculation = 0.67%
Count of DTW = 6729
Time for calculation lower bounding = 98.20%
Time for initializing arrays with start and end segment values = 0.000%
Time for finding the best subsequence = 1.52%
Runtime of reading 0.33
Total Runtime of finding the best subsequence 2.24


number of a  series of experiments = 2
count of threads = 3

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bfs_local = 158.788956, position = 333291
bfs_local = 120.641106, position = 333624
bfs_local = 118.337769, position = 666915
bfs_local = 60.380657, position = 667
bfs_local = 58.020573, position = 666609
bfs_local = 55.308708, position = 666610
bfs_local = 53.240074, position = 666611
bfs_local = 50.745861, position = 666612
bfs_local = 44.466137, position = 667337
bfs_local = 41.149200, position = 667338
bfs_local = 37.575169, position = 667340
bfs_local = 37.158833, position = 667342
bfs_local = 31.491907, position = 333570
bfs_local = 28.767593, position = 333571
bfs_local = 28.072241, position = 333572
bfs_local = 27.896215, position = 333573
bfs_local = 27.872950, position = 2374
bfs_local = 25.931755, position = 2375
bfs_local = 24.583559, position = 33340
bfs_local = 23.140800, position = 38645
bfs_local = 23.113501, position = 38648
bfs_local = 20.539707, position = 430268
bfs_local = 19.419260, position = 48114
bfs_local = 18.486671, position = 48115
bfs_local = 17.464245, position = 48116
bfs_local = 16.939854, position = 48117
bfs_local = 16.353062, position = 223370
bsf = 4.04389
position = 223370
DTW Calculation = 1.18%
Count of DTW = 11815
Time for calculation lower bounding = 97.36%
Time for initializing arrays with start and end segment values = 0.001%
Time for finding the best subsequence = 2.17%
Runtime of reading 0.33
Total Runtime of finding the best subsequence 0.77


number of a  series of experiments = 2
count of threads = 6

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bfs_local = 202.132446, position = 166646
bfs_local = 110.530159, position = 333625
bfs_local = 88.031967, position = 500771
bfs_local = 60.380657, position = 667
bfs_local = 59.029716, position = 500105
bfs_local = 54.218662, position = 500106
bfs_local = 49.332451, position = 500107
bfs_local = 41.522850, position = 500108
bfs_local = 35.000397, position = 500109
bfs_local = 32.391541, position = 500110
bfs_local = 30.698460, position = 500111
bfs_local = 30.681719, position = 833758
bfs_local = 30.068380, position = 833759
bfs_local = 28.800150, position = 833760
bfs_local = 26.926502, position = 167237
bfs_local = 26.025536, position = 167238
bfs_local = 25.445486, position = 48112
bfs_local = 21.121153, position = 519777
bfs_local = 19.419260, position = 48114
bfs_local = 18.486671, position = 48115
bfs_local = 17.464245, position = 48116
bfs_local = 16.939854, position = 48117
bfs_local = 16.353062, position = 223370
bsf = 4.04389
position = 223370
DTW Calculation = 1.50%
Count of DTW = 15003
Time for calculation lower bounding = 96.58%
Time for initializing arrays with start and end segment values = 0.001%
Time for finding the best subsequence = 2.68%
Runtime of reading 0.33
Total Runtime of finding the best subsequence 0.42


number of a  series of experiments = 2
count of threads = 12

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bfs_local = 127.181213, position = 83323
bfs_local = 89.782211, position = 416615
bfs_local = 79.348068, position = 750073
bfs_local = 48.458794, position = 417448
bfs_local = 46.717445, position = 417450
bfs_local = 44.466137, position = 667337
bfs_local = 41.149200, position = 667338
bfs_local = 40.519955, position = 167234
bfs_local = 37.575169, position = 667340
bfs_local = 35.000397, position = 500109
bfs_local = 32.391541, position = 500110
bfs_local = 29.206373, position = 167236
bfs_local = 26.926502, position = 167237
bfs_local = 26.025536, position = 167238
bfs_local = 26.000038, position = 213652
bfs_local = 23.246679, position = 213653
bfs_local = 21.566925, position = 213654
bfs_local = 21.535198, position = 963653
bfs_local = 20.979221, position = 607762
bfs_local = 19.838844, position = 607763
bfs_local = 18.648207, position = 607764
bfs_local = 18.551853, position = 122078
bfs_local = 18.403307, position = 122079
bfs_local = 17.464245, position = 48116
bfs_local = 16.939854, position = 48117
bfs_local = 16.353062, position = 223370
bsf = 4.04389
position = 223370
DTW Calculation = 2.55%
Count of DTW = 25546
Time for calculation lower bounding = 96.98%
Time for initializing arrays with start and end segment values = 0.001%
Time for finding the best subsequence = 2.42%
Runtime of reading 0.33
Total Runtime of finding the best subsequence 0.50


number of a  series of experiments = 2
count of threads = 18

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bfs_local = 262.463257, position = 222418
bfs_local = 74.724007, position = 111209
bfs_local = 56.279186, position = 833457
bfs_local = 53.769878, position = 111487
bfs_local = 53.357990, position = 555601
bfs_local = 45.439129, position = 888784
bfs_local = 39.859550, position = 278412
bfs_local = 37.606892, position = 278413
bfs_local = 35.642471, position = 278414
bfs_local = 35.000397, position = 500109
bfs_local = 29.560741, position = 278415
bfs_local = 28.800150, position = 833760
bfs_local = 28.072241, position = 333572
bfs_local = 27.896215, position = 333573
bfs_local = 26.926502, position = 167237
bfs_local = 26.025536, position = 167238
bfs_local = 25.049694, position = 223367
bfs_local = 24.901159, position = 932726
bfs_local = 19.482222, position = 223368
bfs_local = 17.483942, position = 223369
bfs_local = 16.353062, position = 223370
bsf = 4.04389
position = 223370
DTW Calculation = 3.59%
Count of DTW = 35922
Time for calculation lower bounding = 93.54%
Time for initializing arrays with start and end segment values = 0.005%
Time for finding the best subsequence = 5.37%
Runtime of reading 0.33
Total Runtime of finding the best subsequence 0.35


number of a  series of experiments = 2
count of threads = 24

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bfs_local = 161.713303, position = 333504
bfs_local = 140.037231, position = 625
bfs_local = 117.447968, position = 708295
bfs_local = 102.803848, position = 708795
bfs_local = 71.736732, position = 834156
bfs_local = 42.105076, position = 458699
bfs_local = 35.742088, position = 458700
bfs_local = 35.343723, position = 167235
bfs_local = 29.206373, position = 167236
bfs_local = 26.926502, position = 167237
bfs_local = 26.025536, position = 167238
bfs_local = 23.148270, position = 48113
bfs_local = 19.419260, position = 48114
bfs_local = 18.486671, position = 48115
bfs_local = 17.464245, position = 48116
bfs_local = 16.939854, position = 48117
bfs_local = 16.353062, position = 223370
bsf = 4.04389
position = 223370
DTW Calculation = 4.19%
Count of DTW = 41907
Time for calculation lower bounding = 93.46%
Time for initializing arrays with start and end segment values = 0.006%
Time for finding the best subsequence = 5.67%
Runtime of reading 0.33
Total Runtime of finding the best subsequence 0.32


