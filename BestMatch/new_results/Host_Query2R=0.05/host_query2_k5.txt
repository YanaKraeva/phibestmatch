number of a  series of experiments = 0
count of threads = 1

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bfs_local = 331.023254, position = 8
bfs_local = 318.669739, position = 9
bfs_local = 296.215729, position = 10
bfs_local = 287.683807, position = 11
bfs_local = 280.313782, position = 12
bfs_local = 273.292236, position = 13
bfs_local = 266.512299, position = 14
bfs_local = 263.441864, position = 15
bfs_local = 260.405457, position = 16
bfs_local = 260.237671, position = 17
bfs_local = 257.753296, position = 20
bfs_local = 251.134399, position = 21
bfs_local = 242.582504, position = 22
bfs_local = 233.340271, position = 23
bfs_local = 225.882736, position = 24
bfs_local = 218.148422, position = 25
bfs_local = 213.393127, position = 26
bfs_local = 212.282486, position = 27
bfs_local = 210.792252, position = 28
bfs_local = 207.359833, position = 29
bfs_local = 204.954147, position = 30
bfs_local = 202.519257, position = 31
bfs_local = 200.540253, position = 72
bfs_local = 196.008636, position = 73
bfs_local = 189.615891, position = 74
bfs_local = 181.814896, position = 75
bfs_local = 173.498856, position = 76
bfs_local = 167.110184, position = 77
bfs_local = 163.472794, position = 78
bfs_local = 162.806366, position = 79
bfs_local = 161.465118, position = 80
bfs_local = 158.023849, position = 81
bfs_local = 154.501572, position = 82
bfs_local = 149.160400, position = 83
bfs_local = 141.522964, position = 84
bfs_local = 138.315399, position = 85
bfs_local = 137.348206, position = 86
bfs_local = 132.668030, position = 123
bfs_local = 118.871078, position = 124
bfs_local = 106.985115, position = 125
bfs_local = 94.707817, position = 126
bfs_local = 86.262077, position = 127
bfs_local = 80.716881, position = 128
bfs_local = 76.177689, position = 129
bfs_local = 73.051994, position = 130
bfs_local = 71.865570, position = 131
bfs_local = 71.614456, position = 132
bfs_local = 69.846733, position = 133
bfs_local = 63.599754, position = 134
bfs_local = 58.314983, position = 135
bfs_local = 56.493637, position = 136
bfs_local = 56.004280, position = 138
bfs_local = 54.256195, position = 262
bfs_local = 51.119732, position = 263
bfs_local = 46.568924, position = 264
bfs_local = 42.756683, position = 265
bfs_local = 41.278423, position = 266
bfs_local = 38.495296, position = 267
bfs_local = 38.044487, position = 655
bfs_local = 37.672665, position = 1805
bfs_local = 33.796528, position = 1806
bfs_local = 31.685881, position = 1807
bfs_local = 30.068129, position = 1808
bfs_local = 29.133039, position = 2373
bfs_local = 27.872950, position = 2374
bfs_local = 25.931755, position = 2375
bfs_local = 23.420702, position = 25605
bfs_local = 23.140800, position = 38645
bfs_local = 23.113501, position = 38648
bfs_local = 19.419260, position = 48114
bfs_local = 18.486671, position = 48115
bfs_local = 17.464245, position = 48116
bfs_local = 16.939854, position = 48117
bfs_local = 16.353062, position = 223370
bsf = 4.04389
position = 223370
DTW Calculation = 0.48%
Count of DTW = 4831
Time for calculation lower bounding = 58.61%
Time for initializing arrays with start and end segment values = 0.000%
Time for finding the best subsequence = 41.23%
Runtime of reading 0.33
Total Runtime of finding the best subsequence 3.75


number of a  series of experiments = 0
count of threads = 3

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bfs_local = 218.330338, position = 666583
bfs_local = 158.788956, position = 333291
bfs_local = 156.124481, position = 666590
bfs_local = 87.628998, position = 666595
bfs_local = 82.985519, position = 666596
bfs_local = 81.445686, position = 666597
bfs_local = 79.803360, position = 666598
bfs_local = 77.264160, position = 666599
bfs_local = 55.308708, position = 666610
bfs_local = 53.240074, position = 666611
bfs_local = 50.745861, position = 666612
bfs_local = 41.278423, position = 266
bfs_local = 38.495296, position = 267
bfs_local = 38.277462, position = 333569
bfs_local = 31.491907, position = 333570
bfs_local = 28.767593, position = 333571
bfs_local = 28.072241, position = 333572
bfs_local = 27.896215, position = 333573
bfs_local = 27.872950, position = 2374
bfs_local = 25.931755, position = 2375
bfs_local = 25.654453, position = 355279
bfs_local = 25.443348, position = 355278
bfs_local = 23.420702, position = 25605
bfs_local = 23.140800, position = 38645
bfs_local = 23.113501, position = 38648
bfs_local = 16.954269, position = 48119
bfs_local = 16.939854, position = 48117
bfs_local = 16.353062, position = 223370
bsf = 4.04389
position = 223370
DTW Calculation = 0.60%
Count of DTW = 5994
Time for calculation lower bounding = 70.74%
Time for initializing arrays with start and end segment values = 0.001%
Time for finding the best subsequence = 28.92%
Runtime of reading 0.33
Total Runtime of finding the best subsequence 1.07


number of a  series of experiments = 0
count of threads = 6

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bfs_local = 202.132446, position = 166646
bfs_local = 155.770584, position = 499940
bfs_local = 151.416855, position = 499938
bfs_local = 137.644958, position = 666591
bfs_local = 82.985519, position = 666596
bfs_local = 76.357086, position = 499953
bfs_local = 66.931709, position = 666601
bfs_local = 62.773708, position = 499956
bfs_local = 59.640575, position = 499957
bfs_local = 54.200653, position = 499958
bfs_local = 53.240074, position = 666611
bfs_local = 41.673180, position = 499968
bfs_local = 34.082893, position = 499971
bfs_local = 33.048023, position = 499972
bfs_local = 32.391541, position = 500110
bfs_local = 30.698460, position = 500111
bfs_local = 29.771259, position = 333575
bfs_local = 28.072241, position = 333572
bfs_local = 27.896215, position = 333573
bfs_local = 26.926502, position = 167237
bfs_local = 26.025536, position = 167238
bfs_local = 25.931755, position = 2375
bfs_local = 25.079632, position = 170735
bfs_local = 24.677402, position = 170736
bfs_local = 23.857525, position = 850999
bfs_local = 21.121153, position = 519777
bfs_local = 20.759874, position = 872578
bfs_local = 19.419260, position = 48114
bfs_local = 16.954269, position = 48119
bfs_local = 16.939854, position = 48117
bfs_local = 16.353062, position = 223370
bsf = 4.04389
position = 223370
DTW Calculation = 0.65%
Count of DTW = 6474
Time for calculation lower bounding = 78.84%
Time for initializing arrays with start and end segment values = 0.001%
Time for finding the best subsequence = 20.58%
Runtime of reading 0.34
Total Runtime of finding the best subsequence 0.51


number of a  series of experiments = 0
count of threads = 12

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bfs_local = 176.107620, position = 333295
bfs_local = 89.782211, position = 416615
bfs_local = 81.575211, position = 416618
bfs_local = 81.032593, position = 583265
bfs_local = 67.872482, position = 499955
bfs_local = 62.773708, position = 499956
bfs_local = 59.640575, position = 499957
bfs_local = 50.331802, position = 250002
bfs_local = 49.669220, position = 249996
bfs_local = 46.211739, position = 499967
bfs_local = 33.048023, position = 499972
bfs_local = 32.391541, position = 500110
bfs_local = 30.698460, position = 500111
bfs_local = 28.474979, position = 333574
bfs_local = 28.072241, position = 333572
bfs_local = 27.896215, position = 333573
bfs_local = 26.926502, position = 167237
bfs_local = 26.025536, position = 167238
bfs_local = 25.166687, position = 418594
bfs_local = 24.677402, position = 170736
bfs_local = 23.387964, position = 762011
bfs_local = 23.067556, position = 762012
bfs_local = 20.539707, position = 430268
bfs_local = 20.134230, position = 430266
bfs_local = 18.199667, position = 101270
bfs_local = 16.954269, position = 48119
bfs_local = 16.939854, position = 48117
bfs_local = 16.353062, position = 223370
bsf = 4.04389
position = 223370
DTW Calculation = 0.72%
Count of DTW = 7187
Time for calculation lower bounding = 85.04%
Time for initializing arrays with start and end segment values = 0.002%
Time for finding the best subsequence = 14.21%
Runtime of reading 0.33
Total Runtime of finding the best subsequence 0.40


number of a  series of experiments = 0
count of threads = 18

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bfs_local = 206.281052, position = 944334
bfs_local = 128.020615, position = 55551
bfs_local = 114.865158, position = 111100
bfs_local = 111.720886, position = 55549
bfs_local = 55.796890, position = 888787
bfs_local = 51.672268, position = 888786
bfs_local = 45.439129, position = 888784
bfs_local = 45.281212, position = 888785
bfs_local = 40.455219, position = 499975
bfs_local = 35.693993, position = 499970
bfs_local = 34.082893, position = 499971
bfs_local = 33.048023, position = 499972
bfs_local = 31.997919, position = 111200
bfs_local = 31.593561, position = 555610
bfs_local = 30.698460, position = 500111
bfs_local = 28.474979, position = 333574
bfs_local = 28.072241, position = 333572
bfs_local = 27.896215, position = 333573
bfs_local = 26.926502, position = 167237
bfs_local = 26.025536, position = 167238
bfs_local = 25.049694, position = 223367
bfs_local = 19.482222, position = 223368
bfs_local = 17.811596, position = 223371
bfs_local = 17.483942, position = 223369
bfs_local = 16.353062, position = 223370
bsf = 4.04389
position = 223370
DTW Calculation = 0.46%
Count of DTW = 4570
Time for calculation lower bounding = 86.08%
Time for initializing arrays with start and end segment values = 0.003%
Time for finding the best subsequence = 12.86%
Runtime of reading 0.33
Total Runtime of finding the best subsequence 0.28


number of a  series of experiments = 0
count of threads = 24

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bfs_local = 233.588989, position = 166652
bfs_local = 88.150726, position = 41663
bfs_local = 42.535648, position = 208314
bfs_local = 41.510498, position = 208311
bfs_local = 40.140190, position = 208312
bfs_local = 36.467964, position = 208327
bfs_local = 35.693993, position = 499970
bfs_local = 35.160736, position = 208321
bfs_local = 34.082893, position = 499971
bfs_local = 33.048023, position = 499972
bfs_local = 31.045666, position = 333576
bfs_local = 30.698460, position = 500111
bfs_local = 29.771259, position = 333575
bfs_local = 27.896215, position = 333573
bfs_local = 27.508711, position = 167239
bfs_local = 26.926502, position = 167237
bfs_local = 26.025536, position = 167238
bfs_local = 25.166687, position = 418594
bfs_local = 25.135641, position = 378489
bfs_local = 24.677402, position = 170736
bfs_local = 19.168098, position = 462346
bfs_local = 19.021299, position = 462344
bfs_local = 18.850758, position = 462345
bfs_local = 17.503967, position = 48118
bfs_local = 17.464245, position = 48116
bfs_local = 16.954269, position = 48119
bfs_local = 16.939854, position = 48117
bfs_local = 16.353062, position = 223370
bsf = 4.04389
position = 223370
DTW Calculation = 0.58%
Count of DTW = 5750
Time for calculation lower bounding = 87.43%
Time for initializing arrays with start and end segment values = 0.005%
Time for finding the best subsequence = 11.76%
Runtime of reading 0.33
Total Runtime of finding the best subsequence 0.37


number of a  series of experiments = 1
count of threads = 1

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bfs_local = 331.023254, position = 8
bfs_local = 318.669739, position = 9
bfs_local = 296.215729, position = 10
bfs_local = 287.683807, position = 11
bfs_local = 280.313782, position = 12
bfs_local = 273.292236, position = 13
bfs_local = 266.512299, position = 14
bfs_local = 263.441864, position = 15
bfs_local = 260.405457, position = 16
bfs_local = 260.237671, position = 17
bfs_local = 257.753296, position = 20
bfs_local = 251.134399, position = 21
bfs_local = 242.582504, position = 22
bfs_local = 233.340271, position = 23
bfs_local = 225.882736, position = 24
bfs_local = 218.148422, position = 25
bfs_local = 213.393127, position = 26
bfs_local = 212.282486, position = 27
bfs_local = 210.792252, position = 28
bfs_local = 207.359833, position = 29
bfs_local = 204.954147, position = 30
bfs_local = 202.519257, position = 31
bfs_local = 200.540253, position = 72
bfs_local = 196.008636, position = 73
bfs_local = 189.615891, position = 74
bfs_local = 181.814896, position = 75
bfs_local = 173.498856, position = 76
bfs_local = 167.110184, position = 77
bfs_local = 163.472794, position = 78
bfs_local = 162.806366, position = 79
bfs_local = 161.465118, position = 80
bfs_local = 158.023849, position = 81
bfs_local = 154.501572, position = 82
bfs_local = 149.160400, position = 83
bfs_local = 141.522964, position = 84
bfs_local = 138.315399, position = 85
bfs_local = 137.348206, position = 86
bfs_local = 132.668030, position = 123
bfs_local = 118.871078, position = 124
bfs_local = 106.985115, position = 125
bfs_local = 94.707817, position = 126
bfs_local = 86.262077, position = 127
bfs_local = 80.716881, position = 128
bfs_local = 76.177689, position = 129
bfs_local = 73.051994, position = 130
bfs_local = 71.865570, position = 131
bfs_local = 71.614456, position = 132
bfs_local = 69.846733, position = 133
bfs_local = 63.599754, position = 134
bfs_local = 58.314983, position = 135
bfs_local = 56.493637, position = 136
bfs_local = 56.004280, position = 138
bfs_local = 54.256195, position = 262
bfs_local = 51.119732, position = 263
bfs_local = 46.568924, position = 264
bfs_local = 42.756683, position = 265
bfs_local = 41.278423, position = 266
bfs_local = 38.495296, position = 267
bfs_local = 38.044487, position = 655
bfs_local = 37.672665, position = 1805
bfs_local = 33.796528, position = 1806
bfs_local = 31.685881, position = 1807
bfs_local = 30.068129, position = 1808
bfs_local = 29.133039, position = 2373
bfs_local = 27.872950, position = 2374
bfs_local = 25.931755, position = 2375
bfs_local = 23.420702, position = 25605
bfs_local = 23.140800, position = 38645
bfs_local = 23.113501, position = 38648
bfs_local = 19.419260, position = 48114
bfs_local = 18.486671, position = 48115
bfs_local = 17.464245, position = 48116
bfs_local = 16.939854, position = 48117
bfs_local = 16.353062, position = 223370
bsf = 4.04389
position = 223370
DTW Calculation = 0.48%
Count of DTW = 4831
Time for calculation lower bounding = 58.01%
Time for initializing arrays with start and end segment values = 0.000%
Time for finding the best subsequence = 41.83%
Runtime of reading 0.33
Total Runtime of finding the best subsequence 3.78


number of a  series of experiments = 1
count of threads = 3

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bfs_local = 173.220398, position = 333294
bfs_local = 158.788956, position = 333291
bfs_local = 87.628998, position = 666595
bfs_local = 82.985519, position = 666596
bfs_local = 81.445686, position = 666597
bfs_local = 79.803360, position = 666598
bfs_local = 77.264160, position = 666599
bfs_local = 55.308708, position = 666610
bfs_local = 53.240074, position = 666611
bfs_local = 50.745861, position = 666612
bfs_local = 41.995934, position = 269
bfs_local = 41.278423, position = 266
bfs_local = 38.495296, position = 267
bfs_local = 38.277462, position = 333569
bfs_local = 31.491907, position = 333570
bfs_local = 28.767593, position = 333571
bfs_local = 28.072241, position = 333572
bfs_local = 27.896215, position = 333573
bfs_local = 27.872950, position = 2374
bfs_local = 25.931755, position = 2375
bfs_local = 25.654453, position = 355279
bfs_local = 25.443348, position = 355278
bfs_local = 23.420702, position = 25605
bfs_local = 23.140800, position = 38645
bfs_local = 23.113501, position = 38648
bfs_local = 16.954269, position = 48119
bfs_local = 16.939854, position = 48117
bfs_local = 16.353062, position = 223370
bsf = 4.04389
position = 223370
DTW Calculation = 0.60%
Count of DTW = 5994
Time for calculation lower bounding = 66.41%
Time for initializing arrays with start and end segment values = 0.001%
Time for finding the best subsequence = 33.22%
Runtime of reading 0.33
Total Runtime of finding the best subsequence 1.20


number of a  series of experiments = 1
count of threads = 6

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bfs_local = 202.132446, position = 166646
bfs_local = 176.107620, position = 333295
bfs_local = 155.770584, position = 499940
bfs_local = 151.416855, position = 499938
bfs_local = 82.985519, position = 666596
bfs_local = 76.357086, position = 499953
bfs_local = 66.931709, position = 666601
bfs_local = 62.773708, position = 499956
bfs_local = 59.640575, position = 499957
bfs_local = 54.200653, position = 499958
bfs_local = 53.240074, position = 666611
bfs_local = 41.673180, position = 499968
bfs_local = 34.082893, position = 499971
bfs_local = 33.048023, position = 499972
bfs_local = 32.391541, position = 500110
bfs_local = 30.698460, position = 500111
bfs_local = 28.072241, position = 333572
bfs_local = 27.896215, position = 333573
bfs_local = 26.926502, position = 167237
bfs_local = 26.025536, position = 167238
bfs_local = 25.931755, position = 2375
bfs_local = 25.079632, position = 170735
bfs_local = 24.677402, position = 170736
bfs_local = 23.857525, position = 850999
bfs_local = 21.121153, position = 519777
bfs_local = 20.759874, position = 872578
bfs_local = 19.419260, position = 48114
bfs_local = 16.954269, position = 48119
bfs_local = 16.939854, position = 48117
bfs_local = 16.353062, position = 223370
bsf = 4.04389
position = 223370
DTW Calculation = 0.65%
Count of DTW = 6474
Time for calculation lower bounding = 79.18%
Time for initializing arrays with start and end segment values = 0.001%
Time for finding the best subsequence = 20.20%
Runtime of reading 0.33
Total Runtime of finding the best subsequence 0.51


number of a  series of experiments = 1
count of threads = 12

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bfs_local = 155.770584, position = 499940
bfs_local = 92.913994, position = 583263
bfs_local = 81.032593, position = 583265
bfs_local = 77.264160, position = 666599
bfs_local = 76.831688, position = 499952
bfs_local = 62.773708, position = 499956
bfs_local = 54.208485, position = 249997
bfs_local = 50.331802, position = 250002
bfs_local = 46.211739, position = 499967
bfs_local = 33.048023, position = 499972
bfs_local = 32.391541, position = 500110
bfs_local = 30.698460, position = 500111
bfs_local = 28.474979, position = 333574
bfs_local = 28.072241, position = 333572
bfs_local = 27.896215, position = 333573
bfs_local = 26.926502, position = 167237
bfs_local = 26.025536, position = 167238
bfs_local = 25.166687, position = 418594
bfs_local = 24.677402, position = 170736
bfs_local = 23.387964, position = 762011
bfs_local = 23.067556, position = 762012
bfs_local = 20.539707, position = 430268
bfs_local = 20.134230, position = 430266
bfs_local = 18.199667, position = 101270
bfs_local = 16.954269, position = 48119
bfs_local = 16.939854, position = 48117
bfs_local = 16.353062, position = 223370
bsf = 4.04389
position = 223370
DTW Calculation = 0.72%
Count of DTW = 7187
Time for calculation lower bounding = 81.33%
Time for initializing arrays with start and end segment values = 0.002%
Time for finding the best subsequence = 17.74%
Runtime of reading 0.33
Total Runtime of finding the best subsequence 0.34


number of a  series of experiments = 1
count of threads = 18

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bfs_local = 184.339569, position = 611042
bfs_local = 137.644958, position = 666591
bfs_local = 111.720886, position = 55549
bfs_local = 55.796890, position = 888787
bfs_local = 45.439129, position = 888784
bfs_local = 45.281212, position = 888785
bfs_local = 40.455219, position = 499975
bfs_local = 35.693993, position = 499970
bfs_local = 34.082893, position = 499971
bfs_local = 33.048023, position = 499972
bfs_local = 32.391541, position = 500110
bfs_local = 31.593561, position = 555610
bfs_local = 30.698460, position = 500111
bfs_local = 28.474979, position = 333574
bfs_local = 28.072241, position = 333572
bfs_local = 27.896215, position = 333573
bfs_local = 26.926502, position = 167237
bfs_local = 26.025536, position = 167238
bfs_local = 25.049694, position = 223367
bfs_local = 19.482222, position = 223368
bfs_local = 17.811596, position = 223371
bfs_local = 17.483942, position = 223369
bfs_local = 16.353062, position = 223370
bsf = 4.04389
position = 223370
DTW Calculation = 0.46%
Count of DTW = 4570
Time for calculation lower bounding = 85.90%
Time for initializing arrays with start and end segment values = 0.003%
Time for finding the best subsequence = 13.03%
Runtime of reading 0.33
Total Runtime of finding the best subsequence 0.28


number of a  series of experiments = 1
count of threads = 24

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bfs_local = 95.859253, position = 41662
bfs_local = 85.579994, position = 416620
bfs_local = 75.574539, position = 41665
bfs_local = 45.465416, position = 208310
bfs_local = 41.614014, position = 208313
bfs_local = 41.510498, position = 208311
bfs_local = 40.140190, position = 208312
bfs_local = 38.382408, position = 208315
bfs_local = 37.619644, position = 208320
bfs_local = 36.467964, position = 208327
bfs_local = 35.160736, position = 208321
bfs_local = 34.082893, position = 499971
bfs_local = 33.048023, position = 499972
bfs_local = 30.698460, position = 500111
bfs_local = 27.896215, position = 333573
bfs_local = 26.926502, position = 167237
bfs_local = 26.025536, position = 167238
bfs_local = 25.166687, position = 418594
bfs_local = 25.135641, position = 378489
bfs_local = 19.168098, position = 462346
bfs_local = 19.021299, position = 462344
bfs_local = 18.850758, position = 462345
bfs_local = 17.464245, position = 48116
bfs_local = 16.939854, position = 48117
bfs_local = 16.353062, position = 223370
bsf = 4.04389
position = 223370
DTW Calculation = 0.58%
Count of DTW = 5750
Time for calculation lower bounding = 83.48%
Time for initializing arrays with start and end segment values = 0.009%
Time for finding the best subsequence = 15.55%
Runtime of reading 0.33
Total Runtime of finding the best subsequence 0.40


number of a  series of experiments = 2
count of threads = 1

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bfs_local = 331.023254, position = 8
bfs_local = 318.669739, position = 9
bfs_local = 296.215729, position = 10
bfs_local = 287.683807, position = 11
bfs_local = 280.313782, position = 12
bfs_local = 273.292236, position = 13
bfs_local = 266.512299, position = 14
bfs_local = 263.441864, position = 15
bfs_local = 260.405457, position = 16
bfs_local = 260.237671, position = 17
bfs_local = 257.753296, position = 20
bfs_local = 251.134399, position = 21
bfs_local = 242.582504, position = 22
bfs_local = 233.340271, position = 23
bfs_local = 225.882736, position = 24
bfs_local = 218.148422, position = 25
bfs_local = 213.393127, position = 26
bfs_local = 212.282486, position = 27
bfs_local = 210.792252, position = 28
bfs_local = 207.359833, position = 29
bfs_local = 204.954147, position = 30
bfs_local = 202.519257, position = 31
bfs_local = 200.540253, position = 72
bfs_local = 196.008636, position = 73
bfs_local = 189.615891, position = 74
bfs_local = 181.814896, position = 75
bfs_local = 173.498856, position = 76
bfs_local = 167.110184, position = 77
bfs_local = 163.472794, position = 78
bfs_local = 162.806366, position = 79
bfs_local = 161.465118, position = 80
bfs_local = 158.023849, position = 81
bfs_local = 154.501572, position = 82
bfs_local = 149.160400, position = 83
bfs_local = 141.522964, position = 84
bfs_local = 138.315399, position = 85
bfs_local = 137.348206, position = 86
bfs_local = 132.668030, position = 123
bfs_local = 118.871078, position = 124
bfs_local = 106.985115, position = 125
bfs_local = 94.707817, position = 126
bfs_local = 86.262077, position = 127
bfs_local = 80.716881, position = 128
bfs_local = 76.177689, position = 129
bfs_local = 73.051994, position = 130
bfs_local = 71.865570, position = 131
bfs_local = 71.614456, position = 132
bfs_local = 69.846733, position = 133
bfs_local = 63.599754, position = 134
bfs_local = 58.314983, position = 135
bfs_local = 56.493637, position = 136
bfs_local = 56.004280, position = 138
bfs_local = 54.256195, position = 262
bfs_local = 51.119732, position = 263
bfs_local = 46.568924, position = 264
bfs_local = 42.756683, position = 265
bfs_local = 41.278423, position = 266
bfs_local = 38.495296, position = 267
bfs_local = 38.044487, position = 655
bfs_local = 37.672665, position = 1805
bfs_local = 33.796528, position = 1806
bfs_local = 31.685881, position = 1807
bfs_local = 30.068129, position = 1808
bfs_local = 29.133039, position = 2373
bfs_local = 27.872950, position = 2374
bfs_local = 25.931755, position = 2375
bfs_local = 23.420702, position = 25605
bfs_local = 23.140800, position = 38645
bfs_local = 23.113501, position = 38648
bfs_local = 19.419260, position = 48114
bfs_local = 18.486671, position = 48115
bfs_local = 17.464245, position = 48116
bfs_local = 16.939854, position = 48117
bfs_local = 16.353062, position = 223370
bsf = 4.04389
position = 223370
DTW Calculation = 0.48%
Count of DTW = 4831
Time for calculation lower bounding = 57.76%
Time for initializing arrays with start and end segment values = 0.000%
Time for finding the best subsequence = 42.08%
Runtime of reading 0.33
Total Runtime of finding the best subsequence 3.81


number of a  series of experiments = 2
count of threads = 3

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bfs_local = 158.788956, position = 333291
bfs_local = 87.628998, position = 666595
bfs_local = 82.985519, position = 666596
bfs_local = 81.445686, position = 666597
bfs_local = 79.803360, position = 666598
bfs_local = 77.264160, position = 666599
bfs_local = 55.308708, position = 666610
bfs_local = 53.240074, position = 666611
bfs_local = 50.745861, position = 666612
bfs_local = 41.278423, position = 266
bfs_local = 38.495296, position = 267
bfs_local = 38.277462, position = 333569
bfs_local = 31.491907, position = 333570
bfs_local = 28.767593, position = 333571
bfs_local = 28.072241, position = 333572
bfs_local = 27.896215, position = 333573
bfs_local = 27.872950, position = 2374
bfs_local = 25.931755, position = 2375
bfs_local = 25.654453, position = 355279
bfs_local = 25.443348, position = 355278
bfs_local = 23.420702, position = 25605
bfs_local = 23.140800, position = 38645
bfs_local = 23.113501, position = 38648
bfs_local = 16.954269, position = 48119
bfs_local = 16.939854, position = 48117
bfs_local = 16.353062, position = 223370
bsf = 4.04389
position = 223370
DTW Calculation = 0.60%
Count of DTW = 5994
Time for calculation lower bounding = 69.64%
Time for initializing arrays with start and end segment values = 0.001%
Time for finding the best subsequence = 30.00%
Runtime of reading 0.33
Total Runtime of finding the best subsequence 1.10


number of a  series of experiments = 2
count of threads = 6

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bfs_local = 155.770584, position = 499940
bfs_local = 153.798874, position = 499939
bfs_local = 151.416855, position = 499938
bfs_local = 137.644958, position = 666591
bfs_local = 82.985519, position = 666596
bfs_local = 76.357086, position = 499953
bfs_local = 66.931709, position = 666601
bfs_local = 62.773708, position = 499956
bfs_local = 59.640575, position = 499957
bfs_local = 53.240074, position = 666611
bfs_local = 41.673180, position = 499968
bfs_local = 34.082893, position = 499971
bfs_local = 33.048023, position = 499972
bfs_local = 32.391541, position = 500110
bfs_local = 30.698460, position = 500111
bfs_local = 28.072241, position = 333572
bfs_local = 27.896215, position = 333573
bfs_local = 26.926502, position = 167237
bfs_local = 26.025536, position = 167238
bfs_local = 25.931755, position = 2375
bfs_local = 25.079632, position = 170735
bfs_local = 24.677402, position = 170736
bfs_local = 23.857525, position = 850999
bfs_local = 21.121153, position = 519777
bfs_local = 20.759874, position = 872578
bfs_local = 19.419260, position = 48114
bfs_local = 16.954269, position = 48119
bfs_local = 16.939854, position = 48117
bfs_local = 16.353062, position = 223370
bsf = 4.04389
position = 223370
DTW Calculation = 0.65%
Count of DTW = 6474
Time for calculation lower bounding = 79.62%
Time for initializing arrays with start and end segment values = 0.001%
Time for finding the best subsequence = 19.76%
Runtime of reading 0.33
Total Runtime of finding the best subsequence 0.50


number of a  series of experiments = 2
count of threads = 12

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bfs_local = 176.107620, position = 333295
bfs_local = 81.575211, position = 416618
bfs_local = 81.032593, position = 583265
bfs_local = 76.831688, position = 499952
bfs_local = 67.872482, position = 499955
bfs_local = 62.773708, position = 499956
bfs_local = 53.460228, position = 666614
bfs_local = 50.331802, position = 250002
bfs_local = 46.211739, position = 499967
bfs_local = 33.048023, position = 499972
bfs_local = 32.391541, position = 500110
bfs_local = 30.698460, position = 500111
bfs_local = 28.474979, position = 333574
bfs_local = 28.072241, position = 333572
bfs_local = 27.896215, position = 333573
bfs_local = 26.926502, position = 167237
bfs_local = 26.025536, position = 167238
bfs_local = 25.166687, position = 418594
bfs_local = 24.677402, position = 170736
bfs_local = 23.387964, position = 762011
bfs_local = 23.067556, position = 762012
bfs_local = 20.539707, position = 430268
bfs_local = 20.134230, position = 430266
bfs_local = 18.199667, position = 101270
bfs_local = 16.954269, position = 48119
bfs_local = 16.939854, position = 48117
bfs_local = 16.353062, position = 223370
bsf = 4.04389
position = 223370
DTW Calculation = 0.72%
Count of DTW = 7187
Time for calculation lower bounding = 83.96%
Time for initializing arrays with start and end segment values = 0.002%
Time for finding the best subsequence = 15.33%
Runtime of reading 0.33
Total Runtime of finding the best subsequence 0.43


number of a  series of experiments = 2
count of threads = 18

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bfs_local = 154.081146, position = 499943
bfs_local = 150.119476, position = 555492
bfs_local = 131.186279, position = 55552
bfs_local = 111.720886, position = 55549
bfs_local = 55.796890, position = 888787
bfs_local = 45.439129, position = 888784
bfs_local = 45.281212, position = 888785
bfs_local = 35.693993, position = 499970
bfs_local = 34.082893, position = 499971
bfs_local = 33.048023, position = 499972
bfs_local = 32.391541, position = 500110
bfs_local = 31.997919, position = 111200
bfs_local = 31.593561, position = 555610
bfs_local = 30.698460, position = 500111
bfs_local = 28.474979, position = 333574
bfs_local = 28.072241, position = 333572
bfs_local = 27.896215, position = 333573
bfs_local = 26.926502, position = 167237
bfs_local = 26.025536, position = 167238
bfs_local = 25.049694, position = 223367
bfs_local = 19.482222, position = 223368
bfs_local = 17.811596, position = 223371
bfs_local = 17.483942, position = 223369
bfs_local = 16.353062, position = 223370
bsf = 4.04389
position = 223370
DTW Calculation = 0.46%
Count of DTW = 4570
Time for calculation lower bounding = 88.75%
Time for initializing arrays with start and end segment values = 0.002%
Time for finding the best subsequence = 10.35%
Runtime of reading 0.33
Total Runtime of finding the best subsequence 0.32


number of a  series of experiments = 2
count of threads = 24

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bfs_local = 146.366180, position = 624930
bfs_local = 108.753876, position = 249973
bfs_local = 88.150726, position = 41663
bfs_local = 87.327576, position = 541608
bfs_local = 83.045456, position = 374959
bfs_local = 41.614014, position = 208313
bfs_local = 41.510498, position = 208311
bfs_local = 40.140190, position = 208312
bfs_local = 38.382408, position = 208315
bfs_local = 37.619644, position = 208320
bfs_local = 36.467964, position = 208327
bfs_local = 35.160736, position = 208321
bfs_local = 33.178238, position = 499973
bfs_local = 33.048023, position = 499972
bfs_local = 30.698460, position = 500111
bfs_local = 27.896215, position = 333573
bfs_local = 26.926502, position = 167237
bfs_local = 26.025536, position = 167238
bfs_local = 25.166687, position = 418594
bfs_local = 25.135641, position = 378489
bfs_local = 24.677402, position = 170736
bfs_local = 19.168098, position = 462346
bfs_local = 19.021299, position = 462344
bfs_local = 18.850758, position = 462345
bfs_local = 17.503967, position = 48118
bfs_local = 17.464245, position = 48116
bfs_local = 16.954269, position = 48119
bfs_local = 16.939854, position = 48117
bfs_local = 16.353062, position = 223370
bsf = 4.04389
position = 223370
DTW Calculation = 0.58%
Count of DTW = 5750
Time for calculation lower bounding = 84.40%
Time for initializing arrays with start and end segment values = 0.009%
Time for finding the best subsequence = 14.61%
Runtime of reading 0.33
Total Runtime of finding the best subsequence 0.35


