number of a  series of experiments = 0
count of threads = 1

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bfs_local = 331.023224, position = 8
bfs_local = 318.669739, position = 9
bfs_local = 296.215698, position = 10
bfs_local = 287.683777, position = 11
bfs_local = 280.313782, position = 12
bfs_local = 273.292236, position = 13
bfs_local = 266.512299, position = 14
bfs_local = 263.441864, position = 15
bfs_local = 260.405457, position = 16
bfs_local = 260.237671, position = 17
bfs_local = 257.753296, position = 20
bfs_local = 251.134384, position = 21
bfs_local = 242.582504, position = 22
bfs_local = 233.340286, position = 23
bfs_local = 225.882736, position = 24
bfs_local = 218.148438, position = 25
bfs_local = 213.393097, position = 26
bfs_local = 212.282486, position = 27
bfs_local = 210.792236, position = 28
bfs_local = 207.359818, position = 29
bfs_local = 204.954117, position = 30
bfs_local = 202.519287, position = 31
bfs_local = 200.540237, position = 72
bfs_local = 196.008636, position = 73
bfs_local = 189.615891, position = 74
bfs_local = 181.814896, position = 75
bfs_local = 173.498856, position = 76
bfs_local = 167.110184, position = 77
bfs_local = 163.472778, position = 78
bfs_local = 162.806366, position = 79
bfs_local = 161.465103, position = 80
bfs_local = 158.023819, position = 81
bfs_local = 154.501556, position = 82
bfs_local = 149.160400, position = 83
bfs_local = 141.522949, position = 84
bfs_local = 138.315399, position = 85
bfs_local = 137.348206, position = 86
bfs_local = 132.668030, position = 123
bfs_local = 118.871078, position = 124
bfs_local = 106.985107, position = 125
bfs_local = 94.707901, position = 126
bfs_local = 86.262062, position = 127
bfs_local = 80.716850, position = 128
bfs_local = 76.177666, position = 129
bfs_local = 73.051987, position = 130
bfs_local = 71.865570, position = 131
bfs_local = 71.614456, position = 132
bfs_local = 69.846680, position = 133
bfs_local = 63.599731, position = 134
bfs_local = 58.314983, position = 135
bfs_local = 56.493622, position = 136
bfs_local = 56.004276, position = 138
bfs_local = 54.256336, position = 262
bfs_local = 51.119877, position = 263
bfs_local = 46.568928, position = 264
bfs_local = 42.756687, position = 265
bfs_local = 41.278046, position = 266
bfs_local = 38.494972, position = 267
bfs_local = 38.044487, position = 655
bfs_local = 37.672665, position = 1805
bfs_local = 33.796532, position = 1806
bfs_local = 31.685883, position = 1807
bfs_local = 30.068125, position = 1808
bfs_local = 29.133123, position = 2373
bfs_local = 27.872950, position = 2374
bfs_local = 25.931755, position = 2375
bfs_local = 23.420702, position = 25605
bfs_local = 23.130287, position = 38645
bfs_local = 19.407377, position = 48114
bfs_local = 18.483559, position = 48115
bfs_local = 17.459635, position = 48116
bfs_local = 16.934858, position = 48117
bfs_local = 16.353062, position = 223370
bsf = 4.04389
position = 223370
DTW Calculation = 0.48%
Count of DTW = 4845
Time for calculation lower bounding = 54.58%
Time for initializing arrays with start and end segment values = 0.000%
Time for finding the best subsequence = 45.14%
Runtime of reading 4.59
Total Runtime of finding the best subsequence 57.26


count of threads = 10

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bfs_local = 289.128693, position = 399952
bfs_local = 151.363342, position = 299964
bfs_local = 113.971619, position = 199976
bfs_local = 112.258888, position = 699921
bfs_local = 111.261742, position = 799910
bfs_local = 108.489250, position = 499947
bfs_local = 103.638023, position = 799911
bfs_local = 93.406296, position = 499948
bfs_local = 91.307190, position = 799913
bfs_local = 84.736084, position = 499949
bfs_local = 81.348854, position = 499950
bfs_local = 79.191963, position = 499951
bfs_local = 76.958809, position = 499953
bfs_local = 75.747482, position = 799916
bfs_local = 65.492813, position = 799919
bfs_local = 62.470425, position = 499956
bfs_local = 54.297779, position = 499958
bfs_local = 51.635513, position = 499960
bfs_local = 51.066620, position = 499959
bfs_local = 41.421024, position = 499968
bfs_local = 37.247112, position = 499969
bfs_local = 33.979160, position = 499971
bfs_local = 33.048027, position = 499972
bfs_local = 31.671900, position = 300125
bfs_local = 30.698458, position = 500111
bfs_local = 29.949909, position = 600410
bfs_local = 26.974546, position = 600411
bfs_local = 22.740864, position = 901009
bfs_local = 18.198055, position = 101270
bfs_local = 17.479368, position = 607764
bfs_local = 16.353062, position = 223370
bsf = 4.04389
position = 223370
DTW Calculation = 0.45%
Count of DTW = 4483
Time for calculation lower bounding = 90.03%
Time for initializing arrays with start and end segment values = 0.000%
Time for finding the best subsequence = 9.69%
Runtime of reading 4.53
Total Runtime of finding the best subsequence 3.51


count of threads = 30

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bfs_local = 50.329647, position = 33330
bfs_local = 48.832935, position = 33331
bfs_local = 45.873783, position = 33334
bfs_local = 43.030128, position = 33335
bfs_local = 40.641998, position = 33336
bfs_local = 31.377510, position = 33338
bfs_local = 28.665384, position = 33339
bfs_local = 25.671486, position = 33341
bfs_local = 24.583557, position = 33340
bfs_local = 24.116644, position = 133751
bfs_local = 24.107256, position = 133752
bfs_local = 23.764566, position = 901008
bfs_local = 22.740864, position = 901009
bfs_local = 20.246920, position = 101268
bfs_local = 18.198055, position = 101270
bfs_local = 17.479368, position = 607764
bfs_local = 16.955357, position = 48119
bfs_local = 16.934858, position = 48117
bfs_local = 16.353062, position = 223370
bsf = 4.04389
position = 223370
DTW Calculation = 0.51%
Count of DTW = 5140
Time for calculation lower bounding = 87.10%
Time for initializing arrays with start and end segment values = 0.000%
Time for finding the best subsequence = 12.90%
Runtime of reading 4.52
Total Runtime of finding the best subsequence 1.24


count of threads = 60

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bfs_local = 127.573715, position = 83325
bfs_local = 50.329647, position = 33330
bfs_local = 48.832935, position = 33331
bfs_local = 31.377510, position = 33338
bfs_local = 28.665384, position = 33339
bfs_local = 24.583557, position = 33340
bfs_local = 24.116644, position = 133751
bfs_local = 24.107256, position = 133752
bfs_local = 23.553417, position = 850999
bfs_local = 22.740864, position = 901009
bfs_local = 21.285566, position = 101269
bfs_local = 20.246920, position = 101268
bfs_local = 18.198055, position = 101270
bfs_local = 17.483942, position = 223369
bfs_local = 16.353062, position = 223370
bsf = 4.04389
position = 223370
DTW Calculation = 0.54%
Count of DTW = 5435
Time for calculation lower bounding = 75.00%
Time for initializing arrays with start and end segment values = 0.000%
Time for finding the best subsequence = 23.68%
Runtime of reading 4.46
Total Runtime of finding the best subsequence 0.76


count of threads = 120

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bfs_local = 177.204559, position = 508313
bfs_local = 166.494659, position = 491647
bfs_local = 91.644447, position = 8333
bfs_local = 64.157387, position = 516646
bfs_local = 52.756535, position = 33332
bfs_local = 45.707260, position = 358319
bfs_local = 39.546230, position = 208325
bfs_local = 26.300177, position = 33343
bfs_local = 25.671486, position = 33341
bfs_local = 25.567745, position = 33342
bfs_local = 24.583557, position = 33340
bfs_local = 24.437056, position = 58439
bfs_local = 23.352354, position = 158414
bfs_local = 22.178610, position = 158413
bfs_local = 21.801853, position = 58440
bfs_local = 20.246920, position = 101268
bfs_local = 18.198055, position = 101270
bfs_local = 16.955357, position = 48119
bfs_local = 16.934858, position = 48117
bfs_local = 16.353062, position = 223370
bsf = 4.04389
position = 223370
DTW Calculation = 0.64%
Count of DTW = 6365
Time for calculation lower bounding = 65.67%
Time for initializing arrays with start and end segment values = 0.000%
Time for finding the best subsequence = 34.33%
Runtime of reading 4.52
Total Runtime of finding the best subsequence 0.67


count of threads = 180

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bfs_local = 204.575226, position = 333300
bfs_local = 61.626919, position = 322190
bfs_local = 50.329647, position = 33330
bfs_local = 31.377510, position = 33338
bfs_local = 28.665384, position = 33339
bfs_local = 24.583557, position = 33340
bfs_local = 23.627203, position = 605584
bfs_local = 23.301130, position = 239072
bfs_local = 20.759872, position = 872578
bfs_local = 17.483942, position = 223369
bfs_local = 16.353062, position = 223370
bsf = 4.04389
position = 223370
DTW Calculation = 0.58%
Count of DTW = 5812
Time for calculation lower bounding = 58.02%
Time for initializing arrays with start and end segment values = 0.000%
Time for finding the best subsequence = 40.74%
Runtime of reading 4.53
Total Runtime of finding the best subsequence 0.81


count of threads = 240

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bfs_local = 144.754059, position = 495873
bfs_local = 57.434135, position = 750060
bfs_local = 43.474602, position = 279189
bfs_local = 42.733345, position = 387531
bfs_local = 40.641998, position = 33336
bfs_local = 32.391575, position = 191682
bfs_local = 30.163301, position = 441727
bfs_local = 28.665384, position = 33339
bfs_local = 25.671486, position = 33341
bfs_local = 25.567745, position = 33342
bfs_local = 24.583557, position = 33340
bfs_local = 22.178610, position = 158413
bfs_local = 21.801853, position = 58440
bfs_local = 21.641378, position = 963649
bfs_local = 19.550343, position = 430266
bfs_local = 18.404104, position = 122079
bfs_local = 18.198055, position = 101270
bfs_local = 17.503759, position = 48118
bfs_local = 16.955357, position = 48119
bfs_local = 16.934858, position = 48117
bfs_local = 16.353062, position = 223370
bsf = 4.04389
position = 223370
DTW Calculation = 0.68%
Count of DTW = 6835
Time for calculation lower bounding = 58.62%
Time for initializing arrays with start and end segment values = 0.000%
Time for finding the best subsequence = 41.38%
Runtime of reading 4.46
Total Runtime of finding the best subsequence 0.87


number of a  series of experiments = 1
count of threads = 1

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bfs_local = 331.023224, position = 8
bfs_local = 318.669739, position = 9
bfs_local = 296.215698, position = 10
bfs_local = 287.683777, position = 11
bfs_local = 280.313782, position = 12
bfs_local = 273.292236, position = 13
bfs_local = 266.512299, position = 14
bfs_local = 263.441864, position = 15
bfs_local = 260.405457, position = 16
bfs_local = 260.237671, position = 17
bfs_local = 257.753296, position = 20
bfs_local = 251.134384, position = 21
bfs_local = 242.582504, position = 22
bfs_local = 233.340286, position = 23
bfs_local = 225.882736, position = 24
bfs_local = 218.148438, position = 25
bfs_local = 213.393097, position = 26
bfs_local = 212.282486, position = 27
bfs_local = 210.792236, position = 28
bfs_local = 207.359818, position = 29
bfs_local = 204.954117, position = 30
bfs_local = 202.519287, position = 31
bfs_local = 200.540237, position = 72
bfs_local = 196.008636, position = 73
bfs_local = 189.615891, position = 74
bfs_local = 181.814896, position = 75
bfs_local = 173.498856, position = 76
bfs_local = 167.110184, position = 77
bfs_local = 163.472778, position = 78
bfs_local = 162.806366, position = 79
bfs_local = 161.465103, position = 80
bfs_local = 158.023819, position = 81
bfs_local = 154.501556, position = 82
bfs_local = 149.160400, position = 83
bfs_local = 141.522949, position = 84
bfs_local = 138.315399, position = 85
bfs_local = 137.348206, position = 86
bfs_local = 132.668030, position = 123
bfs_local = 118.871078, position = 124
bfs_local = 106.985107, position = 125
bfs_local = 94.707901, position = 126
bfs_local = 86.262062, position = 127
bfs_local = 80.716850, position = 128
bfs_local = 76.177666, position = 129
bfs_local = 73.051987, position = 130
bfs_local = 71.865570, position = 131
bfs_local = 71.614456, position = 132
bfs_local = 69.846680, position = 133
bfs_local = 63.599731, position = 134
bfs_local = 58.314983, position = 135
bfs_local = 56.493622, position = 136
bfs_local = 56.004276, position = 138
bfs_local = 54.256336, position = 262
bfs_local = 51.119877, position = 263
bfs_local = 46.568928, position = 264
bfs_local = 42.756687, position = 265
bfs_local = 41.278046, position = 266
bfs_local = 38.494972, position = 267
bfs_local = 38.044487, position = 655
bfs_local = 37.672665, position = 1805
bfs_local = 33.796532, position = 1806
bfs_local = 31.685883, position = 1807
bfs_local = 30.068125, position = 1808
bfs_local = 29.133123, position = 2373
bfs_local = 27.872950, position = 2374
bfs_local = 25.931755, position = 2375
bfs_local = 23.420702, position = 25605
bfs_local = 23.130287, position = 38645
bfs_local = 19.407377, position = 48114
bfs_local = 18.483559, position = 48115
bfs_local = 17.459635, position = 48116
bfs_local = 16.934858, position = 48117
bfs_local = 16.353062, position = 223370
bsf = 4.04389
position = 223370
DTW Calculation = 0.48%
Count of DTW = 4845
Time for calculation lower bounding = 54.78%
Time for initializing arrays with start and end segment values = 0.000%
Time for finding the best subsequence = 44.96%
Runtime of reading 4.56
Total Runtime of finding the best subsequence 57.01


count of threads = 10

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bfs_local = 156.528534, position = 99988
bfs_local = 130.915771, position = 699916
bfs_local = 113.971619, position = 199976
bfs_local = 112.258888, position = 699921
bfs_local = 111.261742, position = 799910
bfs_local = 108.489250, position = 499947
bfs_local = 103.638023, position = 799911
bfs_local = 93.406296, position = 499948
bfs_local = 84.736084, position = 499949
bfs_local = 81.348854, position = 499950
bfs_local = 79.191963, position = 499951
bfs_local = 72.915298, position = 799917
bfs_local = 67.872482, position = 499955
bfs_local = 65.492813, position = 799919
bfs_local = 62.470425, position = 499956
bfs_local = 54.297779, position = 499958
bfs_local = 51.066620, position = 499959
bfs_local = 45.972466, position = 499967
bfs_local = 33.979160, position = 499971
bfs_local = 33.048027, position = 499972
bfs_local = 32.860783, position = 300126
bfs_local = 31.671900, position = 300125
bfs_local = 30.698458, position = 500111
bfs_local = 29.949909, position = 600410
bfs_local = 26.974546, position = 600411
bfs_local = 22.740864, position = 901009
bfs_local = 18.198055, position = 101270
bfs_local = 17.479368, position = 607764
bfs_local = 16.353062, position = 223370
bsf = 4.04389
position = 223370
DTW Calculation = 0.45%
Count of DTW = 4483
Time for calculation lower bounding = 89.77%
Time for initializing arrays with start and end segment values = 0.000%
Time for finding the best subsequence = 9.66%
Runtime of reading 4.47
Total Runtime of finding the best subsequence 3.52


count of threads = 30

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bfs_local = 139.371826, position = 933240
bfs_local = 65.584999, position = 799920
bfs_local = 50.329647, position = 33330
bfs_local = 48.832935, position = 33331
bfs_local = 40.641998, position = 33336
bfs_local = 31.377510, position = 33338
bfs_local = 28.665384, position = 33339
bfs_local = 25.567745, position = 33342
bfs_local = 24.583557, position = 33340
bfs_local = 24.107256, position = 133752
bfs_local = 23.764566, position = 901008
bfs_local = 22.740864, position = 901009
bfs_local = 21.285566, position = 101269
bfs_local = 18.198055, position = 101270
bfs_local = 17.479368, position = 607764
bfs_local = 16.955357, position = 48119
bfs_local = 16.934858, position = 48117
bfs_local = 16.353062, position = 223370
bsf = 4.04389
position = 223370
DTW Calculation = 0.51%
Count of DTW = 5140
Time for calculation lower bounding = 86.29%
Time for initializing arrays with start and end segment values = 0.000%
Time for finding the best subsequence = 12.90%
Runtime of reading 4.56
Total Runtime of finding the best subsequence 1.24


count of threads = 60

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bfs_local = 177.154343, position = 983235
bfs_local = 81.348854, position = 499950
bfs_local = 79.859474, position = 783255
bfs_local = 68.890007, position = 666600
bfs_local = 50.329647, position = 33330
bfs_local = 48.832935, position = 33331
bfs_local = 45.873783, position = 33334
bfs_local = 43.030128, position = 33335
bfs_local = 31.377510, position = 33338
bfs_local = 28.665384, position = 33339
bfs_local = 25.567745, position = 33342
bfs_local = 24.583557, position = 33340
bfs_local = 24.107256, position = 133752
bfs_local = 23.553417, position = 850999
bfs_local = 22.740864, position = 901009
bfs_local = 20.246920, position = 101268
bfs_local = 18.198055, position = 101270
bfs_local = 17.483942, position = 223369
bfs_local = 16.353062, position = 223370
bsf = 4.04389
position = 223370
DTW Calculation = 0.54%
Count of DTW = 5435
Time for calculation lower bounding = 75.00%
Time for initializing arrays with start and end segment values = 0.000%
Time for finding the best subsequence = 23.68%
Runtime of reading 4.45
Total Runtime of finding the best subsequence 0.76


count of threads = 120

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bfs_local = 177.204559, position = 508313
bfs_local = 96.566780, position = 499980
bfs_local = 52.756535, position = 33332
bfs_local = 39.546230, position = 208325
bfs_local = 31.060169, position = 33344
bfs_local = 28.665384, position = 33339
bfs_local = 24.583557, position = 33340
bfs_local = 22.178610, position = 158413
bfs_local = 21.801853, position = 58440
bfs_local = 21.285566, position = 101269
bfs_local = 20.246920, position = 101268
bfs_local = 18.892626, position = 101271
bfs_local = 18.198055, position = 101270
bfs_local = 17.459635, position = 48116
bfs_local = 16.955357, position = 48119
bfs_local = 16.934858, position = 48117
bfs_local = 16.353062, position = 223370
bsf = 4.04389
position = 223370
DTW Calculation = 0.64%
Count of DTW = 6365
Time for calculation lower bounding = 63.24%
Time for initializing arrays with start and end segment values = 0.000%
Time for finding the best subsequence = 35.29%
Runtime of reading 4.46
Total Runtime of finding the best subsequence 0.68


count of threads = 180

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bfs_local = 68.161568, position = 672155
bfs_local = 61.626919, position = 322190
bfs_local = 50.329647, position = 33330
bfs_local = 31.377510, position = 33338
bfs_local = 28.665384, position = 33339
bfs_local = 25.671486, position = 33341
bfs_local = 24.583557, position = 33340
bfs_local = 23.627203, position = 605584
bfs_local = 23.301130, position = 239072
bfs_local = 22.579802, position = 872577
bfs_local = 21.164333, position = 872579
bfs_local = 20.759872, position = 872578
bfs_local = 19.166168, position = 223372
bfs_local = 16.353062, position = 223370
bsf = 4.04389
position = 223370
DTW Calculation = 0.58%
Count of DTW = 5812
Time for calculation lower bounding = 59.49%
Time for initializing arrays with start and end segment values = 0.000%
Time for finding the best subsequence = 40.51%
Runtime of reading 4.47
Total Runtime of finding the best subsequence 0.79


count of threads = 240

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bfs_local = 143.905182, position = 758394
bfs_local = 135.211685, position = 254187
bfs_local = 88.063103, position = 20835
bfs_local = 80.983765, position = 250020
bfs_local = 57.434135, position = 750060
bfs_local = 52.981373, position = 437535
bfs_local = 33.597698, position = 308358
bfs_local = 32.391575, position = 191682
bfs_local = 25.671486, position = 33341
bfs_local = 25.567745, position = 33342
bfs_local = 24.583557, position = 33340
bfs_local = 22.178610, position = 158413
bfs_local = 21.801853, position = 58440
bfs_local = 19.550343, position = 430266
bfs_local = 18.557951, position = 122078
bfs_local = 18.404104, position = 122079
bfs_local = 18.198055, position = 101270
bfs_local = 16.955357, position = 48119
bfs_local = 16.934858, position = 48117
bfs_local = 16.353062, position = 223370
bsf = 4.04389
position = 223370
DTW Calculation = 0.68%
Count of DTW = 6835
Time for calculation lower bounding = 58.14%
Time for initializing arrays with start and end segment values = 0.000%
Time for finding the best subsequence = 40.70%
Runtime of reading 4.50
Total Runtime of finding the best subsequence 0.86


number of a  series of experiments = 2
count of threads = 1

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bfs_local = 331.023224, position = 8
bfs_local = 318.669739, position = 9
bfs_local = 296.215698, position = 10
bfs_local = 287.683777, position = 11
bfs_local = 280.313782, position = 12
bfs_local = 273.292236, position = 13
bfs_local = 266.512299, position = 14
bfs_local = 263.441864, position = 15
bfs_local = 260.405457, position = 16
bfs_local = 260.237671, position = 17
bfs_local = 257.753296, position = 20
bfs_local = 251.134384, position = 21
bfs_local = 242.582504, position = 22
bfs_local = 233.340286, position = 23
bfs_local = 225.882736, position = 24
bfs_local = 218.148438, position = 25
bfs_local = 213.393097, position = 26
bfs_local = 212.282486, position = 27
bfs_local = 210.792236, position = 28
bfs_local = 207.359818, position = 29
bfs_local = 204.954117, position = 30
bfs_local = 202.519287, position = 31
bfs_local = 200.540237, position = 72
bfs_local = 196.008636, position = 73
bfs_local = 189.615891, position = 74
bfs_local = 181.814896, position = 75
bfs_local = 173.498856, position = 76
bfs_local = 167.110184, position = 77
bfs_local = 163.472778, position = 78
bfs_local = 162.806366, position = 79
bfs_local = 161.465103, position = 80
bfs_local = 158.023819, position = 81
bfs_local = 154.501556, position = 82
bfs_local = 149.160400, position = 83
bfs_local = 141.522949, position = 84
bfs_local = 138.315399, position = 85
bfs_local = 137.348206, position = 86
bfs_local = 132.668030, position = 123
bfs_local = 118.871078, position = 124
bfs_local = 106.985107, position = 125
bfs_local = 94.707901, position = 126
bfs_local = 86.262062, position = 127
bfs_local = 80.716850, position = 128
bfs_local = 76.177666, position = 129
bfs_local = 73.051987, position = 130
bfs_local = 71.865570, position = 131
bfs_local = 71.614456, position = 132
bfs_local = 69.846680, position = 133
bfs_local = 63.599731, position = 134
bfs_local = 58.314983, position = 135
bfs_local = 56.493622, position = 136
bfs_local = 56.004276, position = 138
bfs_local = 54.256336, position = 262
bfs_local = 51.119877, position = 263
bfs_local = 46.568928, position = 264
bfs_local = 42.756687, position = 265
bfs_local = 41.278046, position = 266
bfs_local = 38.494972, position = 267
bfs_local = 38.044487, position = 655
bfs_local = 37.672665, position = 1805
bfs_local = 33.796532, position = 1806
bfs_local = 31.685883, position = 1807
bfs_local = 30.068125, position = 1808
bfs_local = 29.133123, position = 2373
bfs_local = 27.872950, position = 2374
bfs_local = 25.931755, position = 2375
bfs_local = 23.420702, position = 25605
bfs_local = 23.130287, position = 38645
bfs_local = 19.407377, position = 48114
bfs_local = 18.483559, position = 48115
bfs_local = 17.459635, position = 48116
bfs_local = 16.934858, position = 48117
bfs_local = 16.353062, position = 223370
bsf = 4.04389
position = 223370
DTW Calculation = 0.48%
Count of DTW = 4845
Time for calculation lower bounding = 54.68%
Time for initializing arrays with start and end segment values = 0.000%
Time for finding the best subsequence = 45.06%
Runtime of reading 4.55
Total Runtime of finding the best subsequence 57.13


count of threads = 10

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bfs_local = 289.128693, position = 399952
bfs_local = 171.533737, position = 899892
bfs_local = 151.363342, position = 299964
bfs_local = 113.971619, position = 199976
bfs_local = 112.258888, position = 699921
bfs_local = 111.261742, position = 799910
bfs_local = 108.489250, position = 499947
bfs_local = 103.638023, position = 799911
bfs_local = 93.406296, position = 499948
bfs_local = 84.736084, position = 499949
bfs_local = 81.348854, position = 499950
bfs_local = 79.191963, position = 499951
bfs_local = 76.958809, position = 499953
bfs_local = 72.915298, position = 799917
bfs_local = 65.492813, position = 799919
bfs_local = 62.470425, position = 499956
bfs_local = 54.297779, position = 499958
bfs_local = 51.066620, position = 499959
bfs_local = 45.972466, position = 499967
bfs_local = 33.979160, position = 499971
bfs_local = 33.048027, position = 499972
bfs_local = 32.860783, position = 300126
bfs_local = 31.671900, position = 300125
bfs_local = 30.698458, position = 500111
bfs_local = 29.949909, position = 600410
bfs_local = 26.974546, position = 600411
bfs_local = 22.740864, position = 901009
bfs_local = 20.246920, position = 101268
bfs_local = 18.198055, position = 101270
bfs_local = 17.479368, position = 607764
bfs_local = 16.353062, position = 223370
bsf = 4.04389
position = 223370
DTW Calculation = 0.45%
Count of DTW = 4483
Time for calculation lower bounding = 89.97%
Time for initializing arrays with start and end segment values = 0.000%
Time for finding the best subsequence = 9.74%
Runtime of reading 4.46
Total Runtime of finding the best subsequence 3.49


count of threads = 30

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bfs_local = 250.643845, position = 133320
bfs_local = 204.575226, position = 333300
bfs_local = 202.159393, position = 533280
bfs_local = 162.000809, position = 733260
bfs_local = 140.712997, position = 99990
bfs_local = 65.584999, position = 799920
bfs_local = 50.329647, position = 33330
bfs_local = 48.832935, position = 33331
bfs_local = 45.873783, position = 33334
bfs_local = 34.049992, position = 33337
bfs_local = 31.377510, position = 33338
bfs_local = 28.665384, position = 33339
bfs_local = 25.671486, position = 33341
bfs_local = 25.567745, position = 33342
bfs_local = 24.583557, position = 33340
bfs_local = 24.107256, position = 133752
bfs_local = 23.764566, position = 901008
bfs_local = 22.740864, position = 901009
bfs_local = 20.246920, position = 101268
bfs_local = 18.198055, position = 101270
bfs_local = 17.479368, position = 607764
bfs_local = 16.934858, position = 48117
bfs_local = 16.353062, position = 223370
bsf = 4.04389
position = 223370
DTW Calculation = 0.51%
Count of DTW = 5140
Time for calculation lower bounding = 86.29%
Time for initializing arrays with start and end segment values = 0.000%
Time for finding the best subsequence = 12.90%
Runtime of reading 4.47
Total Runtime of finding the best subsequence 1.24


count of threads = 60

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bfs_local = 141.900406, position = 199980
bfs_local = 140.712997, position = 99990
bfs_local = 113.369179, position = 683265
bfs_local = 90.220306, position = 949905
bfs_local = 79.859474, position = 783255
bfs_local = 50.329647, position = 33330
bfs_local = 48.832935, position = 33331
bfs_local = 43.030128, position = 33335
bfs_local = 34.049992, position = 33337
bfs_local = 31.377510, position = 33338
bfs_local = 28.665384, position = 33339
bfs_local = 24.583557, position = 33340
bfs_local = 24.116644, position = 133751
bfs_local = 24.107256, position = 133752
bfs_local = 23.764566, position = 901008
bfs_local = 23.553417, position = 850999
bfs_local = 22.740864, position = 901009
bfs_local = 21.285566, position = 101269
bfs_local = 20.246920, position = 101268
bfs_local = 18.198055, position = 101270
bfs_local = 17.483942, position = 223369
bfs_local = 16.353062, position = 223370
bsf = 4.04389
position = 223370
DTW Calculation = 0.54%
Count of DTW = 5435
Time for calculation lower bounding = 76.32%
Time for initializing arrays with start and end segment values = 0.000%
Time for finding the best subsequence = 23.68%
Runtime of reading 4.57
Total Runtime of finding the best subsequence 0.76


count of threads = 120

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bfs_local = 177.204559, position = 508313
bfs_local = 64.157387, position = 516646
bfs_local = 52.756535, position = 33332
bfs_local = 39.546230, position = 208325
bfs_local = 26.300177, position = 33343
bfs_local = 24.583557, position = 33340
bfs_local = 23.352354, position = 158414
bfs_local = 22.178610, position = 158413
bfs_local = 21.801853, position = 58440
bfs_local = 20.246920, position = 101268
bfs_local = 18.892626, position = 101271
bfs_local = 18.198055, position = 101270
bfs_local = 17.459635, position = 48116
bfs_local = 16.955357, position = 48119
bfs_local = 16.934858, position = 48117
bfs_local = 16.353062, position = 223370
bsf = 4.04389
position = 223370
DTW Calculation = 0.64%
Count of DTW = 6365
Time for calculation lower bounding = 61.11%
Time for initializing arrays with start and end segment values = 0.000%
Time for finding the best subsequence = 38.89%
Runtime of reading 4.46
Total Runtime of finding the best subsequence 0.72


count of threads = 180

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bfs_local = 177.616486, position = 327745
bfs_local = 146.027786, position = 699930
bfs_local = 68.890007, position = 666600
bfs_local = 68.161568, position = 672155
bfs_local = 61.626919, position = 322190
bfs_local = 50.329647, position = 33330
bfs_local = 48.832935, position = 33331
bfs_local = 31.377510, position = 33338
bfs_local = 28.665384, position = 33339
bfs_local = 25.671486, position = 33341
bfs_local = 24.583557, position = 33340
bfs_local = 23.627203, position = 605584
bfs_local = 23.301130, position = 239072
bfs_local = 20.759872, position = 872578
bfs_local = 17.483942, position = 223369
bfs_local = 16.353062, position = 223370
bsf = 4.04389
position = 223370
DTW Calculation = 0.58%
Count of DTW = 5812
Time for calculation lower bounding = 57.32%
Time for initializing arrays with start and end segment values = 0.000%
Time for finding the best subsequence = 42.68%
Runtime of reading 4.46
Total Runtime of finding the best subsequence 0.82


count of threads = 240

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bfs_local = 197.923294, position = 508374
bfs_local = 135.211685, position = 254187
bfs_local = 80.983765, position = 250020
bfs_local = 43.474602, position = 279189
bfs_local = 40.641998, position = 33336
bfs_local = 33.597698, position = 308358
bfs_local = 32.391575, position = 191682
bfs_local = 26.300177, position = 33343
bfs_local = 25.567745, position = 33342
bfs_local = 24.583557, position = 33340
bfs_local = 22.178610, position = 158413
bfs_local = 21.801853, position = 58440
bfs_local = 19.550343, position = 430266
bfs_local = 18.557951, position = 122078
bfs_local = 18.504238, position = 122080
bfs_local = 18.198055, position = 101270
bfs_local = 16.955357, position = 48119
bfs_local = 16.934858, position = 48117
bfs_local = 16.353062, position = 223370
bsf = 4.04389
position = 223370
DTW Calculation = 0.68%
Count of DTW = 6835
Time for calculation lower bounding = 55.68%
Time for initializing arrays with start and end segment values = 0.000%
Time for finding the best subsequence = 43.18%
Runtime of reading 4.54
Total Runtime of finding the best subsequence 0.88


