number of a  series of experiments = 0
count of threads = 1

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bfs_local = 331.023224, position = 8
bfs_local = 318.669739, position = 9
bfs_local = 296.215698, position = 10
bfs_local = 287.683777, position = 11
bfs_local = 280.313782, position = 12
bfs_local = 273.292236, position = 13
bfs_local = 266.512299, position = 14
bfs_local = 263.441864, position = 15
bfs_local = 260.405457, position = 16
bfs_local = 260.237671, position = 17
bfs_local = 257.753296, position = 20
bfs_local = 251.134384, position = 21
bfs_local = 242.582504, position = 22
bfs_local = 233.340286, position = 23
bfs_local = 225.882736, position = 24
bfs_local = 218.148438, position = 25
bfs_local = 213.393097, position = 26
bfs_local = 212.282486, position = 27
bfs_local = 210.792236, position = 28
bfs_local = 207.359818, position = 29
bfs_local = 204.954117, position = 30
bfs_local = 202.519287, position = 31
bfs_local = 200.540237, position = 72
bfs_local = 196.008636, position = 73
bfs_local = 189.615891, position = 74
bfs_local = 181.814896, position = 75
bfs_local = 173.498856, position = 76
bfs_local = 167.110184, position = 77
bfs_local = 163.472778, position = 78
bfs_local = 162.806366, position = 79
bfs_local = 161.465103, position = 80
bfs_local = 158.023819, position = 81
bfs_local = 154.501556, position = 82
bfs_local = 149.160400, position = 83
bfs_local = 141.522949, position = 84
bfs_local = 138.315399, position = 85
bfs_local = 137.348206, position = 86
bfs_local = 132.668030, position = 123
bfs_local = 118.871078, position = 124
bfs_local = 106.985107, position = 125
bfs_local = 94.707901, position = 126
bfs_local = 86.262062, position = 127
bfs_local = 80.716850, position = 128
bfs_local = 76.177666, position = 129
bfs_local = 73.051987, position = 130
bfs_local = 71.865570, position = 131
bfs_local = 71.614456, position = 132
bfs_local = 69.846680, position = 133
bfs_local = 63.599731, position = 134
bfs_local = 58.314983, position = 135
bfs_local = 56.493622, position = 136
bfs_local = 56.004276, position = 138
bfs_local = 54.256336, position = 262
bfs_local = 51.119877, position = 263
bfs_local = 46.568928, position = 264
bfs_local = 42.756687, position = 265
bfs_local = 41.278046, position = 266
bfs_local = 38.494972, position = 267
bfs_local = 38.044487, position = 655
bfs_local = 37.672665, position = 1805
bfs_local = 33.796532, position = 1806
bfs_local = 31.685883, position = 1807
bfs_local = 30.068125, position = 1808
bfs_local = 29.133123, position = 2373
bfs_local = 27.872950, position = 2374
bfs_local = 25.931755, position = 2375
bfs_local = 23.420702, position = 25605
bfs_local = 23.130287, position = 38645
bfs_local = 19.407377, position = 48114
bfs_local = 18.483559, position = 48115
bfs_local = 17.459635, position = 48116
bfs_local = 16.934858, position = 48117
bfs_local = 16.353062, position = 223370
bsf = 4.04389
position = 223370
DTW Calculation = 0.48%
Count of DTW = 4849
Time for calculation lower bounding = 91.50%
Time for initializing arrays with start and end segment values = 0.000%
Time for finding the best subsequence = 8.06%
Runtime of reading 4.65
Total Runtime of finding the best subsequence 34.38


count of threads = 10

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bfs_local = 331.023224, position = 8
bfs_local = 318.669739, position = 9
bfs_local = 156.528534, position = 99988
bfs_local = 141.242722, position = 99992
bfs_local = 139.794113, position = 99991
bfs_local = 138.213898, position = 99993
bfs_local = 136.507858, position = 99994
bfs_local = 113.971619, position = 199976
bfs_local = 93.406296, position = 499948
bfs_local = 84.736084, position = 499949
bfs_local = 81.348854, position = 499950
bfs_local = 63.676682, position = 299988
bfs_local = 45.972466, position = 499967
bfs_local = 41.421024, position = 499968
bfs_local = 37.199856, position = 499974
bfs_local = 33.979160, position = 499971
bfs_local = 33.048027, position = 499972
bfs_local = 32.382904, position = 500110
bfs_local = 30.698458, position = 500111
bfs_local = 29.949909, position = 600410
bfs_local = 26.974546, position = 600411
bfs_local = 18.198055, position = 101270
bfs_local = 17.479368, position = 607764
bfs_local = 16.353062, position = 223370
bsf = 4.04389
position = 223370
DTW Calculation = 0.46%
Count of DTW = 4635
Time for calculation lower bounding = 96.57%
Time for initializing arrays with start and end segment values = 0.001%
Time for finding the best subsequence = 2.94%
Runtime of reading 4.50
Total Runtime of finding the best subsequence 3.27


count of threads = 30

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bfs_local = 204.575226, position = 333300
bfs_local = 68.801109, position = 666602
bfs_local = 60.532696, position = 799922
bfs_local = 58.160984, position = 666609
bfs_local = 50.329647, position = 33330
bfs_local = 48.832935, position = 33331
bfs_local = 40.641998, position = 33336
bfs_local = 34.049992, position = 33337
bfs_local = 31.377510, position = 33338
bfs_local = 28.665384, position = 33339
bfs_local = 24.583557, position = 33340
bfs_local = 24.116644, position = 133751
bfs_local = 24.107256, position = 133752
bfs_local = 22.968922, position = 234711
bfs_local = 18.892626, position = 101271
bfs_local = 18.198055, position = 101270
bfs_local = 17.479368, position = 607764
bfs_local = 17.459635, position = 48116
bfs_local = 16.934858, position = 48117
bfs_local = 16.353062, position = 223370
bsf = 4.04389
position = 223370
DTW Calculation = 0.57%
Count of DTW = 5655
Time for calculation lower bounding = 89.99%
Time for initializing arrays with start and end segment values = 0.004%
Time for finding the best subsequence = 9.53%
Runtime of reading 4.50
Total Runtime of finding the best subsequence 1.20


count of threads = 60

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bfs_local = 68.890007, position = 666600
bfs_local = 65.584999, position = 799920
bfs_local = 57.711437, position = 283305
bfs_local = 54.297779, position = 499958
bfs_local = 31.377510, position = 33338
bfs_local = 28.665384, position = 33339
bfs_local = 24.583557, position = 33340
bfs_local = 24.116644, position = 133751
bfs_local = 24.107256, position = 133752
bfs_local = 23.553417, position = 850999
bfs_local = 22.740864, position = 901009
bfs_local = 20.246920, position = 101268
bfs_local = 18.198055, position = 101270
bfs_local = 17.483942, position = 223369
bfs_local = 16.353062, position = 223370
bsf = 4.04389
position = 223370
DTW Calculation = 0.65%
Count of DTW = 6462
Time for calculation lower bounding = 77.55%
Time for initializing arrays with start and end segment values = 0.007%
Time for finding the best subsequence = 22.02%
Runtime of reading 4.61
Total Runtime of finding the best subsequence 0.75


count of threads = 120

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bfs_local = 187.884247, position = 83335
bfs_local = 182.960419, position = 108334
bfs_local = 88.443779, position = 424988
bfs_local = 68.698441, position = 191662
bfs_local = 39.675560, position = 208328
bfs_local = 24.583557, position = 33340
bfs_local = 22.512331, position = 58441
bfs_local = 22.178610, position = 158413
bfs_local = 21.801853, position = 58440
bfs_local = 21.285566, position = 101269
bfs_local = 18.198055, position = 101270
bfs_local = 16.955357, position = 48119
bfs_local = 16.353062, position = 223370
bsf = 4.04389
position = 223370
DTW Calculation = 0.79%
Count of DTW = 7947
Time for calculation lower bounding = 67.36%
Time for initializing arrays with start and end segment values = 0.009%
Time for finding the best subsequence = 32.27%
Runtime of reading 4.57
Total Runtime of finding the best subsequence 0.65


count of threads = 180

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bfs_local = 219.262924, position = 833251
bfs_local = 173.487213, position = 838806
bfs_local = 58.936226, position = 283308
bfs_local = 54.435711, position = 872142
bfs_local = 51.066620, position = 499959
bfs_local = 28.665384, position = 33339
bfs_local = 24.583557, position = 33340
bfs_local = 24.116644, position = 133751
bfs_local = 23.301130, position = 239072
bfs_local = 19.730610, position = 223373
bfs_local = 16.353062, position = 223370
bsf = 4.04389
position = 223370
DTW Calculation = 0.85%
Count of DTW = 8504
Time for calculation lower bounding = 59.63%
Time for initializing arrays with start and end segment values = 0.008%
Time for finding the best subsequence = 40.12%
Runtime of reading 4.51
Total Runtime of finding the best subsequence 0.82


count of threads = 240

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bfs_local = 151.179443, position = 375034
bfs_local = 58.740509, position = 379201
bfs_local = 32.956680, position = 387535
bfs_local = 32.884449, position = 387536
bfs_local = 32.344376, position = 387533
bfs_local = 31.060169, position = 33344
bfs_local = 30.794626, position = 387534
bfs_local = 25.671486, position = 33341
bfs_local = 24.583557, position = 33340
bfs_local = 24.300802, position = 25606
bfs_local = 23.352354, position = 158414
bfs_local = 22.178610, position = 158413
bfs_local = 21.801853, position = 58440
bfs_local = 18.892626, position = 101271
bfs_local = 18.404104, position = 122079
bfs_local = 18.198055, position = 101270
bfs_local = 17.811596, position = 223371
bfs_local = 16.955357, position = 48119
bfs_local = 16.934858, position = 48117
bfs_local = 16.353062, position = 223370
bsf = 4.04389
position = 223370
DTW Calculation = 1.05%
Count of DTW = 10452
Time for calculation lower bounding = 61.16%
Time for initializing arrays with start and end segment values = 0.007%
Time for finding the best subsequence = 38.53%
Runtime of reading 4.50
Total Runtime of finding the best subsequence 0.83


number of a  series of experiments = 1
count of threads = 1

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bfs_local = 331.023224, position = 8
bfs_local = 318.669739, position = 9
bfs_local = 296.215698, position = 10
bfs_local = 287.683777, position = 11
bfs_local = 280.313782, position = 12
bfs_local = 273.292236, position = 13
bfs_local = 266.512299, position = 14
bfs_local = 263.441864, position = 15
bfs_local = 260.405457, position = 16
bfs_local = 260.237671, position = 17
bfs_local = 257.753296, position = 20
bfs_local = 251.134384, position = 21
bfs_local = 242.582504, position = 22
bfs_local = 233.340286, position = 23
bfs_local = 225.882736, position = 24
bfs_local = 218.148438, position = 25
bfs_local = 213.393097, position = 26
bfs_local = 212.282486, position = 27
bfs_local = 210.792236, position = 28
bfs_local = 207.359818, position = 29
bfs_local = 204.954117, position = 30
bfs_local = 202.519287, position = 31
bfs_local = 200.540237, position = 72
bfs_local = 196.008636, position = 73
bfs_local = 189.615891, position = 74
bfs_local = 181.814896, position = 75
bfs_local = 173.498856, position = 76
bfs_local = 167.110184, position = 77
bfs_local = 163.472778, position = 78
bfs_local = 162.806366, position = 79
bfs_local = 161.465103, position = 80
bfs_local = 158.023819, position = 81
bfs_local = 154.501556, position = 82
bfs_local = 149.160400, position = 83
bfs_local = 141.522949, position = 84
bfs_local = 138.315399, position = 85
bfs_local = 137.348206, position = 86
bfs_local = 132.668030, position = 123
bfs_local = 118.871078, position = 124
bfs_local = 106.985107, position = 125
bfs_local = 94.707901, position = 126
bfs_local = 86.262062, position = 127
bfs_local = 80.716850, position = 128
bfs_local = 76.177666, position = 129
bfs_local = 73.051987, position = 130
bfs_local = 71.865570, position = 131
bfs_local = 71.614456, position = 132
bfs_local = 69.846680, position = 133
bfs_local = 63.599731, position = 134
bfs_local = 58.314983, position = 135
bfs_local = 56.493622, position = 136
bfs_local = 56.004276, position = 138
bfs_local = 54.256336, position = 262
bfs_local = 51.119877, position = 263
bfs_local = 46.568928, position = 264
bfs_local = 42.756687, position = 265
bfs_local = 41.278046, position = 266
bfs_local = 38.494972, position = 267
bfs_local = 38.044487, position = 655
bfs_local = 37.672665, position = 1805
bfs_local = 33.796532, position = 1806
bfs_local = 31.685883, position = 1807
bfs_local = 30.068125, position = 1808
bfs_local = 29.133123, position = 2373
bfs_local = 27.872950, position = 2374
bfs_local = 25.931755, position = 2375
bfs_local = 23.420702, position = 25605
bfs_local = 23.130287, position = 38645
bfs_local = 19.407377, position = 48114
bfs_local = 18.483559, position = 48115
bfs_local = 17.459635, position = 48116
bfs_local = 16.934858, position = 48117
bfs_local = 16.353062, position = 223370
bsf = 4.04389
position = 223370
DTW Calculation = 0.48%
Count of DTW = 4849
Time for calculation lower bounding = 91.48%
Time for initializing arrays with start and end segment values = 0.000%
Time for finding the best subsequence = 8.07%
Runtime of reading 4.61
Total Runtime of finding the best subsequence 34.39


count of threads = 10

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bfs_local = 331.023224, position = 8
bfs_local = 318.669739, position = 9
bfs_local = 156.528534, position = 99988
bfs_local = 141.242722, position = 99992
bfs_local = 140.712997, position = 99990
bfs_local = 139.794113, position = 99991
bfs_local = 138.213898, position = 99993
bfs_local = 136.507858, position = 99994
bfs_local = 113.971619, position = 199976
bfs_local = 84.736084, position = 499949
bfs_local = 81.348854, position = 499950
bfs_local = 63.676682, position = 299988
bfs_local = 53.758106, position = 499977
bfs_local = 45.972466, position = 499967
bfs_local = 37.199856, position = 499974
bfs_local = 33.979160, position = 499971
bfs_local = 33.048027, position = 499972
bfs_local = 32.382904, position = 500110
bfs_local = 30.698458, position = 500111
bfs_local = 29.949909, position = 600410
bfs_local = 26.974546, position = 600411
bfs_local = 18.198055, position = 101270
bfs_local = 17.479368, position = 607764
bfs_local = 16.353062, position = 223370
bsf = 4.04389
position = 223370
DTW Calculation = 0.46%
Count of DTW = 4635
Time for calculation lower bounding = 96.60%
Time for initializing arrays with start and end segment values = 0.001%
Time for finding the best subsequence = 2.91%
Runtime of reading 4.50
Total Runtime of finding the best subsequence 3.27


count of threads = 30

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bfs_local = 239.043076, position = 333305
bfs_local = 204.575226, position = 333300
bfs_local = 68.801109, position = 666602
bfs_local = 66.214340, position = 666601
bfs_local = 60.532696, position = 799922
bfs_local = 58.160984, position = 666609
bfs_local = 50.329647, position = 33330
bfs_local = 45.873783, position = 33334
bfs_local = 43.030128, position = 33335
bfs_local = 40.641998, position = 33336
bfs_local = 34.049992, position = 33337
bfs_local = 31.377510, position = 33338
bfs_local = 28.665384, position = 33339
bfs_local = 24.583557, position = 33340
bfs_local = 24.116644, position = 133751
bfs_local = 24.107256, position = 133752
bfs_local = 22.968922, position = 234711
bfs_local = 18.892626, position = 101271
bfs_local = 18.198055, position = 101270
bfs_local = 17.479368, position = 607764
bfs_local = 17.459635, position = 48116
bfs_local = 16.934858, position = 48117
bfs_local = 16.353062, position = 223370
bsf = 4.04389
position = 223370
DTW Calculation = 0.57%
Count of DTW = 5655
Time for calculation lower bounding = 89.85%
Time for initializing arrays with start and end segment values = 0.004%
Time for finding the best subsequence = 9.67%
Runtime of reading 4.50
Total Runtime of finding the best subsequence 1.20


count of threads = 60

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bfs_local = 331.023224, position = 8
bfs_local = 180.539993, position = 16673
bfs_local = 68.890007, position = 666600
bfs_local = 67.872482, position = 499955
bfs_local = 31.377510, position = 33338
bfs_local = 28.665384, position = 33339
bfs_local = 24.583557, position = 33340
bfs_local = 24.116644, position = 133751
bfs_local = 24.107256, position = 133752
bfs_local = 23.553417, position = 850999
bfs_local = 22.740864, position = 901009
bfs_local = 20.246920, position = 101268
bfs_local = 18.198055, position = 101270
bfs_local = 17.483942, position = 223369
bfs_local = 16.353062, position = 223370
bsf = 4.04389
position = 223370
DTW Calculation = 0.65%
Count of DTW = 6462
Time for calculation lower bounding = 77.64%
Time for initializing arrays with start and end segment values = 0.008%
Time for finding the best subsequence = 21.90%
Runtime of reading 4.50
Total Runtime of finding the best subsequence 0.75


count of threads = 120

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bfs_local = 187.884247, position = 83335
bfs_local = 113.547783, position = 924967
bfs_local = 41.512924, position = 358324
bfs_local = 41.375717, position = 208330
bfs_local = 36.222309, position = 358325
bfs_local = 24.583557, position = 33340
bfs_local = 22.512331, position = 58441
bfs_local = 22.178610, position = 158413
bfs_local = 21.801853, position = 58440
bfs_local = 21.285566, position = 101269
bfs_local = 18.198055, position = 101270
bfs_local = 16.955357, position = 48119
bfs_local = 16.353062, position = 223370
bsf = 4.04389
position = 223370
DTW Calculation = 0.79%
Count of DTW = 7947
Time for calculation lower bounding = 66.62%
Time for initializing arrays with start and end segment values = 0.009%
Time for finding the best subsequence = 33.02%
Runtime of reading 4.50
Total Runtime of finding the best subsequence 0.65


count of threads = 180

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bfs_local = 199.604172, position = 277753
bfs_local = 58.936226, position = 283308
bfs_local = 54.826591, position = 283309
bfs_local = 54.297779, position = 499958
bfs_local = 51.066620, position = 499959
bfs_local = 28.665384, position = 33339
bfs_local = 25.574455, position = 922436
bfs_local = 25.312016, position = 239071
bfs_local = 24.583557, position = 33340
bfs_local = 24.116644, position = 133751
bfs_local = 23.627203, position = 605584
bfs_local = 23.301130, position = 239072
bfs_local = 19.730610, position = 223373
bfs_local = 16.353062, position = 223370
bsf = 4.04389
position = 223370
DTW Calculation = 0.85%
Count of DTW = 8504
Time for calculation lower bounding = 57.97%
Time for initializing arrays with start and end segment values = 0.008%
Time for finding the best subsequence = 41.73%
Runtime of reading 4.51
Total Runtime of finding the best subsequence 0.81


count of threads = 240

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bfs_local = 151.002090, position = 375032
bfs_local = 69.636475, position = 379199
bfs_local = 32.344376, position = 387533
bfs_local = 30.794626, position = 387534
bfs_local = 25.671486, position = 33341
bfs_local = 24.583557, position = 33340
bfs_local = 24.300802, position = 25606
bfs_local = 23.352354, position = 158414
bfs_local = 22.178610, position = 158413
bfs_local = 21.801853, position = 58440
bfs_local = 20.246920, position = 101268
bfs_local = 18.892626, position = 101271
bfs_local = 18.404104, position = 122079
bfs_local = 18.198055, position = 101270
bfs_local = 17.811596, position = 223371
bfs_local = 16.955357, position = 48119
bfs_local = 16.934858, position = 48117
bfs_local = 16.353062, position = 223370
bsf = 4.04389
position = 223370
DTW Calculation = 1.05%
Count of DTW = 10452
Time for calculation lower bounding = 59.16%
Time for initializing arrays with start and end segment values = 0.007%
Time for finding the best subsequence = 40.53%
Runtime of reading 4.57
Total Runtime of finding the best subsequence 0.86


number of a  series of experiments = 2
count of threads = 1

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bfs_local = 331.023224, position = 8
bfs_local = 318.669739, position = 9
bfs_local = 296.215698, position = 10
bfs_local = 287.683777, position = 11
bfs_local = 280.313782, position = 12
bfs_local = 273.292236, position = 13
bfs_local = 266.512299, position = 14
bfs_local = 263.441864, position = 15
bfs_local = 260.405457, position = 16
bfs_local = 260.237671, position = 17
bfs_local = 257.753296, position = 20
bfs_local = 251.134384, position = 21
bfs_local = 242.582504, position = 22
bfs_local = 233.340286, position = 23
bfs_local = 225.882736, position = 24
bfs_local = 218.148438, position = 25
bfs_local = 213.393097, position = 26
bfs_local = 212.282486, position = 27
bfs_local = 210.792236, position = 28
bfs_local = 207.359818, position = 29
bfs_local = 204.954117, position = 30
bfs_local = 202.519287, position = 31
bfs_local = 200.540237, position = 72
bfs_local = 196.008636, position = 73
bfs_local = 189.615891, position = 74
bfs_local = 181.814896, position = 75
bfs_local = 173.498856, position = 76
bfs_local = 167.110184, position = 77
bfs_local = 163.472778, position = 78
bfs_local = 162.806366, position = 79
bfs_local = 161.465103, position = 80
bfs_local = 158.023819, position = 81
bfs_local = 154.501556, position = 82
bfs_local = 149.160400, position = 83
bfs_local = 141.522949, position = 84
bfs_local = 138.315399, position = 85
bfs_local = 137.348206, position = 86
bfs_local = 132.668030, position = 123
bfs_local = 118.871078, position = 124
bfs_local = 106.985107, position = 125
bfs_local = 94.707901, position = 126
bfs_local = 86.262062, position = 127
bfs_local = 80.716850, position = 128
bfs_local = 76.177666, position = 129
bfs_local = 73.051987, position = 130
bfs_local = 71.865570, position = 131
bfs_local = 71.614456, position = 132
bfs_local = 69.846680, position = 133
bfs_local = 63.599731, position = 134
bfs_local = 58.314983, position = 135
bfs_local = 56.493622, position = 136
bfs_local = 56.004276, position = 138
bfs_local = 54.256336, position = 262
bfs_local = 51.119877, position = 263
bfs_local = 46.568928, position = 264
bfs_local = 42.756687, position = 265
bfs_local = 41.278046, position = 266
bfs_local = 38.494972, position = 267
bfs_local = 38.044487, position = 655
bfs_local = 37.672665, position = 1805
bfs_local = 33.796532, position = 1806
bfs_local = 31.685883, position = 1807
bfs_local = 30.068125, position = 1808
bfs_local = 29.133123, position = 2373
bfs_local = 27.872950, position = 2374
bfs_local = 25.931755, position = 2375
bfs_local = 23.420702, position = 25605
bfs_local = 23.130287, position = 38645
bfs_local = 19.407377, position = 48114
bfs_local = 18.483559, position = 48115
bfs_local = 17.459635, position = 48116
bfs_local = 16.934858, position = 48117
bfs_local = 16.353062, position = 223370
bsf = 4.04389
position = 223370
DTW Calculation = 0.48%
Count of DTW = 4849
Time for calculation lower bounding = 91.48%
Time for initializing arrays with start and end segment values = 0.000%
Time for finding the best subsequence = 8.07%
Runtime of reading 4.50
Total Runtime of finding the best subsequence 34.36


count of threads = 10

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bfs_local = 331.023224, position = 8
bfs_local = 318.669739, position = 9
bfs_local = 156.528534, position = 99988
bfs_local = 141.242722, position = 99992
bfs_local = 140.712997, position = 99990
bfs_local = 139.794113, position = 99991
bfs_local = 136.507858, position = 99994
bfs_local = 113.971619, position = 199976
bfs_local = 93.406296, position = 499948
bfs_local = 84.736084, position = 499949
bfs_local = 81.348854, position = 499950
bfs_local = 59.962414, position = 499957
bfs_local = 45.972466, position = 499967
bfs_local = 41.421024, position = 499968
bfs_local = 37.199856, position = 499974
bfs_local = 33.979160, position = 499971
bfs_local = 33.048027, position = 499972
bfs_local = 32.382904, position = 500110
bfs_local = 30.698458, position = 500111
bfs_local = 29.949909, position = 600410
bfs_local = 26.974546, position = 600411
bfs_local = 18.198055, position = 101270
bfs_local = 17.479368, position = 607764
bfs_local = 16.353062, position = 223370
bsf = 4.04389
position = 223370
DTW Calculation = 0.46%
Count of DTW = 4635
Time for calculation lower bounding = 96.24%
Time for initializing arrays with start and end segment values = 0.001%
Time for finding the best subsequence = 3.27%
Runtime of reading 4.56
Total Runtime of finding the best subsequence 3.29


count of threads = 30

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bfs_local = 204.575226, position = 333300
bfs_local = 68.801109, position = 666602
bfs_local = 60.532696, position = 799922
bfs_local = 57.987724, position = 799923
bfs_local = 54.297779, position = 499958
bfs_local = 50.329647, position = 33330
bfs_local = 45.873783, position = 33334
bfs_local = 40.641998, position = 33336
bfs_local = 34.049992, position = 33337
bfs_local = 31.377510, position = 33338
bfs_local = 28.665384, position = 33339
bfs_local = 24.583557, position = 33340
bfs_local = 24.116644, position = 133751
bfs_local = 24.107256, position = 133752
bfs_local = 22.968922, position = 234711
bfs_local = 18.892626, position = 101271
bfs_local = 18.198055, position = 101270
bfs_local = 17.479368, position = 607764
bfs_local = 17.459635, position = 48116
bfs_local = 16.934858, position = 48117
bfs_local = 16.353062, position = 223370
bsf = 4.04389
position = 223370
DTW Calculation = 0.57%
Count of DTW = 5655
Time for calculation lower bounding = 89.87%
Time for initializing arrays with start and end segment values = 0.004%
Time for finding the best subsequence = 9.65%
Runtime of reading 4.51
Total Runtime of finding the best subsequence 1.21


count of threads = 60

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bfs_local = 81.348854, position = 499950
bfs_local = 67.872482, position = 499955
bfs_local = 28.665384, position = 33339
bfs_local = 24.583557, position = 33340
bfs_local = 24.116644, position = 133751
bfs_local = 24.107256, position = 133752
bfs_local = 23.553417, position = 850999
bfs_local = 22.740864, position = 901009
bfs_local = 20.246920, position = 101268
bfs_local = 18.198055, position = 101270
bfs_local = 17.483942, position = 223369
bfs_local = 16.353062, position = 223370
bsf = 4.04389
position = 223370
DTW Calculation = 0.65%
Count of DTW = 6462
Time for calculation lower bounding = 77.66%
Time for initializing arrays with start and end segment values = 0.009%
Time for finding the best subsequence = 21.89%
Runtime of reading 4.50
Total Runtime of finding the best subsequence 0.75


count of threads = 120

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bfs_local = 213.111694, position = 916634
bfs_local = 113.547783, position = 924967
bfs_local = 88.443779, position = 424988
bfs_local = 81.775826, position = 983298
bfs_local = 54.115250, position = 191664
bfs_local = 41.375717, position = 208330
bfs_local = 24.583557, position = 33340
bfs_local = 22.512331, position = 58441
bfs_local = 22.178610, position = 158413
bfs_local = 21.801853, position = 58440
bfs_local = 21.285566, position = 101269
bfs_local = 18.198055, position = 101270
bfs_local = 17.811596, position = 223371
bfs_local = 16.955357, position = 48119
bfs_local = 16.353062, position = 223370
bsf = 4.04389
position = 223370
DTW Calculation = 0.79%
Count of DTW = 7947
Time for calculation lower bounding = 66.41%
Time for initializing arrays with start and end segment values = 0.008%
Time for finding the best subsequence = 33.23%
Runtime of reading 4.51
Total Runtime of finding the best subsequence 0.67


count of threads = 180

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bfs_local = 222.811066, position = 333303
bfs_local = 179.980682, position = 344413
bfs_local = 149.889984, position = 722156
bfs_local = 88.535522, position = 888801
bfs_local = 63.124531, position = 666608
bfs_local = 55.806831, position = 283310
bfs_local = 54.297779, position = 499958
bfs_local = 28.665384, position = 33339
bfs_local = 25.312016, position = 239071
bfs_local = 24.583557, position = 33340
bfs_local = 24.116644, position = 133751
bfs_local = 23.301130, position = 239072
bfs_local = 19.730610, position = 223373
bfs_local = 16.353062, position = 223370
bsf = 4.04389
position = 223370
DTW Calculation = 0.85%
Count of DTW = 8504
Time for calculation lower bounding = 56.95%
Time for initializing arrays with start and end segment values = 0.008%
Time for finding the best subsequence = 42.77%
Runtime of reading 4.61
Total Runtime of finding the best subsequence 0.81


count of threads = 240

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bfs_local = 176.801102, position = 625057
bfs_local = 159.398224, position = 629224
bfs_local = 64.310623, position = 545884
bfs_local = 60.539986, position = 545879
bfs_local = 51.437786, position = 666728
bfs_local = 46.730915, position = 962585
bfs_local = 38.254013, position = 437541
bfs_local = 31.060169, position = 33344
bfs_local = 30.794626, position = 387534
bfs_local = 25.671486, position = 33341
bfs_local = 25.567745, position = 33342
bfs_local = 24.583557, position = 33340
bfs_local = 24.300802, position = 25606
bfs_local = 23.352354, position = 158414
bfs_local = 22.178610, position = 158413
bfs_local = 21.801853, position = 58440
bfs_local = 18.892626, position = 101271
bfs_local = 18.404104, position = 122079
bfs_local = 18.198055, position = 101270
bfs_local = 17.811596, position = 223371
bfs_local = 16.955357, position = 48119
bfs_local = 16.934858, position = 48117
bfs_local = 16.353062, position = 223370
bsf = 4.04389
position = 223370
DTW Calculation = 1.05%
Count of DTW = 10452
Time for calculation lower bounding = 61.73%
Time for initializing arrays with start and end segment values = 0.007%
Time for finding the best subsequence = 37.95%
Runtime of reading 4.56
Total Runtime of finding the best subsequence 0.84


