number of a  series of experiments = 0
count of threads = 1

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bfs_local = 205.160965, position = 68
bfs_local = 201.295975, position = 109
bfs_local = 198.454041, position = 110
bfs_local = 181.586685, position = 111
bfs_local = 177.698914, position = 112
bfs_local = 177.272308, position = 113
bfs_local = 173.585770, position = 114
bfs_local = 168.219498, position = 115
bfs_local = 166.177505, position = 116
bfs_local = 153.458847, position = 117
bfs_local = 137.000046, position = 118
bfs_local = 134.529373, position = 122
bfs_local = 128.011932, position = 123
bfs_local = 121.794411, position = 124
bfs_local = 115.587074, position = 125
bfs_local = 106.061142, position = 126
bfs_local = 90.850601, position = 127
bfs_local = 90.638985, position = 129
bfs_local = 88.713676, position = 130
bfs_local = 87.106239, position = 209
bfs_local = 86.763412, position = 210
bfs_local = 84.332840, position = 211
bfs_local = 82.125427, position = 212
bfs_local = 66.927124, position = 276
bfs_local = 57.716431, position = 277
bfs_local = 51.453167, position = 278
bfs_local = 39.160603, position = 279
bfs_local = 31.621851, position = 280
bfs_local = 28.650408, position = 281
bfs_local = 23.193083, position = 282
bfs_local = 18.942251, position = 283
bfs_local = 16.442295, position = 284
bfs_local = 14.960265, position = 285
bfs_local = 14.029982, position = 286
bfs_local = 13.455287, position = 12317
bfs_local = 13.343568, position = 12318
bfs_local = 11.354399, position = 25846
bfs_local = 11.098724, position = 43099
bfs_local = 10.341489, position = 131638
bfs_local = 10.241185, position = 131639
bfs_local = 10.189466, position = 148874
bfs_local = 9.236251, position = 148875
bfs_local = 8.066576, position = 148876
bsf = 2.84017
position = 148876
DTW Calculation = 3.22%
Count of DTW = 32226
Time for calculation lower bounding = 76.85%
Time for initializing arrays with start and end segment values = 0.000%
Time for finding the best subsequence = 22.87%
Runtime of reading 4.51
Total Runtime of finding the best subsequence 82.17


count of threads = 10

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bfs_local = 107.732956, position = 99988
bfs_local = 54.801617, position = 199984
bfs_local = 54.301651, position = 199985
bfs_local = 40.817909, position = 899898
bfs_local = 38.483379, position = 899897
bfs_local = 37.310665, position = 899896
bfs_local = 35.852566, position = 899895
bfs_local = 34.579025, position = 899893
bfs_local = 32.803574, position = 899892
bfs_local = 28.973774, position = 899976
bfs_local = 27.948656, position = 899977
bfs_local = 27.778095, position = 899974
bfs_local = 23.035563, position = 899978
bfs_local = 19.881905, position = 900148
bfs_local = 18.704517, position = 900152
bfs_local = 18.210606, position = 900153
bfs_local = 17.990850, position = 900150
bfs_local = 17.681797, position = 900151
bfs_local = 14.029982, position = 286
bfs_local = 13.420880, position = 800612
bfs_local = 13.254978, position = 800613
bfs_local = 12.667230, position = 301539
bfs_local = 12.474792, position = 307199
bfs_local = 11.560240, position = 307200
bfs_local = 11.354399, position = 25846
bfs_local = 10.341489, position = 131638
bfs_local = 10.241185, position = 131639
bfs_local = 10.189466, position = 148874
bfs_local = 9.236251, position = 148875
bfs_local = 8.066576, position = 148876
bsf = 2.84017
position = 148876
DTW Calculation = 4.10%
Count of DTW = 40960
Time for calculation lower bounding = 88.24%
Time for initializing arrays with start and end segment values = 0.001%
Time for finding the best subsequence = 11.42%
Runtime of reading 4.58
Total Runtime of finding the best subsequence 7.18


count of threads = 30

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bfs_local = 100.553925, position = 333300
bfs_local = 73.125473, position = 33330
bfs_local = 59.591976, position = 33331
bfs_local = 36.884579, position = 33334
bfs_local = 22.457497, position = 666602
bfs_local = 20.563293, position = 666601
bfs_local = 18.879551, position = 433439
bfs_local = 17.462156, position = 433442
bfs_local = 16.081520, position = 566830
bfs_local = 14.905022, position = 566832
bfs_local = 14.677940, position = 566831
bfs_local = 14.654401, position = 533606
bfs_local = 14.029982, position = 286
bfs_local = 13.782787, position = 800614
bfs_local = 13.420880, position = 800612
bfs_local = 13.254978, position = 800613
bfs_local = 13.009433, position = 301544
bfs_local = 12.667230, position = 301539
bfs_local = 12.644225, position = 168475
bfs_local = 11.789434, position = 968401
bfs_local = 11.688601, position = 769737
bfs_local = 11.384415, position = 769735
bfs_local = 10.791825, position = 769736
bfs_local = 10.542933, position = 340701
bfs_local = 9.789495, position = 874191
bfs_local = 9.721350, position = 874194
bfs_local = 9.236251, position = 148875
bfs_local = 8.066576, position = 148876
bsf = 2.84017
position = 148876
DTW Calculation = 3.83%
Count of DTW = 38253
Time for calculation lower bounding = 86.36%
Time for initializing arrays with start and end segment values = 0.003%
Time for finding the best subsequence = 13.28%
Runtime of reading 4.51
Total Runtime of finding the best subsequence 2.47


count of threads = 60

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bfs_local = 103.129005, position = 333301
bfs_local = 99.469933, position = 16673
bfs_local = 92.177040, position = 16674
bfs_local = 30.742846, position = 666604
bfs_local = 28.512045, position = 383296
bfs_local = 27.296200, position = 383295
bfs_local = 20.669662, position = 666600
bfs_local = 20.563293, position = 666601
bfs_local = 19.681322, position = 516643
bfs_local = 17.502886, position = 16725
bfs_local = 16.769062, position = 516641
bfs_local = 16.631578, position = 516741
bfs_local = 16.046032, position = 516749
bfs_local = 15.858474, position = 516751
bfs_local = 15.703591, position = 516750
bfs_local = 15.380412, position = 566833
bfs_local = 14.905022, position = 566832
bfs_local = 14.029982, position = 286
bfs_local = 11.817424, position = 850360
bfs_local = 11.789434, position = 968401
bfs_local = 11.688601, position = 769737
bfs_local = 11.384415, position = 769735
bfs_local = 10.791825, position = 769736
bfs_local = 10.093108, position = 286687
bfs_local = 9.721350, position = 874194
bfs_local = 9.402809, position = 295832
bfs_local = 8.066576, position = 148876
bsf = 2.84017
position = 148876
DTW Calculation = 4.53%
Count of DTW = 45287
Time for calculation lower bounding = 80.27%
Time for initializing arrays with start and end segment values = 0.004%
Time for finding the best subsequence = 19.36%
Runtime of reading 4.51
Total Runtime of finding the best subsequence 1.39


count of threads = 120

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bfs_local = 144.706924, position = 166660
bfs_local = 52.065403, position = 8338
bfs_local = 50.822723, position = 924967
bfs_local = 47.237518, position = 8340
bfs_local = 47.043407, position = 924968
bfs_local = 42.236362, position = 924970
bfs_local = 41.534702, position = 924972
bfs_local = 40.276382, position = 924971
bfs_local = 34.945061, position = 249997
bfs_local = 32.248035, position = 783308
bfs_local = 31.636507, position = 783309
bfs_local = 26.071812, position = 41670
bfs_local = 25.365734, position = 41669
bfs_local = 20.319420, position = 516646
bfs_local = 19.365086, position = 516647
bfs_local = 19.340477, position = 16728
bfs_local = 17.502886, position = 16725
bfs_local = 17.397680, position = 516747
bfs_local = 17.033276, position = 58451
bfs_local = 11.969646, position = 925121
bfs_local = 11.579897, position = 525209
bfs_local = 11.354399, position = 25846
bfs_local = 11.286866, position = 92623
bfs_local = 10.973717, position = 259693
bfs_local = 10.818749, position = 259691
bfs_local = 10.791825, position = 769736
bfs_local = 10.654775, position = 286686
bfs_local = 10.093108, position = 286687
bfs_local = 9.471311, position = 295833
bfs_local = 9.402809, position = 295832
bfs_local = 9.236251, position = 148875
bfs_local = 8.811771, position = 148877
bfs_local = 8.066576, position = 148876
bsf = 2.84017
position = 148876
DTW Calculation = 4.58%
Count of DTW = 45759
Time for calculation lower bounding = 74.56%
Time for initializing arrays with start and end segment values = 0.009%
Time for finding the best subsequence = 25.10%
Runtime of reading 4.66
Total Runtime of finding the best subsequence 1.06


count of threads = 180

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bfs_local = 110.359367, position = 333303
bfs_local = 68.675903, position = 838808
bfs_local = 59.903648, position = 838811
bfs_local = 57.180580, position = 838810
bfs_local = 53.205673, position = 11118
bfs_local = 28.651278, position = 855470
bfs_local = 22.457497, position = 666602
bfs_local = 20.669662, position = 666600
bfs_local = 15.522529, position = 811036
bfs_local = 15.037633, position = 227762
bfs_local = 11.724231, position = 811033
bfs_local = 11.569302, position = 756002
bfs_local = 11.234343, position = 756564
bfs_local = 10.232831, position = 756562
bfs_local = 10.108885, position = 295831
bfs_local = 9.471311, position = 295833
bfs_local = 9.402809, position = 295832
bfs_local = 8.066576, position = 148876
bsf = 2.84017
position = 148876
DTW Calculation = 4.21%
Count of DTW = 42076
Time for calculation lower bounding = 68.22%
Time for initializing arrays with start and end segment values = 0.008%
Time for finding the best subsequence = 31.52%
Runtime of reading 4.51
Total Runtime of finding the best subsequence 1.14


count of threads = 240

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bfs_local = 169.422058, position = 500047
bfs_local = 136.258835, position = 962581
bfs_local = 131.984802, position = 962577
bfs_local = 102.925423, position = 250026
bfs_local = 80.472176, position = 250028
bfs_local = 71.881439, position = 250029
bfs_local = 36.337475, position = 175014
bfs_local = 36.252007, position = 175022
bfs_local = 27.627979, position = 187515
bfs_local = 21.824396, position = 187517
bfs_local = 21.292439, position = 187519
bfs_local = 20.350189, position = 754234
bfs_local = 19.303181, position = 145850
bfs_local = 16.954260, position = 754236
bfs_local = 14.681325, position = 620964
bfs_local = 12.864885, position = 995961
bfs_local = 12.189212, position = 925123
bfs_local = 12.041904, position = 995960
bfs_local = 11.969646, position = 925121
bfs_local = 11.221785, position = 645991
bfs_local = 10.523934, position = 645992
bfs_local = 10.232831, position = 756562
bfs_local = 8.811771, position = 148877
bfs_local = 8.066576, position = 148876
bsf = 2.84017
position = 148876
DTW Calculation = 4.64%
Count of DTW = 46393
Time for calculation lower bounding = 67.66%
Time for initializing arrays with start and end segment values = 0.006%
Time for finding the best subsequence = 32.06%
Runtime of reading 4.51
Total Runtime of finding the best subsequence 1.14


number of a  series of experiments = 1
count of threads = 1

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bfs_local = 205.160965, position = 68
bfs_local = 201.295975, position = 109
bfs_local = 198.454041, position = 110
bfs_local = 181.586685, position = 111
bfs_local = 177.698914, position = 112
bfs_local = 177.272308, position = 113
bfs_local = 173.585770, position = 114
bfs_local = 168.219498, position = 115
bfs_local = 166.177505, position = 116
bfs_local = 153.458847, position = 117
bfs_local = 137.000046, position = 118
bfs_local = 134.529373, position = 122
bfs_local = 128.011932, position = 123
bfs_local = 121.794411, position = 124
bfs_local = 115.587074, position = 125
bfs_local = 106.061142, position = 126
bfs_local = 90.850601, position = 127
bfs_local = 90.638985, position = 129
bfs_local = 88.713676, position = 130
bfs_local = 87.106239, position = 209
bfs_local = 86.763412, position = 210
bfs_local = 84.332840, position = 211
bfs_local = 82.125427, position = 212
bfs_local = 66.927124, position = 276
bfs_local = 57.716431, position = 277
bfs_local = 51.453167, position = 278
bfs_local = 39.160603, position = 279
bfs_local = 31.621851, position = 280
bfs_local = 28.650408, position = 281
bfs_local = 23.193083, position = 282
bfs_local = 18.942251, position = 283
bfs_local = 16.442295, position = 284
bfs_local = 14.960265, position = 285
bfs_local = 14.029982, position = 286
bfs_local = 13.455287, position = 12317
bfs_local = 13.343568, position = 12318
bfs_local = 11.354399, position = 25846
bfs_local = 11.098724, position = 43099
bfs_local = 10.341489, position = 131638
bfs_local = 10.241185, position = 131639
bfs_local = 10.189466, position = 148874
bfs_local = 9.236251, position = 148875
bfs_local = 8.066576, position = 148876
bsf = 2.84017
position = 148876
DTW Calculation = 3.22%
Count of DTW = 32226
Time for calculation lower bounding = 76.82%
Time for initializing arrays with start and end segment values = 0.000%
Time for finding the best subsequence = 22.90%
Runtime of reading 4.52
Total Runtime of finding the best subsequence 82.19


count of threads = 10

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bfs_local = 107.732956, position = 99988
bfs_local = 54.801617, position = 199984
bfs_local = 54.301651, position = 199985
bfs_local = 40.817909, position = 899898
bfs_local = 38.483379, position = 899897
bfs_local = 37.310665, position = 899896
bfs_local = 35.852566, position = 899895
bfs_local = 35.009220, position = 899894
bfs_local = 34.579025, position = 899893
bfs_local = 32.803574, position = 899892
bfs_local = 28.973774, position = 899976
bfs_local = 27.948656, position = 899977
bfs_local = 27.778095, position = 899974
bfs_local = 23.035563, position = 899978
bfs_local = 19.881905, position = 900148
bfs_local = 18.704517, position = 900152
bfs_local = 18.210606, position = 900153
bfs_local = 17.990850, position = 900150
bfs_local = 17.681797, position = 900151
bfs_local = 14.029982, position = 286
bfs_local = 13.420880, position = 800612
bfs_local = 13.254978, position = 800613
bfs_local = 12.667230, position = 301539
bfs_local = 12.474792, position = 307199
bfs_local = 11.560240, position = 307200
bfs_local = 11.354399, position = 25846
bfs_local = 10.341489, position = 131638
bfs_local = 10.241185, position = 131639
bfs_local = 10.189466, position = 148874
bfs_local = 9.236251, position = 148875
bfs_local = 8.066576, position = 148876
bsf = 2.84017
position = 148876
DTW Calculation = 4.10%
Count of DTW = 40960
Time for calculation lower bounding = 88.21%
Time for initializing arrays with start and end segment values = 0.001%
Time for finding the best subsequence = 11.44%
Runtime of reading 4.57
Total Runtime of finding the best subsequence 7.18


count of threads = 30

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bfs_local = 103.129005, position = 333301
bfs_local = 22.457497, position = 666602
bfs_local = 20.669662, position = 666600
bfs_local = 20.563293, position = 666601
bfs_local = 18.879551, position = 433439
bfs_local = 17.462156, position = 433442
bfs_local = 16.081520, position = 566830
bfs_local = 14.905022, position = 566832
bfs_local = 14.677940, position = 566831
bfs_local = 14.654401, position = 533606
bfs_local = 14.029982, position = 286
bfs_local = 13.782787, position = 800614
bfs_local = 13.420880, position = 800612
bfs_local = 13.254978, position = 800613
bfs_local = 13.009433, position = 301544
bfs_local = 12.667230, position = 301539
bfs_local = 12.644225, position = 168475
bfs_local = 11.789434, position = 968401
bfs_local = 11.688601, position = 769737
bfs_local = 11.384415, position = 769735
bfs_local = 10.791825, position = 769736
bfs_local = 10.542933, position = 340701
bfs_local = 9.789495, position = 874191
bfs_local = 9.721350, position = 874194
bfs_local = 9.236251, position = 148875
bfs_local = 8.066576, position = 148876
bsf = 2.84017
position = 148876
DTW Calculation = 3.83%
Count of DTW = 38253
Time for calculation lower bounding = 86.40%
Time for initializing arrays with start and end segment values = 0.002%
Time for finding the best subsequence = 13.25%
Runtime of reading 4.51
Total Runtime of finding the best subsequence 2.47


count of threads = 60

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bfs_local = 163.160416, position = 166651
bfs_local = 134.969955, position = 16665
bfs_local = 134.568069, position = 16667
bfs_local = 99.469933, position = 16673
bfs_local = 92.177040, position = 16674
bfs_local = 38.112709, position = 666608
bfs_local = 20.669662, position = 666600
bfs_local = 20.563293, position = 666601
bfs_local = 19.681322, position = 516643
bfs_local = 17.502886, position = 16725
bfs_local = 16.769062, position = 516641
bfs_local = 16.631578, position = 516741
bfs_local = 16.046032, position = 516749
bfs_local = 15.858474, position = 516751
bfs_local = 15.703591, position = 516750
bfs_local = 15.380412, position = 566833
bfs_local = 14.905022, position = 566832
bfs_local = 14.029982, position = 286
bfs_local = 11.817424, position = 850360
bfs_local = 11.789434, position = 968401
bfs_local = 11.688601, position = 769737
bfs_local = 11.384415, position = 769735
bfs_local = 10.791825, position = 769736
bfs_local = 10.093108, position = 286687
bfs_local = 9.721350, position = 874194
bfs_local = 9.402809, position = 295832
bfs_local = 8.066576, position = 148876
bsf = 2.84017
position = 148876
DTW Calculation = 4.53%
Count of DTW = 45287
Time for calculation lower bounding = 80.32%
Time for initializing arrays with start and end segment values = 0.004%
Time for finding the best subsequence = 19.31%
Runtime of reading 4.52
Total Runtime of finding the best subsequence 1.38


count of threads = 120

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bfs_local = 161.994553, position = 166665
bfs_local = 52.065403, position = 8338
bfs_local = 50.822723, position = 924967
bfs_local = 47.237518, position = 8340
bfs_local = 44.389679, position = 8341
bfs_local = 41.720978, position = 8342
bfs_local = 40.276382, position = 924971
bfs_local = 38.886436, position = 249996
bfs_local = 37.990715, position = 783305
bfs_local = 33.340252, position = 783307
bfs_local = 31.636507, position = 783309
bfs_local = 26.071812, position = 41670
bfs_local = 25.365734, position = 41669
bfs_local = 19.365086, position = 516647
bfs_local = 19.340477, position = 16728
bfs_local = 17.502886, position = 16725
bfs_local = 17.397680, position = 516747
bfs_local = 17.033276, position = 58451
bfs_local = 11.969646, position = 925121
bfs_local = 11.579897, position = 525209
bfs_local = 11.354399, position = 25846
bfs_local = 11.286866, position = 92623
bfs_local = 10.973717, position = 259693
bfs_local = 10.818749, position = 259691
bfs_local = 10.791825, position = 769736
bfs_local = 10.654775, position = 286686
bfs_local = 10.093108, position = 286687
bfs_local = 9.471311, position = 295833
bfs_local = 9.402809, position = 295832
bfs_local = 9.236251, position = 148875
bfs_local = 8.811771, position = 148877
bfs_local = 8.066576, position = 148876
bsf = 2.84017
position = 148876
DTW Calculation = 4.58%
Count of DTW = 45759
Time for calculation lower bounding = 74.18%
Time for initializing arrays with start and end segment values = 0.008%
Time for finding the best subsequence = 25.49%
Runtime of reading 4.65
Total Runtime of finding the best subsequence 1.06


count of threads = 180

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bfs_local = 29.348007, position = 666606
bfs_local = 28.651278, position = 855470
bfs_local = 26.402102, position = 227758
bfs_local = 22.457497, position = 666602
bfs_local = 20.669662, position = 666600
bfs_local = 20.563293, position = 666601
bfs_local = 15.522529, position = 811036
bfs_local = 12.403892, position = 811031
bfs_local = 11.724231, position = 811033
bfs_local = 11.569302, position = 756002
bfs_local = 11.234343, position = 756564
bfs_local = 10.232831, position = 756562
bfs_local = 10.108885, position = 295831
bfs_local = 9.471311, position = 295833
bfs_local = 9.402809, position = 295832
bfs_local = 8.066576, position = 148876
bsf = 2.84017
position = 148876
DTW Calculation = 4.21%
Count of DTW = 42076
Time for calculation lower bounding = 67.34%
Time for initializing arrays with start and end segment values = 0.006%
Time for finding the best subsequence = 32.40%
Runtime of reading 4.51
Total Runtime of finding the best subsequence 1.15


count of threads = 240

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bfs_local = 181.003601, position = 541717
bfs_local = 32.583656, position = 125016
bfs_local = 19.303181, position = 145850
bfs_local = 19.106312, position = 429205
bfs_local = 16.954260, position = 754236
bfs_local = 15.541553, position = 754239
bfs_local = 14.976000, position = 620959
bfs_local = 14.838525, position = 620965
bfs_local = 14.681325, position = 620964
bfs_local = 12.864885, position = 995961
bfs_local = 12.189212, position = 925123
bfs_local = 12.041904, position = 995960
bfs_local = 11.969646, position = 925121
bfs_local = 11.221785, position = 645991
bfs_local = 10.523934, position = 645992
bfs_local = 10.232831, position = 756562
bfs_local = 8.811771, position = 148877
bfs_local = 8.066576, position = 148876
bsf = 2.84017
position = 148876
DTW Calculation = 4.64%
Count of DTW = 46393
Time for calculation lower bounding = 67.77%
Time for initializing arrays with start and end segment values = 0.006%
Time for finding the best subsequence = 31.94%
Runtime of reading 4.59
Total Runtime of finding the best subsequence 1.15


number of a  series of experiments = 2
count of threads = 1

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bfs_local = 205.160965, position = 68
bfs_local = 201.295975, position = 109
bfs_local = 198.454041, position = 110
bfs_local = 181.586685, position = 111
bfs_local = 177.698914, position = 112
bfs_local = 177.272308, position = 113
bfs_local = 173.585770, position = 114
bfs_local = 168.219498, position = 115
bfs_local = 166.177505, position = 116
bfs_local = 153.458847, position = 117
bfs_local = 137.000046, position = 118
bfs_local = 134.529373, position = 122
bfs_local = 128.011932, position = 123
bfs_local = 121.794411, position = 124
bfs_local = 115.587074, position = 125
bfs_local = 106.061142, position = 126
bfs_local = 90.850601, position = 127
bfs_local = 90.638985, position = 129
bfs_local = 88.713676, position = 130
bfs_local = 87.106239, position = 209
bfs_local = 86.763412, position = 210
bfs_local = 84.332840, position = 211
bfs_local = 82.125427, position = 212
bfs_local = 66.927124, position = 276
bfs_local = 57.716431, position = 277
bfs_local = 51.453167, position = 278
bfs_local = 39.160603, position = 279
bfs_local = 31.621851, position = 280
bfs_local = 28.650408, position = 281
bfs_local = 23.193083, position = 282
bfs_local = 18.942251, position = 283
bfs_local = 16.442295, position = 284
bfs_local = 14.960265, position = 285
bfs_local = 14.029982, position = 286
bfs_local = 13.455287, position = 12317
bfs_local = 13.343568, position = 12318
bfs_local = 11.354399, position = 25846
bfs_local = 11.098724, position = 43099
bfs_local = 10.341489, position = 131638
bfs_local = 10.241185, position = 131639
bfs_local = 10.189466, position = 148874
bfs_local = 9.236251, position = 148875
bfs_local = 8.066576, position = 148876
bsf = 2.84017
position = 148876
DTW Calculation = 3.22%
Count of DTW = 32226
Time for calculation lower bounding = 76.83%
Time for initializing arrays with start and end segment values = 0.000%
Time for finding the best subsequence = 22.89%
Runtime of reading 4.66
Total Runtime of finding the best subsequence 82.15


count of threads = 10

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bfs_local = 107.732956, position = 99988
bfs_local = 54.801617, position = 199984
bfs_local = 54.301651, position = 199985
bfs_local = 40.817909, position = 899898
bfs_local = 38.483379, position = 899897
bfs_local = 37.310665, position = 899896
bfs_local = 35.852566, position = 899895
bfs_local = 35.009220, position = 899894
bfs_local = 34.579025, position = 899893
bfs_local = 32.803574, position = 899892
bfs_local = 28.973774, position = 899976
bfs_local = 27.948656, position = 899977
bfs_local = 27.778095, position = 899974
bfs_local = 23.035563, position = 899978
bfs_local = 19.881905, position = 900148
bfs_local = 18.704517, position = 900152
bfs_local = 18.210606, position = 900153
bfs_local = 17.990850, position = 900150
bfs_local = 17.681797, position = 900151
bfs_local = 14.029982, position = 286
bfs_local = 13.420880, position = 800612
bfs_local = 13.254978, position = 800613
bfs_local = 12.667230, position = 301539
bfs_local = 12.474792, position = 307199
bfs_local = 11.560240, position = 307200
bfs_local = 11.354399, position = 25846
bfs_local = 10.341489, position = 131638
bfs_local = 10.241185, position = 131639
bfs_local = 10.189466, position = 148874
bfs_local = 9.236251, position = 148875
bfs_local = 8.066576, position = 148876
bsf = 2.84017
position = 148876
DTW Calculation = 4.10%
Count of DTW = 40960
Time for calculation lower bounding = 88.18%
Time for initializing arrays with start and end segment values = 0.001%
Time for finding the best subsequence = 11.48%
Runtime of reading 4.66
Total Runtime of finding the best subsequence 7.17


count of threads = 30

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bfs_local = 114.973183, position = 333304
bfs_local = 73.125473, position = 33330
bfs_local = 59.591976, position = 33331
bfs_local = 36.884579, position = 33334
bfs_local = 30.282339, position = 666605
bfs_local = 22.457497, position = 666602
bfs_local = 20.669662, position = 666600
bfs_local = 20.563293, position = 666601
bfs_local = 18.879551, position = 433439
bfs_local = 17.462156, position = 433442
bfs_local = 16.081520, position = 566830
bfs_local = 14.905022, position = 566832
bfs_local = 14.677940, position = 566831
bfs_local = 14.654401, position = 533606
bfs_local = 14.029982, position = 286
bfs_local = 13.782787, position = 800614
bfs_local = 13.420880, position = 800612
bfs_local = 13.254978, position = 800613
bfs_local = 13.009433, position = 301544
bfs_local = 12.667230, position = 301539
bfs_local = 12.644225, position = 168475
bfs_local = 11.789434, position = 968401
bfs_local = 11.688601, position = 769737
bfs_local = 11.384415, position = 769735
bfs_local = 10.791825, position = 769736
bfs_local = 10.542933, position = 340701
bfs_local = 9.789495, position = 874191
bfs_local = 9.721350, position = 874194
bfs_local = 9.236251, position = 148875
bfs_local = 8.066576, position = 148876
bsf = 2.84017
position = 148876
DTW Calculation = 3.83%
Count of DTW = 38253
Time for calculation lower bounding = 87.21%
Time for initializing arrays with start and end segment values = 0.002%
Time for finding the best subsequence = 12.43%
Runtime of reading 4.59
Total Runtime of finding the best subsequence 2.45


count of threads = 60

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bfs_local = 29.348007, position = 666606
bfs_local = 20.669662, position = 666600
bfs_local = 20.563293, position = 666601
bfs_local = 19.681322, position = 516643
bfs_local = 17.502886, position = 16725
bfs_local = 16.769062, position = 516641
bfs_local = 16.631578, position = 516741
bfs_local = 16.046032, position = 516749
bfs_local = 15.858474, position = 516751
bfs_local = 15.703591, position = 516750
bfs_local = 15.380412, position = 566833
bfs_local = 14.905022, position = 566832
bfs_local = 14.029982, position = 286
bfs_local = 11.817424, position = 850360
bfs_local = 11.789434, position = 968401
bfs_local = 11.688601, position = 769737
bfs_local = 11.384415, position = 769735
bfs_local = 10.791825, position = 769736
bfs_local = 10.093108, position = 286687
bfs_local = 9.721350, position = 874194
bfs_local = 9.402809, position = 295832
bfs_local = 8.066576, position = 148876
bsf = 2.84017
position = 148876
DTW Calculation = 4.53%
Count of DTW = 45287
Time for calculation lower bounding = 77.64%
Time for initializing arrays with start and end segment values = 0.004%
Time for finding the best subsequence = 22.00%
Runtime of reading 4.51
Total Runtime of finding the best subsequence 1.43


count of threads = 120

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bfs_local = 45.554543, position = 249995
bfs_local = 41.534702, position = 924972
bfs_local = 40.276382, position = 924971
bfs_local = 38.886436, position = 249996
bfs_local = 35.473003, position = 783306
bfs_local = 26.071812, position = 41670
bfs_local = 25.365734, position = 41669
bfs_local = 19.365086, position = 516647
bfs_local = 19.340477, position = 16728
bfs_local = 17.502886, position = 16725
bfs_local = 17.033276, position = 58451
bfs_local = 11.969646, position = 925121
bfs_local = 11.579897, position = 525209
bfs_local = 11.354399, position = 25846
bfs_local = 11.286866, position = 92623
bfs_local = 10.973717, position = 259693
bfs_local = 10.818749, position = 259691
bfs_local = 10.791825, position = 769736
bfs_local = 10.654775, position = 286686
bfs_local = 10.093108, position = 286687
bfs_local = 9.471311, position = 295833
bfs_local = 9.402809, position = 295832
bfs_local = 9.236251, position = 148875
bfs_local = 8.811771, position = 148877
bfs_local = 8.066576, position = 148876
bsf = 2.84017
position = 148876
DTW Calculation = 4.58%
Count of DTW = 45759
Time for calculation lower bounding = 72.48%
Time for initializing arrays with start and end segment values = 0.006%
Time for finding the best subsequence = 27.20%
Runtime of reading 4.65
Total Runtime of finding the best subsequence 1.08


count of threads = 180

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bfs_local = 110.359367, position = 333303
bfs_local = 68.675903, position = 838808
bfs_local = 65.981209, position = 838812
bfs_local = 59.903648, position = 838811
bfs_local = 57.180580, position = 838810
bfs_local = 53.205673, position = 11118
bfs_local = 28.651278, position = 855470
bfs_local = 22.457497, position = 666602
bfs_local = 20.669662, position = 666600
bfs_local = 15.522529, position = 811036
bfs_local = 15.037633, position = 227762
bfs_local = 12.403892, position = 811031
bfs_local = 11.724231, position = 811033
bfs_local = 11.569302, position = 756002
bfs_local = 11.234343, position = 756564
bfs_local = 10.232831, position = 756562
bfs_local = 10.108885, position = 295831
bfs_local = 9.471311, position = 295833
bfs_local = 9.402809, position = 295832
bfs_local = 8.066576, position = 148876
bsf = 2.84017
position = 148876
DTW Calculation = 4.21%
Count of DTW = 42076
Time for calculation lower bounding = 68.70%
Time for initializing arrays with start and end segment values = 0.006%
Time for finding the best subsequence = 31.01%
Runtime of reading 4.56
Total Runtime of finding the best subsequence 1.11


count of threads = 240

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bfs_local = 43.461643, position = 416702
bfs_local = 32.387775, position = 125014
bfs_local = 22.506598, position = 429203
bfs_local = 21.444206, position = 145848
bfs_local = 20.540184, position = 145849
bfs_local = 19.239515, position = 429207
bfs_local = 19.106312, position = 429205
bfs_local = 16.954260, position = 754236
bfs_local = 15.541553, position = 754239
bfs_local = 14.838525, position = 620965
bfs_local = 14.681325, position = 620964
bfs_local = 12.189212, position = 925123
bfs_local = 12.041904, position = 995960
bfs_local = 11.969646, position = 925121
bfs_local = 11.221785, position = 645991
bfs_local = 10.523934, position = 645992
bfs_local = 10.232831, position = 756562
bfs_local = 8.811771, position = 148877
bfs_local = 8.066576, position = 148876
bsf = 2.84017
position = 148876
DTW Calculation = 4.64%
Count of DTW = 46393
Time for calculation lower bounding = 66.57%
Time for initializing arrays with start and end segment values = 0.006%
Time for finding the best subsequence = 33.10%
Runtime of reading 4.51
Total Runtime of finding the best subsequence 1.13


