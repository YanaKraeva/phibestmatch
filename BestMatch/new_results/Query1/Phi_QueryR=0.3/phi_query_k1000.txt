number of a  series of experiments = 0
count of threads = 1

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bfs_local = 217.902512, position = 49
bfs_local = 195.642578, position = 68
bfs_local = 193.637802, position = 69
bfs_local = 192.775009, position = 71
bfs_local = 189.489044, position = 72
bfs_local = 187.095184, position = 73
bfs_local = 184.339813, position = 74
bfs_local = 181.737457, position = 75
bfs_local = 180.180328, position = 76
bfs_local = 173.804504, position = 111
bfs_local = 167.831223, position = 112
bfs_local = 164.025787, position = 113
bfs_local = 156.747375, position = 114
bfs_local = 147.730194, position = 115
bfs_local = 141.820160, position = 116
bfs_local = 125.221336, position = 117
bfs_local = 105.388420, position = 118
bfs_local = 103.470390, position = 119
bfs_local = 102.000336, position = 120
bfs_local = 100.440971, position = 121
bfs_local = 96.495041, position = 122
bfs_local = 93.008415, position = 123
bfs_local = 90.095947, position = 124
bfs_local = 87.125870, position = 125
bfs_local = 81.895576, position = 126
bfs_local = 70.764061, position = 127
bfs_local = 68.156570, position = 270
bfs_local = 61.426308, position = 271
bfs_local = 57.186081, position = 272
bfs_local = 52.750832, position = 273
bfs_local = 52.364506, position = 274
bfs_local = 50.733974, position = 275
bfs_local = 38.286472, position = 276
bfs_local = 35.146641, position = 277
bfs_local = 27.123358, position = 279
bfs_local = 22.783644, position = 280
bfs_local = 21.594175, position = 281
bfs_local = 18.515766, position = 282
bfs_local = 16.781168, position = 283
bfs_local = 15.399029, position = 284
bfs_local = 14.566250, position = 285
bfs_local = 14.029982, position = 286
bfs_local = 13.455287, position = 12317
bfs_local = 13.343568, position = 12318
bfs_local = 11.354399, position = 25846
bfs_local = 11.098724, position = 43099
bfs_local = 10.341489, position = 131638
bfs_local = 10.241185, position = 131639
bfs_local = 10.189466, position = 148874
bfs_local = 9.236251, position = 148875
bfs_local = 8.066576, position = 148876
bsf = 2.84017
position = 148876
DTW Calculation = 6.16%
Count of DTW = 61618
Time for calculation lower bounding = 93.53%
Time for initializing arrays with start and end segment values = 0.000%
Time for finding the best subsequence = 6.18%
Runtime of reading 4.58
Total Runtime of finding the best subsequence 85.49


count of threads = 10

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bfs_local = 24.951794, position = 600
bfs_local = 23.430786, position = 300
bfs_local = 21.769669, position = 200876
bfs_local = 20.667128, position = 400252
bfs_local = 20.323555, position = 200879
bfs_local = 20.070511, position = 200880
bfs_local = 19.962025, position = 200881
bfs_local = 18.552818, position = 800610
bfs_local = 14.840156, position = 800611
bfs_local = 13.420880, position = 800612
bfs_local = 13.254978, position = 800613
bfs_local = 12.474792, position = 307199
bfs_local = 11.560240, position = 307200
bfs_local = 11.525405, position = 212738
bfs_local = 11.269180, position = 418682
bfs_local = 11.080192, position = 418685
bfs_local = 10.341489, position = 131638
bfs_local = 10.241185, position = 131639
bfs_local = 9.805469, position = 629634
bfs_local = 9.236251, position = 148875
bfs_local = 8.066576, position = 148876
bsf = 2.84017
position = 148876
DTW Calculation = 8.57%
Count of DTW = 85738
Time for calculation lower bounding = 90.54%
Time for initializing arrays with start and end segment values = 0.005%
Time for finding the best subsequence = 9.16%
Runtime of reading 4.51
Total Runtime of finding the best subsequence 8.86


count of threads = 30

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bfs_local = 42.827217, position = 333433
bfs_local = 21.994339, position = 667066
bfs_local = 20.070511, position = 200880
bfs_local = 18.465216, position = 899976
bfs_local = 16.516710, position = 567443
bfs_local = 15.931929, position = 400528
bfs_local = 15.283618, position = 400529
bfs_local = 14.566250, position = 285
bfs_local = 14.029982, position = 286
bfs_local = 13.420880, position = 800612
bfs_local = 13.254978, position = 800613
bfs_local = 13.032800, position = 335233
bfs_local = 12.128454, position = 734758
bfs_local = 9.789495, position = 874191
bfs_local = 9.721350, position = 874194
bfs_local = 9.236251, position = 148875
bfs_local = 8.066576, position = 148876
bsf = 2.84017
position = 148876
DTW Calculation = 10.86%
Count of DTW = 108587
Time for calculation lower bounding = 87.58%
Time for initializing arrays with start and end segment values = 0.014%
Time for finding the best subsequence = 12.09%
Runtime of reading 4.52
Total Runtime of finding the best subsequence 3.09


count of threads = 60

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bfs_local = 60.396622, position = 666616
bfs_local = 38.144569, position = 16715
bfs_local = 36.722893, position = 50445
bfs_local = 24.689753, position = 366763
bfs_local = 20.070511, position = 200880
bfs_local = 19.389124, position = 516648
bfs_local = 17.880121, position = 516748
bfs_local = 17.080967, position = 834066
bfs_local = 16.516710, position = 567443
bfs_local = 15.414685, position = 516632
bfs_local = 15.190704, position = 50529
bfs_local = 12.268681, position = 217229
bfs_local = 12.121199, position = 284061
bfs_local = 11.311694, position = 750551
bfs_local = 11.223995, position = 340695
bfs_local = 10.817071, position = 340696
bfs_local = 10.542933, position = 340701
bfs_local = 9.789495, position = 874191
bfs_local = 9.721350, position = 874194
bfs_local = 9.236251, position = 148875
bfs_local = 8.066576, position = 148876
bsf = 2.84017
position = 148876
DTW Calculation = 14.58%
Count of DTW = 145797
Time for calculation lower bounding = 80.52%
Time for initializing arrays with start and end segment values = 0.049%
Time for finding the best subsequence = 19.12%
Runtime of reading 4.62
Total Runtime of finding the best subsequence 1.74


count of threads = 120

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bfs_local = 51.714088, position = 500
bfs_local = 43.120770, position = 8358
bfs_local = 39.913673, position = 342286
bfs_local = 37.014545, position = 8383
bfs_local = 28.153255, position = 342261
bfs_local = 18.228897, position = 8833
bfs_local = 17.490183, position = 700463
bfs_local = 15.141444, position = 709071
bfs_local = 14.135927, position = 850357
bfs_local = 11.732228, position = 892347
bfs_local = 11.663574, position = 892348
bfs_local = 11.241044, position = 92623
bfs_local = 10.817071, position = 340696
bfs_local = 9.837740, position = 789813
bfs_local = 9.092925, position = 295830
bfs_local = 8.929319, position = 295831
bfs_local = 8.066576, position = 148876
bsf = 2.84017
position = 148876
DTW Calculation = 20.67%
Count of DTW = 206670
Time for calculation lower bounding = 70.94%
Time for initializing arrays with start and end segment values = 0.033%
Time for finding the best subsequence = 28.75%
Runtime of reading 4.53
Total Runtime of finding the best subsequence 1.35


count of threads = 180

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bfs_local = 21.994339, position = 667066
bfs_local = 19.881905, position = 900148
bfs_local = 17.755795, position = 128526
bfs_local = 15.459907, position = 278072
bfs_local = 14.371515, position = 561510
bfs_local = 13.492498, position = 811035
bfs_local = 11.529962, position = 284066
bfs_local = 11.034246, position = 789807
bfs_local = 10.156672, position = 789808
bfs_local = 10.153045, position = 789809
bfs_local = 9.092925, position = 295830
bfs_local = 8.929319, position = 295831
bfs_local = 8.066576, position = 148876
bsf = 2.84017
position = 148876
DTW Calculation = 24.72%
Count of DTW = 247129
Time for calculation lower bounding = 67.84%
Time for initializing arrays with start and end segment values = 0.019%
Time for finding the best subsequence = 31.90%
Runtime of reading 4.61
Total Runtime of finding the best subsequence 1.37


count of threads = 240

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bfs_local = 116.937584, position = 833645
bfs_local = 63.360508, position = 334118
bfs_local = 51.714088, position = 500
bfs_local = 37.420815, position = 334143
bfs_local = 32.774731, position = 334268
bfs_local = 24.523388, position = 5067
bfs_local = 22.308340, position = 338185
bfs_local = 19.992214, position = 338285
bfs_local = 17.927504, position = 512978
bfs_local = 17.057177, position = 516745
bfs_local = 15.108865, position = 629629
bfs_local = 14.633133, position = 96766
bfs_local = 12.700335, position = 925544
bfs_local = 11.311694, position = 750551
bfs_local = 10.549894, position = 645993
bfs_local = 9.805469, position = 629634
bfs_local = 9.789495, position = 874191
bfs_local = 9.092925, position = 295830
bfs_local = 8.929319, position = 295831
bfs_local = 8.066576, position = 148876
bsf = 2.84017
position = 148876
DTW Calculation = 29.78%
Count of DTW = 297719
Time for calculation lower bounding = 67.88%
Time for initializing arrays with start and end segment values = 0.152%
Time for finding the best subsequence = 31.73%
Runtime of reading 4.63
Total Runtime of finding the best subsequence 1.33


number of a  series of experiments = 1
count of threads = 1

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bfs_local = 217.902512, position = 49
bfs_local = 195.642578, position = 68
bfs_local = 193.637802, position = 69
bfs_local = 192.775009, position = 71
bfs_local = 189.489044, position = 72
bfs_local = 187.095184, position = 73
bfs_local = 184.339813, position = 74
bfs_local = 181.737457, position = 75
bfs_local = 180.180328, position = 76
bfs_local = 173.804504, position = 111
bfs_local = 167.831223, position = 112
bfs_local = 164.025787, position = 113
bfs_local = 156.747375, position = 114
bfs_local = 147.730194, position = 115
bfs_local = 141.820160, position = 116
bfs_local = 125.221336, position = 117
bfs_local = 105.388420, position = 118
bfs_local = 103.470390, position = 119
bfs_local = 102.000336, position = 120
bfs_local = 100.440971, position = 121
bfs_local = 96.495041, position = 122
bfs_local = 93.008415, position = 123
bfs_local = 90.095947, position = 124
bfs_local = 87.125870, position = 125
bfs_local = 81.895576, position = 126
bfs_local = 70.764061, position = 127
bfs_local = 68.156570, position = 270
bfs_local = 61.426308, position = 271
bfs_local = 57.186081, position = 272
bfs_local = 52.750832, position = 273
bfs_local = 52.364506, position = 274
bfs_local = 50.733974, position = 275
bfs_local = 38.286472, position = 276
bfs_local = 35.146641, position = 277
bfs_local = 27.123358, position = 279
bfs_local = 22.783644, position = 280
bfs_local = 21.594175, position = 281
bfs_local = 18.515766, position = 282
bfs_local = 16.781168, position = 283
bfs_local = 15.399029, position = 284
bfs_local = 14.566250, position = 285
bfs_local = 14.029982, position = 286
bfs_local = 13.455287, position = 12317
bfs_local = 13.343568, position = 12318
bfs_local = 11.354399, position = 25846
bfs_local = 11.098724, position = 43099
bfs_local = 10.341489, position = 131638
bfs_local = 10.241185, position = 131639
bfs_local = 10.189466, position = 148874
bfs_local = 9.236251, position = 148875
bfs_local = 8.066576, position = 148876
bsf = 2.84017
position = 148876
DTW Calculation = 6.16%
Count of DTW = 61618
Time for calculation lower bounding = 93.53%
Time for initializing arrays with start and end segment values = 0.000%
Time for finding the best subsequence = 6.17%
Runtime of reading 4.53
Total Runtime of finding the best subsequence 85.52


count of threads = 10

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bfs_local = 66.933228, position = 800
bfs_local = 23.430786, position = 300
bfs_local = 21.769669, position = 200876
bfs_local = 20.667128, position = 400252
bfs_local = 20.323555, position = 200879
bfs_local = 20.070511, position = 200880
bfs_local = 18.552818, position = 800610
bfs_local = 14.840156, position = 800611
bfs_local = 13.420880, position = 800612
bfs_local = 13.254978, position = 800613
bfs_local = 12.474792, position = 307199
bfs_local = 11.560240, position = 307200
bfs_local = 11.525405, position = 212738
bfs_local = 11.269180, position = 418682
bfs_local = 11.080192, position = 418685
bfs_local = 10.341489, position = 131638
bfs_local = 10.241185, position = 131639
bfs_local = 9.805469, position = 629634
bfs_local = 9.236251, position = 148875
bfs_local = 8.066576, position = 148876
bsf = 2.84017
position = 148876
DTW Calculation = 8.57%
Count of DTW = 85738
Time for calculation lower bounding = 90.67%
Time for initializing arrays with start and end segment values = 0.005%
Time for finding the best subsequence = 9.02%
Runtime of reading 4.52
Total Runtime of finding the best subsequence 8.85


count of threads = 30

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bfs_local = 164.623734, position = 333333
bfs_local = 61.299992, position = 33330
bfs_local = 40.397835, position = 667166
bfs_local = 24.951794, position = 600
bfs_local = 21.994339, position = 667066
bfs_local = 20.070511, position = 200880
bfs_local = 18.465216, position = 899976
bfs_local = 16.516710, position = 567443
bfs_local = 15.931929, position = 400528
bfs_local = 15.283618, position = 400529
bfs_local = 14.566250, position = 285
bfs_local = 14.029982, position = 286
bfs_local = 13.420880, position = 800612
bfs_local = 13.254978, position = 800613
bfs_local = 13.032800, position = 335233
bfs_local = 12.128454, position = 734758
bfs_local = 9.789495, position = 874191
bfs_local = 9.721350, position = 874194
bfs_local = 9.236251, position = 148875
bfs_local = 8.066576, position = 148876
bsf = 2.84017
position = 148876
DTW Calculation = 10.86%
Count of DTW = 108587
Time for calculation lower bounding = 87.60%
Time for initializing arrays with start and end segment values = 0.014%
Time for finding the best subsequence = 12.07%
Runtime of reading 4.69
Total Runtime of finding the best subsequence 3.08


count of threads = 60

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bfs_local = 60.396622, position = 666616
bfs_local = 38.144569, position = 16715
bfs_local = 35.368534, position = 683331
bfs_local = 31.580626, position = 50045
bfs_local = 24.951794, position = 600
bfs_local = 23.703402, position = 750
bfs_local = 21.994339, position = 667066
bfs_local = 20.070511, position = 200880
bfs_local = 18.465216, position = 899976
bfs_local = 17.080967, position = 834066
bfs_local = 16.516710, position = 567443
bfs_local = 15.414685, position = 516632
bfs_local = 15.190704, position = 50529
bfs_local = 12.268681, position = 217229
bfs_local = 12.121199, position = 284061
bfs_local = 11.311694, position = 750551
bfs_local = 11.223995, position = 340695
bfs_local = 10.817071, position = 340696
bfs_local = 10.542933, position = 340701
bfs_local = 9.789495, position = 874191
bfs_local = 9.721350, position = 874194
bfs_local = 9.236251, position = 148875
bfs_local = 8.066576, position = 148876
bsf = 2.84017
position = 148876
DTW Calculation = 14.58%
Count of DTW = 145797
Time for calculation lower bounding = 80.49%
Time for initializing arrays with start and end segment values = 0.054%
Time for finding the best subsequence = 19.14%
Runtime of reading 4.51
Total Runtime of finding the best subsequence 1.74


count of threads = 120

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bfs_local = 173.153503, position = 667131
bfs_local = 71.957878, position = 8333
bfs_local = 39.913673, position = 342286
bfs_local = 37.014545, position = 8383
bfs_local = 28.153255, position = 342261
bfs_local = 17.490183, position = 700463
bfs_local = 15.141444, position = 709071
bfs_local = 14.135927, position = 850357
bfs_local = 11.732228, position = 892347
bfs_local = 11.663574, position = 892348
bfs_local = 11.241044, position = 92623
bfs_local = 10.817071, position = 340696
bfs_local = 9.837740, position = 789813
bfs_local = 9.092925, position = 295830
bfs_local = 8.929319, position = 295831
bfs_local = 8.066576, position = 148876
bsf = 2.84017
position = 148876
DTW Calculation = 20.67%
Count of DTW = 206670
Time for calculation lower bounding = 73.73%
Time for initializing arrays with start and end segment values = 0.038%
Time for finding the best subsequence = 25.94%
Runtime of reading 4.51
Total Runtime of finding the best subsequence 1.31


count of threads = 180

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bfs_local = 56.524483, position = 667266
bfs_local = 30.631609, position = 444794
bfs_local = 30.461372, position = 444894
bfs_local = 28.258175, position = 223072
bfs_local = 24.951794, position = 600
bfs_local = 23.430786, position = 300
bfs_local = 21.994339, position = 667066
bfs_local = 14.371515, position = 561510
bfs_local = 13.492498, position = 811035
bfs_local = 11.529962, position = 284066
bfs_local = 11.034246, position = 789807
bfs_local = 10.156672, position = 789808
bfs_local = 10.153045, position = 789809
bfs_local = 9.092925, position = 295830
bfs_local = 8.929319, position = 295831
bfs_local = 8.066576, position = 148876
bsf = 2.84017
position = 148876
DTW Calculation = 24.72%
Count of DTW = 247129
Time for calculation lower bounding = 67.76%
Time for initializing arrays with start and end segment values = 0.009%
Time for finding the best subsequence = 31.99%
Runtime of reading 4.52
Total Runtime of finding the best subsequence 1.33


count of threads = 240

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bfs_local = 89.574402, position = 167434
bfs_local = 34.020885, position = 4417
bfs_local = 24.523388, position = 5067
bfs_local = 22.308340, position = 338185
bfs_local = 17.927504, position = 512978
bfs_local = 17.057177, position = 516745
bfs_local = 15.108865, position = 629629
bfs_local = 14.633133, position = 96766
bfs_local = 11.311694, position = 750551
bfs_local = 10.549894, position = 645993
bfs_local = 9.805469, position = 629634
bfs_local = 9.789495, position = 874191
bfs_local = 9.092925, position = 295830
bfs_local = 8.929319, position = 295831
bfs_local = 8.066576, position = 148876
bsf = 2.84017
position = 148876
DTW Calculation = 29.78%
Count of DTW = 297719
Time for calculation lower bounding = 66.92%
Time for initializing arrays with start and end segment values = 0.128%
Time for finding the best subsequence = 32.73%
Runtime of reading 4.51
Total Runtime of finding the best subsequence 1.36


number of a  series of experiments = 2
count of threads = 1

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bfs_local = 217.902512, position = 49
bfs_local = 195.642578, position = 68
bfs_local = 193.637802, position = 69
bfs_local = 192.775009, position = 71
bfs_local = 189.489044, position = 72
bfs_local = 187.095184, position = 73
bfs_local = 184.339813, position = 74
bfs_local = 181.737457, position = 75
bfs_local = 180.180328, position = 76
bfs_local = 173.804504, position = 111
bfs_local = 167.831223, position = 112
bfs_local = 164.025787, position = 113
bfs_local = 156.747375, position = 114
bfs_local = 147.730194, position = 115
bfs_local = 141.820160, position = 116
bfs_local = 125.221336, position = 117
bfs_local = 105.388420, position = 118
bfs_local = 103.470390, position = 119
bfs_local = 102.000336, position = 120
bfs_local = 100.440971, position = 121
bfs_local = 96.495041, position = 122
bfs_local = 93.008415, position = 123
bfs_local = 90.095947, position = 124
bfs_local = 87.125870, position = 125
bfs_local = 81.895576, position = 126
bfs_local = 70.764061, position = 127
bfs_local = 68.156570, position = 270
bfs_local = 61.426308, position = 271
bfs_local = 57.186081, position = 272
bfs_local = 52.750832, position = 273
bfs_local = 52.364506, position = 274
bfs_local = 50.733974, position = 275
bfs_local = 38.286472, position = 276
bfs_local = 35.146641, position = 277
bfs_local = 27.123358, position = 279
bfs_local = 22.783644, position = 280
bfs_local = 21.594175, position = 281
bfs_local = 18.515766, position = 282
bfs_local = 16.781168, position = 283
bfs_local = 15.399029, position = 284
bfs_local = 14.566250, position = 285
bfs_local = 14.029982, position = 286
bfs_local = 13.455287, position = 12317
bfs_local = 13.343568, position = 12318
bfs_local = 11.354399, position = 25846
bfs_local = 11.098724, position = 43099
bfs_local = 10.341489, position = 131638
bfs_local = 10.241185, position = 131639
bfs_local = 10.189466, position = 148874
bfs_local = 9.236251, position = 148875
bfs_local = 8.066576, position = 148876
bsf = 2.84017
position = 148876
DTW Calculation = 6.16%
Count of DTW = 61618
Time for calculation lower bounding = 93.53%
Time for initializing arrays with start and end segment values = 0.000%
Time for finding the best subsequence = 6.18%
Runtime of reading 4.51
Total Runtime of finding the best subsequence 85.53


count of threads = 10

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bfs_local = 175.541489, position = 400
bfs_local = 107.732956, position = 99988
bfs_local = 66.933228, position = 800
bfs_local = 24.951794, position = 600
bfs_local = 23.430786, position = 300
bfs_local = 21.769669, position = 200876
bfs_local = 20.667128, position = 400252
bfs_local = 20.323555, position = 200879
bfs_local = 20.070511, position = 200880
bfs_local = 19.962025, position = 200881
bfs_local = 18.552818, position = 800610
bfs_local = 14.840156, position = 800611
bfs_local = 13.420880, position = 800612
bfs_local = 13.254978, position = 800613
bfs_local = 12.474792, position = 307199
bfs_local = 11.560240, position = 307200
bfs_local = 11.525405, position = 212738
bfs_local = 11.269180, position = 418682
bfs_local = 11.080192, position = 418685
bfs_local = 10.341489, position = 131638
bfs_local = 10.241185, position = 131639
bfs_local = 9.805469, position = 629634
bfs_local = 9.236251, position = 148875
bfs_local = 8.066576, position = 148876
bsf = 2.84017
position = 148876
DTW Calculation = 8.57%
Count of DTW = 85738
Time for calculation lower bounding = 90.65%
Time for initializing arrays with start and end segment values = 0.005%
Time for finding the best subsequence = 9.05%
Runtime of reading 4.58
Total Runtime of finding the best subsequence 8.85


count of threads = 30

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bfs_local = 164.623734, position = 333333
bfs_local = 61.299992, position = 33330
bfs_local = 55.349358, position = 333833
bfs_local = 51.607235, position = 334233
bfs_local = 23.430786, position = 300
bfs_local = 21.994339, position = 667066
bfs_local = 20.070511, position = 200880
bfs_local = 18.465216, position = 899976
bfs_local = 16.516710, position = 567443
bfs_local = 15.931929, position = 400528
bfs_local = 15.283618, position = 400529
bfs_local = 14.566250, position = 285
bfs_local = 14.029982, position = 286
bfs_local = 13.420880, position = 800612
bfs_local = 13.254978, position = 800613
bfs_local = 13.032800, position = 335233
bfs_local = 12.128454, position = 734758
bfs_local = 9.789495, position = 874191
bfs_local = 9.721350, position = 874194
bfs_local = 9.236251, position = 148875
bfs_local = 8.066576, position = 148876
bsf = 2.84017
position = 148876
DTW Calculation = 10.86%
Count of DTW = 108587
Time for calculation lower bounding = 87.55%
Time for initializing arrays with start and end segment values = 0.014%
Time for finding the best subsequence = 12.13%
Runtime of reading 4.52
Total Runtime of finding the best subsequence 3.08


count of threads = 60

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bfs_local = 164.623734, position = 333333
bfs_local = 134.969955, position = 16665
bfs_local = 51.668530, position = 16765
bfs_local = 38.144569, position = 16715
bfs_local = 33.383865, position = 350298
bfs_local = 23.430786, position = 300
bfs_local = 21.994339, position = 667066
bfs_local = 20.070511, position = 200880
bfs_local = 19.389124, position = 516648
bfs_local = 18.974783, position = 517398
bfs_local = 17.080967, position = 834066
bfs_local = 16.516710, position = 567443
bfs_local = 15.414685, position = 516632
bfs_local = 15.190704, position = 50529
bfs_local = 12.268681, position = 217229
bfs_local = 12.121199, position = 284061
bfs_local = 11.311694, position = 750551
bfs_local = 11.223995, position = 340695
bfs_local = 10.817071, position = 340696
bfs_local = 10.542933, position = 340701
bfs_local = 9.789495, position = 874191
bfs_local = 9.721350, position = 874194
bfs_local = 9.236251, position = 148875
bfs_local = 8.066576, position = 148876
bsf = 2.84017
position = 148876
DTW Calculation = 14.58%
Count of DTW = 145797
Time for calculation lower bounding = 80.45%
Time for initializing arrays with start and end segment values = 0.057%
Time for finding the best subsequence = 19.18%
Runtime of reading 4.52
Total Runtime of finding the best subsequence 1.74


count of threads = 120

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bfs_local = 51.714088, position = 500
bfs_local = 43.120770, position = 8358
bfs_local = 39.913673, position = 342286
bfs_local = 37.014545, position = 8383
bfs_local = 28.153255, position = 342261
bfs_local = 18.228897, position = 8833
bfs_local = 17.490183, position = 700463
bfs_local = 15.141444, position = 709071
bfs_local = 14.135927, position = 850357
bfs_local = 11.732228, position = 892347
bfs_local = 11.663574, position = 892348
bfs_local = 11.241044, position = 92623
bfs_local = 10.817071, position = 340696
bfs_local = 9.837740, position = 789813
bfs_local = 9.092925, position = 295830
bfs_local = 8.929319, position = 295831
bfs_local = 8.066576, position = 148876
bsf = 2.84017
position = 148876
DTW Calculation = 20.67%
Count of DTW = 206670
Time for calculation lower bounding = 71.79%
Time for initializing arrays with start and end segment values = 0.033%
Time for finding the best subsequence = 27.90%
Runtime of reading 4.52
Total Runtime of finding the best subsequence 1.39


count of threads = 180

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bfs_local = 94.317146, position = 222872
bfs_local = 36.422913, position = 667366
bfs_local = 30.059958, position = 778077
bfs_local = 28.258175, position = 223072
bfs_local = 23.758892, position = 777827
bfs_local = 23.430786, position = 300
bfs_local = 14.371515, position = 561510
bfs_local = 13.492498, position = 811035
bfs_local = 11.529962, position = 284066
bfs_local = 11.034246, position = 789807
bfs_local = 10.156672, position = 789808
bfs_local = 10.153045, position = 789809
bfs_local = 9.092925, position = 295830
bfs_local = 8.929319, position = 295831
bfs_local = 8.066576, position = 148876
bsf = 2.84017
position = 148876
DTW Calculation = 24.72%
Count of DTW = 247129
Time for calculation lower bounding = 68.53%
Time for initializing arrays with start and end segment values = 0.033%
Time for finding the best subsequence = 31.19%
Runtime of reading 4.51
Total Runtime of finding the best subsequence 1.32


count of threads = 240

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bfs_local = 166.102570, position = 833895
bfs_local = 34.020885, position = 4417
bfs_local = 26.167156, position = 667061
bfs_local = 24.523388, position = 5067
bfs_local = 23.430786, position = 300
bfs_local = 22.308340, position = 338185
bfs_local = 17.927504, position = 512978
bfs_local = 17.057177, position = 516745
bfs_local = 15.108865, position = 629629
bfs_local = 14.633133, position = 96766
bfs_local = 13.861280, position = 62805
bfs_local = 11.311694, position = 750551
bfs_local = 10.549894, position = 645993
bfs_local = 9.805469, position = 629634
bfs_local = 9.789495, position = 874191
bfs_local = 9.092925, position = 295830
bfs_local = 8.929319, position = 295831
bfs_local = 8.066576, position = 148876
bsf = 2.84017
position = 148876
DTW Calculation = 29.78%
Count of DTW = 297719
Time for calculation lower bounding = 67.30%
Time for initializing arrays with start and end segment values = 0.101%
Time for finding the best subsequence = 32.38%
Runtime of reading 4.51
Total Runtime of finding the best subsequence 1.35


