number of a  series of experiments = 0
count of threads = 1

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bfs_local = 217.902512, position = 49
bfs_local = 195.642578, position = 68
bfs_local = 193.637802, position = 69
bfs_local = 192.775009, position = 71
bfs_local = 189.489044, position = 72
bfs_local = 187.095184, position = 73
bfs_local = 184.339813, position = 74
bfs_local = 181.737457, position = 75
bfs_local = 180.180328, position = 76
bfs_local = 173.804504, position = 111
bfs_local = 167.831223, position = 112
bfs_local = 164.025787, position = 113
bfs_local = 156.747375, position = 114
bfs_local = 147.730194, position = 115
bfs_local = 141.820160, position = 116
bfs_local = 125.221336, position = 117
bfs_local = 105.388420, position = 118
bfs_local = 103.470390, position = 119
bfs_local = 102.000336, position = 120
bfs_local = 100.440971, position = 121
bfs_local = 96.495041, position = 122
bfs_local = 93.008415, position = 123
bfs_local = 90.095947, position = 124
bfs_local = 87.125870, position = 125
bfs_local = 81.895576, position = 126
bfs_local = 70.764061, position = 127
bfs_local = 68.156570, position = 270
bfs_local = 61.426308, position = 271
bfs_local = 57.186081, position = 272
bfs_local = 52.750832, position = 273
bfs_local = 52.364506, position = 274
bfs_local = 50.733974, position = 275
bfs_local = 38.286472, position = 276
bfs_local = 35.146641, position = 277
bfs_local = 27.123358, position = 279
bfs_local = 22.783644, position = 280
bfs_local = 21.594175, position = 281
bfs_local = 18.515766, position = 282
bfs_local = 16.781168, position = 283
bfs_local = 15.399029, position = 284
bfs_local = 14.566250, position = 285
bfs_local = 14.029982, position = 286
bfs_local = 13.455287, position = 12317
bfs_local = 13.343568, position = 12318
bfs_local = 11.354399, position = 25846
bfs_local = 11.098724, position = 43099
bfs_local = 10.341489, position = 131638
bfs_local = 10.241185, position = 131639
bfs_local = 10.189466, position = 148874
bfs_local = 9.236251, position = 148875
bfs_local = 8.066576, position = 148876
bsf = 2.84017
position = 148876
DTW Calculation = 6.05%
Count of DTW = 60446
Time for calculation lower bounding = 53.89%
Time for initializing arrays with start and end segment values = 0.000%
Time for finding the best subsequence = 45.94%
Runtime of reading 4.63
Total Runtime of finding the best subsequence 148.25


count of threads = 10

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bfs_local = 163.320374, position = 499941
bfs_local = 107.732956, position = 99988
bfs_local = 80.077698, position = 199978
bfs_local = 71.309280, position = 199980
bfs_local = 34.433132, position = 899893
bfs_local = 32.623161, position = 899892
bfs_local = 31.422348, position = 400001
bfs_local = 31.057961, position = 400002
bfs_local = 21.528841, position = 899972
bfs_local = 19.737476, position = 899973
bfs_local = 15.850533, position = 899974
bfs_local = 14.029982, position = 286
bfs_local = 13.420880, position = 800612
bfs_local = 13.254978, position = 800613
bfs_local = 13.166487, position = 301540
bfs_local = 13.009433, position = 301544
bfs_local = 12.667230, position = 301539
bfs_local = 12.474792, position = 307199
bfs_local = 11.560240, position = 307200
bfs_local = 11.525405, position = 212738
bfs_local = 11.311034, position = 418683
bfs_local = 11.080192, position = 418685
bfs_local = 10.098009, position = 629635
bfs_local = 9.805469, position = 629634
bfs_local = 8.066576, position = 148876
bsf = 2.84017
position = 148876
DTW Calculation = 7.40%
Count of DTW = 73968
Time for calculation lower bounding = 79.02%
Time for initializing arrays with start and end segment values = 0.004%
Time for finding the best subsequence = 20.71%
Runtime of reading 4.51
Total Runtime of finding the best subsequence 10.15


count of threads = 30

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bfs_local = 94.907532, position = 333301
bfs_local = 61.299992, position = 33330
bfs_local = 43.381245, position = 33332
bfs_local = 36.607162, position = 33334
bfs_local = 17.263212, position = 666600
bfs_local = 15.850533, position = 899974
bfs_local = 15.380412, position = 566833
bfs_local = 14.905022, position = 566832
bfs_local = 14.677940, position = 566831
bfs_local = 14.566250, position = 285
bfs_local = 14.029982, position = 286
bfs_local = 13.420880, position = 800612
bfs_local = 13.254978, position = 800613
bfs_local = 12.128454, position = 734758
bfs_local = 11.789434, position = 968401
bfs_local = 11.433337, position = 769734
bfs_local = 11.384415, position = 769735
bfs_local = 10.791825, position = 769736
bfs_local = 10.542933, position = 340701
bfs_local = 9.721350, position = 874194
bfs_local = 8.066576, position = 148876
bsf = 2.84017
position = 148876
DTW Calculation = 7.06%
Count of DTW = 70628
Time for calculation lower bounding = 81.22%
Time for initializing arrays with start and end segment values = 0.013%
Time for finding the best subsequence = 18.48%
Runtime of reading 4.62
Total Runtime of finding the best subsequence 3.32


count of threads = 60

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bfs_local = 110.945641, position = 333304
bfs_local = 82.241547, position = 249975
bfs_local = 17.263212, position = 666600
bfs_local = 14.329779, position = 516638
bfs_local = 14.029982, position = 286
bfs_local = 12.173938, position = 850358
bfs_local = 12.012808, position = 850361
bfs_local = 11.817424, position = 850360
bfs_local = 11.311694, position = 750551
bfs_local = 11.269180, position = 418682
bfs_local = 11.080192, position = 418685
bfs_local = 10.791825, position = 769736
bfs_local = 10.755066, position = 286688
bfs_local = 10.654775, position = 286686
bfs_local = 10.093108, position = 286687
bfs_local = 9.837740, position = 789813
bfs_local = 9.789495, position = 874191
bfs_local = 9.721350, position = 874194
bfs_local = 9.471311, position = 295833
bfs_local = 9.092925, position = 295830
bfs_local = 8.929319, position = 295831
bfs_local = 8.066576, position = 148876
bsf = 2.84017
position = 148876
DTW Calculation = 7.90%
Count of DTW = 78990
Time for calculation lower bounding = 76.85%
Time for initializing arrays with start and end segment values = 0.048%
Time for finding the best subsequence = 22.81%
Runtime of reading 4.58
Total Runtime of finding the best subsequence 1.82


count of threads = 120

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bfs_local = 79.681023, position = 499982
bfs_local = 65.682281, position = 8334
bfs_local = 54.700623, position = 8337
bfs_local = 47.430328, position = 924967
bfs_local = 47.205021, position = 508315
bfs_local = 19.389124, position = 516648
bfs_local = 19.365086, position = 516647
bfs_local = 17.213741, position = 899979
bfs_local = 15.850533, position = 899974
bfs_local = 15.703591, position = 516750
bfs_local = 11.969646, position = 925121
bfs_local = 11.555554, position = 525207
bfs_local = 11.545627, position = 525208
bfs_local = 11.311694, position = 750551
bfs_local = 11.241044, position = 92623
bfs_local = 11.160996, position = 259692
bfs_local = 10.973717, position = 259693
bfs_local = 10.818749, position = 259691
bfs_local = 10.791825, position = 769736
bfs_local = 10.755066, position = 286688
bfs_local = 10.654775, position = 286686
bfs_local = 10.093108, position = 286687
bfs_local = 9.092925, position = 295830
bfs_local = 8.929319, position = 295831
bfs_local = 8.066576, position = 148876
bsf = 2.84017
position = 148876
DTW Calculation = 7.90%
Count of DTW = 79021
Time for calculation lower bounding = 69.43%
Time for initializing arrays with start and end segment values = 0.027%
Time for finding the best subsequence = 30.27%
Runtime of reading 4.53
Total Runtime of finding the best subsequence 1.38


count of threads = 180

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bfs_local = 17.615097, position = 666601
bfs_local = 17.263212, position = 666600
bfs_local = 11.724231, position = 811033
bfs_local = 11.569302, position = 756002
bfs_local = 11.311694, position = 750551
bfs_local = 10.206429, position = 789814
bfs_local = 10.153045, position = 789809
bfs_local = 9.837740, position = 789813
bfs_local = 9.471311, position = 295833
bfs_local = 8.929319, position = 295831
bfs_local = 8.066576, position = 148876
bsf = 2.84017
position = 148876
DTW Calculation = 7.05%
Count of DTW = 70532
Time for calculation lower bounding = 67.70%
Time for initializing arrays with start and end segment values = 0.178%
Time for finding the best subsequence = 31.88%
Runtime of reading 4.58
Total Runtime of finding the best subsequence 1.32


count of threads = 240

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bfs_local = 119.253242, position = 208351
bfs_local = 19.282528, position = 145845
bfs_local = 19.106312, position = 429205
bfs_local = 17.702467, position = 925091
bfs_local = 17.349236, position = 570903
bfs_local = 16.323790, position = 829243
bfs_local = 16.272560, position = 141695
bfs_local = 15.805814, position = 754241
bfs_local = 15.541553, position = 754239
bfs_local = 12.189212, position = 925123
bfs_local = 12.041904, position = 995960
bfs_local = 11.969646, position = 925121
bfs_local = 11.221785, position = 645991
bfs_local = 10.549894, position = 645993
bfs_local = 10.523934, position = 645992
bfs_local = 9.805469, position = 629634
bfs_local = 8.066576, position = 148876
bsf = 2.84017
position = 148876
DTW Calculation = 7.41%
Count of DTW = 74120
Time for calculation lower bounding = 66.89%
Time for initializing arrays with start and end segment values = 0.444%
Time for finding the best subsequence = 32.44%
Runtime of reading 4.64
Total Runtime of finding the best subsequence 1.36


number of a  series of experiments = 1
count of threads = 1

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bfs_local = 217.902512, position = 49
bfs_local = 195.642578, position = 68
bfs_local = 193.637802, position = 69
bfs_local = 192.775009, position = 71
bfs_local = 189.489044, position = 72
bfs_local = 187.095184, position = 73
bfs_local = 184.339813, position = 74
bfs_local = 181.737457, position = 75
bfs_local = 180.180328, position = 76
bfs_local = 173.804504, position = 111
bfs_local = 167.831223, position = 112
bfs_local = 164.025787, position = 113
bfs_local = 156.747375, position = 114
bfs_local = 147.730194, position = 115
bfs_local = 141.820160, position = 116
bfs_local = 125.221336, position = 117
bfs_local = 105.388420, position = 118
bfs_local = 103.470390, position = 119
bfs_local = 102.000336, position = 120
bfs_local = 100.440971, position = 121
bfs_local = 96.495041, position = 122
bfs_local = 93.008415, position = 123
bfs_local = 90.095947, position = 124
bfs_local = 87.125870, position = 125
bfs_local = 81.895576, position = 126
bfs_local = 70.764061, position = 127
bfs_local = 68.156570, position = 270
bfs_local = 61.426308, position = 271
bfs_local = 57.186081, position = 272
bfs_local = 52.750832, position = 273
bfs_local = 52.364506, position = 274
bfs_local = 50.733974, position = 275
bfs_local = 38.286472, position = 276
bfs_local = 35.146641, position = 277
bfs_local = 27.123358, position = 279
bfs_local = 22.783644, position = 280
bfs_local = 21.594175, position = 281
bfs_local = 18.515766, position = 282
bfs_local = 16.781168, position = 283
bfs_local = 15.399029, position = 284
bfs_local = 14.566250, position = 285
bfs_local = 14.029982, position = 286
bfs_local = 13.455287, position = 12317
bfs_local = 13.343568, position = 12318
bfs_local = 11.354399, position = 25846
bfs_local = 11.098724, position = 43099
bfs_local = 10.341489, position = 131638
bfs_local = 10.241185, position = 131639
bfs_local = 10.189466, position = 148874
bfs_local = 9.236251, position = 148875
bfs_local = 8.066576, position = 148876
bsf = 2.84017
position = 148876
DTW Calculation = 6.05%
Count of DTW = 60446
Time for calculation lower bounding = 53.92%
Time for initializing arrays with start and end segment values = 0.000%
Time for finding the best subsequence = 45.91%
Runtime of reading 4.62
Total Runtime of finding the best subsequence 148.26


count of threads = 10

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bfs_local = 193.239273, position = 499940
bfs_local = 163.320374, position = 499941
bfs_local = 129.990616, position = 499943
bfs_local = 111.703072, position = 499944
bfs_local = 107.732956, position = 99988
bfs_local = 71.309280, position = 199980
bfs_local = 32.623161, position = 899892
bfs_local = 31.422348, position = 400001
bfs_local = 31.057961, position = 400002
bfs_local = 21.528841, position = 899972
bfs_local = 19.737476, position = 899973
bfs_local = 15.850533, position = 899974
bfs_local = 15.261052, position = 289
bfs_local = 14.029982, position = 286
bfs_local = 13.420880, position = 800612
bfs_local = 13.254978, position = 800613
bfs_local = 13.166487, position = 301540
bfs_local = 13.009433, position = 301544
bfs_local = 12.667230, position = 301539
bfs_local = 12.474792, position = 307199
bfs_local = 11.560240, position = 307200
bfs_local = 11.525405, position = 212738
bfs_local = 11.311034, position = 418683
bfs_local = 11.080192, position = 418685
bfs_local = 10.098009, position = 629635
bfs_local = 9.805469, position = 629634
bfs_local = 8.066576, position = 148876
bsf = 2.84017
position = 148876
DTW Calculation = 7.40%
Count of DTW = 73968
Time for calculation lower bounding = 78.94%
Time for initializing arrays with start and end segment values = 0.004%
Time for finding the best subsequence = 20.79%
Runtime of reading 4.64
Total Runtime of finding the best subsequence 10.16


count of threads = 30

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bfs_local = 94.907532, position = 333301
bfs_local = 61.299992, position = 33330
bfs_local = 27.928719, position = 666604
bfs_local = 17.615097, position = 666601
bfs_local = 17.263212, position = 666600
bfs_local = 15.850533, position = 899974
bfs_local = 14.677940, position = 566831
bfs_local = 14.566250, position = 285
bfs_local = 14.029982, position = 286
bfs_local = 13.420880, position = 800612
bfs_local = 13.254978, position = 800613
bfs_local = 12.128454, position = 734758
bfs_local = 11.789434, position = 968401
bfs_local = 11.433337, position = 769734
bfs_local = 11.384415, position = 769735
bfs_local = 10.791825, position = 769736
bfs_local = 10.542933, position = 340701
bfs_local = 9.721350, position = 874194
bfs_local = 8.066576, position = 148876
bsf = 2.84017
position = 148876
DTW Calculation = 7.06%
Count of DTW = 70628
Time for calculation lower bounding = 80.64%
Time for initializing arrays with start and end segment values = 0.013%
Time for finding the best subsequence = 19.05%
Runtime of reading 4.61
Total Runtime of finding the best subsequence 3.34


count of threads = 60

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bfs_local = 45.982227, position = 416625
bfs_local = 19.710093, position = 666602
bfs_local = 17.615097, position = 666601
bfs_local = 17.263212, position = 666600
bfs_local = 14.329779, position = 516638
bfs_local = 14.029982, position = 286
bfs_local = 12.173938, position = 850358
bfs_local = 12.012808, position = 850361
bfs_local = 11.817424, position = 850360
bfs_local = 11.311694, position = 750551
bfs_local = 11.269180, position = 418682
bfs_local = 11.080192, position = 418685
bfs_local = 10.791825, position = 769736
bfs_local = 10.755066, position = 286688
bfs_local = 10.654775, position = 286686
bfs_local = 10.093108, position = 286687
bfs_local = 9.837740, position = 789813
bfs_local = 9.789495, position = 874191
bfs_local = 9.721350, position = 874194
bfs_local = 9.471311, position = 295833
bfs_local = 9.092925, position = 295830
bfs_local = 8.929319, position = 295831
bfs_local = 8.066576, position = 148876
bsf = 2.84017
position = 148876
DTW Calculation = 7.90%
Count of DTW = 78990
Time for calculation lower bounding = 76.87%
Time for initializing arrays with start and end segment values = 0.030%
Time for finding the best subsequence = 22.81%
Runtime of reading 4.51
Total Runtime of finding the best subsequence 1.84


count of threads = 120

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bfs_local = 79.681023, position = 499982
bfs_local = 71.957878, position = 8333
bfs_local = 65.682281, position = 8334
bfs_local = 59.339535, position = 924966
bfs_local = 55.530903, position = 8335
bfs_local = 47.430328, position = 924967
bfs_local = 47.205021, position = 508315
bfs_local = 45.912781, position = 491647
bfs_local = 19.389124, position = 516648
bfs_local = 19.365086, position = 516647
bfs_local = 17.213741, position = 899979
bfs_local = 15.850533, position = 899974
bfs_local = 15.703591, position = 516750
bfs_local = 11.969646, position = 925121
bfs_local = 11.555554, position = 525207
bfs_local = 11.545627, position = 525208
bfs_local = 11.311694, position = 750551
bfs_local = 11.241044, position = 92623
bfs_local = 11.160996, position = 259692
bfs_local = 10.973717, position = 259693
bfs_local = 10.818749, position = 259691
bfs_local = 10.791825, position = 769736
bfs_local = 10.755066, position = 286688
bfs_local = 10.654775, position = 286686
bfs_local = 10.093108, position = 286687
bfs_local = 9.103625, position = 295832
bfs_local = 9.092925, position = 295830
bfs_local = 8.929319, position = 295831
bfs_local = 8.066576, position = 148876
bsf = 2.84017
position = 148876
DTW Calculation = 7.90%
Count of DTW = 79021
Time for calculation lower bounding = 70.45%
Time for initializing arrays with start and end segment values = 0.013%
Time for finding the best subsequence = 29.27%
Runtime of reading 4.52
Total Runtime of finding the best subsequence 1.42


count of threads = 180

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bfs_local = 66.437256, position = 611051
bfs_local = 45.434364, position = 499950
bfs_local = 37.425537, position = 583277
bfs_local = 17.615097, position = 666601
bfs_local = 17.263212, position = 666600
bfs_local = 11.724231, position = 811033
bfs_local = 11.569302, position = 756002
bfs_local = 11.311694, position = 750551
bfs_local = 11.034246, position = 789807
bfs_local = 10.208036, position = 789811
bfs_local = 10.206429, position = 789814
bfs_local = 10.153045, position = 789809
bfs_local = 9.837740, position = 789813
bfs_local = 9.471311, position = 295833
bfs_local = 8.929319, position = 295831
bfs_local = 8.066576, position = 148876
bsf = 2.84017
position = 148876
DTW Calculation = 7.05%
Count of DTW = 70532
Time for calculation lower bounding = 67.56%
Time for initializing arrays with start and end segment values = 0.252%
Time for finding the best subsequence = 31.95%
Runtime of reading 4.51
Total Runtime of finding the best subsequence 1.33


count of threads = 240

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bfs_local = 119.253242, position = 208351
bfs_local = 119.082199, position = 625051
bfs_local = 108.410652, position = 625050
bfs_local = 35.219849, position = 416704
bfs_local = 19.106312, position = 429205
bfs_local = 17.702467, position = 925091
bfs_local = 16.323790, position = 829243
bfs_local = 16.272560, position = 141695
bfs_local = 15.805814, position = 754241
bfs_local = 15.541553, position = 754239
bfs_local = 12.189212, position = 925123
bfs_local = 12.041904, position = 995960
bfs_local = 11.969646, position = 925121
bfs_local = 10.549894, position = 645993
bfs_local = 10.523934, position = 645992
bfs_local = 9.805469, position = 629634
bfs_local = 9.236251, position = 148875
bfs_local = 8.066576, position = 148876
bsf = 2.84017
position = 148876
DTW Calculation = 7.41%
Count of DTW = 74120
Time for calculation lower bounding = 66.49%
Time for initializing arrays with start and end segment values = 0.259%
Time for finding the best subsequence = 33.02%
Runtime of reading 4.57
Total Runtime of finding the best subsequence 1.35


number of a  series of experiments = 2
count of threads = 1

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bfs_local = 217.902512, position = 49
bfs_local = 195.642578, position = 68
bfs_local = 193.637802, position = 69
bfs_local = 192.775009, position = 71
bfs_local = 189.489044, position = 72
bfs_local = 187.095184, position = 73
bfs_local = 184.339813, position = 74
bfs_local = 181.737457, position = 75
bfs_local = 180.180328, position = 76
bfs_local = 173.804504, position = 111
bfs_local = 167.831223, position = 112
bfs_local = 164.025787, position = 113
bfs_local = 156.747375, position = 114
bfs_local = 147.730194, position = 115
bfs_local = 141.820160, position = 116
bfs_local = 125.221336, position = 117
bfs_local = 105.388420, position = 118
bfs_local = 103.470390, position = 119
bfs_local = 102.000336, position = 120
bfs_local = 100.440971, position = 121
bfs_local = 96.495041, position = 122
bfs_local = 93.008415, position = 123
bfs_local = 90.095947, position = 124
bfs_local = 87.125870, position = 125
bfs_local = 81.895576, position = 126
bfs_local = 70.764061, position = 127
bfs_local = 68.156570, position = 270
bfs_local = 61.426308, position = 271
bfs_local = 57.186081, position = 272
bfs_local = 52.750832, position = 273
bfs_local = 52.364506, position = 274
bfs_local = 50.733974, position = 275
bfs_local = 38.286472, position = 276
bfs_local = 35.146641, position = 277
bfs_local = 27.123358, position = 279
bfs_local = 22.783644, position = 280
bfs_local = 21.594175, position = 281
bfs_local = 18.515766, position = 282
bfs_local = 16.781168, position = 283
bfs_local = 15.399029, position = 284
bfs_local = 14.566250, position = 285
bfs_local = 14.029982, position = 286
bfs_local = 13.455287, position = 12317
bfs_local = 13.343568, position = 12318
bfs_local = 11.354399, position = 25846
bfs_local = 11.098724, position = 43099
bfs_local = 10.341489, position = 131638
bfs_local = 10.241185, position = 131639
bfs_local = 10.189466, position = 148874
bfs_local = 9.236251, position = 148875
bfs_local = 8.066576, position = 148876
bsf = 2.84017
position = 148876
DTW Calculation = 6.05%
Count of DTW = 60446
Time for calculation lower bounding = 54.01%
Time for initializing arrays with start and end segment values = 0.000%
Time for finding the best subsequence = 45.82%
Runtime of reading 4.51
Total Runtime of finding the best subsequence 148.08


count of threads = 10

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bfs_local = 193.239273, position = 499940
bfs_local = 163.320374, position = 499941
bfs_local = 155.525742, position = 499942
bfs_local = 129.990616, position = 499943
bfs_local = 107.732956, position = 99988
bfs_local = 71.309280, position = 199980
bfs_local = 32.623161, position = 899892
bfs_local = 31.422348, position = 400001
bfs_local = 31.057961, position = 400002
bfs_local = 21.528841, position = 899972
bfs_local = 19.737476, position = 899973
bfs_local = 15.850533, position = 899974
bfs_local = 14.029982, position = 286
bfs_local = 13.420880, position = 800612
bfs_local = 13.254978, position = 800613
bfs_local = 13.166487, position = 301540
bfs_local = 13.009433, position = 301544
bfs_local = 12.667230, position = 301539
bfs_local = 12.474792, position = 307199
bfs_local = 11.560240, position = 307200
bfs_local = 11.525405, position = 212738
bfs_local = 11.311034, position = 418683
bfs_local = 11.080192, position = 418685
bfs_local = 10.098009, position = 629635
bfs_local = 9.805469, position = 629634
bfs_local = 8.066576, position = 148876
bsf = 2.84017
position = 148876
DTW Calculation = 7.40%
Count of DTW = 73968
Time for calculation lower bounding = 79.02%
Time for initializing arrays with start and end segment values = 0.001%
Time for finding the best subsequence = 20.72%
Runtime of reading 4.61
Total Runtime of finding the best subsequence 10.15


count of threads = 30

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bfs_local = 19.710093, position = 666602
bfs_local = 17.263212, position = 666600
bfs_local = 15.850533, position = 899974
bfs_local = 14.677940, position = 566831
bfs_local = 14.566250, position = 285
bfs_local = 14.029982, position = 286
bfs_local = 13.420880, position = 800612
bfs_local = 13.254978, position = 800613
bfs_local = 12.128454, position = 734758
bfs_local = 11.789434, position = 968401
bfs_local = 11.433337, position = 769734
bfs_local = 11.384415, position = 769735
bfs_local = 10.791825, position = 769736
bfs_local = 10.542933, position = 340701
bfs_local = 9.721350, position = 874194
bfs_local = 8.066576, position = 148876
bsf = 2.84017
position = 148876
DTW Calculation = 7.06%
Count of DTW = 70628
Time for calculation lower bounding = 81.23%
Time for initializing arrays with start and end segment values = 0.013%
Time for finding the best subsequence = 18.46%
Runtime of reading 4.52
Total Runtime of finding the best subsequence 3.32


count of threads = 60

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bfs_local = 45.121552, position = 416629
bfs_local = 19.710093, position = 666602
bfs_local = 17.263212, position = 666600
bfs_local = 14.329779, position = 516638
bfs_local = 14.029982, position = 286
bfs_local = 12.012808, position = 850361
bfs_local = 11.817424, position = 850360
bfs_local = 11.311694, position = 750551
bfs_local = 11.269180, position = 418682
bfs_local = 11.080192, position = 418685
bfs_local = 10.791825, position = 769736
bfs_local = 10.755066, position = 286688
bfs_local = 10.654775, position = 286686
bfs_local = 10.093108, position = 286687
bfs_local = 9.837740, position = 789813
bfs_local = 9.789495, position = 874191
bfs_local = 9.721350, position = 874194
bfs_local = 9.471311, position = 295833
bfs_local = 9.092925, position = 295830
bfs_local = 8.929319, position = 295831
bfs_local = 8.066576, position = 148876
bsf = 2.84017
position = 148876
DTW Calculation = 7.90%
Count of DTW = 78990
Time for calculation lower bounding = 76.65%
Time for initializing arrays with start and end segment values = 0.054%
Time for finding the best subsequence = 22.99%
Runtime of reading 4.62
Total Runtime of finding the best subsequence 1.81


count of threads = 120

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bfs_local = 212.670990, position = 458317
bfs_local = 65.682281, position = 8334
bfs_local = 55.530903, position = 8335
bfs_local = 54.700623, position = 8337
bfs_local = 47.430328, position = 924967
bfs_local = 45.912781, position = 491647
bfs_local = 45.784309, position = 941631
bfs_local = 43.530582, position = 941632
bfs_local = 41.789074, position = 941633
bfs_local = 37.990715, position = 783305
bfs_local = 35.473003, position = 783306
bfs_local = 30.817760, position = 124997
bfs_local = 21.202120, position = 41668
bfs_local = 21.096140, position = 41669
bfs_local = 19.389124, position = 516648
bfs_local = 19.365086, position = 516647
bfs_local = 17.213741, position = 899979
bfs_local = 15.850533, position = 899974
bfs_local = 15.703591, position = 516750
bfs_local = 11.969646, position = 925121
bfs_local = 11.555554, position = 525207
bfs_local = 11.545627, position = 525208
bfs_local = 11.311694, position = 750551
bfs_local = 11.241044, position = 92623
bfs_local = 11.160996, position = 259692
bfs_local = 10.973717, position = 259693
bfs_local = 10.818749, position = 259691
bfs_local = 10.791825, position = 769736
bfs_local = 10.755066, position = 286688
bfs_local = 10.654775, position = 286686
bfs_local = 10.093108, position = 286687
bfs_local = 9.092925, position = 295830
bfs_local = 8.929319, position = 295831
bfs_local = 8.066576, position = 148876
bsf = 2.84017
position = 148876
DTW Calculation = 7.90%
Count of DTW = 79021
Time for calculation lower bounding = 72.11%
Time for initializing arrays with start and end segment values = 0.022%
Time for finding the best subsequence = 27.59%
Runtime of reading 4.52
Total Runtime of finding the best subsequence 1.34


count of threads = 180

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bfs_local = 59.304173, position = 638826
bfs_local = 45.434364, position = 499950
bfs_local = 38.359890, position = 583278
bfs_local = 19.710093, position = 666602
bfs_local = 17.263212, position = 666600
bfs_local = 12.403892, position = 811031
bfs_local = 12.196528, position = 811034
bfs_local = 11.724231, position = 811033
bfs_local = 11.569302, position = 756002
bfs_local = 11.311694, position = 750551
bfs_local = 11.034246, position = 789807
bfs_local = 10.208036, position = 789811
bfs_local = 10.206429, position = 789814
bfs_local = 10.153045, position = 789809
bfs_local = 9.837740, position = 789813
bfs_local = 9.471311, position = 295833
bfs_local = 8.929319, position = 295831
bfs_local = 8.066576, position = 148876
bsf = 2.84017
position = 148876
DTW Calculation = 7.05%
Count of DTW = 70532
Time for calculation lower bounding = 67.67%
Time for initializing arrays with start and end segment values = 0.159%
Time for finding the best subsequence = 31.93%
Runtime of reading 4.52
Total Runtime of finding the best subsequence 1.32


count of threads = 240

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bfs_local = 93.127586, position = 395869
bfs_local = 54.354744, position = 666722
bfs_local = 30.275656, position = 41674
bfs_local = 27.627979, position = 187515
bfs_local = 21.824396, position = 187517
bfs_local = 19.799891, position = 145849
bfs_local = 19.603134, position = 145847
bfs_local = 19.365982, position = 145846
bfs_local = 19.282528, position = 145845
bfs_local = 19.106312, position = 429205
bfs_local = 17.349236, position = 570903
bfs_local = 16.323790, position = 829243
bfs_local = 16.272560, position = 141695
bfs_local = 15.805814, position = 754241
bfs_local = 15.541553, position = 754239
bfs_local = 12.465285, position = 925122
bfs_local = 12.189212, position = 925123
bfs_local = 12.041904, position = 995960
bfs_local = 11.969646, position = 925121
bfs_local = 10.549894, position = 645993
bfs_local = 10.523934, position = 645992
bfs_local = 9.805469, position = 629634
bfs_local = 9.236251, position = 148875
bfs_local = 8.066576, position = 148876
bsf = 2.84017
position = 148876
DTW Calculation = 7.41%
Count of DTW = 74120
Time for calculation lower bounding = 67.16%
Time for initializing arrays with start and end segment values = 0.112%
Time for finding the best subsequence = 32.49%
Runtime of reading 4.51
Total Runtime of finding the best subsequence 1.33


