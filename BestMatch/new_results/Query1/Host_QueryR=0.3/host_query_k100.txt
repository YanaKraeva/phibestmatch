number of a  series of experiments = 0
count of threads = 1

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bfs_local = 217.902573, position = 49
bfs_local = 195.642609, position = 68
bfs_local = 193.637848, position = 69
bfs_local = 192.775055, position = 71
bfs_local = 189.489090, position = 72
bfs_local = 187.095245, position = 73
bfs_local = 184.339859, position = 74
bfs_local = 181.737534, position = 75
bfs_local = 180.180344, position = 76
bfs_local = 173.804565, position = 111
bfs_local = 167.831268, position = 112
bfs_local = 164.025848, position = 113
bfs_local = 156.747406, position = 114
bfs_local = 147.730225, position = 115
bfs_local = 141.820221, position = 116
bfs_local = 125.221489, position = 117
bfs_local = 105.388641, position = 118
bfs_local = 103.470436, position = 119
bfs_local = 102.000336, position = 120
bfs_local = 100.440979, position = 121
bfs_local = 96.495033, position = 122
bfs_local = 93.008453, position = 123
bfs_local = 90.095970, position = 124
bfs_local = 87.125893, position = 125
bfs_local = 81.895569, position = 126
bfs_local = 70.764099, position = 127
bfs_local = 68.156723, position = 270
bfs_local = 61.426331, position = 271
bfs_local = 57.186100, position = 272
bfs_local = 52.750870, position = 273
bfs_local = 52.364532, position = 274
bfs_local = 50.734005, position = 275
bfs_local = 38.286484, position = 276
bfs_local = 35.146648, position = 277
bfs_local = 27.123549, position = 279
bfs_local = 22.783813, position = 280
bfs_local = 21.594234, position = 281
bfs_local = 18.515770, position = 282
bfs_local = 16.781172, position = 283
bfs_local = 15.399072, position = 284
bfs_local = 14.566254, position = 285
bfs_local = 14.029958, position = 286
bfs_local = 13.455294, position = 12317
bfs_local = 13.343624, position = 12318
bfs_local = 11.353244, position = 25846
bfs_local = 11.098734, position = 43099
bfs_local = 10.341493, position = 131638
bfs_local = 10.244247, position = 131639
bfs_local = 10.189466, position = 148874
bfs_local = 9.236251, position = 148875
bfs_local = 8.076555, position = 148876
bsf = 2.84193
position = 148876
DTW Calculation = 6.06%
Count of DTW = 60599
Time for calculation lower bounding = 83.07%
Time for initializing arrays with start and end segment values = 0.000%
Time for finding the best subsequence = 16.64%
Runtime of reading 0.33
Total Runtime of finding the best subsequence 9.41


number of a  series of experiments = 0
count of threads = 3

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bfs_local = 134.735672, position = 666648
bfs_local = 66.408272, position = 333291
bfs_local = 43.646950, position = 666615
bfs_local = 31.358585, position = 666582
bfs_local = 30.565271, position = 666583
bfs_local = 23.162945, position = 666584
bfs_local = 21.950909, position = 666589
bfs_local = 21.757963, position = 666590
bfs_local = 20.977331, position = 666591
bfs_local = 20.798550, position = 666592
bfs_local = 20.524908, position = 666593
bfs_local = 19.440695, position = 666594
bfs_local = 18.700260, position = 666595
bfs_local = 18.059488, position = 666596
bfs_local = 16.914772, position = 666599
bfs_local = 16.781172, position = 283
bfs_local = 15.399072, position = 284
bfs_local = 14.566254, position = 285
bfs_local = 14.029958, position = 286
bfs_local = 13.050027, position = 335233
bfs_local = 11.187132, position = 340695
bfs_local = 10.858139, position = 340696
bfs_local = 10.625361, position = 340701
bfs_local = 10.232833, position = 756562
bfs_local = 10.005934, position = 789808
bfs_local = 9.701295, position = 789812
bfs_local = 9.236251, position = 148875
bfs_local = 8.076555, position = 148876
bsf = 2.84193
position = 148876
DTW Calculation = 6.83%
Count of DTW = 68276
Time for calculation lower bounding = 85.75%
Time for initializing arrays with start and end segment values = 0.000%
Time for finding the best subsequence = 13.55%
Runtime of reading 0.33
Total Runtime of finding the best subsequence 3.09


number of a  series of experiments = 0
count of threads = 6

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bfs_local = 126.953560, position = 666650
bfs_local = 72.925850, position = 333375
bfs_local = 17.171980, position = 666600
bfs_local = 16.914772, position = 666599
bfs_local = 16.480627, position = 295
bfs_local = 15.399072, position = 284
bfs_local = 14.566254, position = 285
bfs_local = 14.029958, position = 286
bfs_local = 13.550267, position = 168473
bfs_local = 12.763593, position = 168474
bfs_local = 12.408418, position = 168475
bfs_local = 11.187132, position = 340695
bfs_local = 10.858139, position = 340696
bfs_local = 10.625361, position = 340701
bfs_local = 10.538024, position = 847007
bfs_local = 9.738815, position = 874191
bfs_local = 9.735808, position = 874194
bfs_local = 9.701295, position = 789812
bfs_local = 9.158608, position = 295832
bfs_local = 9.092924, position = 295830
bfs_local = 8.985011, position = 295831
bfs_local = 8.076555, position = 148876
bsf = 2.84193
position = 148876
DTW Calculation = 7.67%
Count of DTW = 76715
Time for calculation lower bounding = 86.07%
Time for initializing arrays with start and end segment values = 0.000%
Time for finding the best subsequence = 12.79%
Runtime of reading 0.33
Total Runtime of finding the best subsequence 1.62


number of a  series of experiments = 0
count of threads = 12

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bfs_local = 104.031403, position = 666675
bfs_local = 103.429581, position = 83323
bfs_local = 91.052063, position = 333300
bfs_local = 17.171980, position = 666600
bfs_local = 16.914772, position = 666599
bfs_local = 15.261059, position = 289
bfs_local = 14.469719, position = 292
bfs_local = 14.029958, position = 286
bfs_local = 13.362963, position = 750548
bfs_local = 13.138631, position = 750549
bfs_local = 13.108140, position = 750550
bfs_local = 11.311702, position = 750551
bfs_local = 11.223123, position = 418682
bfs_local = 11.080197, position = 418685
bfs_local = 10.232833, position = 756562
bfs_local = 10.107504, position = 286687
bfs_local = 10.005934, position = 789808
bfs_local = 9.701295, position = 789812
bfs_local = 9.092924, position = 295830
bfs_local = 8.985011, position = 295831
bfs_local = 8.076555, position = 148876
bsf = 2.84193
position = 148876
DTW Calculation = 7.47%
Count of DTW = 74716
Time for calculation lower bounding = 87.27%
Time for initializing arrays with start and end segment values = 0.001%
Time for finding the best subsequence = 11.12%
Runtime of reading 0.33
Total Runtime of finding the best subsequence 1.21


number of a  series of experiments = 0
count of threads = 18

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bfs_local = 74.037460, position = 333377
bfs_local = 67.568893, position = 777763
bfs_local = 46.468178, position = 444475
bfs_local = 27.919195, position = 666604
bfs_local = 24.588051, position = 222257
bfs_local = 20.524908, position = 666593
bfs_local = 16.914772, position = 666599
bfs_local = 15.445699, position = 222291
bfs_local = 15.337241, position = 278068
bfs_local = 13.857821, position = 278069
bfs_local = 13.545185, position = 278070
bfs_local = 13.109679, position = 890566
bfs_local = 12.614532, position = 890567
bfs_local = 12.438791, position = 279451
bfs_local = 11.600005, position = 279452
bfs_local = 11.563806, position = 447997
bfs_local = 11.529969, position = 284066
bfs_local = 11.187132, position = 340695
bfs_local = 10.858139, position = 340696
bfs_local = 10.625361, position = 340701
bfs_local = 10.107504, position = 286687
bfs_local = 10.005934, position = 789808
bfs_local = 9.701295, position = 789812
bfs_local = 9.092924, position = 295830
bfs_local = 8.985011, position = 295831
bfs_local = 8.076555, position = 148876
bsf = 2.84193
position = 148876
DTW Calculation = 7.22%
Count of DTW = 72239
Time for calculation lower bounding = 85.70%
Time for initializing arrays with start and end segment values = 0.001%
Time for finding the best subsequence = 12.25%
Runtime of reading 0.33
Total Runtime of finding the best subsequence 0.86


number of a  series of experiments = 0
count of threads = 24

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bfs_local = 128.666138, position = 666658
bfs_local = 24.568333, position = 41662
bfs_local = 19.085686, position = 874989
bfs_local = 18.700260, position = 666595
bfs_local = 18.059488, position = 666596
bfs_local = 17.171980, position = 666600
bfs_local = 16.914772, position = 666599
bfs_local = 16.356503, position = 41960
bfs_local = 14.029958, position = 286
bfs_local = 13.362963, position = 750548
bfs_local = 13.138631, position = 750549
bfs_local = 13.108140, position = 750550
bfs_local = 11.311702, position = 750551
bfs_local = 11.098734, position = 43099
bfs_local = 11.080197, position = 418685
bfs_local = 11.058647, position = 295828
bfs_local = 10.299012, position = 295829
bfs_local = 9.092924, position = 295830
bfs_local = 8.985011, position = 295831
bfs_local = 8.076555, position = 148876
bsf = 2.84193
position = 148876
DTW Calculation = 6.70%
Count of DTW = 66989
Time for calculation lower bounding = 86.45%
Time for initializing arrays with start and end segment values = 0.182%
Time for finding the best subsequence = 11.49%
Runtime of reading 0.33
Total Runtime of finding the best subsequence 0.99


number of a  series of experiments = 1
count of threads = 1

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bfs_local = 217.902573, position = 49
bfs_local = 195.642609, position = 68
bfs_local = 193.637848, position = 69
bfs_local = 192.775055, position = 71
bfs_local = 189.489090, position = 72
bfs_local = 187.095245, position = 73
bfs_local = 184.339859, position = 74
bfs_local = 181.737534, position = 75
bfs_local = 180.180344, position = 76
bfs_local = 173.804565, position = 111
bfs_local = 167.831268, position = 112
bfs_local = 164.025848, position = 113
bfs_local = 156.747406, position = 114
bfs_local = 147.730225, position = 115
bfs_local = 141.820221, position = 116
bfs_local = 125.221489, position = 117
bfs_local = 105.388641, position = 118
bfs_local = 103.470436, position = 119
bfs_local = 102.000336, position = 120
bfs_local = 100.440979, position = 121
bfs_local = 96.495033, position = 122
bfs_local = 93.008453, position = 123
bfs_local = 90.095970, position = 124
bfs_local = 87.125893, position = 125
bfs_local = 81.895569, position = 126
bfs_local = 70.764099, position = 127
bfs_local = 68.156723, position = 270
bfs_local = 61.426331, position = 271
bfs_local = 57.186100, position = 272
bfs_local = 52.750870, position = 273
bfs_local = 52.364532, position = 274
bfs_local = 50.734005, position = 275
bfs_local = 38.286484, position = 276
bfs_local = 35.146648, position = 277
bfs_local = 27.123549, position = 279
bfs_local = 22.783813, position = 280
bfs_local = 21.594234, position = 281
bfs_local = 18.515770, position = 282
bfs_local = 16.781172, position = 283
bfs_local = 15.399072, position = 284
bfs_local = 14.566254, position = 285
bfs_local = 14.029958, position = 286
bfs_local = 13.455294, position = 12317
bfs_local = 13.343624, position = 12318
bfs_local = 11.353244, position = 25846
bfs_local = 11.098734, position = 43099
bfs_local = 10.341493, position = 131638
bfs_local = 10.244247, position = 131639
bfs_local = 10.189466, position = 148874
bfs_local = 9.236251, position = 148875
bfs_local = 8.076555, position = 148876
bsf = 2.84193
position = 148876
DTW Calculation = 6.06%
Count of DTW = 60599
Time for calculation lower bounding = 82.96%
Time for initializing arrays with start and end segment values = 0.000%
Time for finding the best subsequence = 16.74%
Runtime of reading 0.33
Total Runtime of finding the best subsequence 9.42


number of a  series of experiments = 1
count of threads = 3

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bfs_local = 202.936249, position = 333324
bfs_local = 134.735672, position = 666648
bfs_local = 66.408272, position = 333291
bfs_local = 43.646950, position = 666615
bfs_local = 31.358585, position = 666582
bfs_local = 30.565271, position = 666583
bfs_local = 23.162945, position = 666584
bfs_local = 21.950909, position = 666589
bfs_local = 21.757963, position = 666590
bfs_local = 20.977331, position = 666591
bfs_local = 20.798550, position = 666592
bfs_local = 20.524908, position = 666593
bfs_local = 19.440695, position = 666594
bfs_local = 18.700260, position = 666595
bfs_local = 18.059488, position = 666596
bfs_local = 16.914772, position = 666599
bfs_local = 16.781172, position = 283
bfs_local = 15.399072, position = 284
bfs_local = 14.566254, position = 285
bfs_local = 14.029958, position = 286
bfs_local = 13.050027, position = 335233
bfs_local = 11.187132, position = 340695
bfs_local = 10.858139, position = 340696
bfs_local = 10.625361, position = 340701
bfs_local = 10.232833, position = 756562
bfs_local = 10.005934, position = 789808
bfs_local = 9.701295, position = 789812
bfs_local = 9.236251, position = 148875
bfs_local = 8.076555, position = 148876
bsf = 2.84193
position = 148876
DTW Calculation = 6.83%
Count of DTW = 68276
Time for calculation lower bounding = 85.67%
Time for initializing arrays with start and end segment values = 0.000%
Time for finding the best subsequence = 13.63%
Runtime of reading 0.33
Total Runtime of finding the best subsequence 3.10


number of a  series of experiments = 1
count of threads = 6

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bfs_local = 126.953560, position = 666650
bfs_local = 17.171980, position = 666600
bfs_local = 16.914772, position = 666599
bfs_local = 16.480627, position = 295
bfs_local = 15.399072, position = 284
bfs_local = 14.566254, position = 285
bfs_local = 14.029958, position = 286
bfs_local = 13.550267, position = 168473
bfs_local = 12.763593, position = 168474
bfs_local = 12.408418, position = 168475
bfs_local = 11.187132, position = 340695
bfs_local = 10.858139, position = 340696
bfs_local = 10.625361, position = 340701
bfs_local = 10.538024, position = 847007
bfs_local = 9.738815, position = 874191
bfs_local = 9.735808, position = 874194
bfs_local = 9.701295, position = 789812
bfs_local = 9.158608, position = 295832
bfs_local = 9.092924, position = 295830
bfs_local = 8.985011, position = 295831
bfs_local = 8.076555, position = 148876
bsf = 2.84193
position = 148876
DTW Calculation = 7.67%
Count of DTW = 76715
Time for calculation lower bounding = 85.95%
Time for initializing arrays with start and end segment values = 0.000%
Time for finding the best subsequence = 12.91%
Runtime of reading 0.33
Total Runtime of finding the best subsequence 1.59


number of a  series of experiments = 1
count of threads = 12

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bfs_local = 37.726868, position = 333350
bfs_local = 17.171980, position = 666600
bfs_local = 16.914772, position = 666599
bfs_local = 15.261059, position = 289
bfs_local = 14.469719, position = 292
bfs_local = 14.029958, position = 286
bfs_local = 13.362963, position = 750548
bfs_local = 13.138631, position = 750549
bfs_local = 13.108140, position = 750550
bfs_local = 11.311702, position = 750551
bfs_local = 11.223123, position = 418682
bfs_local = 11.080197, position = 418685
bfs_local = 10.232833, position = 756562
bfs_local = 10.107504, position = 286687
bfs_local = 10.005934, position = 789808
bfs_local = 9.701295, position = 789812
bfs_local = 9.092924, position = 295830
bfs_local = 8.985011, position = 295831
bfs_local = 8.076555, position = 148876
bsf = 2.84193
position = 148876
DTW Calculation = 7.47%
Count of DTW = 74716
Time for calculation lower bounding = 87.93%
Time for initializing arrays with start and end segment values = 0.001%
Time for finding the best subsequence = 10.64%
Runtime of reading 0.33
Total Runtime of finding the best subsequence 1.20


number of a  series of experiments = 1
count of threads = 18

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bfs_local = 92.904671, position = 222218
bfs_local = 87.260231, position = 55549
bfs_local = 71.597733, position = 277767
bfs_local = 32.755161, position = 222268
bfs_local = 27.919195, position = 666604
bfs_local = 20.524908, position = 666593
bfs_local = 16.914772, position = 666599
bfs_local = 15.445699, position = 222291
bfs_local = 15.337241, position = 278068
bfs_local = 13.857821, position = 278069
bfs_local = 13.545185, position = 278070
bfs_local = 13.109679, position = 890566
bfs_local = 12.614532, position = 890567
bfs_local = 12.438791, position = 279451
bfs_local = 11.600005, position = 279452
bfs_local = 11.563806, position = 447997
bfs_local = 11.529969, position = 284066
bfs_local = 11.187132, position = 340695
bfs_local = 10.858139, position = 340696
bfs_local = 10.625361, position = 340701
bfs_local = 10.107504, position = 286687
bfs_local = 10.005934, position = 789808
bfs_local = 9.701295, position = 789812
bfs_local = 9.092924, position = 295830
bfs_local = 8.985011, position = 295831
bfs_local = 8.076555, position = 148876
bsf = 2.84193
position = 148876
DTW Calculation = 7.22%
Count of DTW = 72239
Time for calculation lower bounding = 85.55%
Time for initializing arrays with start and end segment values = 0.001%
Time for finding the best subsequence = 12.28%
Runtime of reading 0.33
Total Runtime of finding the best subsequence 0.85


number of a  series of experiments = 1
count of threads = 24

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bfs_local = 176.680191, position = 166702
bfs_local = 27.605854, position = 41687
bfs_local = 24.568333, position = 41662
bfs_local = 19.085686, position = 874989
bfs_local = 18.059488, position = 666596
bfs_local = 17.171980, position = 666600
bfs_local = 16.914772, position = 666599
bfs_local = 16.356503, position = 41960
bfs_local = 14.029958, position = 286
bfs_local = 13.362963, position = 750548
bfs_local = 13.138631, position = 750549
bfs_local = 13.108140, position = 750550
bfs_local = 11.311702, position = 750551
bfs_local = 11.098734, position = 43099
bfs_local = 11.080197, position = 418685
bfs_local = 11.058647, position = 295828
bfs_local = 10.299012, position = 295829
bfs_local = 9.092924, position = 295830
bfs_local = 8.985011, position = 295831
bfs_local = 8.076555, position = 148876
bsf = 2.84193
position = 148876
DTW Calculation = 6.70%
Count of DTW = 66989
Time for calculation lower bounding = 87.42%
Time for initializing arrays with start and end segment values = 0.143%
Time for finding the best subsequence = 10.47%
Runtime of reading 0.33
Total Runtime of finding the best subsequence 0.93


number of a  series of experiments = 2
count of threads = 1

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bfs_local = 217.902573, position = 49
bfs_local = 195.642609, position = 68
bfs_local = 193.637848, position = 69
bfs_local = 192.775055, position = 71
bfs_local = 189.489090, position = 72
bfs_local = 187.095245, position = 73
bfs_local = 184.339859, position = 74
bfs_local = 181.737534, position = 75
bfs_local = 180.180344, position = 76
bfs_local = 173.804565, position = 111
bfs_local = 167.831268, position = 112
bfs_local = 164.025848, position = 113
bfs_local = 156.747406, position = 114
bfs_local = 147.730225, position = 115
bfs_local = 141.820221, position = 116
bfs_local = 125.221489, position = 117
bfs_local = 105.388641, position = 118
bfs_local = 103.470436, position = 119
bfs_local = 102.000336, position = 120
bfs_local = 100.440979, position = 121
bfs_local = 96.495033, position = 122
bfs_local = 93.008453, position = 123
bfs_local = 90.095970, position = 124
bfs_local = 87.125893, position = 125
bfs_local = 81.895569, position = 126
bfs_local = 70.764099, position = 127
bfs_local = 68.156723, position = 270
bfs_local = 61.426331, position = 271
bfs_local = 57.186100, position = 272
bfs_local = 52.750870, position = 273
bfs_local = 52.364532, position = 274
bfs_local = 50.734005, position = 275
bfs_local = 38.286484, position = 276
bfs_local = 35.146648, position = 277
bfs_local = 27.123549, position = 279
bfs_local = 22.783813, position = 280
bfs_local = 21.594234, position = 281
bfs_local = 18.515770, position = 282
bfs_local = 16.781172, position = 283
bfs_local = 15.399072, position = 284
bfs_local = 14.566254, position = 285
bfs_local = 14.029958, position = 286
bfs_local = 13.455294, position = 12317
bfs_local = 13.343624, position = 12318
bfs_local = 11.353244, position = 25846
bfs_local = 11.098734, position = 43099
bfs_local = 10.341493, position = 131638
bfs_local = 10.244247, position = 131639
bfs_local = 10.189466, position = 148874
bfs_local = 9.236251, position = 148875
bfs_local = 8.076555, position = 148876
bsf = 2.84193
position = 148876
DTW Calculation = 6.06%
Count of DTW = 60599
Time for calculation lower bounding = 82.90%
Time for initializing arrays with start and end segment values = 0.000%
Time for finding the best subsequence = 16.81%
Runtime of reading 0.33
Total Runtime of finding the best subsequence 9.41


number of a  series of experiments = 2
count of threads = 3

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bfs_local = 134.735672, position = 666648
bfs_local = 66.408272, position = 333291
bfs_local = 43.646950, position = 666615
bfs_local = 31.358585, position = 666582
bfs_local = 30.565271, position = 666583
bfs_local = 23.162945, position = 666584
bfs_local = 21.950909, position = 666589
bfs_local = 21.757963, position = 666590
bfs_local = 20.977331, position = 666591
bfs_local = 20.798550, position = 666592
bfs_local = 20.524908, position = 666593
bfs_local = 19.440695, position = 666594
bfs_local = 18.700260, position = 666595
bfs_local = 18.059488, position = 666596
bfs_local = 16.914772, position = 666599
bfs_local = 16.781172, position = 283
bfs_local = 15.399072, position = 284
bfs_local = 14.566254, position = 285
bfs_local = 14.029958, position = 286
bfs_local = 13.050027, position = 335233
bfs_local = 11.187132, position = 340695
bfs_local = 10.858139, position = 340696
bfs_local = 10.625361, position = 340701
bfs_local = 10.232833, position = 756562
bfs_local = 10.005934, position = 789808
bfs_local = 9.701295, position = 789812
bfs_local = 9.236251, position = 148875
bfs_local = 8.076555, position = 148876
bsf = 2.84193
position = 148876
DTW Calculation = 6.83%
Count of DTW = 68276
Time for calculation lower bounding = 86.39%
Time for initializing arrays with start and end segment values = 0.000%
Time for finding the best subsequence = 12.94%
Runtime of reading 0.33
Total Runtime of finding the best subsequence 3.11


number of a  series of experiments = 2
count of threads = 6

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bfs_local = 126.953560, position = 666650
bfs_local = 72.925850, position = 333375
bfs_local = 17.171980, position = 666600
bfs_local = 16.914772, position = 666599
bfs_local = 16.480627, position = 295
bfs_local = 15.399072, position = 284
bfs_local = 14.566254, position = 285
bfs_local = 14.029958, position = 286
bfs_local = 13.550267, position = 168473
bfs_local = 12.763593, position = 168474
bfs_local = 12.408418, position = 168475
bfs_local = 11.187132, position = 340695
bfs_local = 10.858139, position = 340696
bfs_local = 10.625361, position = 340701
bfs_local = 10.538024, position = 847007
bfs_local = 9.738815, position = 874191
bfs_local = 9.735808, position = 874194
bfs_local = 9.701295, position = 789812
bfs_local = 9.158608, position = 295832
bfs_local = 9.092924, position = 295830
bfs_local = 8.985011, position = 295831
bfs_local = 8.076555, position = 148876
bsf = 2.84193
position = 148876
DTW Calculation = 7.67%
Count of DTW = 76715
Time for calculation lower bounding = 85.86%
Time for initializing arrays with start and end segment values = 0.000%
Time for finding the best subsequence = 13.01%
Runtime of reading 0.33
Total Runtime of finding the best subsequence 1.58


number of a  series of experiments = 2
count of threads = 12

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bfs_local = 91.052063, position = 333300
bfs_local = 83.225571, position = 666625
bfs_local = 72.925850, position = 333375
bfs_local = 37.726868, position = 333350
bfs_local = 17.171980, position = 666600
bfs_local = 16.914772, position = 666599
bfs_local = 15.261059, position = 289
bfs_local = 14.469719, position = 292
bfs_local = 14.029958, position = 286
bfs_local = 13.362963, position = 750548
bfs_local = 13.138631, position = 750549
bfs_local = 13.108140, position = 750550
bfs_local = 11.311702, position = 750551
bfs_local = 11.223123, position = 418682
bfs_local = 11.080197, position = 418685
bfs_local = 10.232833, position = 756562
bfs_local = 10.107504, position = 286687
bfs_local = 10.005934, position = 789808
bfs_local = 9.701295, position = 789812
bfs_local = 9.092924, position = 295830
bfs_local = 8.985011, position = 295831
bfs_local = 8.076555, position = 148876
bsf = 2.84193
position = 148876
DTW Calculation = 7.47%
Count of DTW = 74716
Time for calculation lower bounding = 87.96%
Time for initializing arrays with start and end segment values = 0.001%
Time for finding the best subsequence = 10.33%
Runtime of reading 0.33
Total Runtime of finding the best subsequence 1.08


number of a  series of experiments = 2
count of threads = 18

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bfs_local = 32.755161, position = 222268
bfs_local = 27.919195, position = 666604
bfs_local = 20.524908, position = 666593
bfs_local = 16.914772, position = 666599
bfs_local = 15.445699, position = 222291
bfs_local = 15.337241, position = 278068
bfs_local = 13.857821, position = 278069
bfs_local = 13.545185, position = 278070
bfs_local = 12.438791, position = 279451
bfs_local = 11.600005, position = 279452
bfs_local = 11.563806, position = 447997
bfs_local = 11.529969, position = 284066
bfs_local = 11.187132, position = 340695
bfs_local = 10.858139, position = 340696
bfs_local = 10.625361, position = 340701
bfs_local = 10.107504, position = 286687
bfs_local = 10.005934, position = 789808
bfs_local = 9.701295, position = 789812
bfs_local = 9.092924, position = 295830
bfs_local = 8.985011, position = 295831
bfs_local = 8.076555, position = 148876
bsf = 2.84193
position = 148876
DTW Calculation = 7.22%
Count of DTW = 72239
Time for calculation lower bounding = 87.80%
Time for initializing arrays with start and end segment values = 0.001%
Time for finding the best subsequence = 10.31%
Runtime of reading 0.33
Total Runtime of finding the best subsequence 0.93


number of a  series of experiments = 2
count of threads = 24

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bfs_local = 159.412003, position = 166652
bfs_local = 110.272133, position = 541693
bfs_local = 24.568333, position = 41662
bfs_local = 19.085686, position = 874989
bfs_local = 18.059488, position = 666596
bfs_local = 17.171980, position = 666600
bfs_local = 16.914772, position = 666599
bfs_local = 16.356503, position = 41960
bfs_local = 14.029958, position = 286
bfs_local = 13.362963, position = 750548
bfs_local = 13.138631, position = 750549
bfs_local = 13.108140, position = 750550
bfs_local = 11.311702, position = 750551
bfs_local = 11.098734, position = 43099
bfs_local = 11.080197, position = 418685
bfs_local = 11.058647, position = 295828
bfs_local = 10.299012, position = 295829
bfs_local = 9.092924, position = 295830
bfs_local = 8.985011, position = 295831
bfs_local = 8.076555, position = 148876
bsf = 2.84193
position = 148876
DTW Calculation = 6.70%
Count of DTW = 66989
Time for calculation lower bounding = 86.02%
Time for initializing arrays with start and end segment values = 0.002%
Time for finding the best subsequence = 11.42%
Runtime of reading 0.33
Total Runtime of finding the best subsequence 0.66


