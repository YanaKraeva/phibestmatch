number of a  series of experiments = 0
count of threads = 1

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bfs_local = 223.352234, position = 109
bfs_local = 217.986282, position = 110
bfs_local = 202.107590, position = 111
bfs_local = 196.548218, position = 112
bfs_local = 194.699081, position = 113
bfs_local = 188.548889, position = 114
bfs_local = 181.456696, position = 115
bfs_local = 178.447052, position = 116
bfs_local = 166.090591, position = 117
bfs_local = 148.301498, position = 118
bfs_local = 148.018250, position = 122
bfs_local = 147.223511, position = 123
bfs_local = 138.668015, position = 127
bfs_local = 132.793213, position = 211
bfs_local = 124.213631, position = 212
bfs_local = 122.048370, position = 213
bfs_local = 120.039169, position = 218
bfs_local = 118.112892, position = 219
bfs_local = 115.822189, position = 220
bfs_local = 114.401009, position = 221
bfs_local = 113.001175, position = 274
bfs_local = 110.061905, position = 275
bfs_local = 97.923744, position = 276
bfs_local = 91.462517, position = 277
bfs_local = 86.409286, position = 278
bfs_local = 73.945534, position = 279
bfs_local = 66.226074, position = 280
bfs_local = 61.142052, position = 281
bfs_local = 52.753788, position = 282
bfs_local = 46.817909, position = 283
bfs_local = 42.409679, position = 284
bfs_local = 40.528988, position = 285
bfs_local = 39.020214, position = 286
bfs_local = 37.167404, position = 289
bfs_local = 34.608784, position = 290
bfs_local = 31.008705, position = 291
bfs_local = 25.376856, position = 292
bfs_local = 23.158686, position = 294
bfs_local = 19.876860, position = 295
bfs_local = 19.615520, position = 298
bfs_local = 19.206821, position = 1892
bfs_local = 18.741386, position = 1893
bfs_local = 18.128325, position = 10385
bfs_local = 13.850060, position = 12317
bfs_local = 13.612634, position = 12318
bfs_local = 11.789265, position = 131632
bfs_local = 11.592933, position = 148876
bfs_local = 11.231969, position = 148877
bfs_local = 10.787928, position = 340701
bfs_local = 10.232833, position = 756562
bsf = 3.19888
position = 756562
DTW Calculation = 1.19%
Count of DTW = 11909
Time for calculation lower bounding = 42.34%
Time for initializing arrays with start and end segment values = 0.000%
Time for finding the best subsequence = 57.36%
Runtime of reading 0.33
Total Runtime of finding the best subsequence 6.48


number of a  series of experiments = 0
count of threads = 3

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bfs_local = 45.623787, position = 666583
bfs_local = 30.432985, position = 666585
bfs_local = 27.034298, position = 666586
bfs_local = 23.361834, position = 666597
bfs_local = 22.443714, position = 666598
bfs_local = 22.263723, position = 666589
bfs_local = 22.214788, position = 333451
bfs_local = 19.876860, position = 295
bfs_local = 19.615520, position = 298
bfs_local = 18.741386, position = 1893
bfs_local = 16.324242, position = 335226
bfs_local = 13.082743, position = 340700
bfs_local = 10.787928, position = 340701
bfs_local = 10.232833, position = 756562
bsf = 3.19888
position = 756562
DTW Calculation = 0.98%
Count of DTW = 9820
Time for calculation lower bounding = 68.19%
Time for initializing arrays with start and end segment values = 0.001%
Time for finding the best subsequence = 30.94%
Runtime of reading 0.33
Total Runtime of finding the best subsequence 1.36


number of a  series of experiments = 0
count of threads = 6

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bfs_local = 216.055359, position = 166650
bfs_local = 92.745247, position = 333292
bfs_local = 30.432985, position = 666585
bfs_local = 27.034298, position = 666586
bfs_local = 24.604746, position = 666587
bfs_local = 23.510569, position = 666588
bfs_local = 22.263723, position = 666589
bfs_local = 22.214788, position = 333451
bfs_local = 21.995607, position = 299
bfs_local = 21.148491, position = 297
bfs_local = 19.876860, position = 295
bfs_local = 19.615520, position = 298
bfs_local = 18.237652, position = 834070
bfs_local = 17.818430, position = 834067
bfs_local = 16.851923, position = 834068
bfs_local = 16.199982, position = 168474
bfs_local = 15.553727, position = 168475
bfs_local = 13.132357, position = 340695
bfs_local = 12.485727, position = 340696
bfs_local = 10.787928, position = 340701
bfs_local = 10.232833, position = 756562
bsf = 3.19888
position = 756562
DTW Calculation = 1.05%
Count of DTW = 10534
Time for calculation lower bounding = 77.00%
Time for initializing arrays with start and end segment values = 0.001%
Time for finding the best subsequence = 21.37%
Runtime of reading 0.33
Total Runtime of finding the best subsequence 0.63


number of a  series of experiments = 0
count of threads = 12

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bfs_local = 101.587234, position = 333295
bfs_local = 89.132156, position = 416618
bfs_local = 77.328758, position = 416615
bfs_local = 69.410065, position = 583265
bfs_local = 30.432985, position = 666585
bfs_local = 23.510569, position = 666588
bfs_local = 23.407967, position = 666599
bfs_local = 22.263723, position = 666589
bfs_local = 22.214788, position = 333451
bfs_local = 19.876860, position = 295
bfs_local = 19.615520, position = 298
bfs_local = 19.448748, position = 834064
bfs_local = 17.112631, position = 834069
bfs_local = 16.851923, position = 834068
bfs_local = 14.333217, position = 585035
bfs_local = 14.223468, position = 585036
bfs_local = 13.345497, position = 585037
bfs_local = 11.821036, position = 756561
bfs_local = 10.232833, position = 756562
bsf = 3.19888
position = 756562
DTW Calculation = 1.01%
Count of DTW = 10126
Time for calculation lower bounding = 84.73%
Time for initializing arrays with start and end segment values = 0.001%
Time for finding the best subsequence = 13.39%
Runtime of reading 0.33
Total Runtime of finding the best subsequence 0.55


number of a  series of experiments = 0
count of threads = 18

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bfs_local = 51.842121, position = 444396
bfs_local = 27.280378, position = 666592
bfs_local = 22.263723, position = 666589
bfs_local = 22.214788, position = 333451
bfs_local = 22.091293, position = 722366
bfs_local = 19.615520, position = 298
bfs_local = 19.165876, position = 834066
bfs_local = 17.112631, position = 834069
bfs_local = 16.851923, position = 834068
bfs_local = 15.171431, position = 890567
bfs_local = 13.812093, position = 890568
bfs_local = 13.336525, position = 892352
bfs_local = 12.483374, position = 892350
bfs_local = 11.991580, position = 892351
bfs_local = 10.787928, position = 340701
bfs_local = 10.232833, position = 756562
bsf = 3.19888
position = 756562
DTW Calculation = 1.14%
Count of DTW = 11387
Time for calculation lower bounding = 84.12%
Time for initializing arrays with start and end segment values = 0.001%
Time for finding the best subsequence = 14.04%
Runtime of reading 0.33
Total Runtime of finding the best subsequence 0.54


number of a  series of experiments = 0
count of threads = 24

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bfs_local = 63.903206, position = 124989
bfs_local = 41.055649, position = 291637
bfs_local = 37.418179, position = 291636
bfs_local = 27.083183, position = 666593
bfs_local = 27.078417, position = 666594
bfs_local = 24.859955, position = 666596
bfs_local = 22.443714, position = 666598
bfs_local = 18.316355, position = 458486
bfs_local = 16.790121, position = 41959
bfs_local = 16.356503, position = 41960
bfs_local = 15.037620, position = 43104
bfs_local = 13.345497, position = 585037
bfs_local = 13.261451, position = 377613
bfs_local = 12.222793, position = 377612
bfs_local = 11.821036, position = 756561
bfs_local = 10.424694, position = 756563
bfs_local = 10.232833, position = 756562
bsf = 3.19888
position = 756562
DTW Calculation = 1.07%
Count of DTW = 10708
Time for calculation lower bounding = 80.32%
Time for initializing arrays with start and end segment values = 0.007%
Time for finding the best subsequence = 17.44%
Runtime of reading 0.33
Total Runtime of finding the best subsequence 0.47


number of a  series of experiments = 1
count of threads = 1

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bfs_local = 223.352234, position = 109
bfs_local = 217.986282, position = 110
bfs_local = 202.107590, position = 111
bfs_local = 196.548218, position = 112
bfs_local = 194.699081, position = 113
bfs_local = 188.548889, position = 114
bfs_local = 181.456696, position = 115
bfs_local = 178.447052, position = 116
bfs_local = 166.090591, position = 117
bfs_local = 148.301498, position = 118
bfs_local = 148.018250, position = 122
bfs_local = 147.223511, position = 123
bfs_local = 138.668015, position = 127
bfs_local = 132.793213, position = 211
bfs_local = 124.213631, position = 212
bfs_local = 122.048370, position = 213
bfs_local = 120.039169, position = 218
bfs_local = 118.112892, position = 219
bfs_local = 115.822189, position = 220
bfs_local = 114.401009, position = 221
bfs_local = 113.001175, position = 274
bfs_local = 110.061905, position = 275
bfs_local = 97.923744, position = 276
bfs_local = 91.462517, position = 277
bfs_local = 86.409286, position = 278
bfs_local = 73.945534, position = 279
bfs_local = 66.226074, position = 280
bfs_local = 61.142052, position = 281
bfs_local = 52.753788, position = 282
bfs_local = 46.817909, position = 283
bfs_local = 42.409679, position = 284
bfs_local = 40.528988, position = 285
bfs_local = 39.020214, position = 286
bfs_local = 37.167404, position = 289
bfs_local = 34.608784, position = 290
bfs_local = 31.008705, position = 291
bfs_local = 25.376856, position = 292
bfs_local = 23.158686, position = 294
bfs_local = 19.876860, position = 295
bfs_local = 19.615520, position = 298
bfs_local = 19.206821, position = 1892
bfs_local = 18.741386, position = 1893
bfs_local = 18.128325, position = 10385
bfs_local = 13.850060, position = 12317
bfs_local = 13.612634, position = 12318
bfs_local = 11.789265, position = 131632
bfs_local = 11.592933, position = 148876
bfs_local = 11.231969, position = 148877
bfs_local = 10.787928, position = 340701
bfs_local = 10.232833, position = 756562
bsf = 3.19888
position = 756562
DTW Calculation = 1.19%
Count of DTW = 11909
Time for calculation lower bounding = 42.48%
Time for initializing arrays with start and end segment values = 0.000%
Time for finding the best subsequence = 57.22%
Runtime of reading 0.33
Total Runtime of finding the best subsequence 6.45


number of a  series of experiments = 1
count of threads = 3

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bfs_local = 45.623787, position = 666583
bfs_local = 30.432985, position = 666585
bfs_local = 27.034298, position = 666586
bfs_local = 23.361834, position = 666597
bfs_local = 22.443714, position = 666598
bfs_local = 22.263723, position = 666589
bfs_local = 22.214788, position = 333451
bfs_local = 19.876860, position = 295
bfs_local = 19.615520, position = 298
bfs_local = 18.741386, position = 1893
bfs_local = 16.324242, position = 335226
bfs_local = 13.082743, position = 340700
bfs_local = 10.787928, position = 340701
bfs_local = 10.232833, position = 756562
bsf = 3.19888
position = 756562
DTW Calculation = 0.98%
Count of DTW = 9820
Time for calculation lower bounding = 67.56%
Time for initializing arrays with start and end segment values = 0.001%
Time for finding the best subsequence = 31.58%
Runtime of reading 0.33
Total Runtime of finding the best subsequence 1.42


number of a  series of experiments = 1
count of threads = 6

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bfs_local = 217.569031, position = 499940
bfs_local = 101.587234, position = 333295
bfs_local = 30.432985, position = 666585
bfs_local = 27.034298, position = 666586
bfs_local = 24.604746, position = 666587
bfs_local = 23.510569, position = 666588
bfs_local = 22.263723, position = 666589
bfs_local = 22.214788, position = 333451
bfs_local = 21.995607, position = 299
bfs_local = 21.148491, position = 297
bfs_local = 19.876860, position = 295
bfs_local = 19.615520, position = 298
bfs_local = 18.237652, position = 834070
bfs_local = 17.818430, position = 834067
bfs_local = 16.851923, position = 834068
bfs_local = 16.199982, position = 168474
bfs_local = 15.553727, position = 168475
bfs_local = 13.132357, position = 340695
bfs_local = 10.787928, position = 340701
bfs_local = 10.232833, position = 756562
bsf = 3.19888
position = 756562
DTW Calculation = 1.05%
Count of DTW = 10534
Time for calculation lower bounding = 78.14%
Time for initializing arrays with start and end segment values = 0.001%
Time for finding the best subsequence = 20.22%
Runtime of reading 0.33
Total Runtime of finding the best subsequence 0.63


number of a  series of experiments = 1
count of threads = 12

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bfs_local = 156.074127, position = 916555
bfs_local = 30.432985, position = 666585
bfs_local = 23.510569, position = 666588
bfs_local = 22.263723, position = 666589
bfs_local = 22.214788, position = 333451
bfs_local = 19.876860, position = 295
bfs_local = 19.615520, position = 298
bfs_local = 19.448748, position = 834064
bfs_local = 17.112631, position = 834069
bfs_local = 16.851923, position = 834068
bfs_local = 14.333217, position = 585035
bfs_local = 14.223468, position = 585036
bfs_local = 13.345497, position = 585037
bfs_local = 12.988788, position = 756566
bfs_local = 11.821036, position = 756561
bfs_local = 10.232833, position = 756562
bsf = 3.19888
position = 756562
DTW Calculation = 1.01%
Count of DTW = 10126
Time for calculation lower bounding = 79.47%
Time for initializing arrays with start and end segment values = 0.002%
Time for finding the best subsequence = 18.47%
Runtime of reading 0.33
Total Runtime of finding the best subsequence 0.54


number of a  series of experiments = 1
count of threads = 18

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bfs_local = 77.288986, position = 277745
bfs_local = 63.225170, position = 444392
bfs_local = 51.842121, position = 444396
bfs_local = 22.263723, position = 666589
bfs_local = 22.214788, position = 333451
bfs_local = 22.091293, position = 722366
bfs_local = 19.615520, position = 298
bfs_local = 19.165876, position = 834066
bfs_local = 17.112631, position = 834069
bfs_local = 16.851923, position = 834068
bfs_local = 15.171431, position = 890567
bfs_local = 13.812093, position = 890568
bfs_local = 13.336525, position = 892352
bfs_local = 12.483374, position = 892350
bfs_local = 11.991580, position = 892351
bfs_local = 10.787928, position = 340701
bfs_local = 10.232833, position = 756562
bsf = 3.19888
position = 756562
DTW Calculation = 1.14%
Count of DTW = 11387
Time for calculation lower bounding = 79.47%
Time for initializing arrays with start and end segment values = 0.002%
Time for finding the best subsequence = 18.41%
Runtime of reading 0.33
Total Runtime of finding the best subsequence 0.50


number of a  series of experiments = 1
count of threads = 24

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bfs_local = 191.124191, position = 166652
bfs_local = 119.829781, position = 208314
bfs_local = 44.565071, position = 291638
bfs_local = 37.418179, position = 291636
bfs_local = 27.083183, position = 666593
bfs_local = 24.859955, position = 666596
bfs_local = 22.443714, position = 666598
bfs_local = 18.316355, position = 458486
bfs_local = 16.790121, position = 41959
bfs_local = 16.356503, position = 41960
bfs_local = 15.037620, position = 43104
bfs_local = 13.345497, position = 585037
bfs_local = 13.261451, position = 377613
bfs_local = 12.222793, position = 377612
bfs_local = 10.424694, position = 756563
bfs_local = 10.232833, position = 756562
bsf = 3.19888
position = 756562
DTW Calculation = 1.07%
Count of DTW = 10708
Time for calculation lower bounding = 78.62%
Time for initializing arrays with start and end segment values = 0.003%
Time for finding the best subsequence = 17.84%
Runtime of reading 0.33
Total Runtime of finding the best subsequence 0.27


number of a  series of experiments = 2
count of threads = 1

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bfs_local = 223.352234, position = 109
bfs_local = 217.986282, position = 110
bfs_local = 202.107590, position = 111
bfs_local = 196.548218, position = 112
bfs_local = 194.699081, position = 113
bfs_local = 188.548889, position = 114
bfs_local = 181.456696, position = 115
bfs_local = 178.447052, position = 116
bfs_local = 166.090591, position = 117
bfs_local = 148.301498, position = 118
bfs_local = 148.018250, position = 122
bfs_local = 147.223511, position = 123
bfs_local = 138.668015, position = 127
bfs_local = 132.793213, position = 211
bfs_local = 124.213631, position = 212
bfs_local = 122.048370, position = 213
bfs_local = 120.039169, position = 218
bfs_local = 118.112892, position = 219
bfs_local = 115.822189, position = 220
bfs_local = 114.401009, position = 221
bfs_local = 113.001175, position = 274
bfs_local = 110.061905, position = 275
bfs_local = 97.923744, position = 276
bfs_local = 91.462517, position = 277
bfs_local = 86.409286, position = 278
bfs_local = 73.945534, position = 279
bfs_local = 66.226074, position = 280
bfs_local = 61.142052, position = 281
bfs_local = 52.753788, position = 282
bfs_local = 46.817909, position = 283
bfs_local = 42.409679, position = 284
bfs_local = 40.528988, position = 285
bfs_local = 39.020214, position = 286
bfs_local = 37.167404, position = 289
bfs_local = 34.608784, position = 290
bfs_local = 31.008705, position = 291
bfs_local = 25.376856, position = 292
bfs_local = 23.158686, position = 294
bfs_local = 19.876860, position = 295
bfs_local = 19.615520, position = 298
bfs_local = 19.206821, position = 1892
bfs_local = 18.741386, position = 1893
bfs_local = 18.128325, position = 10385
bfs_local = 13.850060, position = 12317
bfs_local = 13.612634, position = 12318
bfs_local = 11.789265, position = 131632
bfs_local = 11.592933, position = 148876
bfs_local = 11.231969, position = 148877
bfs_local = 10.787928, position = 340701
bfs_local = 10.232833, position = 756562
bsf = 3.19888
position = 756562
DTW Calculation = 1.19%
Count of DTW = 11909
Time for calculation lower bounding = 42.64%
Time for initializing arrays with start and end segment values = 0.000%
Time for finding the best subsequence = 57.06%
Runtime of reading 0.33
Total Runtime of finding the best subsequence 6.41


number of a  series of experiments = 2
count of threads = 3

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bfs_local = 45.623787, position = 666583
bfs_local = 30.432985, position = 666585
bfs_local = 27.034298, position = 666586
bfs_local = 24.604746, position = 666587
bfs_local = 23.361834, position = 666597
bfs_local = 22.443714, position = 666598
bfs_local = 22.263723, position = 666589
bfs_local = 22.214788, position = 333451
bfs_local = 19.876860, position = 295
bfs_local = 19.615520, position = 298
bfs_local = 18.741386, position = 1893
bfs_local = 16.324242, position = 335226
bfs_local = 13.132357, position = 340695
bfs_local = 13.082743, position = 340700
bfs_local = 12.485727, position = 340696
bfs_local = 10.787928, position = 340701
bfs_local = 10.232833, position = 756562
bsf = 3.19888
position = 756562
DTW Calculation = 0.98%
Count of DTW = 9820
Time for calculation lower bounding = 66.07%
Time for initializing arrays with start and end segment values = 0.000%
Time for finding the best subsequence = 33.00%
Runtime of reading 0.33
Total Runtime of finding the best subsequence 1.41


number of a  series of experiments = 2
count of threads = 6

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bfs_local = 216.055359, position = 166650
bfs_local = 95.426788, position = 333293
bfs_local = 30.432985, position = 666585
bfs_local = 27.034298, position = 666586
bfs_local = 24.604746, position = 666587
bfs_local = 23.510569, position = 666588
bfs_local = 22.263723, position = 666589
bfs_local = 22.214788, position = 333451
bfs_local = 21.148491, position = 297
bfs_local = 19.876860, position = 295
bfs_local = 19.615520, position = 298
bfs_local = 18.237652, position = 834070
bfs_local = 17.818430, position = 834067
bfs_local = 16.851923, position = 834068
bfs_local = 16.199982, position = 168474
bfs_local = 15.553727, position = 168475
bfs_local = 13.132357, position = 340695
bfs_local = 12.485727, position = 340696
bfs_local = 10.787928, position = 340701
bfs_local = 10.232833, position = 756562
bsf = 3.19888
position = 756562
DTW Calculation = 1.05%
Count of DTW = 10534
Time for calculation lower bounding = 76.82%
Time for initializing arrays with start and end segment values = 0.001%
Time for finding the best subsequence = 21.53%
Runtime of reading 0.33
Total Runtime of finding the best subsequence 0.64


number of a  series of experiments = 2
count of threads = 12

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bfs_local = 174.758942, position = 916553
bfs_local = 129.175766, position = 83323
bfs_local = 101.587234, position = 333295
bfs_local = 69.410065, position = 583265
bfs_local = 30.432985, position = 666585
bfs_local = 23.510569, position = 666588
bfs_local = 23.407967, position = 666599
bfs_local = 22.263723, position = 666589
bfs_local = 22.214788, position = 333451
bfs_local = 19.876860, position = 295
bfs_local = 19.615520, position = 298
bfs_local = 19.448748, position = 834064
bfs_local = 17.112631, position = 834069
bfs_local = 16.851923, position = 834068
bfs_local = 14.333217, position = 585035
bfs_local = 14.223468, position = 585036
bfs_local = 13.345497, position = 585037
bfs_local = 11.821036, position = 756561
bfs_local = 10.232833, position = 756562
bsf = 3.19888
position = 756562
DTW Calculation = 1.01%
Count of DTW = 10126
Time for calculation lower bounding = 79.07%
Time for initializing arrays with start and end segment values = 0.001%
Time for finding the best subsequence = 18.98%
Runtime of reading 0.33
Total Runtime of finding the best subsequence 0.50


number of a  series of experiments = 2
count of threads = 18

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bfs_local = 128.902252, position = 888787
bfs_local = 122.018982, position = 55551
bfs_local = 108.035461, position = 555492
bfs_local = 23.448120, position = 666590
bfs_local = 22.263723, position = 666589
bfs_local = 22.214788, position = 333451
bfs_local = 22.091293, position = 722366
bfs_local = 19.615520, position = 298
bfs_local = 19.448748, position = 834064
bfs_local = 19.165876, position = 834066
bfs_local = 17.112631, position = 834069
bfs_local = 16.851923, position = 834068
bfs_local = 15.171431, position = 890567
bfs_local = 13.812093, position = 890568
bfs_local = 13.336525, position = 892352
bfs_local = 12.483374, position = 892350
bfs_local = 11.991580, position = 892351
bfs_local = 10.787928, position = 340701
bfs_local = 10.232833, position = 756562
bsf = 3.19888
position = 756562
DTW Calculation = 1.14%
Count of DTW = 11387
Time for calculation lower bounding = 70.98%
Time for initializing arrays with start and end segment values = 0.004%
Time for finding the best subsequence = 26.59%
Runtime of reading 0.33
Total Runtime of finding the best subsequence 0.49


number of a  series of experiments = 2
count of threads = 24

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bfs_local = 126.216393, position = 499946
bfs_local = 114.093964, position = 541608
bfs_local = 92.452789, position = 583270
bfs_local = 52.019310, position = 874903
bfs_local = 37.418179, position = 291636
bfs_local = 27.083183, position = 666593
bfs_local = 27.078417, position = 666594
bfs_local = 24.859955, position = 666596
bfs_local = 22.443714, position = 666598
bfs_local = 20.654917, position = 458481
bfs_local = 18.316355, position = 458486
bfs_local = 16.790121, position = 41959
bfs_local = 16.356503, position = 41960
bfs_local = 15.037620, position = 43104
bfs_local = 13.345497, position = 585037
bfs_local = 13.261451, position = 377613
bfs_local = 12.222793, position = 377612
bfs_local = 10.424694, position = 756563
bfs_local = 10.232833, position = 756562
bsf = 3.19888
position = 756562
DTW Calculation = 1.07%
Count of DTW = 10708
Time for calculation lower bounding = 78.74%
Time for initializing arrays with start and end segment values = 0.003%
Time for finding the best subsequence = 17.61%
Runtime of reading 0.33
Total Runtime of finding the best subsequence 0.27


