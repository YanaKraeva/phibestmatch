number of a  series of experiments = 0
count of threads = 1

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bfs_local = 223.352234, position = 109
bfs_local = 217.986282, position = 110
bfs_local = 202.107590, position = 111
bfs_local = 196.548218, position = 112
bfs_local = 194.699081, position = 113
bfs_local = 188.548889, position = 114
bfs_local = 181.456696, position = 115
bfs_local = 178.447052, position = 116
bfs_local = 166.090591, position = 117
bfs_local = 148.301498, position = 118
bfs_local = 148.018250, position = 122
bfs_local = 147.223511, position = 123
bfs_local = 138.668015, position = 127
bfs_local = 132.793213, position = 211
bfs_local = 124.213631, position = 212
bfs_local = 122.048370, position = 213
bfs_local = 120.039169, position = 218
bfs_local = 118.112892, position = 219
bfs_local = 115.822189, position = 220
bfs_local = 114.401009, position = 221
bfs_local = 113.001175, position = 274
bfs_local = 110.061905, position = 275
bfs_local = 97.923744, position = 276
bfs_local = 91.462517, position = 277
bfs_local = 86.409286, position = 278
bfs_local = 73.945534, position = 279
bfs_local = 66.226074, position = 280
bfs_local = 61.142052, position = 281
bfs_local = 52.753788, position = 282
bfs_local = 46.817909, position = 283
bfs_local = 42.409679, position = 284
bfs_local = 40.528988, position = 285
bfs_local = 39.020214, position = 286
bfs_local = 37.167404, position = 289
bfs_local = 34.608784, position = 290
bfs_local = 31.008705, position = 291
bfs_local = 25.376856, position = 292
bfs_local = 23.158686, position = 294
bfs_local = 19.876860, position = 295
bfs_local = 19.615520, position = 298
bfs_local = 19.206821, position = 1892
bfs_local = 18.741386, position = 1893
bfs_local = 18.128325, position = 10385
bfs_local = 13.850060, position = 12317
bfs_local = 13.612634, position = 12318
bfs_local = 11.789265, position = 131632
bfs_local = 11.592933, position = 148876
bfs_local = 11.231969, position = 148877
bfs_local = 10.787928, position = 340701
bfs_local = 10.232833, position = 756562
bsf = 3.19888
position = 756562
DTW Calculation = 1.19%
Count of DTW = 11909
Time for calculation lower bounding = 11.41%
Time for initializing arrays with start and end segment values = 0.000%
Time for finding the best subsequence = 88.50%
Runtime of reading 0.40
Total Runtime of finding the best subsequence 24.04


number of a  series of experiments = 0
count of threads = 3

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bfs_local = 90.409081, position = 333291
bfs_local = 51.202122, position = 666582
bfs_local = 30.432985, position = 666585
bfs_local = 23.510569, position = 666588
bfs_local = 23.448120, position = 666590
bfs_local = 22.263723, position = 666589
bfs_local = 22.214788, position = 333451
bfs_local = 19.876860, position = 295
bfs_local = 19.615520, position = 298
bfs_local = 19.206821, position = 1892
bfs_local = 18.763649, position = 1894
bfs_local = 18.741386, position = 1893
bfs_local = 16.324242, position = 335226
bfs_local = 12.485727, position = 340696
bfs_local = 10.787928, position = 340701
bfs_local = 10.232833, position = 756562
bsf = 3.19888
position = 756562
DTW Calculation = 0.98%
Count of DTW = 9810
Time for calculation lower bounding = 30.94%
Time for initializing arrays with start and end segment values = 0.000%
Time for finding the best subsequence = 68.61%
Runtime of reading 0.33
Total Runtime of finding the best subsequence 3.07


number of a  series of experiments = 0
count of threads = 6

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bfs_local = 92.745247, position = 333292
bfs_local = 33.776402, position = 666584
bfs_local = 27.034298, position = 666586
bfs_local = 22.263723, position = 666589
bfs_local = 22.214788, position = 333451
bfs_local = 19.876860, position = 295
bfs_local = 19.615520, position = 298
bfs_local = 16.851923, position = 834068
bfs_local = 16.199982, position = 168474
bfs_local = 15.553727, position = 168475
bfs_local = 12.485727, position = 340696
bfs_local = 10.787928, position = 340701
bfs_local = 10.232833, position = 756562
bsf = 3.19888
position = 756562
DTW Calculation = 1.05%
Count of DTW = 10495
Time for calculation lower bounding = 42.02%
Time for initializing arrays with start and end segment values = 0.001%
Time for finding the best subsequence = 56.89%
Runtime of reading 0.33
Total Runtime of finding the best subsequence 1.17


number of a  series of experiments = 0
count of threads = 12

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bfs_local = 77.328758, position = 416615
bfs_local = 33.776402, position = 666584
bfs_local = 24.604746, position = 666587
bfs_local = 23.448120, position = 666590
bfs_local = 22.263723, position = 666589
bfs_local = 22.214788, position = 333451
bfs_local = 19.876860, position = 295
bfs_local = 19.615520, position = 298
bfs_local = 17.818430, position = 834067
bfs_local = 16.851923, position = 834068
bfs_local = 14.636871, position = 585033
bfs_local = 13.345497, position = 585037
bfs_local = 11.043961, position = 756564
bfs_local = 10.232833, position = 756562
bsf = 3.19888
position = 756562
DTW Calculation = 1.00%
Count of DTW = 10046
Time for calculation lower bounding = 41.77%
Time for initializing arrays with start and end segment values = 0.001%
Time for finding the best subsequence = 56.90%
Runtime of reading 0.33
Total Runtime of finding the best subsequence 0.95


number of a  series of experiments = 0
count of threads = 18

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bfs_local = 119.074692, position = 55549
bfs_local = 63.225170, position = 444392
bfs_local = 23.510569, position = 666588
bfs_local = 22.443714, position = 666598
bfs_local = 22.263723, position = 666589
bfs_local = 22.214788, position = 333451
bfs_local = 22.091293, position = 722366
bfs_local = 19.876860, position = 295
bfs_local = 19.615520, position = 298
bfs_local = 19.448748, position = 834064
bfs_local = 19.165876, position = 834066
bfs_local = 17.112631, position = 834069
bfs_local = 16.851923, position = 834068
bfs_local = 15.119696, position = 279451
bfs_local = 13.812093, position = 890568
bfs_local = 13.741715, position = 892349
bfs_local = 13.336525, position = 892352
bfs_local = 11.991580, position = 892351
bfs_local = 10.787928, position = 340701
bfs_local = 10.424694, position = 756563
bfs_local = 10.232833, position = 756562
bsf = 3.19888
position = 756562
DTW Calculation = 1.13%
Count of DTW = 11255
Time for calculation lower bounding = 38.01%
Time for initializing arrays with start and end segment values = 0.001%
Time for finding the best subsequence = 60.42%
Runtime of reading 0.33
Total Runtime of finding the best subsequence 0.89


number of a  series of experiments = 0
count of threads = 24

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bfs_local = 89.009659, position = 583268
bfs_local = 65.221085, position = 374958
bfs_local = 27.280378, position = 666592
bfs_local = 22.443714, position = 666598
bfs_local = 22.214788, position = 333451
bfs_local = 20.654917, position = 458481
bfs_local = 19.615480, position = 458482
bfs_local = 19.475924, position = 458484
bfs_local = 18.316355, position = 458486
bfs_local = 16.790121, position = 41959
bfs_local = 16.356503, position = 41960
bfs_local = 15.037620, position = 43104
bfs_local = 14.223468, position = 585036
bfs_local = 13.957302, position = 585038
bfs_local = 13.345497, position = 585037
bfs_local = 12.222793, position = 377612
bfs_local = 11.789265, position = 131632
bfs_local = 10.232833, position = 756562
bsf = 3.19888
position = 756562
DTW Calculation = 1.05%
Count of DTW = 10496
Time for calculation lower bounding = 56.38%
Time for initializing arrays with start and end segment values = 0.003%
Time for finding the best subsequence = 41.07%
Runtime of reading 0.33
Total Runtime of finding the best subsequence 0.66


number of a  series of experiments = 1
count of threads = 1

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bfs_local = 223.352234, position = 109
bfs_local = 217.986282, position = 110
bfs_local = 202.107590, position = 111
bfs_local = 196.548218, position = 112
bfs_local = 194.699081, position = 113
bfs_local = 188.548889, position = 114
bfs_local = 181.456696, position = 115
bfs_local = 178.447052, position = 116
bfs_local = 166.090591, position = 117
bfs_local = 148.301498, position = 118
bfs_local = 148.018250, position = 122
bfs_local = 147.223511, position = 123
bfs_local = 138.668015, position = 127
bfs_local = 132.793213, position = 211
bfs_local = 124.213631, position = 212
bfs_local = 122.048370, position = 213
bfs_local = 120.039169, position = 218
bfs_local = 118.112892, position = 219
bfs_local = 115.822189, position = 220
bfs_local = 114.401009, position = 221
bfs_local = 113.001175, position = 274
bfs_local = 110.061905, position = 275
bfs_local = 97.923744, position = 276
bfs_local = 91.462517, position = 277
bfs_local = 86.409286, position = 278
bfs_local = 73.945534, position = 279
bfs_local = 66.226074, position = 280
bfs_local = 61.142052, position = 281
bfs_local = 52.753788, position = 282
bfs_local = 46.817909, position = 283
bfs_local = 42.409679, position = 284
bfs_local = 40.528988, position = 285
bfs_local = 39.020214, position = 286
bfs_local = 37.167404, position = 289
bfs_local = 34.608784, position = 290
bfs_local = 31.008705, position = 291
bfs_local = 25.376856, position = 292
bfs_local = 23.158686, position = 294
bfs_local = 19.876860, position = 295
bfs_local = 19.615520, position = 298
bfs_local = 19.206821, position = 1892
bfs_local = 18.741386, position = 1893
bfs_local = 18.128325, position = 10385
bfs_local = 13.850060, position = 12317
bfs_local = 13.612634, position = 12318
bfs_local = 11.789265, position = 131632
bfs_local = 11.592933, position = 148876
bfs_local = 11.231969, position = 148877
bfs_local = 10.787928, position = 340701
bfs_local = 10.232833, position = 756562
bsf = 3.19888
position = 756562
DTW Calculation = 1.19%
Count of DTW = 11909
Time for calculation lower bounding = 11.45%
Time for initializing arrays with start and end segment values = 0.000%
Time for finding the best subsequence = 88.46%
Runtime of reading 0.33
Total Runtime of finding the best subsequence 23.98


number of a  series of experiments = 1
count of threads = 3

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bfs_local = 90.409081, position = 333291
bfs_local = 51.202122, position = 666582
bfs_local = 30.432985, position = 666585
bfs_local = 23.510569, position = 666588
bfs_local = 23.448120, position = 666590
bfs_local = 22.263723, position = 666589
bfs_local = 22.214788, position = 333451
bfs_local = 19.876860, position = 295
bfs_local = 19.615520, position = 298
bfs_local = 19.206821, position = 1892
bfs_local = 18.763649, position = 1894
bfs_local = 18.741386, position = 1893
bfs_local = 16.324242, position = 335226
bfs_local = 12.485727, position = 340696
bfs_local = 10.787928, position = 340701
bfs_local = 10.232833, position = 756562
bsf = 3.19888
position = 756562
DTW Calculation = 0.98%
Count of DTW = 9810
Time for calculation lower bounding = 30.76%
Time for initializing arrays with start and end segment values = 0.000%
Time for finding the best subsequence = 68.79%
Runtime of reading 0.33
Total Runtime of finding the best subsequence 3.03


number of a  series of experiments = 1
count of threads = 6

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bfs_local = 33.776402, position = 666584
bfs_local = 22.263723, position = 666589
bfs_local = 22.214788, position = 333451
bfs_local = 19.876860, position = 295
bfs_local = 19.615520, position = 298
bfs_local = 17.818430, position = 834067
bfs_local = 16.851923, position = 834068
bfs_local = 16.199982, position = 168474
bfs_local = 15.553727, position = 168475
bfs_local = 13.634612, position = 340697
bfs_local = 12.485727, position = 340696
bfs_local = 10.787928, position = 340701
bfs_local = 10.232833, position = 756562
bsf = 3.19888
position = 756562
DTW Calculation = 1.05%
Count of DTW = 10495
Time for calculation lower bounding = 42.26%
Time for initializing arrays with start and end segment values = 0.001%
Time for finding the best subsequence = 56.62%
Runtime of reading 0.33
Total Runtime of finding the best subsequence 1.16


number of a  series of experiments = 1
count of threads = 12

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bfs_local = 46.557476, position = 583261
bfs_local = 33.776402, position = 666584
bfs_local = 30.432985, position = 666585
bfs_local = 27.034298, position = 666586
bfs_local = 24.604746, position = 666587
bfs_local = 23.448120, position = 666590
bfs_local = 22.263723, position = 666589
bfs_local = 22.214788, position = 333451
bfs_local = 19.876860, position = 295
bfs_local = 19.615520, position = 298
bfs_local = 19.165876, position = 834066
bfs_local = 17.818430, position = 834067
bfs_local = 16.851923, position = 834068
bfs_local = 15.833485, position = 585032
bfs_local = 14.636871, position = 585033
bfs_local = 14.223468, position = 585036
bfs_local = 13.957302, position = 585038
bfs_local = 13.345497, position = 585037
bfs_local = 12.988788, position = 756566
bfs_local = 11.043961, position = 756564
bfs_local = 10.424694, position = 756563
bfs_local = 10.232833, position = 756562
bsf = 3.19888
position = 756562
DTW Calculation = 1.00%
Count of DTW = 10046
Time for calculation lower bounding = 48.69%
Time for initializing arrays with start and end segment values = 0.001%
Time for finding the best subsequence = 49.48%
Runtime of reading 0.33
Total Runtime of finding the best subsequence 0.73


number of a  series of experiments = 1
count of threads = 18

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bfs_local = 117.449219, position = 555490
bfs_local = 98.400955, position = 333294
bfs_local = 63.225170, position = 444392
bfs_local = 23.510569, position = 666588
bfs_local = 22.263723, position = 666589
bfs_local = 22.214788, position = 333451
bfs_local = 22.091293, position = 722366
bfs_local = 19.876860, position = 295
bfs_local = 19.615520, position = 298
bfs_local = 19.482819, position = 834063
bfs_local = 19.448748, position = 834064
bfs_local = 19.165876, position = 834066
bfs_local = 18.237652, position = 834070
bfs_local = 17.112631, position = 834069
bfs_local = 16.851923, position = 834068
bfs_local = 15.171431, position = 890567
bfs_local = 15.119696, position = 279451
bfs_local = 13.812093, position = 890568
bfs_local = 13.741715, position = 892349
bfs_local = 13.336525, position = 892352
bfs_local = 11.991580, position = 892351
bfs_local = 10.787928, position = 340701
bfs_local = 10.232833, position = 756562
bsf = 3.19888
position = 756562
DTW Calculation = 1.13%
Count of DTW = 11255
Time for calculation lower bounding = 47.34%
Time for initializing arrays with start and end segment values = 0.001%
Time for finding the best subsequence = 51.07%
Runtime of reading 0.33
Total Runtime of finding the best subsequence 0.85


number of a  series of experiments = 1
count of threads = 24

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bfs_local = 66.651237, position = 124986
bfs_local = 49.397976, position = 874902
bfs_local = 27.280378, position = 666592
bfs_local = 24.859955, position = 666596
bfs_local = 22.443714, position = 666598
bfs_local = 22.214788, position = 333451
bfs_local = 19.475924, position = 458484
bfs_local = 18.382826, position = 458487
bfs_local = 18.316355, position = 458486
bfs_local = 16.356503, position = 41960
bfs_local = 15.249774, position = 43102
bfs_local = 15.037620, position = 43104
bfs_local = 13.345497, position = 585037
bfs_local = 13.261451, position = 377613
bfs_local = 12.222793, position = 377612
bfs_local = 11.789265, position = 131632
bfs_local = 10.232833, position = 756562
bsf = 3.19888
position = 756562
DTW Calculation = 1.05%
Count of DTW = 10496
Time for calculation lower bounding = 55.94%
Time for initializing arrays with start and end segment values = 0.004%
Time for finding the best subsequence = 41.88%
Runtime of reading 0.33
Total Runtime of finding the best subsequence 0.54


number of a  series of experiments = 2
count of threads = 1

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bfs_local = 223.352234, position = 109
bfs_local = 217.986282, position = 110
bfs_local = 202.107590, position = 111
bfs_local = 196.548218, position = 112
bfs_local = 194.699081, position = 113
bfs_local = 188.548889, position = 114
bfs_local = 181.456696, position = 115
bfs_local = 178.447052, position = 116
bfs_local = 166.090591, position = 117
bfs_local = 148.301498, position = 118
bfs_local = 148.018250, position = 122
bfs_local = 147.223511, position = 123
bfs_local = 138.668015, position = 127
bfs_local = 132.793213, position = 211
bfs_local = 124.213631, position = 212
bfs_local = 122.048370, position = 213
bfs_local = 120.039169, position = 218
bfs_local = 118.112892, position = 219
bfs_local = 115.822189, position = 220
bfs_local = 114.401009, position = 221
bfs_local = 113.001175, position = 274
bfs_local = 110.061905, position = 275
bfs_local = 97.923744, position = 276
bfs_local = 91.462517, position = 277
bfs_local = 86.409286, position = 278
bfs_local = 73.945534, position = 279
bfs_local = 66.226074, position = 280
bfs_local = 61.142052, position = 281
bfs_local = 52.753788, position = 282
bfs_local = 46.817909, position = 283
bfs_local = 42.409679, position = 284
bfs_local = 40.528988, position = 285
bfs_local = 39.020214, position = 286
bfs_local = 37.167404, position = 289
bfs_local = 34.608784, position = 290
bfs_local = 31.008705, position = 291
bfs_local = 25.376856, position = 292
bfs_local = 23.158686, position = 294
bfs_local = 19.876860, position = 295
bfs_local = 19.615520, position = 298
bfs_local = 19.206821, position = 1892
bfs_local = 18.741386, position = 1893
bfs_local = 18.128325, position = 10385
bfs_local = 13.850060, position = 12317
bfs_local = 13.612634, position = 12318
bfs_local = 11.789265, position = 131632
bfs_local = 11.592933, position = 148876
bfs_local = 11.231969, position = 148877
bfs_local = 10.787928, position = 340701
bfs_local = 10.232833, position = 756562
bsf = 3.19888
position = 756562
DTW Calculation = 1.19%
Count of DTW = 11909
Time for calculation lower bounding = 11.23%
Time for initializing arrays with start and end segment values = 0.000%
Time for finding the best subsequence = 88.68%
Runtime of reading 0.33
Total Runtime of finding the best subsequence 24.46


number of a  series of experiments = 2
count of threads = 3

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bfs_local = 90.409081, position = 333291
bfs_local = 51.202122, position = 666582
bfs_local = 45.623787, position = 666583
bfs_local = 30.432985, position = 666585
bfs_local = 27.034298, position = 666586
bfs_local = 24.604746, position = 666587
bfs_local = 23.510569, position = 666588
bfs_local = 22.263723, position = 666589
bfs_local = 22.214788, position = 333451
bfs_local = 19.876860, position = 295
bfs_local = 19.615520, position = 298
bfs_local = 19.206821, position = 1892
bfs_local = 18.741386, position = 1893
bfs_local = 16.324242, position = 335226
bfs_local = 12.485727, position = 340696
bfs_local = 10.787928, position = 340701
bfs_local = 10.232833, position = 756562
bsf = 3.19888
position = 756562
DTW Calculation = 0.98%
Count of DTW = 9810
Time for calculation lower bounding = 27.73%
Time for initializing arrays with start and end segment values = 0.000%
Time for finding the best subsequence = 71.82%
Runtime of reading 0.33
Total Runtime of finding the best subsequence 3.38


number of a  series of experiments = 2
count of threads = 6

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bfs_local = 92.745247, position = 333292
bfs_local = 33.776402, position = 666584
bfs_local = 22.263723, position = 666589
bfs_local = 22.214788, position = 333451
bfs_local = 19.876860, position = 295
bfs_local = 19.615520, position = 298
bfs_local = 19.482819, position = 834063
bfs_local = 19.165876, position = 834066
bfs_local = 16.851923, position = 834068
bfs_local = 16.199982, position = 168474
bfs_local = 15.553727, position = 168475
bfs_local = 13.634612, position = 340697
bfs_local = 13.132357, position = 340695
bfs_local = 12.910530, position = 340698
bfs_local = 12.485727, position = 340696
bfs_local = 10.787928, position = 340701
bfs_local = 10.424694, position = 756563
bfs_local = 10.232833, position = 756562
bsf = 3.19888
position = 756562
DTW Calculation = 1.05%
Count of DTW = 10495
Time for calculation lower bounding = 41.99%
Time for initializing arrays with start and end segment values = 0.001%
Time for finding the best subsequence = 56.93%
Runtime of reading 0.33
Total Runtime of finding the best subsequence 1.18


number of a  series of experiments = 2
count of threads = 12

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bfs_local = 46.557476, position = 583261
bfs_local = 33.776402, position = 666584
bfs_local = 24.604746, position = 666587
bfs_local = 23.510569, position = 666588
bfs_local = 23.448120, position = 666590
bfs_local = 22.263723, position = 666589
bfs_local = 22.214788, position = 333451
bfs_local = 19.876860, position = 295
bfs_local = 19.615520, position = 298
bfs_local = 19.482819, position = 834063
bfs_local = 19.165876, position = 834066
bfs_local = 17.818430, position = 834067
bfs_local = 16.851923, position = 834068
bfs_local = 14.653507, position = 585034
bfs_local = 14.636871, position = 585033
bfs_local = 14.223468, position = 585036
bfs_local = 13.957302, position = 585038
bfs_local = 13.345497, position = 585037
bfs_local = 10.232833, position = 756562
bsf = 3.19888
position = 756562
DTW Calculation = 1.00%
Count of DTW = 10046
Time for calculation lower bounding = 48.97%
Time for initializing arrays with start and end segment values = 0.002%
Time for finding the best subsequence = 48.89%
Runtime of reading 0.33
Total Runtime of finding the best subsequence 0.62


number of a  series of experiments = 2
count of threads = 18

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bfs_local = 117.449219, position = 555490
bfs_local = 77.288986, position = 277745
bfs_local = 63.225170, position = 444392
bfs_local = 23.510569, position = 666588
bfs_local = 22.263723, position = 666589
bfs_local = 22.214788, position = 333451
bfs_local = 22.091293, position = 722366
bfs_local = 19.876860, position = 295
bfs_local = 19.615520, position = 298
bfs_local = 19.448748, position = 834064
bfs_local = 17.818430, position = 834067
bfs_local = 17.112631, position = 834069
bfs_local = 16.851923, position = 834068
bfs_local = 15.119696, position = 279451
bfs_local = 13.867013, position = 890569
bfs_local = 13.812093, position = 890568
bfs_local = 13.741715, position = 892349
bfs_local = 13.336525, position = 892352
bfs_local = 11.991580, position = 892351
bfs_local = 10.787928, position = 340701
bfs_local = 10.424694, position = 756563
bfs_local = 10.232833, position = 756562
bsf = 3.19888
position = 756562
DTW Calculation = 1.13%
Count of DTW = 11255
Time for calculation lower bounding = 37.68%
Time for initializing arrays with start and end segment values = 0.001%
Time for finding the best subsequence = 60.58%
Runtime of reading 0.33
Total Runtime of finding the best subsequence 0.73


number of a  series of experiments = 2
count of threads = 24

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bfs_local = 124.303406, position = 791578
bfs_local = 88.930634, position = 41662
bfs_local = 27.280378, position = 666592
bfs_local = 23.361834, position = 666597
bfs_local = 22.443714, position = 666598
bfs_local = 22.214788, position = 333451
bfs_local = 20.654917, position = 458481
bfs_local = 19.656607, position = 458485
bfs_local = 19.475924, position = 458484
bfs_local = 18.382826, position = 458487
bfs_local = 18.316355, position = 458486
bfs_local = 16.790121, position = 41959
bfs_local = 16.356503, position = 41960
bfs_local = 15.665491, position = 43101
bfs_local = 15.390733, position = 43103
bfs_local = 15.037620, position = 43104
bfs_local = 14.901890, position = 585039
bfs_local = 14.223468, position = 585036
bfs_local = 13.957302, position = 585038
bfs_local = 13.345497, position = 585037
bfs_local = 12.222793, position = 377612
bfs_local = 11.789265, position = 131632
bfs_local = 10.232833, position = 756562
bsf = 3.19888
position = 756562
DTW Calculation = 1.05%
Count of DTW = 10496
Time for calculation lower bounding = 52.10%
Time for initializing arrays with start and end segment values = 0.003%
Time for finding the best subsequence = 45.90%
Runtime of reading 0.33
Total Runtime of finding the best subsequence 0.60


