number of a  series of experiments = 0
count of threads = 1

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bfs_local = 223.352158, position = 109
bfs_local = 217.986160, position = 110
bfs_local = 202.107498, position = 111
bfs_local = 196.548157, position = 112
bfs_local = 194.698990, position = 113
bfs_local = 188.548859, position = 114
bfs_local = 181.456650, position = 115
bfs_local = 178.447006, position = 116
bfs_local = 166.090378, position = 117
bfs_local = 148.301239, position = 118
bfs_local = 148.018295, position = 122
bfs_local = 147.223465, position = 123
bfs_local = 138.667908, position = 127
bfs_local = 132.793137, position = 211
bfs_local = 124.213554, position = 212
bfs_local = 122.048347, position = 213
bfs_local = 120.039093, position = 218
bfs_local = 118.112823, position = 219
bfs_local = 115.822121, position = 220
bfs_local = 114.400940, position = 221
bfs_local = 113.001137, position = 274
bfs_local = 110.061897, position = 275
bfs_local = 97.923721, position = 276
bfs_local = 91.462502, position = 277
bfs_local = 86.409271, position = 278
bfs_local = 73.944862, position = 279
bfs_local = 66.225494, position = 280
bfs_local = 61.141834, position = 281
bfs_local = 52.753777, position = 282
bfs_local = 46.817894, position = 283
bfs_local = 42.409554, position = 284
bfs_local = 40.528980, position = 285
bfs_local = 39.020290, position = 286
bfs_local = 37.167389, position = 289
bfs_local = 34.608711, position = 290
bfs_local = 31.008766, position = 291
bfs_local = 25.376842, position = 292
bfs_local = 23.158642, position = 294
bfs_local = 19.876856, position = 295
bfs_local = 19.615494, position = 298
bfs_local = 19.206808, position = 1892
bfs_local = 18.741369, position = 1893
bfs_local = 18.128344, position = 10385
bfs_local = 13.850056, position = 12317
bfs_local = 13.612545, position = 12318
bfs_local = 11.785606, position = 131632
bfs_local = 11.555799, position = 148876
bfs_local = 11.218790, position = 148877
bfs_local = 10.712418, position = 340701
bfs_local = 10.232831, position = 756562
bsf = 3.19888
position = 756562
DTW Calculation = 1.19%
Count of DTW = 11854
Time for calculation lower bounding = 58.46%
Time for initializing arrays with start and end segment values = 0.000%
Time for finding the best subsequence = 41.25%
Runtime of reading 4.51
Total Runtime of finding the best subsequence 76.08


count of threads = 10

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bfs_local = 106.461197, position = 199976
bfs_local = 96.439774, position = 199977
bfs_local = 43.077858, position = 899893
bfs_local = 40.753334, position = 899892
bfs_local = 31.447886, position = 899988
bfs_local = 30.958160, position = 899989
bfs_local = 23.576424, position = 900150
bfs_local = 20.158697, position = 900152
bfs_local = 18.875156, position = 900153
bfs_local = 18.734680, position = 900157
bfs_local = 15.731411, position = 300317
bfs_local = 15.341487, position = 300318
bfs_local = 15.049784, position = 301548
bfs_local = 14.845078, position = 301545
bfs_local = 14.371484, position = 301549
bfs_local = 14.068690, position = 901704
bfs_local = 13.681659, position = 402642
bfs_local = 13.174680, position = 403649
bfs_local = 13.099792, position = 403650
bfs_local = 13.070073, position = 808121
bfs_local = 12.759387, position = 808126
bfs_local = 12.196528, position = 811034
bfs_local = 11.854239, position = 811032
bfs_local = 11.724231, position = 811033
bfs_local = 10.712418, position = 340701
bfs_local = 10.232831, position = 756562
bsf = 3.19888
position = 756562
DTW Calculation = 1.15%
Count of DTW = 11499
Time for calculation lower bounding = 89.25%
Time for initializing arrays with start and end segment values = 0.001%
Time for finding the best subsequence = 10.29%
Runtime of reading 4.60
Total Runtime of finding the best subsequence 4.99


count of threads = 30

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bfs_local = 115.287605, position = 66660
bfs_local = 44.577816, position = 566610
bfs_local = 43.380886, position = 566611
bfs_local = 27.126228, position = 666600
bfs_local = 25.765356, position = 333449
bfs_local = 22.911522, position = 333452
bfs_local = 22.214779, position = 333451
bfs_local = 21.106730, position = 633451
bfs_local = 20.716211, position = 633453
bfs_local = 20.615412, position = 633452
bfs_local = 17.634567, position = 566830
bfs_local = 15.962701, position = 566832
bfs_local = 15.267047, position = 566831
bfs_local = 15.182995, position = 567689
bfs_local = 15.167145, position = 567688
bfs_local = 15.049784, position = 301548
bfs_local = 14.845078, position = 301545
bfs_local = 14.371484, position = 301549
bfs_local = 14.147721, position = 235045
bfs_local = 14.068690, position = 901704
bfs_local = 13.958596, position = 968398
bfs_local = 13.586378, position = 968400
bfs_local = 12.739891, position = 968402
bfs_local = 11.789434, position = 968401
bfs_local = 10.712418, position = 340701
bfs_local = 10.232831, position = 756562
bsf = 3.19888
position = 756562
DTW Calculation = 1.13%
Count of DTW = 11271
Time for calculation lower bounding = 88.38%
Time for initializing arrays with start and end segment values = 0.003%
Time for finding the best subsequence = 11.14%
Runtime of reading 4.51
Total Runtime of finding the best subsequence 1.72


count of threads = 60

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bfs_local = 79.268631, position = 499950
bfs_local = 39.136879, position = 383296
bfs_local = 34.274632, position = 383295
bfs_local = 30.902407, position = 666601
bfs_local = 23.557411, position = 449955
bfs_local = 21.668566, position = 516747
bfs_local = 20.533031, position = 516748
bfs_local = 18.116138, position = 516749
bfs_local = 17.691706, position = 516750
bfs_local = 17.203636, position = 516751
bfs_local = 17.199057, position = 516752
bfs_local = 15.267047, position = 566831
bfs_local = 15.069548, position = 217227
bfs_local = 12.268681, position = 217229
bfs_local = 11.789434, position = 968401
bfs_local = 10.232831, position = 756562
bsf = 3.19888
position = 756562
DTW Calculation = 1.14%
Count of DTW = 11355
Time for calculation lower bounding = 80.09%
Time for initializing arrays with start and end segment values = 0.006%
Time for finding the best subsequence = 19.45%
Runtime of reading 4.50
Total Runtime of finding the best subsequence 0.99


count of threads = 120

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bfs_local = 91.034706, position = 8334
bfs_local = 89.221817, position = 966628
bfs_local = 75.734322, position = 491648
bfs_local = 66.772217, position = 708306
bfs_local = 65.232368, position = 624976
bfs_local = 40.914314, position = 224991
bfs_local = 39.433212, position = 983294
bfs_local = 39.108009, position = 224992
bfs_local = 37.726643, position = 516653
bfs_local = 31.805063, position = 224997
bfs_local = 28.721586, position = 516657
bfs_local = 28.455008, position = 683316
bfs_local = 26.774086, position = 683317
bfs_local = 25.539711, position = 924980
bfs_local = 25.513063, position = 924982
bfs_local = 24.451521, position = 16726
bfs_local = 24.278341, position = 16727
bfs_local = 22.248856, position = 516757
bfs_local = 18.116138, position = 516749
bfs_local = 17.203636, position = 516751
bfs_local = 17.199057, position = 516752
bfs_local = 17.095100, position = 566833
bfs_local = 15.267047, position = 566831
bfs_local = 15.150573, position = 391984
bfs_local = 13.015071, position = 217228
bfs_local = 12.802618, position = 217230
bfs_local = 12.268681, position = 217229
bfs_local = 12.179371, position = 892351
bfs_local = 11.789434, position = 968401
bfs_local = 11.724231, position = 811033
bfs_local = 10.560412, position = 756563
bfs_local = 10.232831, position = 756562
bsf = 3.19888
position = 756562
DTW Calculation = 1.41%
Count of DTW = 14141
Time for calculation lower bounding = 67.44%
Time for initializing arrays with start and end segment values = 0.007%
Time for finding the best subsequence = 32.19%
Runtime of reading 4.56
Total Runtime of finding the best subsequence 0.83


count of threads = 180

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bfs_local = 86.269493, position = 644380
bfs_local = 86.221375, position = 899910
bfs_local = 43.645294, position = 711041
bfs_local = 15.055966, position = 811030
bfs_local = 13.209687, position = 811031
bfs_local = 11.854239, position = 811032
bfs_local = 11.724231, position = 811033
bfs_local = 10.232831, position = 756562
bsf = 3.19888
position = 756562
DTW Calculation = 1.01%
Count of DTW = 10057
Time for calculation lower bounding = 63.35%
Time for initializing arrays with start and end segment values = 0.007%
Time for finding the best subsequence = 36.31%
Runtime of reading 4.58
Total Runtime of finding the best subsequence 0.94


count of threads = 240

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bfs_local = 78.504723, position = 475038
bfs_local = 69.397476, position = 179182
bfs_local = 69.392235, position = 470872
bfs_local = 61.258572, position = 404200
bfs_local = 29.107388, position = 145846
bfs_local = 28.328793, position = 429202
bfs_local = 27.590628, position = 429201
bfs_local = 24.459970, position = 983433
bfs_local = 24.237432, position = 754248
bfs_local = 22.214071, position = 754246
bfs_local = 17.085194, position = 754244
bfs_local = 17.076223, position = 995965
bfs_local = 15.813758, position = 620964
bfs_local = 14.838525, position = 620965
bfs_local = 14.528972, position = 287661
bfs_local = 14.139847, position = 287659
bfs_local = 13.015071, position = 217228
bfs_local = 12.802618, position = 217230
bfs_local = 12.268681, position = 217229
bfs_local = 12.179371, position = 892351
bfs_local = 11.789434, position = 968401
bfs_local = 10.232831, position = 756562
bsf = 3.19888
position = 756562
DTW Calculation = 1.36%
Count of DTW = 13565
Time for calculation lower bounding = 63.31%
Time for initializing arrays with start and end segment values = 0.007%
Time for finding the best subsequence = 36.38%
Runtime of reading 4.51
Total Runtime of finding the best subsequence 0.98


number of a  series of experiments = 1
count of threads = 1

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bfs_local = 223.352158, position = 109
bfs_local = 217.986160, position = 110
bfs_local = 202.107498, position = 111
bfs_local = 196.548157, position = 112
bfs_local = 194.698990, position = 113
bfs_local = 188.548859, position = 114
bfs_local = 181.456650, position = 115
bfs_local = 178.447006, position = 116
bfs_local = 166.090378, position = 117
bfs_local = 148.301239, position = 118
bfs_local = 148.018295, position = 122
bfs_local = 147.223465, position = 123
bfs_local = 138.667908, position = 127
bfs_local = 132.793137, position = 211
bfs_local = 124.213554, position = 212
bfs_local = 122.048347, position = 213
bfs_local = 120.039093, position = 218
bfs_local = 118.112823, position = 219
bfs_local = 115.822121, position = 220
bfs_local = 114.400940, position = 221
bfs_local = 113.001137, position = 274
bfs_local = 110.061897, position = 275
bfs_local = 97.923721, position = 276
bfs_local = 91.462502, position = 277
bfs_local = 86.409271, position = 278
bfs_local = 73.944862, position = 279
bfs_local = 66.225494, position = 280
bfs_local = 61.141834, position = 281
bfs_local = 52.753777, position = 282
bfs_local = 46.817894, position = 283
bfs_local = 42.409554, position = 284
bfs_local = 40.528980, position = 285
bfs_local = 39.020290, position = 286
bfs_local = 37.167389, position = 289
bfs_local = 34.608711, position = 290
bfs_local = 31.008766, position = 291
bfs_local = 25.376842, position = 292
bfs_local = 23.158642, position = 294
bfs_local = 19.876856, position = 295
bfs_local = 19.615494, position = 298
bfs_local = 19.206808, position = 1892
bfs_local = 18.741369, position = 1893
bfs_local = 18.128344, position = 10385
bfs_local = 13.850056, position = 12317
bfs_local = 13.612545, position = 12318
bfs_local = 11.785606, position = 131632
bfs_local = 11.555799, position = 148876
bfs_local = 11.218790, position = 148877
bfs_local = 10.712418, position = 340701
bfs_local = 10.232831, position = 756562
bsf = 3.19888
position = 756562
DTW Calculation = 1.19%
Count of DTW = 11854
Time for calculation lower bounding = 58.33%
Time for initializing arrays with start and end segment values = 0.000%
Time for finding the best subsequence = 41.38%
Runtime of reading 4.57
Total Runtime of finding the best subsequence 76.24


count of threads = 10

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bfs_local = 215.108063, position = 799904
bfs_local = 141.080246, position = 99988
bfs_local = 110.840675, position = 399952
bfs_local = 106.461197, position = 199976
bfs_local = 96.439774, position = 199977
bfs_local = 40.753334, position = 899892
bfs_local = 31.447886, position = 899988
bfs_local = 30.958160, position = 899989
bfs_local = 23.576424, position = 900150
bfs_local = 20.158697, position = 900152
bfs_local = 18.875156, position = 900153
bfs_local = 18.734680, position = 900157
bfs_local = 15.731411, position = 300317
bfs_local = 15.341487, position = 300318
bfs_local = 15.049784, position = 301548
bfs_local = 14.845078, position = 301545
bfs_local = 14.371484, position = 301549
bfs_local = 14.068690, position = 901704
bfs_local = 13.681659, position = 402642
bfs_local = 13.174680, position = 403649
bfs_local = 13.099792, position = 403650
bfs_local = 13.070073, position = 808121
bfs_local = 12.759387, position = 808126
bfs_local = 11.854239, position = 811032
bfs_local = 11.724231, position = 811033
bfs_local = 10.712418, position = 340701
bfs_local = 10.232831, position = 756562
bsf = 3.19888
position = 756562
DTW Calculation = 1.15%
Count of DTW = 11499
Time for calculation lower bounding = 89.41%
Time for initializing arrays with start and end segment values = 0.001%
Time for finding the best subsequence = 10.13%
Runtime of reading 4.53
Total Runtime of finding the best subsequence 4.98


count of threads = 30

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bfs_local = 125.139603, position = 733260
bfs_local = 44.577816, position = 566610
bfs_local = 43.380886, position = 566611
bfs_local = 39.318565, position = 466620
bfs_local = 30.902407, position = 666601
bfs_local = 27.126228, position = 666600
bfs_local = 25.765356, position = 333449
bfs_local = 23.422663, position = 333453
bfs_local = 22.911522, position = 333452
bfs_local = 22.214779, position = 333451
bfs_local = 21.106730, position = 633451
bfs_local = 20.615412, position = 633452
bfs_local = 17.634567, position = 566830
bfs_local = 15.962701, position = 566832
bfs_local = 15.267047, position = 566831
bfs_local = 15.182995, position = 567689
bfs_local = 15.167145, position = 567688
bfs_local = 15.049784, position = 301548
bfs_local = 14.845078, position = 301545
bfs_local = 14.371484, position = 301549
bfs_local = 14.147721, position = 235045
bfs_local = 14.068690, position = 901704
bfs_local = 12.739891, position = 968402
bfs_local = 11.789434, position = 968401
bfs_local = 10.712418, position = 340701
bfs_local = 10.232831, position = 756562
bsf = 3.19888
position = 756562
DTW Calculation = 1.13%
Count of DTW = 11271
Time for calculation lower bounding = 88.27%
Time for initializing arrays with start and end segment values = 0.003%
Time for finding the best subsequence = 11.25%
Runtime of reading 4.51
Total Runtime of finding the best subsequence 1.71


count of threads = 60

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bfs_local = 142.838226, position = 333301
bfs_local = 39.136879, position = 383296
bfs_local = 34.274632, position = 383295
bfs_local = 27.126228, position = 666600
bfs_local = 25.317770, position = 449956
bfs_local = 23.557411, position = 449955
bfs_local = 17.691706, position = 516750
bfs_local = 17.203636, position = 516751
bfs_local = 17.199057, position = 516752
bfs_local = 15.267047, position = 566831
bfs_local = 12.802618, position = 217230
bfs_local = 12.268681, position = 217229
bfs_local = 11.789434, position = 968401
bfs_local = 10.232831, position = 756562
bsf = 3.19888
position = 756562
DTW Calculation = 1.14%
Count of DTW = 11355
Time for calculation lower bounding = 80.13%
Time for initializing arrays with start and end segment values = 0.006%
Time for finding the best subsequence = 19.40%
Runtime of reading 4.51
Total Runtime of finding the best subsequence 0.99


count of threads = 120

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bfs_local = 91.034706, position = 8334
bfs_local = 65.232368, position = 624976
bfs_local = 40.914314, position = 224991
bfs_local = 39.433212, position = 983294
bfs_local = 39.108009, position = 224992
bfs_local = 37.726643, position = 516653
bfs_local = 30.898050, position = 516656
bfs_local = 28.721586, position = 516657
bfs_local = 28.455008, position = 683316
bfs_local = 26.774086, position = 683317
bfs_local = 25.539711, position = 924980
bfs_local = 25.513063, position = 924982
bfs_local = 24.451521, position = 16726
bfs_local = 24.278341, position = 16727
bfs_local = 20.533031, position = 516748
bfs_local = 17.203636, position = 516751
bfs_local = 17.199057, position = 516752
bfs_local = 15.267047, position = 566831
bfs_local = 15.150573, position = 391984
bfs_local = 13.015071, position = 217228
bfs_local = 12.268681, position = 217229
bfs_local = 12.179371, position = 892351
bfs_local = 11.789434, position = 968401
bfs_local = 11.724231, position = 811033
bfs_local = 10.560412, position = 756563
bfs_local = 10.232831, position = 756562
bsf = 3.19888
position = 756562
DTW Calculation = 1.41%
Count of DTW = 14141
Time for calculation lower bounding = 70.16%
Time for initializing arrays with start and end segment values = 0.007%
Time for finding the best subsequence = 29.44%
Runtime of reading 4.58
Total Runtime of finding the best subsequence 0.83


count of threads = 180

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bfs_local = 27.126228, position = 666600
bfs_local = 25.317770, position = 449956
bfs_local = 15.055966, position = 811030
bfs_local = 13.209687, position = 811031
bfs_local = 11.854239, position = 811032
bfs_local = 11.724231, position = 811033
bfs_local = 10.232831, position = 756562
bsf = 3.19888
position = 756562
DTW Calculation = 1.01%
Count of DTW = 10057
Time for calculation lower bounding = 62.04%
Time for initializing arrays with start and end segment values = 0.007%
Time for finding the best subsequence = 37.64%
Runtime of reading 4.51
Total Runtime of finding the best subsequence 0.92


count of threads = 240

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bfs_local = 194.331787, position = 520876
bfs_local = 89.655441, position = 995913
bfs_local = 80.176926, position = 666720
bfs_local = 27.590628, position = 429201
bfs_local = 24.459970, position = 983433
bfs_local = 22.214071, position = 754246
bfs_local = 17.085194, position = 754244
bfs_local = 17.076223, position = 995965
bfs_local = 15.813758, position = 620964
bfs_local = 14.838525, position = 620965
bfs_local = 14.528972, position = 287661
bfs_local = 14.139847, position = 287659
bfs_local = 13.015071, position = 217228
bfs_local = 12.268681, position = 217229
bfs_local = 12.179371, position = 892351
bfs_local = 11.789434, position = 968401
bfs_local = 10.560412, position = 756563
bfs_local = 10.232831, position = 756562
bsf = 3.19888
position = 756562
DTW Calculation = 1.36%
Count of DTW = 13565
Time for calculation lower bounding = 64.01%
Time for initializing arrays with start and end segment values = 0.007%
Time for finding the best subsequence = 35.66%
Runtime of reading 4.51
Total Runtime of finding the best subsequence 0.94


number of a  series of experiments = 2
count of threads = 1

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bfs_local = 223.352158, position = 109
bfs_local = 217.986160, position = 110
bfs_local = 202.107498, position = 111
bfs_local = 196.548157, position = 112
bfs_local = 194.698990, position = 113
bfs_local = 188.548859, position = 114
bfs_local = 181.456650, position = 115
bfs_local = 178.447006, position = 116
bfs_local = 166.090378, position = 117
bfs_local = 148.301239, position = 118
bfs_local = 148.018295, position = 122
bfs_local = 147.223465, position = 123
bfs_local = 138.667908, position = 127
bfs_local = 132.793137, position = 211
bfs_local = 124.213554, position = 212
bfs_local = 122.048347, position = 213
bfs_local = 120.039093, position = 218
bfs_local = 118.112823, position = 219
bfs_local = 115.822121, position = 220
bfs_local = 114.400940, position = 221
bfs_local = 113.001137, position = 274
bfs_local = 110.061897, position = 275
bfs_local = 97.923721, position = 276
bfs_local = 91.462502, position = 277
bfs_local = 86.409271, position = 278
bfs_local = 73.944862, position = 279
bfs_local = 66.225494, position = 280
bfs_local = 61.141834, position = 281
bfs_local = 52.753777, position = 282
bfs_local = 46.817894, position = 283
bfs_local = 42.409554, position = 284
bfs_local = 40.528980, position = 285
bfs_local = 39.020290, position = 286
bfs_local = 37.167389, position = 289
bfs_local = 34.608711, position = 290
bfs_local = 31.008766, position = 291
bfs_local = 25.376842, position = 292
bfs_local = 23.158642, position = 294
bfs_local = 19.876856, position = 295
bfs_local = 19.615494, position = 298
bfs_local = 19.206808, position = 1892
bfs_local = 18.741369, position = 1893
bfs_local = 18.128344, position = 10385
bfs_local = 13.850056, position = 12317
bfs_local = 13.612545, position = 12318
bfs_local = 11.785606, position = 131632
bfs_local = 11.555799, position = 148876
bfs_local = 11.218790, position = 148877
bfs_local = 10.712418, position = 340701
bfs_local = 10.232831, position = 756562
bsf = 3.19888
position = 756562
DTW Calculation = 1.19%
Count of DTW = 11854
Time for calculation lower bounding = 58.24%
Time for initializing arrays with start and end segment values = 0.000%
Time for finding the best subsequence = 41.47%
Runtime of reading 4.51
Total Runtime of finding the best subsequence 76.35


count of threads = 10

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bfs_local = 110.840675, position = 399952
bfs_local = 106.461197, position = 199976
bfs_local = 96.439774, position = 199977
bfs_local = 40.753334, position = 899892
bfs_local = 31.447886, position = 899988
bfs_local = 30.958160, position = 899989
bfs_local = 23.576424, position = 900150
bfs_local = 20.158697, position = 900152
bfs_local = 18.875156, position = 900153
bfs_local = 18.734680, position = 900157
bfs_local = 15.731411, position = 300317
bfs_local = 15.341487, position = 300318
bfs_local = 15.049784, position = 301548
bfs_local = 14.845078, position = 301545
bfs_local = 14.371484, position = 301549
bfs_local = 14.068690, position = 901704
bfs_local = 13.681659, position = 402642
bfs_local = 13.174680, position = 403649
bfs_local = 13.099792, position = 403650
bfs_local = 13.070073, position = 808121
bfs_local = 12.759387, position = 808126
bfs_local = 11.854239, position = 811032
bfs_local = 11.724231, position = 811033
bfs_local = 10.712418, position = 340701
bfs_local = 10.232831, position = 756562
bsf = 3.19888
position = 756562
DTW Calculation = 1.15%
Count of DTW = 11499
Time for calculation lower bounding = 89.44%
Time for initializing arrays with start and end segment values = 0.001%
Time for finding the best subsequence = 10.10%
Runtime of reading 4.54
Total Runtime of finding the best subsequence 4.98


count of threads = 30

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bfs_local = 115.287605, position = 66660
bfs_local = 44.577816, position = 566610
bfs_local = 43.380886, position = 566611
bfs_local = 27.126228, position = 666600
bfs_local = 25.765356, position = 333449
bfs_local = 22.214779, position = 333451
bfs_local = 21.106730, position = 633451
bfs_local = 20.615412, position = 633452
bfs_local = 17.634567, position = 566830
bfs_local = 15.962701, position = 566832
bfs_local = 15.267047, position = 566831
bfs_local = 15.182995, position = 567689
bfs_local = 15.167145, position = 567688
bfs_local = 15.049784, position = 301548
bfs_local = 14.845078, position = 301545
bfs_local = 14.371484, position = 301549
bfs_local = 14.147721, position = 235045
bfs_local = 14.068690, position = 901704
bfs_local = 13.958596, position = 968398
bfs_local = 13.671538, position = 968404
bfs_local = 13.586378, position = 968400
bfs_local = 12.739891, position = 968402
bfs_local = 11.789434, position = 968401
bfs_local = 10.712418, position = 340701
bfs_local = 10.560412, position = 756563
bfs_local = 10.232831, position = 756562
bsf = 3.19888
position = 756562
DTW Calculation = 1.13%
Count of DTW = 11271
Time for calculation lower bounding = 88.37%
Time for initializing arrays with start and end segment values = 0.003%
Time for finding the best subsequence = 11.15%
Runtime of reading 4.50
Total Runtime of finding the best subsequence 1.71


count of threads = 60

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bfs_local = 216.055344, position = 166650
bfs_local = 163.455872, position = 16665
bfs_local = 147.056076, position = 283305
bfs_local = 133.757965, position = 249975
bfs_local = 127.198372, position = 249976
bfs_local = 39.136879, position = 383296
bfs_local = 34.274632, position = 383295
bfs_local = 23.557411, position = 449955
bfs_local = 21.668566, position = 516747
bfs_local = 20.533031, position = 516748
bfs_local = 17.203636, position = 516751
bfs_local = 17.199057, position = 516752
bfs_local = 15.267047, position = 566831
bfs_local = 12.268681, position = 217229
bfs_local = 11.789434, position = 968401
bfs_local = 10.232831, position = 756562
bsf = 3.19888
position = 756562
DTW Calculation = 1.14%
Count of DTW = 11355
Time for calculation lower bounding = 80.20%
Time for initializing arrays with start and end segment values = 0.007%
Time for finding the best subsequence = 19.33%
Runtime of reading 4.51
Total Runtime of finding the best subsequence 0.99


count of threads = 120

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bfs_local = 158.611160, position = 16666
bfs_local = 75.734322, position = 491648
bfs_local = 74.127464, position = 741638
bfs_local = 65.232368, position = 624976
bfs_local = 40.914314, position = 224991
bfs_local = 39.433212, position = 983294
bfs_local = 39.108009, position = 224992
bfs_local = 37.726643, position = 516653
bfs_local = 37.344162, position = 674981
bfs_local = 28.721586, position = 516657
bfs_local = 26.774086, position = 683317
bfs_local = 25.539711, position = 924980
bfs_local = 25.513063, position = 924982
bfs_local = 24.451521, position = 16726
bfs_local = 24.278341, position = 16727
bfs_local = 22.248856, position = 516757
bfs_local = 20.533031, position = 516748
bfs_local = 18.116138, position = 516749
bfs_local = 17.203636, position = 516751
bfs_local = 17.199057, position = 516752
bfs_local = 15.267047, position = 566831
bfs_local = 15.150573, position = 391984
bfs_local = 13.015071, position = 217228
bfs_local = 12.268681, position = 217229
bfs_local = 12.179371, position = 892351
bfs_local = 11.789434, position = 968401
bfs_local = 11.724231, position = 811033
bfs_local = 10.560412, position = 756563
bfs_local = 10.232831, position = 756562
bsf = 3.19888
position = 756562
DTW Calculation = 1.41%
Count of DTW = 14141
Time for calculation lower bounding = 70.41%
Time for initializing arrays with start and end segment values = 0.007%
Time for finding the best subsequence = 29.17%
Runtime of reading 4.51
Total Runtime of finding the best subsequence 0.81


count of threads = 180

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bfs_local = 27.126228, position = 666600
bfs_local = 25.317770, position = 449956
bfs_local = 15.055966, position = 811030
bfs_local = 13.209687, position = 811031
bfs_local = 11.854239, position = 811032
bfs_local = 11.724231, position = 811033
bfs_local = 10.232831, position = 756562
bsf = 3.19888
position = 756562
DTW Calculation = 1.01%
Count of DTW = 10057
Time for calculation lower bounding = 64.60%
Time for initializing arrays with start and end segment values = 0.007%
Time for finding the best subsequence = 35.06%
Runtime of reading 4.59
Total Runtime of finding the best subsequence 0.87


count of threads = 240

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bfs_local = 215.641296, position = 687555
bfs_local = 105.753670, position = 912573
bfs_local = 94.779060, position = 887571
bfs_local = 81.460793, position = 633385
bfs_local = 78.504723, position = 475038
bfs_local = 29.107388, position = 145846
bfs_local = 27.590628, position = 429201
bfs_local = 24.459970, position = 983433
bfs_local = 24.237432, position = 754248
bfs_local = 22.214071, position = 754246
bfs_local = 17.085194, position = 754244
bfs_local = 17.076223, position = 995965
bfs_local = 15.813758, position = 620964
bfs_local = 14.838525, position = 620965
bfs_local = 14.611937, position = 287658
bfs_local = 14.364726, position = 287660
bfs_local = 14.139847, position = 287659
bfs_local = 13.015071, position = 217228
bfs_local = 12.802618, position = 217230
bfs_local = 12.268681, position = 217229
bfs_local = 12.179371, position = 892351
bfs_local = 11.789434, position = 968401
bfs_local = 10.232831, position = 756562
bsf = 3.19888
position = 756562
DTW Calculation = 1.36%
Count of DTW = 13565
Time for calculation lower bounding = 65.95%
Time for initializing arrays with start and end segment values = 0.007%
Time for finding the best subsequence = 33.70%
Runtime of reading 4.51
Total Runtime of finding the best subsequence 0.93


