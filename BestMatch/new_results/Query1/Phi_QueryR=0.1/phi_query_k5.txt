number of a  series of experiments = 0
count of threads = 1

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bfs_local = 223.352158, position = 109
bfs_local = 217.986160, position = 110
bfs_local = 202.107498, position = 111
bfs_local = 196.548157, position = 112
bfs_local = 194.698990, position = 113
bfs_local = 188.548859, position = 114
bfs_local = 181.456650, position = 115
bfs_local = 178.447006, position = 116
bfs_local = 166.090378, position = 117
bfs_local = 148.301239, position = 118
bfs_local = 148.018295, position = 122
bfs_local = 147.223465, position = 123
bfs_local = 138.667908, position = 127
bfs_local = 132.793137, position = 211
bfs_local = 124.213554, position = 212
bfs_local = 122.048347, position = 213
bfs_local = 120.039093, position = 218
bfs_local = 118.112823, position = 219
bfs_local = 115.822121, position = 220
bfs_local = 114.400940, position = 221
bfs_local = 113.001137, position = 274
bfs_local = 110.061897, position = 275
bfs_local = 97.923721, position = 276
bfs_local = 91.462502, position = 277
bfs_local = 86.409271, position = 278
bfs_local = 73.944862, position = 279
bfs_local = 66.225494, position = 280
bfs_local = 61.141834, position = 281
bfs_local = 52.753777, position = 282
bfs_local = 46.817894, position = 283
bfs_local = 42.409554, position = 284
bfs_local = 40.528980, position = 285
bfs_local = 39.020290, position = 286
bfs_local = 37.167389, position = 289
bfs_local = 34.608711, position = 290
bfs_local = 31.008766, position = 291
bfs_local = 25.376842, position = 292
bfs_local = 23.158642, position = 294
bfs_local = 19.876856, position = 295
bfs_local = 19.615494, position = 298
bfs_local = 19.206808, position = 1892
bfs_local = 18.741369, position = 1893
bfs_local = 18.128344, position = 10385
bfs_local = 13.850056, position = 12317
bfs_local = 13.612545, position = 12318
bfs_local = 11.785606, position = 131632
bfs_local = 11.555799, position = 148876
bfs_local = 11.218790, position = 148877
bfs_local = 10.712418, position = 340701
bfs_local = 10.232831, position = 756562
bsf = 3.19888
position = 756562
DTW Calculation = 1.19%
Count of DTW = 11854
Time for calculation lower bounding = 77.12%
Time for initializing arrays with start and end segment values = 0.000%
Time for finding the best subsequence = 22.50%
Runtime of reading 4.58
Total Runtime of finding the best subsequence 57.52


count of threads = 10

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bfs_local = 217.569016, position = 499940
bfs_local = 141.080246, position = 99988
bfs_local = 106.461197, position = 199976
bfs_local = 96.439774, position = 199977
bfs_local = 87.586914, position = 199978
bfs_local = 77.763084, position = 199980
bfs_local = 46.239803, position = 899895
bfs_local = 43.077858, position = 899893
bfs_local = 40.753334, position = 899892
bfs_local = 30.958160, position = 899989
bfs_local = 28.481806, position = 900148
bfs_local = 18.875156, position = 900153
bfs_local = 18.734680, position = 900157
bfs_local = 15.815476, position = 300319
bfs_local = 15.731411, position = 300317
bfs_local = 15.341487, position = 300318
bfs_local = 14.845078, position = 301545
bfs_local = 14.371484, position = 301549
bfs_local = 14.068690, position = 901704
bfs_local = 13.681659, position = 402642
bfs_local = 13.099792, position = 403650
bfs_local = 12.759387, position = 808126
bfs_local = 12.196528, position = 811034
bfs_local = 11.854239, position = 811032
bfs_local = 11.724231, position = 811033
bfs_local = 10.712418, position = 340701
bfs_local = 10.232831, position = 756562
bsf = 3.19888
position = 756562
DTW Calculation = 1.16%
Count of DTW = 11551
Time for calculation lower bounding = 93.53%
Time for initializing arrays with start and end segment values = 0.001%
Time for finding the best subsequence = 5.98%
Runtime of reading 4.57
Total Runtime of finding the best subsequence 4.76


count of threads = 30

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bfs_local = 125.925842, position = 33330
bfs_local = 27.126228, position = 666600
bfs_local = 22.214779, position = 333451
bfs_local = 21.106730, position = 633451
bfs_local = 20.615412, position = 633452
bfs_local = 17.634567, position = 566830
bfs_local = 15.962701, position = 566832
bfs_local = 15.267047, position = 566831
bfs_local = 15.167145, position = 567688
bfs_local = 14.371484, position = 301549
bfs_local = 14.264340, position = 901703
bfs_local = 14.147721, position = 235045
bfs_local = 14.068690, position = 901704
bfs_local = 12.739891, position = 968402
bfs_local = 11.789434, position = 968401
bfs_local = 10.712418, position = 340701
bfs_local = 10.560412, position = 756563
bfs_local = 10.232831, position = 756562
bsf = 3.19888
position = 756562
DTW Calculation = 1.14%
Count of DTW = 11407
Time for calculation lower bounding = 88.97%
Time for initializing arrays with start and end segment values = 0.003%
Time for finding the best subsequence = 10.55%
Runtime of reading 4.51
Total Runtime of finding the best subsequence 1.72


count of threads = 60

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bfs_local = 163.455872, position = 16665
bfs_local = 158.611160, position = 16666
bfs_local = 154.932129, position = 16667
bfs_local = 152.930618, position = 16668
bfs_local = 139.178345, position = 866580
bfs_local = 102.942513, position = 33334
bfs_local = 42.071224, position = 583277
bfs_local = 24.760118, position = 449958
bfs_local = 23.557411, position = 449955
bfs_local = 20.533031, position = 516748
bfs_local = 18.854218, position = 516753
bfs_local = 17.203636, position = 516751
bfs_local = 17.199057, position = 516752
bfs_local = 15.267047, position = 566831
bfs_local = 12.802618, position = 217230
bfs_local = 12.268681, position = 217229
bfs_local = 11.789434, position = 968401
bfs_local = 11.234343, position = 756564
bfs_local = 10.232831, position = 756562
bsf = 3.19888
position = 756562
DTW Calculation = 1.16%
Count of DTW = 11637
Time for calculation lower bounding = 81.31%
Time for initializing arrays with start and end segment values = 0.006%
Time for finding the best subsequence = 18.22%
Runtime of reading 4.58
Total Runtime of finding the best subsequence 1.00


count of threads = 120

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bfs_local = 67.863785, position = 524981
bfs_local = 39.108009, position = 224992
bfs_local = 35.665997, position = 224993
bfs_local = 31.011066, position = 224995
bfs_local = 30.867245, position = 683321
bfs_local = 27.174356, position = 924979
bfs_local = 26.253702, position = 141701
bfs_local = 25.539711, position = 924980
bfs_local = 25.047003, position = 141702
bfs_local = 22.214779, position = 333451
bfs_local = 18.854218, position = 516753
bfs_local = 17.691706, position = 516750
bfs_local = 17.203636, position = 516751
bfs_local = 17.199057, position = 516752
bfs_local = 15.962701, position = 566832
bfs_local = 15.267047, position = 566831
bfs_local = 15.150573, position = 391984
bfs_local = 15.069548, position = 217227
bfs_local = 12.802618, position = 217230
bfs_local = 12.268681, position = 217229
bfs_local = 12.179371, position = 892351
bfs_local = 11.789434, position = 968401
bfs_local = 11.724231, position = 811033
bfs_local = 10.232831, position = 756562
bsf = 3.19888
position = 756562
DTW Calculation = 1.49%
Count of DTW = 14860
Time for calculation lower bounding = 66.32%
Time for initializing arrays with start and end segment values = 0.007%
Time for finding the best subsequence = 33.31%
Runtime of reading 4.52
Total Runtime of finding the best subsequence 0.86


count of threads = 180

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bfs_local = 61.066895, position = 27775
bfs_local = 30.902407, position = 666601
bfs_local = 12.196528, position = 811034
bfs_local = 11.854239, position = 811032
bfs_local = 11.724231, position = 811033
bfs_local = 10.560412, position = 756563
bfs_local = 10.232831, position = 756562
bsf = 3.19888
position = 756562
DTW Calculation = 1.06%
Count of DTW = 10568
Time for calculation lower bounding = 61.86%
Time for initializing arrays with start and end segment values = 0.010%
Time for finding the best subsequence = 37.79%
Runtime of reading 4.58
Total Runtime of finding the best subsequence 0.91


count of threads = 240

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bfs_local = 137.220551, position = 254188
bfs_local = 120.967346, position = 354199
bfs_local = 109.309814, position = 70841
bfs_local = 81.659012, position = 37507
bfs_local = 39.069405, position = 120847
bfs_local = 24.754135, position = 429204
bfs_local = 22.336803, position = 429205
bfs_local = 21.106730, position = 633451
bfs_local = 20.716211, position = 633453
bfs_local = 20.199034, position = 12554
bfs_local = 18.605391, position = 754242
bfs_local = 17.887316, position = 620969
bfs_local = 17.203636, position = 516751
bfs_local = 17.085194, position = 754244
bfs_local = 16.123905, position = 620967
bfs_local = 15.905126, position = 587608
bfs_local = 15.322683, position = 620966
bfs_local = 14.838525, position = 620965
bfs_local = 14.364726, position = 287660
bfs_local = 14.139847, position = 287659
bfs_local = 12.622711, position = 892350
bfs_local = 12.179371, position = 892351
bfs_local = 11.789434, position = 968401
bfs_local = 10.232831, position = 756562
bsf = 3.19888
position = 756562
DTW Calculation = 1.53%
Count of DTW = 15303
Time for calculation lower bounding = 64.91%
Time for initializing arrays with start and end segment values = 0.007%
Time for finding the best subsequence = 34.75%
Runtime of reading 4.57
Total Runtime of finding the best subsequence 0.93


number of a  series of experiments = 1
count of threads = 1

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bfs_local = 223.352158, position = 109
bfs_local = 217.986160, position = 110
bfs_local = 202.107498, position = 111
bfs_local = 196.548157, position = 112
bfs_local = 194.698990, position = 113
bfs_local = 188.548859, position = 114
bfs_local = 181.456650, position = 115
bfs_local = 178.447006, position = 116
bfs_local = 166.090378, position = 117
bfs_local = 148.301239, position = 118
bfs_local = 148.018295, position = 122
bfs_local = 147.223465, position = 123
bfs_local = 138.667908, position = 127
bfs_local = 132.793137, position = 211
bfs_local = 124.213554, position = 212
bfs_local = 122.048347, position = 213
bfs_local = 120.039093, position = 218
bfs_local = 118.112823, position = 219
bfs_local = 115.822121, position = 220
bfs_local = 114.400940, position = 221
bfs_local = 113.001137, position = 274
bfs_local = 110.061897, position = 275
bfs_local = 97.923721, position = 276
bfs_local = 91.462502, position = 277
bfs_local = 86.409271, position = 278
bfs_local = 73.944862, position = 279
bfs_local = 66.225494, position = 280
bfs_local = 61.141834, position = 281
bfs_local = 52.753777, position = 282
bfs_local = 46.817894, position = 283
bfs_local = 42.409554, position = 284
bfs_local = 40.528980, position = 285
bfs_local = 39.020290, position = 286
bfs_local = 37.167389, position = 289
bfs_local = 34.608711, position = 290
bfs_local = 31.008766, position = 291
bfs_local = 25.376842, position = 292
bfs_local = 23.158642, position = 294
bfs_local = 19.876856, position = 295
bfs_local = 19.615494, position = 298
bfs_local = 19.206808, position = 1892
bfs_local = 18.741369, position = 1893
bfs_local = 18.128344, position = 10385
bfs_local = 13.850056, position = 12317
bfs_local = 13.612545, position = 12318
bfs_local = 11.785606, position = 131632
bfs_local = 11.555799, position = 148876
bfs_local = 11.218790, position = 148877
bfs_local = 10.712418, position = 340701
bfs_local = 10.232831, position = 756562
bsf = 3.19888
position = 756562
DTW Calculation = 1.19%
Count of DTW = 11854
Time for calculation lower bounding = 77.09%
Time for initializing arrays with start and end segment values = 0.000%
Time for finding the best subsequence = 22.53%
Runtime of reading 4.51
Total Runtime of finding the best subsequence 57.54


count of threads = 10

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bfs_local = 217.569016, position = 499940
bfs_local = 141.080246, position = 99988
bfs_local = 106.461197, position = 199976
bfs_local = 87.586914, position = 199978
bfs_local = 77.763084, position = 199980
bfs_local = 40.753334, position = 899892
bfs_local = 30.958160, position = 899989
bfs_local = 28.481806, position = 900148
bfs_local = 18.875156, position = 900153
bfs_local = 18.734680, position = 900157
bfs_local = 15.815476, position = 300319
bfs_local = 15.731411, position = 300317
bfs_local = 15.341487, position = 300318
bfs_local = 14.845078, position = 301545
bfs_local = 14.371484, position = 301549
bfs_local = 14.068690, position = 901704
bfs_local = 13.681659, position = 402642
bfs_local = 13.099792, position = 403650
bfs_local = 12.759387, position = 808126
bfs_local = 12.196528, position = 811034
bfs_local = 11.854239, position = 811032
bfs_local = 11.724231, position = 811033
bfs_local = 10.712418, position = 340701
bfs_local = 10.232831, position = 756562
bsf = 3.19888
position = 756562
DTW Calculation = 1.16%
Count of DTW = 11551
Time for calculation lower bounding = 93.52%
Time for initializing arrays with start and end segment values = 0.001%
Time for finding the best subsequence = 6.00%
Runtime of reading 4.57
Total Runtime of finding the best subsequence 4.76


count of threads = 30

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bfs_local = 27.126228, position = 666600
bfs_local = 24.715113, position = 433443
bfs_local = 22.214779, position = 333451
bfs_local = 21.106730, position = 633451
bfs_local = 20.615412, position = 633452
bfs_local = 17.634567, position = 566830
bfs_local = 15.962701, position = 566832
bfs_local = 15.267047, position = 566831
bfs_local = 15.167145, position = 567688
bfs_local = 14.371484, position = 301549
bfs_local = 14.264340, position = 901703
bfs_local = 14.147721, position = 235045
bfs_local = 14.068690, position = 901704
bfs_local = 12.739891, position = 968402
bfs_local = 11.789434, position = 968401
bfs_local = 10.712418, position = 340701
bfs_local = 10.560412, position = 756563
bfs_local = 10.232831, position = 756562
bsf = 3.19888
position = 756562
DTW Calculation = 1.14%
Count of DTW = 11407
Time for calculation lower bounding = 90.14%
Time for initializing arrays with start and end segment values = 0.003%
Time for finding the best subsequence = 9.37%
Runtime of reading 4.51
Total Runtime of finding the best subsequence 1.68


count of threads = 60

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bfs_local = 141.707092, position = 83325
bfs_local = 139.178345, position = 866580
bfs_local = 102.942513, position = 33334
bfs_local = 45.372478, position = 583279
bfs_local = 35.906540, position = 666602
bfs_local = 27.126228, position = 666600
bfs_local = 25.617512, position = 449957
bfs_local = 24.760118, position = 449958
bfs_local = 23.557411, position = 449955
bfs_local = 18.854218, position = 516753
bfs_local = 17.203636, position = 516751
bfs_local = 17.199057, position = 516752
bfs_local = 15.267047, position = 566831
bfs_local = 13.015071, position = 217228
bfs_local = 12.802618, position = 217230
bfs_local = 12.268681, position = 217229
bfs_local = 11.789434, position = 968401
bfs_local = 11.234343, position = 756564
bfs_local = 10.232831, position = 756562
bsf = 3.19888
position = 756562
DTW Calculation = 1.16%
Count of DTW = 11637
Time for calculation lower bounding = 80.70%
Time for initializing arrays with start and end segment values = 0.007%
Time for finding the best subsequence = 18.83%
Runtime of reading 4.65
Total Runtime of finding the best subsequence 0.99


count of threads = 120

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bfs_local = 67.863785, position = 524981
bfs_local = 61.906094, position = 8338
bfs_local = 54.865108, position = 624978
bfs_local = 39.108009, position = 224992
bfs_local = 35.665997, position = 224993
bfs_local = 32.767689, position = 224994
bfs_local = 31.011066, position = 224995
bfs_local = 30.867245, position = 683321
bfs_local = 25.539711, position = 924980
bfs_local = 25.047003, position = 141702
bfs_local = 18.854218, position = 516753
bfs_local = 17.691706, position = 516750
bfs_local = 17.203636, position = 516751
bfs_local = 17.199057, position = 516752
bfs_local = 15.962701, position = 566832
bfs_local = 15.267047, position = 566831
bfs_local = 15.150573, position = 391984
bfs_local = 15.069548, position = 217227
bfs_local = 12.802618, position = 217230
bfs_local = 12.268681, position = 217229
bfs_local = 12.179371, position = 892351
bfs_local = 11.789434, position = 968401
bfs_local = 11.724231, position = 811033
bfs_local = 10.232831, position = 756562
bsf = 3.19888
position = 756562
DTW Calculation = 1.49%
Count of DTW = 14860
Time for calculation lower bounding = 68.95%
Time for initializing arrays with start and end segment values = 0.011%
Time for finding the best subsequence = 30.65%
Runtime of reading 4.51
Total Runtime of finding the best subsequence 0.82


count of threads = 180

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bfs_local = 157.176620, position = 333303
bfs_local = 91.830765, position = 394408
bfs_local = 91.526642, position = 394409
bfs_local = 89.092270, position = 5559
bfs_local = 85.783470, position = 5555
bfs_local = 85.185051, position = 5557
bfs_local = 12.196528, position = 811034
bfs_local = 11.724231, position = 811033
bfs_local = 10.560412, position = 756563
bfs_local = 10.232831, position = 756562
bsf = 3.19888
position = 756562
DTW Calculation = 1.06%
Count of DTW = 10568
Time for calculation lower bounding = 63.08%
Time for initializing arrays with start and end segment values = 0.007%
Time for finding the best subsequence = 36.59%
Runtime of reading 4.58
Total Runtime of finding the best subsequence 0.89


count of threads = 240

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bfs_local = 211.841248, position = 808401
bfs_local = 188.825195, position = 133345
bfs_local = 72.356865, position = 337528
bfs_local = 24.754135, position = 429204
bfs_local = 22.336803, position = 429205
bfs_local = 20.716211, position = 633453
bfs_local = 20.025177, position = 620971
bfs_local = 19.321030, position = 995960
bfs_local = 18.605391, position = 754242
bfs_local = 15.905126, position = 587608
bfs_local = 15.322683, position = 620966
bfs_local = 14.838525, position = 620965
bfs_local = 14.611937, position = 287658
bfs_local = 14.528972, position = 287661
bfs_local = 14.364726, position = 287660
bfs_local = 14.139847, position = 287659
bfs_local = 13.015071, position = 217228
bfs_local = 12.179371, position = 892351
bfs_local = 11.789434, position = 968401
bfs_local = 10.232831, position = 756562
bsf = 3.19888
position = 756562
DTW Calculation = 1.53%
Count of DTW = 15303
Time for calculation lower bounding = 64.05%
Time for initializing arrays with start and end segment values = 0.007%
Time for finding the best subsequence = 35.62%
Runtime of reading 4.51
Total Runtime of finding the best subsequence 0.94


number of a  series of experiments = 2
count of threads = 1

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bfs_local = 223.352158, position = 109
bfs_local = 217.986160, position = 110
bfs_local = 202.107498, position = 111
bfs_local = 196.548157, position = 112
bfs_local = 194.698990, position = 113
bfs_local = 188.548859, position = 114
bfs_local = 181.456650, position = 115
bfs_local = 178.447006, position = 116
bfs_local = 166.090378, position = 117
bfs_local = 148.301239, position = 118
bfs_local = 148.018295, position = 122
bfs_local = 147.223465, position = 123
bfs_local = 138.667908, position = 127
bfs_local = 132.793137, position = 211
bfs_local = 124.213554, position = 212
bfs_local = 122.048347, position = 213
bfs_local = 120.039093, position = 218
bfs_local = 118.112823, position = 219
bfs_local = 115.822121, position = 220
bfs_local = 114.400940, position = 221
bfs_local = 113.001137, position = 274
bfs_local = 110.061897, position = 275
bfs_local = 97.923721, position = 276
bfs_local = 91.462502, position = 277
bfs_local = 86.409271, position = 278
bfs_local = 73.944862, position = 279
bfs_local = 66.225494, position = 280
bfs_local = 61.141834, position = 281
bfs_local = 52.753777, position = 282
bfs_local = 46.817894, position = 283
bfs_local = 42.409554, position = 284
bfs_local = 40.528980, position = 285
bfs_local = 39.020290, position = 286
bfs_local = 37.167389, position = 289
bfs_local = 34.608711, position = 290
bfs_local = 31.008766, position = 291
bfs_local = 25.376842, position = 292
bfs_local = 23.158642, position = 294
bfs_local = 19.876856, position = 295
bfs_local = 19.615494, position = 298
bfs_local = 19.206808, position = 1892
bfs_local = 18.741369, position = 1893
bfs_local = 18.128344, position = 10385
bfs_local = 13.850056, position = 12317
bfs_local = 13.612545, position = 12318
bfs_local = 11.785606, position = 131632
bfs_local = 11.555799, position = 148876
bfs_local = 11.218790, position = 148877
bfs_local = 10.712418, position = 340701
bfs_local = 10.232831, position = 756562
bsf = 3.19888
position = 756562
DTW Calculation = 1.19%
Count of DTW = 11854
Time for calculation lower bounding = 77.09%
Time for initializing arrays with start and end segment values = 0.000%
Time for finding the best subsequence = 22.53%
Runtime of reading 4.51
Total Runtime of finding the best subsequence 57.57


count of threads = 10

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bfs_local = 217.569016, position = 499940
bfs_local = 141.080246, position = 99988
bfs_local = 106.461197, position = 199976
bfs_local = 77.763084, position = 199980
bfs_local = 46.239803, position = 899895
bfs_local = 44.157852, position = 899894
bfs_local = 43.077858, position = 899893
bfs_local = 40.753334, position = 899892
bfs_local = 30.958160, position = 899989
bfs_local = 28.481806, position = 900148
bfs_local = 18.875156, position = 900153
bfs_local = 18.734680, position = 900157
bfs_local = 15.815476, position = 300319
bfs_local = 15.731411, position = 300317
bfs_local = 15.341487, position = 300318
bfs_local = 14.845078, position = 301545
bfs_local = 14.371484, position = 301549
bfs_local = 14.068690, position = 901704
bfs_local = 13.681659, position = 402642
bfs_local = 13.099792, position = 403650
bfs_local = 12.759387, position = 808126
bfs_local = 12.196528, position = 811034
bfs_local = 11.854239, position = 811032
bfs_local = 11.724231, position = 811033
bfs_local = 10.712418, position = 340701
bfs_local = 10.232831, position = 756562
bsf = 3.19888
position = 756562
DTW Calculation = 1.16%
Count of DTW = 11551
Time for calculation lower bounding = 93.52%
Time for initializing arrays with start and end segment values = 0.001%
Time for finding the best subsequence = 6.00%
Runtime of reading 4.51
Total Runtime of finding the best subsequence 4.77


count of threads = 30

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bfs_local = 27.126228, position = 666600
bfs_local = 24.715113, position = 433443
bfs_local = 22.214779, position = 333451
bfs_local = 21.106730, position = 633451
bfs_local = 20.615412, position = 633452
bfs_local = 17.634567, position = 566830
bfs_local = 15.962701, position = 566832
bfs_local = 15.267047, position = 566831
bfs_local = 15.167145, position = 567688
bfs_local = 14.371484, position = 301549
bfs_local = 14.264340, position = 901703
bfs_local = 14.147721, position = 235045
bfs_local = 14.068690, position = 901704
bfs_local = 12.739891, position = 968402
bfs_local = 11.789434, position = 968401
bfs_local = 10.712418, position = 340701
bfs_local = 10.560412, position = 756563
bfs_local = 10.232831, position = 756562
bsf = 3.19888
position = 756562
DTW Calculation = 1.14%
Count of DTW = 11407
Time for calculation lower bounding = 90.14%
Time for initializing arrays with start and end segment values = 0.003%
Time for finding the best subsequence = 9.37%
Runtime of reading 4.65
Total Runtime of finding the best subsequence 1.68


count of threads = 60

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bfs_local = 142.838226, position = 333301
bfs_local = 139.178345, position = 866580
bfs_local = 125.925842, position = 33330
bfs_local = 118.230629, position = 33331
bfs_local = 108.622200, position = 33333
bfs_local = 102.942513, position = 33334
bfs_local = 27.126228, position = 666600
bfs_local = 25.617512, position = 449957
bfs_local = 23.557411, position = 449955
bfs_local = 20.533031, position = 516748
bfs_local = 18.854218, position = 516753
bfs_local = 17.203636, position = 516751
bfs_local = 17.199057, position = 516752
bfs_local = 15.267047, position = 566831
bfs_local = 12.802618, position = 217230
bfs_local = 12.268681, position = 217229
bfs_local = 11.789434, position = 968401
bfs_local = 11.234343, position = 756564
bfs_local = 10.232831, position = 756562
bsf = 3.19888
position = 756562
DTW Calculation = 1.16%
Count of DTW = 11637
Time for calculation lower bounding = 77.53%
Time for initializing arrays with start and end segment values = 0.006%
Time for finding the best subsequence = 22.03%
Runtime of reading 4.57
Total Runtime of finding the best subsequence 1.04


count of threads = 120

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bfs_local = 200.110626, position = 908300
bfs_local = 142.211884, position = 833303
bfs_local = 74.644920, position = 491649
bfs_local = 39.108009, position = 224992
bfs_local = 31.011066, position = 224995
bfs_local = 30.867245, position = 683321
bfs_local = 27.174356, position = 924979
bfs_local = 26.253702, position = 141701
bfs_local = 25.539711, position = 924980
bfs_local = 25.047003, position = 141702
bfs_local = 24.515139, position = 516758
bfs_local = 18.854218, position = 516753
bfs_local = 17.691706, position = 516750
bfs_local = 17.203636, position = 516751
bfs_local = 17.199057, position = 516752
bfs_local = 15.962701, position = 566832
bfs_local = 15.267047, position = 566831
bfs_local = 15.150573, position = 391984
bfs_local = 15.069548, position = 217227
bfs_local = 12.802618, position = 217230
bfs_local = 12.268681, position = 217229
bfs_local = 12.179371, position = 892351
bfs_local = 11.789434, position = 968401
bfs_local = 11.724231, position = 811033
bfs_local = 10.232831, position = 756562
bsf = 3.19888
position = 756562
DTW Calculation = 1.49%
Count of DTW = 14860
Time for calculation lower bounding = 70.79%
Time for initializing arrays with start and end segment values = 0.008%
Time for finding the best subsequence = 28.81%
Runtime of reading 4.51
Total Runtime of finding the best subsequence 0.80


count of threads = 180

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bfs_local = 61.066895, position = 27775
bfs_local = 52.745377, position = 444401
bfs_local = 42.895462, position = 583276
bfs_local = 30.902407, position = 666601
bfs_local = 12.196528, position = 811034
bfs_local = 11.854239, position = 811032
bfs_local = 11.724231, position = 811033
bfs_local = 11.234343, position = 756564
bfs_local = 10.560412, position = 756563
bfs_local = 10.232831, position = 756562
bsf = 3.19888
position = 756562
DTW Calculation = 1.06%
Count of DTW = 10568
Time for calculation lower bounding = 62.83%
Time for initializing arrays with start and end segment values = 0.007%
Time for finding the best subsequence = 36.84%
Runtime of reading 4.51
Total Runtime of finding the best subsequence 0.92


count of threads = 240

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bfs_local = 89.502243, position = 437535
bfs_local = 67.244438, position = 704226
bfs_local = 50.028343, position = 741728
bfs_local = 24.754135, position = 429204
bfs_local = 22.336803, position = 429205
bfs_local = 20.716211, position = 633453
bfs_local = 20.025177, position = 620971
bfs_local = 19.581022, position = 900154
bfs_local = 18.605391, position = 754242
bfs_local = 17.887316, position = 620969
bfs_local = 17.591955, position = 620962
bfs_local = 17.085194, position = 754244
bfs_local = 15.905126, position = 587608
bfs_local = 15.322683, position = 620966
bfs_local = 14.838525, position = 620965
bfs_local = 14.364726, position = 287660
bfs_local = 14.139847, position = 287659
bfs_local = 13.015071, position = 217228
bfs_local = 12.622711, position = 892350
bfs_local = 12.179371, position = 892351
bfs_local = 11.789434, position = 968401
bfs_local = 10.232831, position = 756562
bsf = 3.19888
position = 756562
DTW Calculation = 1.53%
Count of DTW = 15303
Time for calculation lower bounding = 64.24%
Time for initializing arrays with start and end segment values = 0.007%
Time for finding the best subsequence = 35.44%
Runtime of reading 4.52
Total Runtime of finding the best subsequence 0.94


