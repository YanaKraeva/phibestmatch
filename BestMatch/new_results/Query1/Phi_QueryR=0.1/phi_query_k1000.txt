number of a  series of experiments = 0
count of threads = 1

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bfs_local = 223.352158, position = 109
bfs_local = 217.986160, position = 110
bfs_local = 202.107498, position = 111
bfs_local = 196.548157, position = 112
bfs_local = 194.698990, position = 113
bfs_local = 188.548859, position = 114
bfs_local = 181.456650, position = 115
bfs_local = 178.447006, position = 116
bfs_local = 166.090378, position = 117
bfs_local = 148.301239, position = 118
bfs_local = 148.018295, position = 122
bfs_local = 147.223465, position = 123
bfs_local = 138.667908, position = 127
bfs_local = 132.793137, position = 211
bfs_local = 124.213554, position = 212
bfs_local = 122.048347, position = 213
bfs_local = 120.039093, position = 218
bfs_local = 118.112823, position = 219
bfs_local = 115.822121, position = 220
bfs_local = 114.400940, position = 221
bfs_local = 113.001137, position = 274
bfs_local = 110.061897, position = 275
bfs_local = 97.923721, position = 276
bfs_local = 91.462502, position = 277
bfs_local = 86.409271, position = 278
bfs_local = 73.944862, position = 279
bfs_local = 66.225494, position = 280
bfs_local = 61.141834, position = 281
bfs_local = 52.753777, position = 282
bfs_local = 46.817894, position = 283
bfs_local = 42.409554, position = 284
bfs_local = 40.528980, position = 285
bfs_local = 39.020290, position = 286
bfs_local = 37.167389, position = 289
bfs_local = 34.608711, position = 290
bfs_local = 31.008766, position = 291
bfs_local = 25.376842, position = 292
bfs_local = 23.158642, position = 294
bfs_local = 19.876856, position = 295
bfs_local = 19.615494, position = 298
bfs_local = 19.206808, position = 1892
bfs_local = 18.741369, position = 1893
bfs_local = 18.128344, position = 10385
bfs_local = 13.850056, position = 12317
bfs_local = 13.612545, position = 12318
bfs_local = 11.785606, position = 131632
bfs_local = 11.555799, position = 148876
bfs_local = 11.218790, position = 148877
bfs_local = 10.712418, position = 340701
bfs_local = 10.232831, position = 756562
bsf = 3.19888
position = 756562
DTW Calculation = 1.31%
Count of DTW = 13136
Time for calculation lower bounding = 98.11%
Time for initializing arrays with start and end segment values = 0.000%
Time for finding the best subsequence = 1.41%
Runtime of reading 4.59
Total Runtime of finding the best subsequence 45.28


count of threads = 10

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bfs_local = 123.115593, position = 400358
bfs_local = 120.271683, position = 100191
bfs_local = 85.920746, position = 400053
bfs_local = 24.552345, position = 304
bfs_local = 22.562319, position = 200883
bfs_local = 20.375956, position = 200884
bfs_local = 20.275223, position = 200885
bfs_local = 19.299591, position = 800613
bfs_local = 16.097792, position = 800614
bfs_local = 15.731411, position = 300317
bfs_local = 15.341487, position = 300318
bfs_local = 13.850056, position = 12317
bfs_local = 13.612545, position = 12318
bfs_local = 11.785606, position = 131632
bfs_local = 11.724231, position = 811033
bfs_local = 10.712418, position = 340701
bfs_local = 10.232831, position = 756562
bsf = 3.19888
position = 756562
DTW Calculation = 2.76%
Count of DTW = 27617
Time for calculation lower bounding = 96.79%
Time for initializing arrays with start and end segment values = 0.001%
Time for finding the best subsequence = 2.71%
Runtime of reading 4.51
Total Runtime of finding the best subsequence 4.61


count of threads = 30

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bfs_local = 98.204437, position = 900481
bfs_local = 54.672604, position = 700869
bfs_local = 43.884560, position = 467392
bfs_local = 34.913322, position = 767529
bfs_local = 31.207016, position = 700635
bfs_local = 29.896526, position = 500220
bfs_local = 27.812973, position = 167623
bfs_local = 20.675138, position = 200886
bfs_local = 20.158697, position = 900152
bfs_local = 18.875156, position = 900153
bfs_local = 16.773579, position = 300316
bfs_local = 15.731411, position = 300317
bfs_local = 15.341487, position = 300318
bfs_local = 15.267047, position = 566831
bfs_local = 15.167145, position = 567688
bfs_local = 14.579066, position = 888010
bfs_local = 14.196568, position = 70810
bfs_local = 14.160030, position = 70812
bfs_local = 13.914597, position = 892348
bfs_local = 13.741701, position = 892349
bfs_local = 12.622711, position = 892350
bfs_local = 12.179371, position = 892351
bfs_local = 11.785606, position = 131632
bfs_local = 11.724231, position = 811033
bfs_local = 10.232831, position = 756562
bsf = 3.19888
position = 756562
DTW Calculation = 5.47%
Count of DTW = 54675
Time for calculation lower bounding = 89.67%
Time for initializing arrays with start and end segment values = 0.003%
Time for finding the best subsequence = 9.84%
Runtime of reading 4.51
Total Runtime of finding the best subsequence 1.68


count of threads = 60

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bfs_local = 163.455872, position = 16665
bfs_local = 100.735794, position = 216695
bfs_local = 17.434525, position = 383898
bfs_local = 17.316759, position = 383899
bfs_local = 17.203636, position = 516751
bfs_local = 15.267047, position = 566831
bfs_local = 15.069548, position = 217227
bfs_local = 13.015071, position = 217228
bfs_local = 12.268681, position = 217229
bfs_local = 12.179371, position = 892351
bfs_local = 11.821023, position = 756561
bfs_local = 10.232831, position = 756562
bsf = 3.19888
position = 756562
DTW Calculation = 7.37%
Count of DTW = 73687
Time for calculation lower bounding = 81.06%
Time for initializing arrays with start and end segment values = 0.006%
Time for finding the best subsequence = 18.48%
Runtime of reading 4.60
Total Runtime of finding the best subsequence 1.00


count of threads = 120

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bfs_local = 49.997337, position = 784041
bfs_local = 41.889427, position = 467404
bfs_local = 38.892189, position = 225933
bfs_local = 29.254589, position = 792602
bfs_local = 25.731209, position = 517402
bfs_local = 24.788448, position = 592038
bfs_local = 23.906906, position = 850369
bfs_local = 22.121580, position = 708901
bfs_local = 18.054392, position = 709061
bfs_local = 15.457510, position = 850361
bfs_local = 15.357864, position = 850362
bfs_local = 12.802618, position = 217230
bfs_local = 12.622711, position = 892350
bfs_local = 12.179371, position = 892351
bfs_local = 11.882188, position = 148879
bfs_local = 10.232831, position = 756562
bsf = 3.19888
position = 756562
DTW Calculation = 13.26%
Count of DTW = 132551
Time for calculation lower bounding = 65.69%
Time for initializing arrays with start and end segment values = 0.008%
Time for finding the best subsequence = 33.94%
Runtime of reading 4.66
Total Runtime of finding the best subsequence 0.86


count of threads = 180

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bfs_local = 223.132202, position = 623032
bfs_local = 165.040543, position = 461629
bfs_local = 61.900711, position = 722351
bfs_local = 61.889729, position = 344963
bfs_local = 58.221199, position = 489013
bfs_local = 43.428623, position = 50448
bfs_local = 36.803349, position = 308
bfs_local = 18.644165, position = 800618
bfs_local = 14.089340, position = 845333
bfs_local = 13.955890, position = 72802
bfs_local = 13.492498, position = 811035
bfs_local = 13.209687, position = 811031
bfs_local = 13.015071, position = 217228
bfs_local = 11.854239, position = 811032
bfs_local = 11.724231, position = 811033
bfs_local = 11.218790, position = 148877
bfs_local = 10.232831, position = 756562
bsf = 3.19888
position = 756562
DTW Calculation = 19.08%
Count of DTW = 190771
Time for calculation lower bounding = 62.85%
Time for initializing arrays with start and end segment values = 0.007%
Time for finding the best subsequence = 36.81%
Runtime of reading 4.65
Total Runtime of finding the best subsequence 0.94


count of threads = 240

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bfs_local = 201.870880, position = 300805
bfs_local = 103.213264, position = 776015
bfs_local = 63.399899, position = 192102
bfs_local = 31.023821, position = 496742
bfs_local = 30.475760, position = 187519
bfs_local = 29.374174, position = 712746
bfs_local = 23.309986, position = 559058
bfs_local = 17.900438, position = 754243
bfs_local = 15.150573, position = 391984
bfs_local = 14.611937, position = 287658
bfs_local = 12.802618, position = 217230
bfs_local = 12.179371, position = 892351
bfs_local = 11.785606, position = 131632
bfs_local = 10.232831, position = 756562
bsf = 3.19888
position = 756562
DTW Calculation = 25.09%
Count of DTW = 250861
Time for calculation lower bounding = 60.09%
Time for initializing arrays with start and end segment values = 0.007%
Time for finding the best subsequence = 39.65%
Runtime of reading 4.52
Total Runtime of finding the best subsequence 1.01


number of a  series of experiments = 1
count of threads = 1

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bfs_local = 223.352158, position = 109
bfs_local = 217.986160, position = 110
bfs_local = 202.107498, position = 111
bfs_local = 196.548157, position = 112
bfs_local = 194.698990, position = 113
bfs_local = 188.548859, position = 114
bfs_local = 181.456650, position = 115
bfs_local = 178.447006, position = 116
bfs_local = 166.090378, position = 117
bfs_local = 148.301239, position = 118
bfs_local = 148.018295, position = 122
bfs_local = 147.223465, position = 123
bfs_local = 138.667908, position = 127
bfs_local = 132.793137, position = 211
bfs_local = 124.213554, position = 212
bfs_local = 122.048347, position = 213
bfs_local = 120.039093, position = 218
bfs_local = 118.112823, position = 219
bfs_local = 115.822121, position = 220
bfs_local = 114.400940, position = 221
bfs_local = 113.001137, position = 274
bfs_local = 110.061897, position = 275
bfs_local = 97.923721, position = 276
bfs_local = 91.462502, position = 277
bfs_local = 86.409271, position = 278
bfs_local = 73.944862, position = 279
bfs_local = 66.225494, position = 280
bfs_local = 61.141834, position = 281
bfs_local = 52.753777, position = 282
bfs_local = 46.817894, position = 283
bfs_local = 42.409554, position = 284
bfs_local = 40.528980, position = 285
bfs_local = 39.020290, position = 286
bfs_local = 37.167389, position = 289
bfs_local = 34.608711, position = 290
bfs_local = 31.008766, position = 291
bfs_local = 25.376842, position = 292
bfs_local = 23.158642, position = 294
bfs_local = 19.876856, position = 295
bfs_local = 19.615494, position = 298
bfs_local = 19.206808, position = 1892
bfs_local = 18.741369, position = 1893
bfs_local = 18.128344, position = 10385
bfs_local = 13.850056, position = 12317
bfs_local = 13.612545, position = 12318
bfs_local = 11.785606, position = 131632
bfs_local = 11.555799, position = 148876
bfs_local = 11.218790, position = 148877
bfs_local = 10.712418, position = 340701
bfs_local = 10.232831, position = 756562
bsf = 3.19888
position = 756562
DTW Calculation = 1.31%
Count of DTW = 13136
Time for calculation lower bounding = 98.11%
Time for initializing arrays with start and end segment values = 0.000%
Time for finding the best subsequence = 1.42%
Runtime of reading 4.51
Total Runtime of finding the best subsequence 45.28


count of threads = 10

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bfs_local = 85.920746, position = 400053
bfs_local = 71.159660, position = 700822
bfs_local = 69.400627, position = 400155
bfs_local = 48.495167, position = 899993
bfs_local = 40.753334, position = 899892
bfs_local = 24.552345, position = 304
bfs_local = 22.562319, position = 200883
bfs_local = 20.375956, position = 200884
bfs_local = 20.275223, position = 200885
bfs_local = 19.299591, position = 800613
bfs_local = 16.097792, position = 800614
bfs_local = 15.731411, position = 300317
bfs_local = 15.341487, position = 300318
bfs_local = 13.850056, position = 12317
bfs_local = 13.612545, position = 12318
bfs_local = 11.785606, position = 131632
bfs_local = 11.724231, position = 811033
bfs_local = 10.712418, position = 340701
bfs_local = 10.232831, position = 756562
bsf = 3.19888
position = 756562
DTW Calculation = 2.76%
Count of DTW = 27617
Time for calculation lower bounding = 96.60%
Time for initializing arrays with start and end segment values = 0.001%
Time for finding the best subsequence = 2.90%
Runtime of reading 4.58
Total Runtime of finding the best subsequence 4.61


count of threads = 30

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bfs_local = 60.438541, position = 266774
bfs_local = 54.672604, position = 700869
bfs_local = 51.639786, position = 300104
bfs_local = 43.884560, position = 467392
bfs_local = 39.433998, position = 566812
bfs_local = 39.318565, position = 466620
bfs_local = 34.913322, position = 767529
bfs_local = 24.552345, position = 304
bfs_local = 20.675138, position = 200886
bfs_local = 20.158697, position = 900152
bfs_local = 19.482813, position = 834063
bfs_local = 18.875156, position = 900153
bfs_local = 16.773579, position = 300316
bfs_local = 15.731411, position = 300317
bfs_local = 15.341487, position = 300318
bfs_local = 15.267047, position = 566831
bfs_local = 15.167145, position = 567688
bfs_local = 14.579066, position = 888010
bfs_local = 14.196568, position = 70810
bfs_local = 13.914597, position = 892348
bfs_local = 13.741701, position = 892349
bfs_local = 12.622711, position = 892350
bfs_local = 12.179371, position = 892351
bfs_local = 11.785606, position = 131632
bfs_local = 11.724231, position = 811033
bfs_local = 10.232831, position = 756562
bsf = 3.19888
position = 756562
DTW Calculation = 5.47%
Count of DTW = 54675
Time for calculation lower bounding = 90.79%
Time for initializing arrays with start and end segment values = 0.003%
Time for finding the best subsequence = 8.71%
Runtime of reading 4.51
Total Runtime of finding the best subsequence 1.66


count of threads = 60

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bfs_local = 93.563141, position = 400161
bfs_local = 17.434525, position = 383898
bfs_local = 17.316759, position = 383899
bfs_local = 17.203636, position = 516751
bfs_local = 15.267047, position = 566831
bfs_local = 15.069548, position = 217227
bfs_local = 13.015071, position = 217228
bfs_local = 12.268681, position = 217229
bfs_local = 12.179371, position = 892351
bfs_local = 11.821023, position = 756561
bfs_local = 10.232831, position = 756562
bsf = 3.19888
position = 756562
DTW Calculation = 7.37%
Count of DTW = 73687
Time for calculation lower bounding = 81.12%
Time for initializing arrays with start and end segment values = 0.007%
Time for finding the best subsequence = 18.42%
Runtime of reading 4.51
Total Runtime of finding the best subsequence 0.98


count of threads = 120

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bfs_local = 219.702484, position = 42161
bfs_local = 190.831757, position = 692420
bfs_local = 152.176834, position = 792038
bfs_local = 110.927376, position = 734237
bfs_local = 81.121437, position = 316907
bfs_local = 65.584488, position = 808813
bfs_local = 65.016029, position = 458710
bfs_local = 64.353905, position = 50991
bfs_local = 50.803036, position = 417246
bfs_local = 41.889427, position = 467404
bfs_local = 34.189785, position = 834140
bfs_local = 32.361095, position = 766846
bfs_local = 30.191822, position = 283977
bfs_local = 29.254589, position = 792602
bfs_local = 25.731209, position = 517402
bfs_local = 24.788448, position = 592038
bfs_local = 23.906906, position = 850369
bfs_local = 21.180243, position = 566829
bfs_local = 18.054392, position = 709061
bfs_local = 15.457510, position = 850361
bfs_local = 15.357864, position = 850362
bfs_local = 12.802618, position = 217230
bfs_local = 12.622711, position = 892350
bfs_local = 12.179371, position = 892351
bfs_local = 11.882188, position = 148879
bfs_local = 10.232831, position = 756562
bsf = 3.19888
position = 756562
DTW Calculation = 13.26%
Count of DTW = 132551
Time for calculation lower bounding = 69.22%
Time for initializing arrays with start and end segment values = 0.008%
Time for finding the best subsequence = 30.38%
Runtime of reading 4.51
Total Runtime of finding the best subsequence 0.82


count of threads = 180

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bfs_local = 80.509216, position = 383971
bfs_local = 76.647911, position = 78665
bfs_local = 64.886948, position = 805659
bfs_local = 48.052364, position = 645196
bfs_local = 18.644165, position = 800618
bfs_local = 14.089340, position = 845333
bfs_local = 14.057554, position = 744862
bfs_local = 13.955890, position = 72802
bfs_local = 13.492498, position = 811035
bfs_local = 13.209687, position = 811031
bfs_local = 13.015071, position = 217228
bfs_local = 11.854239, position = 811032
bfs_local = 11.724231, position = 811033
bfs_local = 11.218790, position = 148877
bfs_local = 10.232831, position = 756562
bsf = 3.19888
position = 756562
DTW Calculation = 19.08%
Count of DTW = 190771
Time for calculation lower bounding = 62.47%
Time for initializing arrays with start and end segment values = 0.007%
Time for finding the best subsequence = 37.19%
Runtime of reading 4.65
Total Runtime of finding the best subsequence 0.92


count of threads = 240

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bfs_local = 133.295441, position = 837823
bfs_local = 28.583227, position = 592209
bfs_local = 25.851358, position = 154796
bfs_local = 23.309986, position = 559058
bfs_local = 21.269825, position = 629645
bfs_local = 18.722910, position = 834067
bfs_local = 17.900438, position = 754243
bfs_local = 15.886868, position = 892346
bfs_local = 15.568241, position = 29325
bfs_local = 15.150573, position = 391984
bfs_local = 12.802618, position = 217230
bfs_local = 12.179371, position = 892351
bfs_local = 11.785606, position = 131632
bfs_local = 10.232831, position = 756562
bsf = 3.19888
position = 756562
DTW Calculation = 25.09%
Count of DTW = 250861
Time for calculation lower bounding = 63.25%
Time for initializing arrays with start and end segment values = 0.009%
Time for finding the best subsequence = 36.41%
Runtime of reading 4.51
Total Runtime of finding the best subsequence 0.95


number of a  series of experiments = 2
count of threads = 1

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bfs_local = 223.352158, position = 109
bfs_local = 217.986160, position = 110
bfs_local = 202.107498, position = 111
bfs_local = 196.548157, position = 112
bfs_local = 194.698990, position = 113
bfs_local = 188.548859, position = 114
bfs_local = 181.456650, position = 115
bfs_local = 178.447006, position = 116
bfs_local = 166.090378, position = 117
bfs_local = 148.301239, position = 118
bfs_local = 148.018295, position = 122
bfs_local = 147.223465, position = 123
bfs_local = 138.667908, position = 127
bfs_local = 132.793137, position = 211
bfs_local = 124.213554, position = 212
bfs_local = 122.048347, position = 213
bfs_local = 120.039093, position = 218
bfs_local = 118.112823, position = 219
bfs_local = 115.822121, position = 220
bfs_local = 114.400940, position = 221
bfs_local = 113.001137, position = 274
bfs_local = 110.061897, position = 275
bfs_local = 97.923721, position = 276
bfs_local = 91.462502, position = 277
bfs_local = 86.409271, position = 278
bfs_local = 73.944862, position = 279
bfs_local = 66.225494, position = 280
bfs_local = 61.141834, position = 281
bfs_local = 52.753777, position = 282
bfs_local = 46.817894, position = 283
bfs_local = 42.409554, position = 284
bfs_local = 40.528980, position = 285
bfs_local = 39.020290, position = 286
bfs_local = 37.167389, position = 289
bfs_local = 34.608711, position = 290
bfs_local = 31.008766, position = 291
bfs_local = 25.376842, position = 292
bfs_local = 23.158642, position = 294
bfs_local = 19.876856, position = 295
bfs_local = 19.615494, position = 298
bfs_local = 19.206808, position = 1892
bfs_local = 18.741369, position = 1893
bfs_local = 18.128344, position = 10385
bfs_local = 13.850056, position = 12317
bfs_local = 13.612545, position = 12318
bfs_local = 11.785606, position = 131632
bfs_local = 11.555799, position = 148876
bfs_local = 11.218790, position = 148877
bfs_local = 10.712418, position = 340701
bfs_local = 10.232831, position = 756562
bsf = 3.19888
position = 756562
DTW Calculation = 1.31%
Count of DTW = 13136
Time for calculation lower bounding = 98.10%
Time for initializing arrays with start and end segment values = 0.000%
Time for finding the best subsequence = 1.42%
Runtime of reading 4.51
Total Runtime of finding the best subsequence 45.26


count of threads = 10

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bfs_local = 225.493912, position = 400758
bfs_local = 123.115593, position = 400358
bfs_local = 120.271683, position = 100191
bfs_local = 85.920746, position = 400053
bfs_local = 77.361855, position = 600734
bfs_local = 71.159660, position = 700822
bfs_local = 24.552345, position = 304
bfs_local = 22.562319, position = 200883
bfs_local = 20.375956, position = 200884
bfs_local = 20.275223, position = 200885
bfs_local = 19.299591, position = 800613
bfs_local = 16.097792, position = 800614
bfs_local = 15.731411, position = 300317
bfs_local = 15.341487, position = 300318
bfs_local = 13.850056, position = 12317
bfs_local = 13.612545, position = 12318
bfs_local = 11.785606, position = 131632
bfs_local = 11.724231, position = 811033
bfs_local = 10.712418, position = 340701
bfs_local = 10.232831, position = 756562
bsf = 3.19888
position = 756562
DTW Calculation = 2.76%
Count of DTW = 27617
Time for calculation lower bounding = 96.80%
Time for initializing arrays with start and end segment values = 0.001%
Time for finding the best subsequence = 2.70%
Runtime of reading 4.51
Total Runtime of finding the best subsequence 4.61


count of threads = 30

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bfs_local = 139.947784, position = 933340
bfs_local = 60.438541, position = 266774
bfs_local = 54.672604, position = 700869
bfs_local = 51.639786, position = 300104
bfs_local = 29.896526, position = 500220
bfs_local = 27.126228, position = 666600
bfs_local = 24.552345, position = 304
bfs_local = 20.675138, position = 200886
bfs_local = 20.158697, position = 900152
bfs_local = 18.875156, position = 900153
bfs_local = 16.773579, position = 300316
bfs_local = 15.731411, position = 300317
bfs_local = 15.341487, position = 300318
bfs_local = 15.267047, position = 566831
bfs_local = 15.167145, position = 567688
bfs_local = 14.579066, position = 888010
bfs_local = 14.196568, position = 70810
bfs_local = 13.914597, position = 892348
bfs_local = 13.741701, position = 892349
bfs_local = 12.622711, position = 892350
bfs_local = 12.179371, position = 892351
bfs_local = 11.785606, position = 131632
bfs_local = 11.724231, position = 811033
bfs_local = 10.232831, position = 756562
bsf = 3.19888
position = 756562
DTW Calculation = 5.47%
Count of DTW = 54675
Time for calculation lower bounding = 90.82%
Time for initializing arrays with start and end segment values = 0.003%
Time for finding the best subsequence = 8.68%
Runtime of reading 4.51
Total Runtime of finding the best subsequence 1.67


count of threads = 60

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bfs_local = 84.952858, position = 666616
bfs_local = 17.434525, position = 383898
bfs_local = 17.316759, position = 383899
bfs_local = 17.203636, position = 516751
bfs_local = 15.267047, position = 566831
bfs_local = 15.069548, position = 217227
bfs_local = 13.015071, position = 217228
bfs_local = 12.268681, position = 217229
bfs_local = 12.179371, position = 892351
bfs_local = 11.821023, position = 756561
bfs_local = 10.232831, position = 756562
bsf = 3.19888
position = 756562
DTW Calculation = 7.37%
Count of DTW = 73687
Time for calculation lower bounding = 77.27%
Time for initializing arrays with start and end segment values = 0.006%
Time for finding the best subsequence = 22.27%
Runtime of reading 4.66
Total Runtime of finding the best subsequence 1.03


count of threads = 120

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bfs_local = 80.939270, position = 417154
bfs_local = 50.803036, position = 417246
bfs_local = 41.889427, position = 467404
bfs_local = 32.361095, position = 766846
bfs_local = 30.191822, position = 283977
bfs_local = 29.254589, position = 792602
bfs_local = 25.731209, position = 517402
bfs_local = 24.788448, position = 592038
bfs_local = 22.121580, position = 708901
bfs_local = 18.054392, position = 709061
bfs_local = 15.457510, position = 850361
bfs_local = 15.357864, position = 850362
bfs_local = 12.802618, position = 217230
bfs_local = 12.622711, position = 892350
bfs_local = 12.179371, position = 892351
bfs_local = 11.882188, position = 148879
bfs_local = 10.232831, position = 756562
bsf = 3.19888
position = 756562
DTW Calculation = 13.26%
Count of DTW = 132551
Time for calculation lower bounding = 70.85%
Time for initializing arrays with start and end segment values = 0.009%
Time for finding the best subsequence = 28.75%
Runtime of reading 4.51
Total Runtime of finding the best subsequence 0.80


count of threads = 180

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bfs_local = 83.881027, position = 283635
bfs_local = 52.889053, position = 850211
bfs_local = 48.052364, position = 645196
bfs_local = 18.644165, position = 800618
bfs_local = 14.089340, position = 845333
bfs_local = 14.057554, position = 744862
bfs_local = 13.955890, position = 72802
bfs_local = 13.492498, position = 811035
bfs_local = 13.209687, position = 811031
bfs_local = 13.015071, position = 217228
bfs_local = 11.854239, position = 811032
bfs_local = 11.724231, position = 811033
bfs_local = 11.218790, position = 148877
bfs_local = 10.232831, position = 756562
bsf = 3.19888
position = 756562
DTW Calculation = 19.08%
Count of DTW = 190771
Time for calculation lower bounding = 61.72%
Time for initializing arrays with start and end segment values = 0.007%
Time for finding the best subsequence = 37.94%
Runtime of reading 4.57
Total Runtime of finding the best subsequence 0.92


count of threads = 240

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bfs_local = 95.262589, position = 592054
bfs_local = 21.985638, position = 720991
bfs_local = 18.722910, position = 834067
bfs_local = 17.900438, position = 754243
bfs_local = 15.886868, position = 892346
bfs_local = 15.568241, position = 29325
bfs_local = 15.150573, position = 391984
bfs_local = 12.802618, position = 217230
bfs_local = 12.179371, position = 892351
bfs_local = 11.785606, position = 131632
bfs_local = 10.232831, position = 756562
bsf = 3.19888
position = 756562
DTW Calculation = 25.09%
Count of DTW = 250861
Time for calculation lower bounding = 63.55%
Time for initializing arrays with start and end segment values = 0.007%
Time for finding the best subsequence = 36.15%
Runtime of reading 4.59
Total Runtime of finding the best subsequence 0.97


