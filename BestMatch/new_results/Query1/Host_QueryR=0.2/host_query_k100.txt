number of a  series of experiments = 0
count of threads = 1

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bfs_local = 205.161011, position = 68
bfs_local = 201.296082, position = 109
bfs_local = 198.454117, position = 110
bfs_local = 181.586792, position = 111
bfs_local = 177.698975, position = 112
bfs_local = 177.272400, position = 113
bfs_local = 173.585785, position = 114
bfs_local = 168.219513, position = 115
bfs_local = 166.177582, position = 116
bfs_local = 153.459076, position = 117
bfs_local = 137.000320, position = 118
bfs_local = 134.529343, position = 122
bfs_local = 128.011978, position = 123
bfs_local = 121.794434, position = 124
bfs_local = 115.587097, position = 125
bfs_local = 106.061104, position = 126
bfs_local = 90.850655, position = 127
bfs_local = 90.639030, position = 129
bfs_local = 88.713707, position = 130
bfs_local = 87.106270, position = 209
bfs_local = 86.763458, position = 210
bfs_local = 84.332901, position = 211
bfs_local = 82.125458, position = 212
bfs_local = 66.927139, position = 276
bfs_local = 57.716461, position = 277
bfs_local = 51.453182, position = 278
bfs_local = 39.160839, position = 279
bfs_local = 31.622044, position = 280
bfs_local = 28.650475, position = 281
bfs_local = 23.193090, position = 282
bfs_local = 18.942255, position = 283
bfs_local = 16.442329, position = 284
bfs_local = 14.960269, position = 285
bfs_local = 14.029958, position = 286
bfs_local = 13.455294, position = 12317
bfs_local = 13.343624, position = 12318
bfs_local = 11.353244, position = 25846
bfs_local = 11.098734, position = 43099
bfs_local = 10.341493, position = 131638
bfs_local = 10.244247, position = 131639
bfs_local = 10.189466, position = 148874
bfs_local = 9.236251, position = 148875
bfs_local = 8.076555, position = 148876
bsf = 2.84193
position = 148876
DTW Calculation = 3.24%
Count of DTW = 32380
Time for calculation lower bounding = 87.59%
Time for initializing arrays with start and end segment values = 0.000%
Time for finding the best subsequence = 12.05%
Runtime of reading 0.33
Total Runtime of finding the best subsequence 6.49


number of a  series of experiments = 0
count of threads = 3

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bfs_local = 134.735672, position = 666648
bfs_local = 76.542137, position = 333291
bfs_local = 49.199272, position = 666615
bfs_local = 44.607243, position = 333358
bfs_local = 31.532650, position = 666582
bfs_local = 30.613543, position = 666583
bfs_local = 23.162945, position = 666584
bfs_local = 21.950909, position = 666589
bfs_local = 21.757963, position = 666590
bfs_local = 20.977331, position = 666591
bfs_local = 20.798550, position = 666592
bfs_local = 20.595728, position = 666600
bfs_local = 20.485851, position = 666601
bfs_local = 18.942255, position = 283
bfs_local = 16.442329, position = 284
bfs_local = 14.960269, position = 285
bfs_local = 14.029958, position = 286
bfs_local = 13.050027, position = 335233
bfs_local = 11.187132, position = 340695
bfs_local = 10.858139, position = 340696
bfs_local = 10.625361, position = 340701
bfs_local = 10.232833, position = 756562
bfs_local = 10.189466, position = 148874
bfs_local = 9.236251, position = 148875
bfs_local = 8.076555, position = 148876
bsf = 2.84193
position = 148876
DTW Calculation = 3.71%
Count of DTW = 37068
Time for calculation lower bounding = 89.88%
Time for initializing arrays with start and end segment values = 0.000%
Time for finding the best subsequence = 9.38%
Runtime of reading 0.33
Total Runtime of finding the best subsequence 2.17


number of a  series of experiments = 0
count of threads = 6

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bfs_local = 75.532822, position = 333375
bfs_local = 20.595728, position = 666600
bfs_local = 20.485851, position = 666601
bfs_local = 19.961882, position = 500208
bfs_local = 18.956606, position = 500209
bfs_local = 18.748430, position = 500210
bfs_local = 16.442329, position = 284
bfs_local = 14.960269, position = 285
bfs_local = 14.029958, position = 286
bfs_local = 13.804946, position = 168474
bfs_local = 12.684419, position = 168475
bfs_local = 11.187132, position = 340695
bfs_local = 10.858139, position = 340696
bfs_local = 10.625361, position = 340701
bfs_local = 9.738815, position = 874191
bfs_local = 9.735808, position = 874194
bfs_local = 9.441595, position = 295832
bfs_local = 9.236251, position = 148875
bfs_local = 8.076555, position = 148876
bsf = 2.84193
position = 148876
DTW Calculation = 4.24%
Count of DTW = 42392
Time for calculation lower bounding = 90.18%
Time for initializing arrays with start and end segment values = 0.001%
Time for finding the best subsequence = 8.46%
Runtime of reading 0.33
Total Runtime of finding the best subsequence 1.10


number of a  series of experiments = 0
count of threads = 12

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bfs_local = 100.592209, position = 333300
bfs_local = 84.989212, position = 666625
bfs_local = 75.532822, position = 333375
bfs_local = 20.595728, position = 666600
bfs_local = 20.485851, position = 666601
bfs_local = 15.418200, position = 291
bfs_local = 14.469719, position = 292
bfs_local = 14.203892, position = 750551
bfs_local = 13.208969, position = 750552
bfs_local = 13.008174, position = 585037
bfs_local = 12.684419, position = 168475
bfs_local = 11.817601, position = 756002
bfs_local = 11.187132, position = 340695
bfs_local = 10.858139, position = 340696
bfs_local = 10.232833, position = 756562
bfs_local = 10.107504, position = 286687
bfs_local = 9.738815, position = 874191
bfs_local = 9.735808, position = 874194
bfs_local = 9.441595, position = 295832
bfs_local = 9.236251, position = 148875
bfs_local = 8.076555, position = 148876
bsf = 2.84193
position = 148876
DTW Calculation = 4.31%
Count of DTW = 43062
Time for calculation lower bounding = 93.76%
Time for initializing arrays with start and end segment values = 0.001%
Time for finding the best subsequence = 4.84%
Runtime of reading 0.33
Total Runtime of finding the best subsequence 1.12


number of a  series of experiments = 0
count of threads = 18

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bfs_local = 126.561592, position = 666654
bfs_local = 87.260231, position = 55549
bfs_local = 44.082752, position = 722203
bfs_local = 30.709898, position = 666604
bfs_local = 24.874847, position = 222263
bfs_local = 21.798094, position = 666593
bfs_local = 20.968914, position = 666599
bfs_local = 15.445699, position = 222291
bfs_local = 14.960269, position = 285
bfs_local = 14.029958, position = 286
bfs_local = 13.109679, position = 890566
bfs_local = 12.614532, position = 890567
bfs_local = 12.438791, position = 279451
bfs_local = 11.600005, position = 279452
bfs_local = 10.858139, position = 340696
bfs_local = 10.625361, position = 340701
bfs_local = 10.107504, position = 286687
bfs_local = 9.441595, position = 295832
bfs_local = 9.236251, position = 148875
bfs_local = 8.076555, position = 148876
bsf = 2.84193
position = 148876
DTW Calculation = 4.14%
Count of DTW = 41428
Time for calculation lower bounding = 89.92%
Time for initializing arrays with start and end segment values = 0.001%
Time for finding the best subsequence = 7.90%
Runtime of reading 0.33
Total Runtime of finding the best subsequence 0.66


number of a  series of experiments = 0
count of threads = 24

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bfs_local = 176.680191, position = 166702
bfs_local = 51.565041, position = 41712
bfs_local = 29.274092, position = 41683
bfs_local = 28.856976, position = 874989
bfs_local = 27.839424, position = 874990
bfs_local = 26.762644, position = 291634
bfs_local = 26.203970, position = 41679
bfs_local = 20.798550, position = 666592
bfs_local = 20.595728, position = 666600
bfs_local = 20.485851, position = 666601
bfs_local = 18.515331, position = 41958
bfs_local = 17.761095, position = 458479
bfs_local = 17.290022, position = 458481
bfs_local = 16.790121, position = 41959
bfs_local = 16.356503, position = 41960
bfs_local = 14.203892, position = 750551
bfs_local = 13.208969, position = 750552
bfs_local = 11.098734, position = 43099
bfs_local = 10.144341, position = 295831
bfs_local = 9.441595, position = 295832
bfs_local = 9.236251, position = 148875
bfs_local = 8.076555, position = 148876
bsf = 2.84193
position = 148876
DTW Calculation = 3.99%
Count of DTW = 39920
Time for calculation lower bounding = 90.08%
Time for initializing arrays with start and end segment values = 0.173%
Time for finding the best subsequence = 7.80%
Runtime of reading 0.33
Total Runtime of finding the best subsequence 0.79


number of a  series of experiments = 1
count of threads = 1

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bfs_local = 205.161011, position = 68
bfs_local = 201.296082, position = 109
bfs_local = 198.454117, position = 110
bfs_local = 181.586792, position = 111
bfs_local = 177.698975, position = 112
bfs_local = 177.272400, position = 113
bfs_local = 173.585785, position = 114
bfs_local = 168.219513, position = 115
bfs_local = 166.177582, position = 116
bfs_local = 153.459076, position = 117
bfs_local = 137.000320, position = 118
bfs_local = 134.529343, position = 122
bfs_local = 128.011978, position = 123
bfs_local = 121.794434, position = 124
bfs_local = 115.587097, position = 125
bfs_local = 106.061104, position = 126
bfs_local = 90.850655, position = 127
bfs_local = 90.639030, position = 129
bfs_local = 88.713707, position = 130
bfs_local = 87.106270, position = 209
bfs_local = 86.763458, position = 210
bfs_local = 84.332901, position = 211
bfs_local = 82.125458, position = 212
bfs_local = 66.927139, position = 276
bfs_local = 57.716461, position = 277
bfs_local = 51.453182, position = 278
bfs_local = 39.160839, position = 279
bfs_local = 31.622044, position = 280
bfs_local = 28.650475, position = 281
bfs_local = 23.193090, position = 282
bfs_local = 18.942255, position = 283
bfs_local = 16.442329, position = 284
bfs_local = 14.960269, position = 285
bfs_local = 14.029958, position = 286
bfs_local = 13.455294, position = 12317
bfs_local = 13.343624, position = 12318
bfs_local = 11.353244, position = 25846
bfs_local = 11.098734, position = 43099
bfs_local = 10.341493, position = 131638
bfs_local = 10.244247, position = 131639
bfs_local = 10.189466, position = 148874
bfs_local = 9.236251, position = 148875
bfs_local = 8.076555, position = 148876
bsf = 2.84193
position = 148876
DTW Calculation = 3.24%
Count of DTW = 32380
Time for calculation lower bounding = 87.60%
Time for initializing arrays with start and end segment values = 0.000%
Time for finding the best subsequence = 12.04%
Runtime of reading 0.33
Total Runtime of finding the best subsequence 6.50


number of a  series of experiments = 1
count of threads = 3

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bfs_local = 218.727905, position = 333324
bfs_local = 134.735672, position = 666648
bfs_local = 76.542137, position = 333291
bfs_local = 49.199272, position = 666615
bfs_local = 44.607243, position = 333358
bfs_local = 31.532650, position = 666582
bfs_local = 30.613543, position = 666583
bfs_local = 23.162945, position = 666584
bfs_local = 21.950909, position = 666589
bfs_local = 21.757963, position = 666590
bfs_local = 20.977331, position = 666591
bfs_local = 20.798550, position = 666592
bfs_local = 20.595728, position = 666600
bfs_local = 20.485851, position = 666601
bfs_local = 18.942255, position = 283
bfs_local = 16.442329, position = 284
bfs_local = 14.960269, position = 285
bfs_local = 14.029958, position = 286
bfs_local = 13.050027, position = 335233
bfs_local = 11.187132, position = 340695
bfs_local = 10.858139, position = 340696
bfs_local = 10.625361, position = 340701
bfs_local = 10.232833, position = 756562
bfs_local = 10.189466, position = 148874
bfs_local = 9.236251, position = 148875
bfs_local = 8.076555, position = 148876
bsf = 2.84193
position = 148876
DTW Calculation = 3.71%
Count of DTW = 37068
Time for calculation lower bounding = 90.15%
Time for initializing arrays with start and end segment values = 0.000%
Time for finding the best subsequence = 9.02%
Runtime of reading 0.33
Total Runtime of finding the best subsequence 2.14


number of a  series of experiments = 1
count of threads = 6

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bfs_local = 75.532822, position = 333375
bfs_local = 20.595728, position = 666600
bfs_local = 20.485851, position = 666601
bfs_local = 19.961882, position = 500208
bfs_local = 18.956606, position = 500209
bfs_local = 18.748430, position = 500210
bfs_local = 16.442329, position = 284
bfs_local = 14.960269, position = 285
bfs_local = 14.029958, position = 286
bfs_local = 13.804946, position = 168474
bfs_local = 12.684419, position = 168475
bfs_local = 11.187132, position = 340695
bfs_local = 10.858139, position = 340696
bfs_local = 10.625361, position = 340701
bfs_local = 9.738815, position = 874191
bfs_local = 9.735808, position = 874194
bfs_local = 9.441595, position = 295832
bfs_local = 9.236251, position = 148875
bfs_local = 8.076555, position = 148876
bsf = 2.84193
position = 148876
DTW Calculation = 4.24%
Count of DTW = 42392
Time for calculation lower bounding = 89.95%
Time for initializing arrays with start and end segment values = 0.001%
Time for finding the best subsequence = 8.70%
Runtime of reading 0.33
Total Runtime of finding the best subsequence 1.11


number of a  series of experiments = 1
count of threads = 12

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bfs_local = 75.532822, position = 333375
bfs_local = 20.595728, position = 666600
bfs_local = 20.485851, position = 666601
bfs_local = 15.418200, position = 291
bfs_local = 14.469719, position = 292
bfs_local = 14.203892, position = 750551
bfs_local = 14.029958, position = 286
bfs_local = 13.208969, position = 750552
bfs_local = 13.008174, position = 585037
bfs_local = 12.684419, position = 168475
bfs_local = 11.817601, position = 756002
bfs_local = 11.187132, position = 340695
bfs_local = 10.858139, position = 340696
bfs_local = 10.232833, position = 756562
bfs_local = 10.107504, position = 286687
bfs_local = 9.738815, position = 874191
bfs_local = 9.735808, position = 874194
bfs_local = 9.441595, position = 295832
bfs_local = 9.236251, position = 148875
bfs_local = 8.076555, position = 148876
bsf = 2.84193
position = 148876
DTW Calculation = 4.31%
Count of DTW = 43062
Time for calculation lower bounding = 90.39%
Time for initializing arrays with start and end segment values = 0.001%
Time for finding the best subsequence = 7.70%
Runtime of reading 0.33
Total Runtime of finding the best subsequence 0.86


number of a  series of experiments = 1
count of threads = 18

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bfs_local = 76.647362, position = 333377
bfs_local = 46.776112, position = 444475
bfs_local = 39.076767, position = 444486
bfs_local = 30.709898, position = 666604
bfs_local = 21.798094, position = 666593
bfs_local = 20.968914, position = 666599
bfs_local = 15.445699, position = 222291
bfs_local = 14.960269, position = 285
bfs_local = 14.029958, position = 286
bfs_local = 13.109679, position = 890566
bfs_local = 12.614532, position = 890567
bfs_local = 12.438791, position = 279451
bfs_local = 11.600005, position = 279452
bfs_local = 10.858139, position = 340696
bfs_local = 10.625361, position = 340701
bfs_local = 10.107504, position = 286687
bfs_local = 9.441595, position = 295832
bfs_local = 9.236251, position = 148875
bfs_local = 8.076555, position = 148876
bsf = 2.84193
position = 148876
DTW Calculation = 4.14%
Count of DTW = 41428
Time for calculation lower bounding = 90.13%
Time for initializing arrays with start and end segment values = 0.001%
Time for finding the best subsequence = 7.65%
Runtime of reading 0.33
Total Runtime of finding the best subsequence 0.67


number of a  series of experiments = 1
count of threads = 24

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bfs_local = 68.774902, position = 333354
bfs_local = 37.736176, position = 375016
bfs_local = 27.605854, position = 41687
bfs_local = 26.203970, position = 41679
bfs_local = 20.798550, position = 666592
bfs_local = 20.595728, position = 666600
bfs_local = 20.485851, position = 666601
bfs_local = 19.961882, position = 500208
bfs_local = 18.515331, position = 41958
bfs_local = 17.761095, position = 458479
bfs_local = 16.442329, position = 284
bfs_local = 14.960269, position = 285
bfs_local = 14.029958, position = 286
bfs_local = 13.208969, position = 750552
bfs_local = 11.098734, position = 43099
bfs_local = 10.144341, position = 295831
bfs_local = 9.441595, position = 295832
bfs_local = 9.236251, position = 148875
bfs_local = 8.076555, position = 148876
bsf = 2.84193
position = 148876
DTW Calculation = 3.99%
Count of DTW = 39920
Time for calculation lower bounding = 88.84%
Time for initializing arrays with start and end segment values = 0.004%
Time for finding the best subsequence = 8.59%
Runtime of reading 0.33
Total Runtime of finding the best subsequence 0.60


number of a  series of experiments = 2
count of threads = 1

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bfs_local = 205.161011, position = 68
bfs_local = 201.296082, position = 109
bfs_local = 198.454117, position = 110
bfs_local = 181.586792, position = 111
bfs_local = 177.698975, position = 112
bfs_local = 177.272400, position = 113
bfs_local = 173.585785, position = 114
bfs_local = 168.219513, position = 115
bfs_local = 166.177582, position = 116
bfs_local = 153.459076, position = 117
bfs_local = 137.000320, position = 118
bfs_local = 134.529343, position = 122
bfs_local = 128.011978, position = 123
bfs_local = 121.794434, position = 124
bfs_local = 115.587097, position = 125
bfs_local = 106.061104, position = 126
bfs_local = 90.850655, position = 127
bfs_local = 90.639030, position = 129
bfs_local = 88.713707, position = 130
bfs_local = 87.106270, position = 209
bfs_local = 86.763458, position = 210
bfs_local = 84.332901, position = 211
bfs_local = 82.125458, position = 212
bfs_local = 66.927139, position = 276
bfs_local = 57.716461, position = 277
bfs_local = 51.453182, position = 278
bfs_local = 39.160839, position = 279
bfs_local = 31.622044, position = 280
bfs_local = 28.650475, position = 281
bfs_local = 23.193090, position = 282
bfs_local = 18.942255, position = 283
bfs_local = 16.442329, position = 284
bfs_local = 14.960269, position = 285
bfs_local = 14.029958, position = 286
bfs_local = 13.455294, position = 12317
bfs_local = 13.343624, position = 12318
bfs_local = 11.353244, position = 25846
bfs_local = 11.098734, position = 43099
bfs_local = 10.341493, position = 131638
bfs_local = 10.244247, position = 131639
bfs_local = 10.189466, position = 148874
bfs_local = 9.236251, position = 148875
bfs_local = 8.076555, position = 148876
bsf = 2.84193
position = 148876
DTW Calculation = 3.24%
Count of DTW = 32380
Time for calculation lower bounding = 87.74%
Time for initializing arrays with start and end segment values = 0.000%
Time for finding the best subsequence = 11.90%
Runtime of reading 0.33
Total Runtime of finding the best subsequence 6.50


number of a  series of experiments = 2
count of threads = 3

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bfs_local = 134.735672, position = 666648
bfs_local = 76.542137, position = 333291
bfs_local = 49.199272, position = 666615
bfs_local = 44.607243, position = 333358
bfs_local = 31.532650, position = 666582
bfs_local = 30.613543, position = 666583
bfs_local = 23.162945, position = 666584
bfs_local = 21.950909, position = 666589
bfs_local = 21.757963, position = 666590
bfs_local = 20.977331, position = 666591
bfs_local = 20.798550, position = 666592
bfs_local = 20.595728, position = 666600
bfs_local = 20.485851, position = 666601
bfs_local = 18.942255, position = 283
bfs_local = 16.442329, position = 284
bfs_local = 14.960269, position = 285
bfs_local = 14.029958, position = 286
bfs_local = 13.050027, position = 335233
bfs_local = 11.187132, position = 340695
bfs_local = 10.858139, position = 340696
bfs_local = 10.625361, position = 340701
bfs_local = 10.232833, position = 756562
bfs_local = 10.189466, position = 148874
bfs_local = 9.236251, position = 148875
bfs_local = 8.076555, position = 148876
bsf = 2.84193
position = 148876
DTW Calculation = 3.71%
Count of DTW = 37068
Time for calculation lower bounding = 89.74%
Time for initializing arrays with start and end segment values = 0.000%
Time for finding the best subsequence = 9.44%
Runtime of reading 0.33
Total Runtime of finding the best subsequence 2.18


number of a  series of experiments = 2
count of threads = 6

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bfs_local = 126.953560, position = 666650
bfs_local = 75.532822, position = 333375
bfs_local = 20.595728, position = 666600
bfs_local = 20.485851, position = 666601
bfs_local = 19.961882, position = 500208
bfs_local = 18.956606, position = 500209
bfs_local = 18.748430, position = 500210
bfs_local = 16.442329, position = 284
bfs_local = 14.960269, position = 285
bfs_local = 14.029958, position = 286
bfs_local = 13.804946, position = 168474
bfs_local = 12.684419, position = 168475
bfs_local = 11.187132, position = 340695
bfs_local = 10.858139, position = 340696
bfs_local = 10.625361, position = 340701
bfs_local = 9.738815, position = 874191
bfs_local = 9.735808, position = 874194
bfs_local = 9.441595, position = 295832
bfs_local = 9.236251, position = 148875
bfs_local = 8.076555, position = 148876
bsf = 2.84193
position = 148876
DTW Calculation = 4.24%
Count of DTW = 42392
Time for calculation lower bounding = 90.07%
Time for initializing arrays with start and end segment values = 0.001%
Time for finding the best subsequence = 8.58%
Runtime of reading 0.33
Total Runtime of finding the best subsequence 1.11


number of a  series of experiments = 2
count of threads = 12

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bfs_local = 20.595728, position = 666600
bfs_local = 20.485851, position = 666601
bfs_local = 15.418200, position = 291
bfs_local = 14.469719, position = 292
bfs_local = 14.203892, position = 750551
bfs_local = 13.208969, position = 750552
bfs_local = 13.008174, position = 585037
bfs_local = 12.684419, position = 168475
bfs_local = 11.817601, position = 756002
bfs_local = 11.187132, position = 340695
bfs_local = 10.858139, position = 340696
bfs_local = 10.625361, position = 340701
bfs_local = 10.232833, position = 756562
bfs_local = 10.107504, position = 286687
bfs_local = 9.738815, position = 874191
bfs_local = 9.735808, position = 874194
bfs_local = 9.441595, position = 295832
bfs_local = 9.236251, position = 148875
bfs_local = 8.076555, position = 148876
bsf = 2.84193
position = 148876
DTW Calculation = 4.31%
Count of DTW = 43062
Time for calculation lower bounding = 90.99%
Time for initializing arrays with start and end segment values = 0.001%
Time for finding the best subsequence = 7.14%
Runtime of reading 0.33
Total Runtime of finding the best subsequence 0.86


number of a  series of experiments = 2
count of threads = 18

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bfs_local = 33.450222, position = 222268
bfs_local = 30.709898, position = 666604
bfs_local = 21.798094, position = 666593
bfs_local = 15.445699, position = 222291
bfs_local = 14.960269, position = 285
bfs_local = 14.029958, position = 286
bfs_local = 13.764850, position = 945660
bfs_local = 13.109679, position = 890566
bfs_local = 12.614532, position = 890567
bfs_local = 12.438791, position = 279451
bfs_local = 11.600005, position = 279452
bfs_local = 10.858139, position = 340696
bfs_local = 10.625361, position = 340701
bfs_local = 10.107504, position = 286687
bfs_local = 9.441595, position = 295832
bfs_local = 9.236251, position = 148875
bfs_local = 8.076555, position = 148876
bsf = 2.84193
position = 148876
DTW Calculation = 4.14%
Count of DTW = 41428
Time for calculation lower bounding = 89.41%
Time for initializing arrays with start and end segment values = 0.001%
Time for finding the best subsequence = 8.14%
Runtime of reading 0.33
Total Runtime of finding the best subsequence 0.60


number of a  series of experiments = 2
count of threads = 24

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bfs_local = 154.849808, position = 166677
bfs_local = 59.838051, position = 41662
bfs_local = 51.565041, position = 41712
bfs_local = 27.605854, position = 41687
bfs_local = 26.762644, position = 291634
bfs_local = 20.798550, position = 666592
bfs_local = 20.595728, position = 666600
bfs_local = 20.485851, position = 666601
bfs_local = 18.515331, position = 41958
bfs_local = 17.761095, position = 458479
bfs_local = 16.790121, position = 41959
bfs_local = 16.356503, position = 41960
bfs_local = 14.203892, position = 750551
bfs_local = 13.208969, position = 750552
bfs_local = 11.098734, position = 43099
bfs_local = 10.144341, position = 295831
bfs_local = 9.441595, position = 295832
bfs_local = 9.236251, position = 148875
bfs_local = 8.076555, position = 148876
bsf = 2.84193
position = 148876
DTW Calculation = 3.99%
Count of DTW = 39920
Time for calculation lower bounding = 87.84%
Time for initializing arrays with start and end segment values = 0.004%
Time for finding the best subsequence = 9.45%
Runtime of reading 0.33
Total Runtime of finding the best subsequence 0.56


