number of a  series of experiments = 0
count of threads = 1

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bfs_local = 205.161011, position = 68
bfs_local = 201.296082, position = 109
bfs_local = 198.454117, position = 110
bfs_local = 181.586792, position = 111
bfs_local = 177.698975, position = 112
bfs_local = 177.272400, position = 113
bfs_local = 173.585785, position = 114
bfs_local = 168.219513, position = 115
bfs_local = 166.177582, position = 116
bfs_local = 153.459076, position = 117
bfs_local = 137.000320, position = 118
bfs_local = 134.529343, position = 122
bfs_local = 128.011978, position = 123
bfs_local = 121.794434, position = 124
bfs_local = 115.587097, position = 125
bfs_local = 106.061104, position = 126
bfs_local = 90.850655, position = 127
bfs_local = 90.639030, position = 129
bfs_local = 88.713707, position = 130
bfs_local = 87.106270, position = 209
bfs_local = 86.763458, position = 210
bfs_local = 84.332901, position = 211
bfs_local = 82.125458, position = 212
bfs_local = 66.927139, position = 276
bfs_local = 57.716461, position = 277
bfs_local = 51.453182, position = 278
bfs_local = 39.160839, position = 279
bfs_local = 31.622044, position = 280
bfs_local = 28.650475, position = 281
bfs_local = 23.193090, position = 282
bfs_local = 18.942255, position = 283
bfs_local = 16.442329, position = 284
bfs_local = 14.960269, position = 285
bfs_local = 14.029958, position = 286
bfs_local = 13.455294, position = 12317
bfs_local = 13.343624, position = 12318
bfs_local = 11.353244, position = 25846
bfs_local = 11.098734, position = 43099
bfs_local = 10.341493, position = 131638
bfs_local = 10.244247, position = 131639
bfs_local = 10.189466, position = 148874
bfs_local = 9.236251, position = 148875
bfs_local = 8.076555, position = 148876
bsf = 2.84193
position = 148876
DTW Calculation = 3.23%
Count of DTW = 32287
Time for calculation lower bounding = 18.75%
Time for initializing arrays with start and end segment values = 0.000%
Time for finding the best subsequence = 81.18%
Runtime of reading 0.33
Total Runtime of finding the best subsequence 30.32


number of a  series of experiments = 0
count of threads = 3

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bfs_local = 31.532650, position = 666582
bfs_local = 30.613543, position = 666583
bfs_local = 23.162945, position = 666584
bfs_local = 21.950909, position = 666589
bfs_local = 21.757963, position = 666590
bfs_local = 20.798550, position = 666592
bfs_local = 20.595728, position = 666600
bfs_local = 20.485851, position = 666601
bfs_local = 18.942255, position = 283
bfs_local = 14.960269, position = 285
bfs_local = 14.029958, position = 286
bfs_local = 13.050027, position = 335233
bfs_local = 11.187132, position = 340695
bfs_local = 10.858139, position = 340696
bfs_local = 10.625361, position = 340701
bfs_local = 10.232833, position = 756562
bfs_local = 9.236251, position = 148875
bfs_local = 8.811631, position = 148877
bfs_local = 8.076555, position = 148876
bsf = 2.84193
position = 148876
DTW Calculation = 3.66%
Count of DTW = 36642
Time for calculation lower bounding = 30.61%
Time for initializing arrays with start and end segment values = 0.000%
Time for finding the best subsequence = 69.12%
Runtime of reading 0.33
Total Runtime of finding the best subsequence 6.39


number of a  series of experiments = 0
count of threads = 6

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bfs_local = 76.842453, position = 333292
bfs_local = 76.572037, position = 333293
bfs_local = 23.910866, position = 666585
bfs_local = 23.162945, position = 666584
bfs_local = 20.798550, position = 666592
bfs_local = 20.595728, position = 666600
bfs_local = 20.485851, position = 666601
bfs_local = 18.748430, position = 500210
bfs_local = 14.960269, position = 285
bfs_local = 14.029958, position = 286
bfs_local = 13.804946, position = 168474
bfs_local = 12.684419, position = 168475
bfs_local = 11.187132, position = 340695
bfs_local = 10.858139, position = 340696
bfs_local = 10.625361, position = 340701
bfs_local = 9.735808, position = 874194
bfs_local = 9.441595, position = 295832
bfs_local = 8.076555, position = 148876
bsf = 2.84193
position = 148876
DTW Calculation = 4.15%
Count of DTW = 41475
Time for calculation lower bounding = 45.55%
Time for initializing arrays with start and end segment values = 0.000%
Time for finding the best subsequence = 53.90%
Runtime of reading 0.33
Total Runtime of finding the best subsequence 2.20


number of a  series of experiments = 0
count of threads = 12

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bfs_local = 76.842453, position = 333292
bfs_local = 23.162945, position = 666584
bfs_local = 21.757963, position = 666590
bfs_local = 20.798550, position = 666592
bfs_local = 20.595728, position = 666600
bfs_local = 20.485851, position = 666601
bfs_local = 18.956606, position = 500209
bfs_local = 18.748430, position = 500210
bfs_local = 14.960269, position = 285
bfs_local = 14.029958, position = 286
bfs_local = 13.851318, position = 750554
bfs_local = 13.208969, position = 750552
bfs_local = 13.008174, position = 585037
bfs_local = 12.684419, position = 168475
bfs_local = 11.817601, position = 756002
bfs_local = 11.541005, position = 756561
bfs_local = 10.232833, position = 756562
bfs_local = 10.107504, position = 286687
bfs_local = 9.738815, position = 874191
bfs_local = 9.735808, position = 874194
bfs_local = 9.441595, position = 295832
bfs_local = 8.076555, position = 148876
bsf = 2.84193
position = 148876
DTW Calculation = 4.12%
Count of DTW = 41180
Time for calculation lower bounding = 44.33%
Time for initializing arrays with start and end segment values = 0.000%
Time for finding the best subsequence = 54.92%
Runtime of reading 0.33
Total Runtime of finding the best subsequence 1.53


number of a  series of experiments = 0
count of threads = 18

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bfs_local = 80.264725, position = 333295
bfs_local = 23.510569, position = 666588
bfs_local = 21.950909, position = 666589
bfs_local = 20.968914, position = 666599
bfs_local = 20.485851, position = 666601
bfs_local = 18.844765, position = 222290
bfs_local = 15.445699, position = 222291
bfs_local = 14.960269, position = 285
bfs_local = 14.469719, position = 292
bfs_local = 14.029958, position = 286
bfs_local = 13.845288, position = 945661
bfs_local = 13.764850, position = 945660
bfs_local = 11.600005, position = 279452
bfs_local = 10.858139, position = 340696
bfs_local = 10.625361, position = 340701
bfs_local = 10.107504, position = 286687
bfs_local = 9.474334, position = 295833
bfs_local = 9.441595, position = 295832
bfs_local = 8.811631, position = 148877
bfs_local = 8.076555, position = 148876
bsf = 2.84193
position = 148876
DTW Calculation = 3.90%
Count of DTW = 38988
Time for calculation lower bounding = 46.87%
Time for initializing arrays with start and end segment values = 0.001%
Time for finding the best subsequence = 52.02%
Runtime of reading 0.33
Total Runtime of finding the best subsequence 1.12


number of a  series of experiments = 0
count of threads = 24

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bfs_local = 79.733414, position = 583269
bfs_local = 62.618347, position = 624931
bfs_local = 56.254715, position = 416621
bfs_local = 55.063313, position = 416620
bfs_local = 20.798550, position = 666592
bfs_local = 20.595728, position = 666600
bfs_local = 20.485851, position = 666601
bfs_local = 20.345522, position = 458470
bfs_local = 18.188726, position = 458473
bfs_local = 17.016047, position = 458474
bfs_local = 16.902988, position = 458476
bfs_local = 16.833176, position = 458475
bfs_local = 16.442329, position = 284
bfs_local = 14.029958, position = 286
bfs_local = 13.851318, position = 750554
bfs_local = 13.208969, position = 750552
bfs_local = 11.893929, position = 43096
bfs_local = 11.391217, position = 43100
bfs_local = 11.098734, position = 43099
bfs_local = 10.144341, position = 295831
bfs_local = 9.474334, position = 295833
bfs_local = 9.441595, position = 295832
bfs_local = 8.811631, position = 148877
bfs_local = 8.076555, position = 148876
bsf = 2.84193
position = 148876
DTW Calculation = 3.59%
Count of DTW = 35922
Time for calculation lower bounding = 57.17%
Time for initializing arrays with start and end segment values = 0.001%
Time for finding the best subsequence = 41.35%
Runtime of reading 0.33
Total Runtime of finding the best subsequence 0.77


number of a  series of experiments = 1
count of threads = 1

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bfs_local = 205.161011, position = 68
bfs_local = 201.296082, position = 109
bfs_local = 198.454117, position = 110
bfs_local = 181.586792, position = 111
bfs_local = 177.698975, position = 112
bfs_local = 177.272400, position = 113
bfs_local = 173.585785, position = 114
bfs_local = 168.219513, position = 115
bfs_local = 166.177582, position = 116
bfs_local = 153.459076, position = 117
bfs_local = 137.000320, position = 118
bfs_local = 134.529343, position = 122
bfs_local = 128.011978, position = 123
bfs_local = 121.794434, position = 124
bfs_local = 115.587097, position = 125
bfs_local = 106.061104, position = 126
bfs_local = 90.850655, position = 127
bfs_local = 90.639030, position = 129
bfs_local = 88.713707, position = 130
bfs_local = 87.106270, position = 209
bfs_local = 86.763458, position = 210
bfs_local = 84.332901, position = 211
bfs_local = 82.125458, position = 212
bfs_local = 66.927139, position = 276
bfs_local = 57.716461, position = 277
bfs_local = 51.453182, position = 278
bfs_local = 39.160839, position = 279
bfs_local = 31.622044, position = 280
bfs_local = 28.650475, position = 281
bfs_local = 23.193090, position = 282
bfs_local = 18.942255, position = 283
bfs_local = 16.442329, position = 284
bfs_local = 14.960269, position = 285
bfs_local = 14.029958, position = 286
bfs_local = 13.455294, position = 12317
bfs_local = 13.343624, position = 12318
bfs_local = 11.353244, position = 25846
bfs_local = 11.098734, position = 43099
bfs_local = 10.341493, position = 131638
bfs_local = 10.244247, position = 131639
bfs_local = 10.189466, position = 148874
bfs_local = 9.236251, position = 148875
bfs_local = 8.076555, position = 148876
bsf = 2.84193
position = 148876
DTW Calculation = 3.23%
Count of DTW = 32287
Time for calculation lower bounding = 19.01%
Time for initializing arrays with start and end segment values = 0.000%
Time for finding the best subsequence = 80.91%
Runtime of reading 0.33
Total Runtime of finding the best subsequence 29.91


number of a  series of experiments = 1
count of threads = 3

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bfs_local = 31.532650, position = 666582
bfs_local = 30.613543, position = 666583
bfs_local = 23.510569, position = 666588
bfs_local = 23.162945, position = 666584
bfs_local = 21.950909, position = 666589
bfs_local = 21.757963, position = 666590
bfs_local = 20.798550, position = 666592
bfs_local = 20.595728, position = 666600
bfs_local = 20.485851, position = 666601
bfs_local = 14.960269, position = 285
bfs_local = 14.029958, position = 286
bfs_local = 13.050027, position = 335233
bfs_local = 11.187132, position = 340695
bfs_local = 10.858139, position = 340696
bfs_local = 10.625361, position = 340701
bfs_local = 10.232833, position = 756562
bfs_local = 8.811631, position = 148877
bfs_local = 8.076555, position = 148876
bsf = 2.84193
position = 148876
DTW Calculation = 3.66%
Count of DTW = 36642
Time for calculation lower bounding = 33.89%
Time for initializing arrays with start and end segment values = 0.000%
Time for finding the best subsequence = 65.84%
Runtime of reading 0.33
Total Runtime of finding the best subsequence 5.76


number of a  series of experiments = 1
count of threads = 6

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bfs_local = 76.572037, position = 333293
bfs_local = 23.910866, position = 666585
bfs_local = 23.162945, position = 666584
bfs_local = 20.798550, position = 666592
bfs_local = 20.595728, position = 666600
bfs_local = 20.485851, position = 666601
bfs_local = 19.231598, position = 500211
bfs_local = 18.748430, position = 500210
bfs_local = 16.442329, position = 284
bfs_local = 14.029958, position = 286
bfs_local = 12.684419, position = 168475
bfs_local = 11.187132, position = 340695
bfs_local = 10.858139, position = 340696
bfs_local = 10.625361, position = 340701
bfs_local = 9.735808, position = 874194
bfs_local = 9.441595, position = 295832
bfs_local = 8.076555, position = 148876
bsf = 2.84193
position = 148876
DTW Calculation = 4.15%
Count of DTW = 41475
Time for calculation lower bounding = 46.78%
Time for initializing arrays with start and end segment values = 0.000%
Time for finding the best subsequence = 52.67%
Runtime of reading 0.33
Total Runtime of finding the best subsequence 2.14


number of a  series of experiments = 1
count of threads = 12

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bfs_local = 189.033630, position = 166646
bfs_local = 124.706146, position = 916554
bfs_local = 106.979721, position = 83324
bfs_local = 23.162945, position = 666584
bfs_local = 21.798094, position = 666593
bfs_local = 21.757963, position = 666590
bfs_local = 20.798550, position = 666592
bfs_local = 20.595728, position = 666600
bfs_local = 20.485851, position = 666601
bfs_local = 18.956606, position = 500209
bfs_local = 18.748430, position = 500210
bfs_local = 14.029958, position = 286
bfs_local = 13.741984, position = 750553
bfs_local = 13.208969, position = 750552
bfs_local = 13.008174, position = 585037
bfs_local = 12.684419, position = 168475
bfs_local = 11.817601, position = 756002
bfs_local = 11.541005, position = 756561
bfs_local = 10.424694, position = 756563
bfs_local = 10.232833, position = 756562
bfs_local = 10.107504, position = 286687
bfs_local = 9.738815, position = 874191
bfs_local = 9.735808, position = 874194
bfs_local = 9.441595, position = 295832
bfs_local = 8.076555, position = 148876
bsf = 2.84193
position = 148876
DTW Calculation = 4.12%
Count of DTW = 41180
Time for calculation lower bounding = 41.16%
Time for initializing arrays with start and end segment values = 0.000%
Time for finding the best subsequence = 58.03%
Runtime of reading 0.33
Total Runtime of finding the best subsequence 1.65


number of a  series of experiments = 1
count of threads = 18

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bfs_local = 82.221611, position = 555491
bfs_local = 77.328461, position = 333294
bfs_local = 32.395626, position = 444392
bfs_local = 23.510569, position = 666588
bfs_local = 21.950909, position = 666589
bfs_local = 21.798094, position = 666593
bfs_local = 20.798550, position = 666592
bfs_local = 20.485851, position = 666601
bfs_local = 15.445699, position = 222291
bfs_local = 14.960269, position = 285
bfs_local = 14.469719, position = 292
bfs_local = 14.029958, position = 286
bfs_local = 13.845288, position = 945661
bfs_local = 13.764850, position = 945660
bfs_local = 11.600005, position = 279452
bfs_local = 11.187132, position = 340695
bfs_local = 10.858139, position = 340696
bfs_local = 10.625361, position = 340701
bfs_local = 10.107504, position = 286687
bfs_local = 9.474334, position = 295833
bfs_local = 9.441595, position = 295832
bfs_local = 8.811631, position = 148877
bfs_local = 8.076555, position = 148876
bsf = 2.84193
position = 148876
DTW Calculation = 3.90%
Count of DTW = 38988
Time for calculation lower bounding = 46.90%
Time for initializing arrays with start and end segment values = 0.001%
Time for finding the best subsequence = 52.01%
Runtime of reading 0.33
Total Runtime of finding the best subsequence 1.13


number of a  series of experiments = 1
count of threads = 24

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bfs_local = 77.617233, position = 791579
bfs_local = 35.743881, position = 874902
bfs_local = 35.099827, position = 874903
bfs_local = 20.798550, position = 666592
bfs_local = 20.595728, position = 666600
bfs_local = 20.485851, position = 666601
bfs_local = 20.345522, position = 458470
bfs_local = 18.188726, position = 458473
bfs_local = 16.833176, position = 458475
bfs_local = 16.442329, position = 284
bfs_local = 14.029958, position = 286
bfs_local = 13.208969, position = 750552
bfs_local = 11.940423, position = 43102
bfs_local = 11.744738, position = 43101
bfs_local = 11.098734, position = 43099
bfs_local = 10.802842, position = 295834
bfs_local = 9.474334, position = 295833
bfs_local = 9.441595, position = 295832
bfs_local = 8.811631, position = 148877
bfs_local = 8.076555, position = 148876
bsf = 2.84193
position = 148876
DTW Calculation = 3.59%
Count of DTW = 35922
Time for calculation lower bounding = 58.97%
Time for initializing arrays with start and end segment values = 0.002%
Time for finding the best subsequence = 39.81%
Runtime of reading 0.33
Total Runtime of finding the best subsequence 0.95


number of a  series of experiments = 2
count of threads = 1

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bfs_local = 205.161011, position = 68
bfs_local = 201.296082, position = 109
bfs_local = 198.454117, position = 110
bfs_local = 181.586792, position = 111
bfs_local = 177.698975, position = 112
bfs_local = 177.272400, position = 113
bfs_local = 173.585785, position = 114
bfs_local = 168.219513, position = 115
bfs_local = 166.177582, position = 116
bfs_local = 153.459076, position = 117
bfs_local = 137.000320, position = 118
bfs_local = 134.529343, position = 122
bfs_local = 128.011978, position = 123
bfs_local = 121.794434, position = 124
bfs_local = 115.587097, position = 125
bfs_local = 106.061104, position = 126
bfs_local = 90.850655, position = 127
bfs_local = 90.639030, position = 129
bfs_local = 88.713707, position = 130
bfs_local = 87.106270, position = 209
bfs_local = 86.763458, position = 210
bfs_local = 84.332901, position = 211
bfs_local = 82.125458, position = 212
bfs_local = 66.927139, position = 276
bfs_local = 57.716461, position = 277
bfs_local = 51.453182, position = 278
bfs_local = 39.160839, position = 279
bfs_local = 31.622044, position = 280
bfs_local = 28.650475, position = 281
bfs_local = 23.193090, position = 282
bfs_local = 18.942255, position = 283
bfs_local = 16.442329, position = 284
bfs_local = 14.960269, position = 285
bfs_local = 14.029958, position = 286
bfs_local = 13.455294, position = 12317
bfs_local = 13.343624, position = 12318
bfs_local = 11.353244, position = 25846
bfs_local = 11.098734, position = 43099
bfs_local = 10.341493, position = 131638
bfs_local = 10.244247, position = 131639
bfs_local = 10.189466, position = 148874
bfs_local = 9.236251, position = 148875
bfs_local = 8.076555, position = 148876
bsf = 2.84193
position = 148876
DTW Calculation = 3.23%
Count of DTW = 32287
Time for calculation lower bounding = 18.78%
Time for initializing arrays with start and end segment values = 0.000%
Time for finding the best subsequence = 81.15%
Runtime of reading 0.33
Total Runtime of finding the best subsequence 30.17


number of a  series of experiments = 2
count of threads = 3

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bfs_local = 31.532650, position = 666582
bfs_local = 30.613543, position = 666583
bfs_local = 23.510569, position = 666588
bfs_local = 23.162945, position = 666584
bfs_local = 21.950909, position = 666589
bfs_local = 20.798550, position = 666592
bfs_local = 20.595728, position = 666600
bfs_local = 20.485851, position = 666601
bfs_local = 14.960269, position = 285
bfs_local = 14.029958, position = 286
bfs_local = 13.050027, position = 335233
bfs_local = 11.187132, position = 340695
bfs_local = 10.858139, position = 340696
bfs_local = 10.625361, position = 340701
bfs_local = 10.232833, position = 756562
bfs_local = 8.811631, position = 148877
bfs_local = 8.076555, position = 148876
bsf = 2.84193
position = 148876
DTW Calculation = 3.66%
Count of DTW = 36642
Time for calculation lower bounding = 30.03%
Time for initializing arrays with start and end segment values = 0.000%
Time for finding the best subsequence = 69.77%
Runtime of reading 0.33
Total Runtime of finding the best subsequence 6.53


number of a  series of experiments = 2
count of threads = 6

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bfs_local = 76.842453, position = 333292
bfs_local = 76.572037, position = 333293
bfs_local = 23.162945, position = 666584
bfs_local = 20.798550, position = 666592
bfs_local = 20.595728, position = 666600
bfs_local = 20.485851, position = 666601
bfs_local = 19.961882, position = 500208
bfs_local = 18.748430, position = 500210
bfs_local = 14.960269, position = 285
bfs_local = 14.029958, position = 286
bfs_local = 13.804946, position = 168474
bfs_local = 12.684419, position = 168475
bfs_local = 11.187132, position = 340695
bfs_local = 10.858139, position = 340696
bfs_local = 10.625361, position = 340701
bfs_local = 9.735808, position = 874194
bfs_local = 9.441595, position = 295832
bfs_local = 8.076555, position = 148876
bsf = 2.84193
position = 148876
DTW Calculation = 4.15%
Count of DTW = 41475
Time for calculation lower bounding = 46.89%
Time for initializing arrays with start and end segment values = 0.000%
Time for finding the best subsequence = 52.55%
Runtime of reading 0.33
Total Runtime of finding the best subsequence 2.14


number of a  series of experiments = 2
count of threads = 12

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bfs_local = 198.740173, position = 499939
bfs_local = 106.979721, position = 83324
bfs_local = 103.429581, position = 83323
bfs_local = 76.842453, position = 333292
bfs_local = 76.572037, position = 333293
bfs_local = 23.162945, position = 666584
bfs_local = 22.405787, position = 666594
bfs_local = 20.798550, position = 666592
bfs_local = 20.595728, position = 666600
bfs_local = 20.485851, position = 666601
bfs_local = 19.231598, position = 500211
bfs_local = 18.748430, position = 500210
bfs_local = 16.442329, position = 284
bfs_local = 14.960269, position = 285
bfs_local = 14.029958, position = 286
bfs_local = 13.208969, position = 750552
bfs_local = 13.008174, position = 585037
bfs_local = 12.684419, position = 168475
bfs_local = 11.817601, position = 756002
bfs_local = 11.043961, position = 756564
bfs_local = 10.232833, position = 756562
bfs_local = 10.107504, position = 286687
bfs_local = 9.738815, position = 874191
bfs_local = 9.735808, position = 874194
bfs_local = 9.441595, position = 295832
bfs_local = 8.076555, position = 148876
bsf = 2.84193
position = 148876
DTW Calculation = 4.12%
Count of DTW = 41180
Time for calculation lower bounding = 46.37%
Time for initializing arrays with start and end segment values = 0.000%
Time for finding the best subsequence = 52.94%
Runtime of reading 0.33
Total Runtime of finding the best subsequence 1.63


number of a  series of experiments = 2
count of threads = 18

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bfs_local = 32.395626, position = 444392
bfs_local = 23.510569, position = 666588
bfs_local = 21.950909, position = 666589
bfs_local = 20.968914, position = 666599
bfs_local = 20.485851, position = 666601
bfs_local = 18.844765, position = 222290
bfs_local = 15.445699, position = 222291
bfs_local = 14.960269, position = 285
bfs_local = 14.029958, position = 286
bfs_local = 13.845288, position = 945661
bfs_local = 13.764850, position = 945660
bfs_local = 11.600005, position = 279452
bfs_local = 10.858139, position = 340696
bfs_local = 10.625361, position = 340701
bfs_local = 10.107504, position = 286687
bfs_local = 9.474334, position = 295833
bfs_local = 9.441595, position = 295832
bfs_local = 8.811631, position = 148877
bfs_local = 8.076555, position = 148876
bsf = 2.84193
position = 148876
DTW Calculation = 3.90%
Count of DTW = 38988
Time for calculation lower bounding = 47.36%
Time for initializing arrays with start and end segment values = 0.001%
Time for finding the best subsequence = 51.53%
Runtime of reading 0.33
Total Runtime of finding the best subsequence 1.10


number of a  series of experiments = 2
count of threads = 24

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bfs_local = 86.147827, position = 916565
bfs_local = 78.085304, position = 791578
bfs_local = 59.838051, position = 41662
bfs_local = 55.063313, position = 416620
bfs_local = 20.798550, position = 666592
bfs_local = 20.595728, position = 666600
bfs_local = 20.485851, position = 666601
bfs_local = 20.345522, position = 458470
bfs_local = 17.016047, position = 458474
bfs_local = 16.902988, position = 458476
bfs_local = 16.833176, position = 458475
bfs_local = 14.029958, position = 286
bfs_local = 13.851318, position = 750554
bfs_local = 13.208969, position = 750552
bfs_local = 11.940423, position = 43102
bfs_local = 11.893929, position = 43096
bfs_local = 11.744738, position = 43101
bfs_local = 11.098734, position = 43099
bfs_local = 10.802842, position = 295834
bfs_local = 10.144341, position = 295831
bfs_local = 9.474334, position = 295833
bfs_local = 9.441595, position = 295832
bfs_local = 9.236251, position = 148875
bfs_local = 8.811631, position = 148877
bfs_local = 8.076555, position = 148876
bsf = 2.84193
position = 148876
DTW Calculation = 3.59%
Count of DTW = 35922
Time for calculation lower bounding = 56.72%
Time for initializing arrays with start and end segment values = 0.002%
Time for finding the best subsequence = 42.11%
Runtime of reading 0.33
Total Runtime of finding the best subsequence 1.06


