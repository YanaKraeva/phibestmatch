number of a  series of experiments = 0
count of threads = 1

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bfs_local = 205.161011, position = 68
bfs_local = 201.296082, position = 109
bfs_local = 198.454117, position = 110
bfs_local = 181.586792, position = 111
bfs_local = 177.698975, position = 112
bfs_local = 177.272400, position = 113
bfs_local = 173.585785, position = 114
bfs_local = 168.219513, position = 115
bfs_local = 166.177582, position = 116
bfs_local = 153.459076, position = 117
bfs_local = 137.000320, position = 118
bfs_local = 134.529343, position = 122
bfs_local = 128.011978, position = 123
bfs_local = 121.794434, position = 124
bfs_local = 115.587097, position = 125
bfs_local = 106.061104, position = 126
bfs_local = 90.850655, position = 127
bfs_local = 90.639030, position = 129
bfs_local = 88.713707, position = 130
bfs_local = 87.106270, position = 209
bfs_local = 86.763458, position = 210
bfs_local = 84.332901, position = 211
bfs_local = 82.125458, position = 212
bfs_local = 66.927139, position = 276
bfs_local = 57.716461, position = 277
bfs_local = 51.453182, position = 278
bfs_local = 39.160839, position = 279
bfs_local = 31.622044, position = 280
bfs_local = 28.650475, position = 281
bfs_local = 23.193090, position = 282
bfs_local = 18.942255, position = 283
bfs_local = 16.442329, position = 284
bfs_local = 14.960269, position = 285
bfs_local = 14.029958, position = 286
bfs_local = 13.455294, position = 12317
bfs_local = 13.343624, position = 12318
bfs_local = 11.353244, position = 25846
bfs_local = 11.098734, position = 43099
bfs_local = 10.341493, position = 131638
bfs_local = 10.244247, position = 131639
bfs_local = 10.189466, position = 148874
bfs_local = 9.236251, position = 148875
bfs_local = 8.076555, position = 148876
bsf = 2.84193
position = 148876
DTW Calculation = 3.31%
Count of DTW = 33110
Time for calculation lower bounding = 95.25%
Time for initializing arrays with start and end segment values = 0.000%
Time for finding the best subsequence = 4.37%
Runtime of reading 0.33
Total Runtime of finding the best subsequence 5.95


number of a  series of experiments = 0
count of threads = 3

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bfs_local = 95.459778, position = 667248
bfs_local = 76.542137, position = 333291
bfs_local = 67.455132, position = 666915
bfs_local = 31.532650, position = 666582
bfs_local = 30.613543, position = 666583
bfs_local = 23.162945, position = 666584
bfs_local = 21.950909, position = 666589
bfs_local = 21.757963, position = 666590
bfs_local = 20.977331, position = 666591
bfs_local = 20.798550, position = 666592
bfs_local = 20.684284, position = 333652
bfs_local = 20.608109, position = 333653
bfs_local = 19.764217, position = 333654
bfs_local = 18.942255, position = 283
bfs_local = 16.442329, position = 284
bfs_local = 14.960269, position = 285
bfs_local = 14.029958, position = 286
bfs_local = 13.050027, position = 335233
bfs_local = 11.187132, position = 340695
bfs_local = 10.858139, position = 340696
bfs_local = 10.625361, position = 340701
bfs_local = 10.232833, position = 756562
bfs_local = 8.076555, position = 148876
bsf = 2.84193
position = 148876
DTW Calculation = 4.11%
Count of DTW = 41047
Time for calculation lower bounding = 93.92%
Time for initializing arrays with start and end segment values = 0.000%
Time for finding the best subsequence = 5.31%
Runtime of reading 0.33
Total Runtime of finding the best subsequence 2.08


number of a  series of experiments = 0
count of threads = 6

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bfs_local = 74.805595, position = 666750
bfs_local = 71.735603, position = 500
bfs_local = 68.313744, position = 833396
bfs_local = 42.726799, position = 333792
bfs_local = 23.162945, position = 666584
bfs_local = 16.643045, position = 834063
bfs_local = 16.349966, position = 834064
bfs_local = 16.100019, position = 834066
bfs_local = 15.003776, position = 834067
bfs_local = 14.960269, position = 285
bfs_local = 14.029958, position = 286
bfs_local = 11.187132, position = 340695
bfs_local = 10.858139, position = 340696
bfs_local = 10.625361, position = 340701
bfs_local = 9.738815, position = 874191
bfs_local = 9.735808, position = 874194
bfs_local = 9.441595, position = 295832
bfs_local = 9.236251, position = 148875
bfs_local = 8.076555, position = 148876
bsf = 2.84193
position = 148876
DTW Calculation = 4.98%
Count of DTW = 49760
Time for calculation lower bounding = 92.73%
Time for initializing arrays with start and end segment values = 0.001%
Time for finding the best subsequence = 6.14%
Runtime of reading 0.33
Total Runtime of finding the best subsequence 1.08


number of a  series of experiments = 0
count of threads = 12

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bfs_local = 79.750710, position = 334125
bfs_local = 44.127365, position = 166896
bfs_local = 23.703405, position = 750
bfs_local = 16.643045, position = 834063
bfs_local = 16.349966, position = 834064
bfs_local = 16.100019, position = 834066
bfs_local = 15.003776, position = 834067
bfs_local = 14.960269, position = 285
bfs_local = 14.029958, position = 286
bfs_local = 13.208969, position = 750552
bfs_local = 11.771042, position = 925121
bfs_local = 11.738503, position = 259688
bfs_local = 11.180024, position = 259689
bfs_local = 11.172758, position = 259690
bfs_local = 10.818756, position = 259691
bfs_local = 10.232833, position = 756562
bfs_local = 10.107504, position = 286687
bfs_local = 9.441595, position = 295832
bfs_local = 9.236251, position = 148875
bfs_local = 8.076555, position = 148876
bsf = 2.84193
position = 148876
DTW Calculation = 5.85%
Count of DTW = 58535
Time for calculation lower bounding = 92.64%
Time for initializing arrays with start and end segment values = 0.001%
Time for finding the best subsequence = 5.85%
Runtime of reading 0.33
Total Runtime of finding the best subsequence 0.84


number of a  series of experiments = 0
count of threads = 18

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bfs_local = 206.107315, position = 222418
bfs_local = 87.260231, position = 55549
bfs_local = 32.795971, position = 944721
bfs_local = 17.376028, position = 277967
bfs_local = 16.022909, position = 834068
bfs_local = 14.960269, position = 285
bfs_local = 14.029958, position = 286
bfs_local = 13.109679, position = 890566
bfs_local = 12.614532, position = 890567
bfs_local = 12.037921, position = 785302
bfs_local = 11.879387, position = 845787
bfs_local = 11.187132, position = 340695
bfs_local = 10.858139, position = 340696
bfs_local = 10.625361, position = 340701
bfs_local = 10.107504, position = 286687
bfs_local = 9.441595, position = 295832
bfs_local = 9.236251, position = 148875
bfs_local = 8.076555, position = 148876
bsf = 2.84193
position = 148876
DTW Calculation = 6.73%
Count of DTW = 67268
Time for calculation lower bounding = 90.45%
Time for initializing arrays with start and end segment values = 0.001%
Time for finding the best subsequence = 7.50%
Runtime of reading 0.33
Total Runtime of finding the best subsequence 0.58


number of a  series of experiments = 0
count of threads = 24

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bfs_local = 115.587097, position = 125
bfs_local = 59.838051, position = 41662
bfs_local = 28.813259, position = 417328
bfs_local = 23.703405, position = 750
bfs_local = 18.835726, position = 458490
bfs_local = 14.469719, position = 292
bfs_local = 14.203892, position = 750551
bfs_local = 13.208969, position = 750552
bfs_local = 11.169123, position = 259694
bfs_local = 10.232833, position = 756562
bfs_local = 10.144341, position = 295831
bfs_local = 9.441595, position = 295832
bfs_local = 9.236251, position = 148875
bfs_local = 8.076555, position = 148876
bsf = 2.84193
position = 148876
DTW Calculation = 7.25%
Count of DTW = 72482
Time for calculation lower bounding = 89.77%
Time for initializing arrays with start and end segment values = 0.004%
Time for finding the best subsequence = 8.07%
Runtime of reading 0.33
Total Runtime of finding the best subsequence 0.58


number of a  series of experiments = 1
count of threads = 1

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bfs_local = 205.161011, position = 68
bfs_local = 201.296082, position = 109
bfs_local = 198.454117, position = 110
bfs_local = 181.586792, position = 111
bfs_local = 177.698975, position = 112
bfs_local = 177.272400, position = 113
bfs_local = 173.585785, position = 114
bfs_local = 168.219513, position = 115
bfs_local = 166.177582, position = 116
bfs_local = 153.459076, position = 117
bfs_local = 137.000320, position = 118
bfs_local = 134.529343, position = 122
bfs_local = 128.011978, position = 123
bfs_local = 121.794434, position = 124
bfs_local = 115.587097, position = 125
bfs_local = 106.061104, position = 126
bfs_local = 90.850655, position = 127
bfs_local = 90.639030, position = 129
bfs_local = 88.713707, position = 130
bfs_local = 87.106270, position = 209
bfs_local = 86.763458, position = 210
bfs_local = 84.332901, position = 211
bfs_local = 82.125458, position = 212
bfs_local = 66.927139, position = 276
bfs_local = 57.716461, position = 277
bfs_local = 51.453182, position = 278
bfs_local = 39.160839, position = 279
bfs_local = 31.622044, position = 280
bfs_local = 28.650475, position = 281
bfs_local = 23.193090, position = 282
bfs_local = 18.942255, position = 283
bfs_local = 16.442329, position = 284
bfs_local = 14.960269, position = 285
bfs_local = 14.029958, position = 286
bfs_local = 13.455294, position = 12317
bfs_local = 13.343624, position = 12318
bfs_local = 11.353244, position = 25846
bfs_local = 11.098734, position = 43099
bfs_local = 10.341493, position = 131638
bfs_local = 10.244247, position = 131639
bfs_local = 10.189466, position = 148874
bfs_local = 9.236251, position = 148875
bfs_local = 8.076555, position = 148876
bsf = 2.84193
position = 148876
DTW Calculation = 3.31%
Count of DTW = 33110
Time for calculation lower bounding = 95.24%
Time for initializing arrays with start and end segment values = 0.000%
Time for finding the best subsequence = 4.38%
Runtime of reading 0.33
Total Runtime of finding the best subsequence 5.94


number of a  series of experiments = 1
count of threads = 3

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bfs_local = 128.372604, position = 333624
bfs_local = 95.459778, position = 667248
bfs_local = 76.542137, position = 333291
bfs_local = 67.455132, position = 666915
bfs_local = 31.532650, position = 666582
bfs_local = 30.613543, position = 666583
bfs_local = 23.162945, position = 666584
bfs_local = 21.950909, position = 666589
bfs_local = 21.757963, position = 666590
bfs_local = 20.977331, position = 666591
bfs_local = 20.798550, position = 666592
bfs_local = 20.684284, position = 333652
bfs_local = 20.608109, position = 333653
bfs_local = 19.764217, position = 333654
bfs_local = 18.942255, position = 283
bfs_local = 16.442329, position = 284
bfs_local = 14.960269, position = 285
bfs_local = 14.029958, position = 286
bfs_local = 13.050027, position = 335233
bfs_local = 11.187132, position = 340695
bfs_local = 10.858139, position = 340696
bfs_local = 10.625361, position = 340701
bfs_local = 10.232833, position = 756562
bfs_local = 8.076555, position = 148876
bsf = 2.84193
position = 148876
DTW Calculation = 4.11%
Count of DTW = 41047
Time for calculation lower bounding = 94.17%
Time for initializing arrays with start and end segment values = 0.000%
Time for finding the best subsequence = 5.09%
Runtime of reading 0.33
Total Runtime of finding the best subsequence 2.04


number of a  series of experiments = 1
count of threads = 6

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bfs_local = 74.805595, position = 666750
bfs_local = 71.735603, position = 500
bfs_local = 68.313744, position = 833396
bfs_local = 42.726799, position = 333792
bfs_local = 23.162945, position = 666584
bfs_local = 16.643045, position = 834063
bfs_local = 16.349966, position = 834064
bfs_local = 16.100019, position = 834066
bfs_local = 15.003776, position = 834067
bfs_local = 14.960269, position = 285
bfs_local = 14.029958, position = 286
bfs_local = 11.187132, position = 340695
bfs_local = 10.858139, position = 340696
bfs_local = 10.625361, position = 340701
bfs_local = 9.738815, position = 874191
bfs_local = 9.735808, position = 874194
bfs_local = 9.441595, position = 295832
bfs_local = 9.236251, position = 148875
bfs_local = 8.076555, position = 148876
bsf = 2.84193
position = 148876
DTW Calculation = 4.98%
Count of DTW = 49760
Time for calculation lower bounding = 92.71%
Time for initializing arrays with start and end segment values = 0.001%
Time for finding the best subsequence = 6.16%
Runtime of reading 0.33
Total Runtime of finding the best subsequence 1.07


number of a  series of experiments = 1
count of threads = 12

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bfs_local = 75.532822, position = 333375
bfs_local = 74.805595, position = 666750
bfs_local = 69.586189, position = 667000
bfs_local = 23.703405, position = 750
bfs_local = 21.280727, position = 834146
bfs_local = 16.643045, position = 834063
bfs_local = 16.349966, position = 834064
bfs_local = 16.100019, position = 834066
bfs_local = 15.003776, position = 834067
bfs_local = 14.960269, position = 285
bfs_local = 14.029958, position = 286
bfs_local = 13.208969, position = 750552
bfs_local = 11.771042, position = 925121
bfs_local = 11.738503, position = 259688
bfs_local = 11.180024, position = 259689
bfs_local = 11.172758, position = 259690
bfs_local = 10.818756, position = 259691
bfs_local = 10.232833, position = 756562
bfs_local = 10.107504, position = 286687
bfs_local = 9.441595, position = 295832
bfs_local = 9.236251, position = 148875
bfs_local = 8.076555, position = 148876
bsf = 2.84193
position = 148876
DTW Calculation = 5.85%
Count of DTW = 58535
Time for calculation lower bounding = 92.46%
Time for initializing arrays with start and end segment values = 0.001%
Time for finding the best subsequence = 6.05%
Runtime of reading 0.33
Total Runtime of finding the best subsequence 0.84


number of a  series of experiments = 1
count of threads = 18

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bfs_local = 43.764885, position = 222918
bfs_local = 32.795971, position = 944721
bfs_local = 17.376028, position = 277967
bfs_local = 16.022909, position = 834068
bfs_local = 14.960269, position = 285
bfs_local = 14.029958, position = 286
bfs_local = 13.109679, position = 890566
bfs_local = 12.614532, position = 890567
bfs_local = 12.037921, position = 785302
bfs_local = 11.879387, position = 845787
bfs_local = 11.187132, position = 340695
bfs_local = 10.858139, position = 340696
bfs_local = 10.625361, position = 340701
bfs_local = 10.107504, position = 286687
bfs_local = 9.441595, position = 295832
bfs_local = 9.236251, position = 148875
bfs_local = 8.076555, position = 148876
bsf = 2.84193
position = 148876
DTW Calculation = 6.73%
Count of DTW = 67268
Time for calculation lower bounding = 90.38%
Time for initializing arrays with start and end segment values = 0.001%
Time for finding the best subsequence = 7.51%
Runtime of reading 0.33
Total Runtime of finding the best subsequence 0.58


number of a  series of experiments = 1
count of threads = 24

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bfs_local = 87.007339, position = 333879
bfs_local = 59.838051, position = 41662
bfs_local = 38.616898, position = 42037
bfs_local = 28.813259, position = 417328
bfs_local = 28.734734, position = 333671
bfs_local = 23.061926, position = 792369
bfs_local = 20.432135, position = 208894
bfs_local = 18.881687, position = 708633
bfs_local = 16.646910, position = 708634
bfs_local = 15.744777, position = 709066
bfs_local = 15.141448, position = 709071
bfs_local = 14.960269, position = 285
bfs_local = 14.029958, position = 286
bfs_local = 13.208969, position = 750552
bfs_local = 11.169123, position = 259694
bfs_local = 10.232833, position = 756562
bfs_local = 10.144341, position = 295831
bfs_local = 9.441595, position = 295832
bfs_local = 9.236251, position = 148875
bfs_local = 8.076555, position = 148876
bsf = 2.84193
position = 148876
DTW Calculation = 7.25%
Count of DTW = 72482
Time for calculation lower bounding = 89.69%
Time for initializing arrays with start and end segment values = 0.003%
Time for finding the best subsequence = 8.36%
Runtime of reading 0.33
Total Runtime of finding the best subsequence 0.63


number of a  series of experiments = 2
count of threads = 1

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bfs_local = 205.161011, position = 68
bfs_local = 201.296082, position = 109
bfs_local = 198.454117, position = 110
bfs_local = 181.586792, position = 111
bfs_local = 177.698975, position = 112
bfs_local = 177.272400, position = 113
bfs_local = 173.585785, position = 114
bfs_local = 168.219513, position = 115
bfs_local = 166.177582, position = 116
bfs_local = 153.459076, position = 117
bfs_local = 137.000320, position = 118
bfs_local = 134.529343, position = 122
bfs_local = 128.011978, position = 123
bfs_local = 121.794434, position = 124
bfs_local = 115.587097, position = 125
bfs_local = 106.061104, position = 126
bfs_local = 90.850655, position = 127
bfs_local = 90.639030, position = 129
bfs_local = 88.713707, position = 130
bfs_local = 87.106270, position = 209
bfs_local = 86.763458, position = 210
bfs_local = 84.332901, position = 211
bfs_local = 82.125458, position = 212
bfs_local = 66.927139, position = 276
bfs_local = 57.716461, position = 277
bfs_local = 51.453182, position = 278
bfs_local = 39.160839, position = 279
bfs_local = 31.622044, position = 280
bfs_local = 28.650475, position = 281
bfs_local = 23.193090, position = 282
bfs_local = 18.942255, position = 283
bfs_local = 16.442329, position = 284
bfs_local = 14.960269, position = 285
bfs_local = 14.029958, position = 286
bfs_local = 13.455294, position = 12317
bfs_local = 13.343624, position = 12318
bfs_local = 11.353244, position = 25846
bfs_local = 11.098734, position = 43099
bfs_local = 10.341493, position = 131638
bfs_local = 10.244247, position = 131639
bfs_local = 10.189466, position = 148874
bfs_local = 9.236251, position = 148875
bfs_local = 8.076555, position = 148876
bsf = 2.84193
position = 148876
DTW Calculation = 3.31%
Count of DTW = 33110
Time for calculation lower bounding = 95.25%
Time for initializing arrays with start and end segment values = 0.000%
Time for finding the best subsequence = 4.37%
Runtime of reading 0.33
Total Runtime of finding the best subsequence 5.95


number of a  series of experiments = 2
count of threads = 3

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bfs_local = 95.459778, position = 667248
bfs_local = 76.542137, position = 333291
bfs_local = 67.455132, position = 666915
bfs_local = 31.532650, position = 666582
bfs_local = 30.613543, position = 666583
bfs_local = 23.162945, position = 666584
bfs_local = 21.950909, position = 666589
bfs_local = 21.757963, position = 666590
bfs_local = 20.977331, position = 666591
bfs_local = 20.798550, position = 666592
bfs_local = 20.684284, position = 333652
bfs_local = 20.608109, position = 333653
bfs_local = 19.764217, position = 333654
bfs_local = 18.942255, position = 283
bfs_local = 16.442329, position = 284
bfs_local = 14.960269, position = 285
bfs_local = 14.029958, position = 286
bfs_local = 13.050027, position = 335233
bfs_local = 11.187132, position = 340695
bfs_local = 10.858139, position = 340696
bfs_local = 10.625361, position = 340701
bfs_local = 10.232833, position = 756562
bfs_local = 8.076555, position = 148876
bsf = 2.84193
position = 148876
DTW Calculation = 4.11%
Count of DTW = 41047
Time for calculation lower bounding = 94.31%
Time for initializing arrays with start and end segment values = 0.000%
Time for finding the best subsequence = 5.02%
Runtime of reading 0.33
Total Runtime of finding the best subsequence 2.06


number of a  series of experiments = 2
count of threads = 6

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bfs_local = 71.735603, position = 500
bfs_local = 68.313744, position = 833396
bfs_local = 42.726799, position = 333792
bfs_local = 23.162945, position = 666584
bfs_local = 16.643045, position = 834063
bfs_local = 16.349966, position = 834064
bfs_local = 16.100019, position = 834066
bfs_local = 15.003776, position = 834067
bfs_local = 14.960269, position = 285
bfs_local = 14.029958, position = 286
bfs_local = 11.187132, position = 340695
bfs_local = 10.858139, position = 340696
bfs_local = 10.625361, position = 340701
bfs_local = 9.738815, position = 874191
bfs_local = 9.735808, position = 874194
bfs_local = 9.441595, position = 295832
bfs_local = 9.236251, position = 148875
bfs_local = 8.076555, position = 148876
bsf = 2.84193
position = 148876
DTW Calculation = 4.98%
Count of DTW = 49760
Time for calculation lower bounding = 92.73%
Time for initializing arrays with start and end segment values = 0.001%
Time for finding the best subsequence = 6.15%
Runtime of reading 0.33
Total Runtime of finding the best subsequence 1.08


number of a  series of experiments = 2
count of threads = 12

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bfs_local = 69.586189, position = 667000
bfs_local = 44.127365, position = 166896
bfs_local = 23.703405, position = 750
bfs_local = 21.280727, position = 834146
bfs_local = 16.643045, position = 834063
bfs_local = 16.349966, position = 834064
bfs_local = 16.100019, position = 834066
bfs_local = 15.003776, position = 834067
bfs_local = 14.960269, position = 285
bfs_local = 14.029958, position = 286
bfs_local = 13.208969, position = 750552
bfs_local = 11.771042, position = 925121
bfs_local = 11.738503, position = 259688
bfs_local = 11.180024, position = 259689
bfs_local = 11.172758, position = 259690
bfs_local = 10.818756, position = 259691
bfs_local = 10.232833, position = 756562
bfs_local = 10.107504, position = 286687
bfs_local = 9.441595, position = 295832
bfs_local = 9.236251, position = 148875
bfs_local = 8.076555, position = 148876
bsf = 2.84193
position = 148876
DTW Calculation = 5.85%
Count of DTW = 58535
Time for calculation lower bounding = 90.90%
Time for initializing arrays with start and end segment values = 0.001%
Time for finding the best subsequence = 6.98%
Runtime of reading 0.33
Total Runtime of finding the best subsequence 0.56


number of a  series of experiments = 2
count of threads = 18

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bfs_local = 43.764885, position = 222918
bfs_local = 32.795971, position = 944721
bfs_local = 32.395626, position = 444392
bfs_local = 32.079697, position = 722859
bfs_local = 23.771036, position = 722692
bfs_local = 17.376028, position = 277967
bfs_local = 16.022909, position = 834068
bfs_local = 14.960269, position = 285
bfs_local = 14.029958, position = 286
bfs_local = 13.109679, position = 890566
bfs_local = 12.614532, position = 890567
bfs_local = 12.037921, position = 785302
bfs_local = 11.879387, position = 845787
bfs_local = 11.187132, position = 340695
bfs_local = 10.858139, position = 340696
bfs_local = 10.625361, position = 340701
bfs_local = 10.107504, position = 286687
bfs_local = 9.441595, position = 295832
bfs_local = 9.236251, position = 148875
bfs_local = 8.076555, position = 148876
bsf = 2.84193
position = 148876
DTW Calculation = 6.73%
Count of DTW = 67268
Time for calculation lower bounding = 90.45%
Time for initializing arrays with start and end segment values = 0.001%
Time for finding the best subsequence = 7.52%
Runtime of reading 0.33
Total Runtime of finding the best subsequence 0.58


number of a  series of experiments = 2
count of threads = 24

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bfs_local = 101.604980, position = 333629
bfs_local = 28.572002, position = 333754
bfs_local = 25.539726, position = 333755
bfs_local = 20.627861, position = 417330
bfs_local = 20.608109, position = 333653
bfs_local = 19.764217, position = 333654
bfs_local = 18.956606, position = 500209
bfs_local = 18.748430, position = 500210
bfs_local = 18.390690, position = 750394
bfs_local = 16.633327, position = 750395
bfs_local = 16.442329, position = 284
bfs_local = 14.960269, position = 285
bfs_local = 14.029958, position = 286
bfs_local = 13.208969, position = 750552
bfs_local = 11.169123, position = 259694
bfs_local = 10.232833, position = 756562
bfs_local = 10.144341, position = 295831
bfs_local = 9.441595, position = 295832
bfs_local = 9.236251, position = 148875
bfs_local = 8.076555, position = 148876
bsf = 2.84193
position = 148876
DTW Calculation = 7.25%
Count of DTW = 72482
Time for calculation lower bounding = 91.73%
Time for initializing arrays with start and end segment values = 0.003%
Time for finding the best subsequence = 6.63%
Runtime of reading 0.33
Total Runtime of finding the best subsequence 0.77


