number of a  series of experiments = 0
count of threads = 1

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bfs_local = 272.069519, position = 66
bfs_local = 261.471954, position = 68
bfs_local = 260.237152, position = 69
bfs_local = 259.988586, position = 72
bfs_local = 258.978485, position = 74
bfs_local = 257.658508, position = 75
bfs_local = 246.786926, position = 109
bfs_local = 243.525406, position = 110
bfs_local = 231.208115, position = 111
bfs_local = 225.559830, position = 112
bfs_local = 222.185684, position = 113
bfs_local = 216.420135, position = 114
bfs_local = 208.122055, position = 115
bfs_local = 203.365707, position = 116
bfs_local = 192.613907, position = 117
bfs_local = 178.490372, position = 118
bfs_local = 176.677536, position = 122
bfs_local = 176.225647, position = 123
bfs_local = 171.471573, position = 213
bfs_local = 168.271423, position = 215
bfs_local = 162.154968, position = 216
bfs_local = 154.559036, position = 217
bfs_local = 147.802399, position = 218
bfs_local = 147.409470, position = 219
bfs_local = 136.467148, position = 276
bfs_local = 119.478745, position = 277
bfs_local = 111.248001, position = 278
bfs_local = 97.767136, position = 279
bfs_local = 91.186790, position = 280
bfs_local = 88.461464, position = 281
bfs_local = 81.444992, position = 282
bfs_local = 76.351196, position = 283
bfs_local = 70.627625, position = 284
bfs_local = 66.497856, position = 285
bfs_local = 64.691162, position = 286
bfs_local = 63.258091, position = 287
bfs_local = 58.124992, position = 288
bfs_local = 51.965195, position = 289
bfs_local = 49.853256, position = 290
bfs_local = 48.226753, position = 295
bfs_local = 47.366440, position = 296
bfs_local = 41.476273, position = 297
bfs_local = 35.048958, position = 298
bfs_local = 34.222851, position = 299
bfs_local = 30.823175, position = 301
bfs_local = 29.411608, position = 302
bfs_local = 28.527399, position = 303
bfs_local = 28.290102, position = 750
bfs_local = 27.358423, position = 751
bfs_local = 27.046579, position = 10387
bfs_local = 24.315945, position = 10388
bfs_local = 23.930904, position = 10389
bfs_local = 23.117802, position = 23825
bfs_local = 22.581512, position = 41362
bfs_local = 20.043903, position = 41363
bfs_local = 18.409355, position = 41364
bfs_local = 17.944241, position = 41961
bfs_local = 16.710615, position = 148876
bfs_local = 16.135900, position = 148877
bfs_local = 14.907098, position = 340701
bfs_local = 14.845065, position = 730917
bfs_local = 14.230558, position = 756562
bsf = 3.77234
position = 756562
DTW Calculation = 0.37%
Count of DTW = 3669
Time for calculation lower bounding = 81.02%
Time for initializing arrays with start and end segment values = 0.000%
Time for finding the best subsequence = 18.49%
Runtime of reading 4.57
Total Runtime of finding the best subsequence 38.79


count of threads = 10

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bfs_local = 210.589447, position = 299964
bfs_local = 171.264465, position = 99988
bfs_local = 145.814850, position = 399952
bfs_local = 122.000877, position = 199976
bfs_local = 51.730286, position = 899892
bfs_local = 50.668819, position = 800045
bfs_local = 47.796062, position = 400139
bfs_local = 47.407501, position = 400141
bfs_local = 46.352119, position = 400140
bfs_local = 41.891052, position = 900149
bfs_local = 29.046331, position = 900152
bfs_local = 24.792389, position = 900156
bfs_local = 22.190273, position = 900157
bfs_local = 21.482826, position = 300319
bfs_local = 21.116850, position = 301551
bfs_local = 17.759609, position = 301548
bfs_local = 16.877314, position = 301549
bfs_local = 16.316532, position = 811033
bfs_local = 14.845065, position = 730917
bfs_local = 14.230558, position = 756562
bsf = 3.77234
position = 756562
DTW Calculation = 0.30%
Count of DTW = 3032
Time for calculation lower bounding = 95.36%
Time for initializing arrays with start and end segment values = 0.001%
Time for finding the best subsequence = 4.03%
Runtime of reading 4.50
Total Runtime of finding the best subsequence 3.31


count of threads = 30

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bfs_local = 164.440369, position = 399960
bfs_local = 137.329971, position = 33332
bfs_local = 46.329315, position = 466620
bfs_local = 43.532913, position = 666601
bfs_local = 40.528076, position = 666600
bfs_local = 35.898132, position = 333453
bfs_local = 33.745243, position = 333455
bfs_local = 23.794403, position = 633453
bfs_local = 22.190273, position = 900157
bfs_local = 21.482826, position = 300319
bfs_local = 21.116850, position = 301551
bfs_local = 17.759609, position = 301548
bfs_local = 16.877314, position = 301549
bfs_local = 16.483076, position = 968401
bfs_local = 14.907098, position = 340701
bfs_local = 14.799088, position = 756563
bfs_local = 14.230558, position = 756562
bsf = 3.77234
position = 756562
DTW Calculation = 0.33%
Count of DTW = 3298
Time for calculation lower bounding = 88.71%
Time for initializing arrays with start and end segment values = 0.004%
Time for finding the best subsequence = 10.70%
Runtime of reading 4.56
Total Runtime of finding the best subsequence 1.22


count of threads = 60

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bfs_local = 178.367203, position = 99990
bfs_local = 70.441849, position = 383295
bfs_local = 69.436127, position = 566612
bfs_local = 40.528076, position = 666600
bfs_local = 31.404686, position = 449955
bfs_local = 26.566319, position = 516754
bfs_local = 25.316599, position = 633452
bfs_local = 24.995846, position = 900155
bfs_local = 23.134346, position = 900158
bfs_local = 22.190273, position = 900157
bfs_local = 22.122601, position = 217225
bfs_local = 20.420355, position = 217226
bfs_local = 19.389399, position = 217229
bfs_local = 18.613129, position = 217228
bfs_local = 17.759609, position = 301548
bfs_local = 16.877314, position = 301549
bfs_local = 16.483076, position = 968401
bfs_local = 15.747613, position = 756561
bfs_local = 14.230558, position = 756562
bsf = 3.77234
position = 756562
DTW Calculation = 0.36%
Count of DTW = 3554
Time for calculation lower bounding = 78.08%
Time for initializing arrays with start and end segment values = 0.008%
Time for finding the best subsequence = 21.39%
Runtime of reading 4.61
Total Runtime of finding the best subsequence 0.77


count of threads = 120

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bfs_local = 184.192505, position = 308321
bfs_local = 114.691742, position = 491649
bfs_local = 82.547546, position = 783302
bfs_local = 71.696121, position = 791635
bfs_local = 63.917747, position = 841634
bfs_local = 63.221676, position = 566645
bfs_local = 43.490498, position = 224992
bfs_local = 40.436356, position = 224993
bfs_local = 40.163605, position = 224995
bfs_local = 39.156898, position = 683318
bfs_local = 30.436478, position = 924985
bfs_local = 27.594410, position = 924983
bfs_local = 24.995846, position = 900155
bfs_local = 24.792389, position = 900156
bfs_local = 23.794403, position = 633453
bfs_local = 22.190273, position = 900157
bfs_local = 18.243155, position = 391983
bfs_local = 15.458848, position = 391984
bfs_local = 14.799088, position = 756563
bfs_local = 14.230558, position = 756562
bsf = 3.77234
position = 756562
DTW Calculation = 0.40%
Count of DTW = 4040
Time for calculation lower bounding = 59.43%
Time for initializing arrays with start and end segment values = 0.009%
Time for finding the best subsequence = 38.99%
Runtime of reading 4.50
Total Runtime of finding the best subsequence 0.75


count of threads = 180

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bfs_local = 259.709106, position = 599940
bfs_local = 76.818390, position = 444400
bfs_local = 57.717033, position = 855471
bfs_local = 51.554790, position = 622161
bfs_local = 43.532913, position = 666601
bfs_local = 40.528076, position = 666600
bfs_local = 31.404686, position = 449955
bfs_local = 22.573338, position = 811030
bfs_local = 16.823044, position = 811032
bfs_local = 16.549961, position = 811034
bfs_local = 16.316532, position = 811033
bfs_local = 15.934624, position = 756564
bfs_local = 14.230558, position = 756562
bsf = 3.77234
position = 756562
DTW Calculation = 0.31%
Count of DTW = 3131
Time for calculation lower bounding = 58.81%
Time for initializing arrays with start and end segment values = 0.008%
Time for finding the best subsequence = 40.83%
Runtime of reading 4.57
Total Runtime of finding the best subsequence 0.82


count of threads = 240

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bfs_local = 256.662445, position = 770895
bfs_local = 164.614624, position = 304193
bfs_local = 98.394249, position = 666721
bfs_local = 42.013649, position = 429203
bfs_local = 37.998222, position = 145847
bfs_local = 34.703053, position = 429201
bfs_local = 30.142466, position = 516749
bfs_local = 25.505444, position = 587607
bfs_local = 25.316599, position = 633452
bfs_local = 23.720903, position = 287660
bfs_local = 23.134346, position = 900158
bfs_local = 21.961109, position = 591881
bfs_local = 18.971350, position = 287661
bfs_local = 18.293655, position = 287662
bfs_local = 15.458848, position = 391984
bfs_local = 14.845065, position = 730917
bfs_local = 14.799088, position = 756563
bfs_local = 14.230558, position = 756562
bsf = 3.77234
position = 756562
DTW Calculation = 0.49%
Count of DTW = 4898
Time for calculation lower bounding = 62.78%
Time for initializing arrays with start and end segment values = 0.008%
Time for finding the best subsequence = 36.90%
Runtime of reading 4.57
Total Runtime of finding the best subsequence 0.87


number of a  series of experiments = 1
count of threads = 1

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bfs_local = 272.069519, position = 66
bfs_local = 261.471954, position = 68
bfs_local = 260.237152, position = 69
bfs_local = 259.988586, position = 72
bfs_local = 258.978485, position = 74
bfs_local = 257.658508, position = 75
bfs_local = 246.786926, position = 109
bfs_local = 243.525406, position = 110
bfs_local = 231.208115, position = 111
bfs_local = 225.559830, position = 112
bfs_local = 222.185684, position = 113
bfs_local = 216.420135, position = 114
bfs_local = 208.122055, position = 115
bfs_local = 203.365707, position = 116
bfs_local = 192.613907, position = 117
bfs_local = 178.490372, position = 118
bfs_local = 176.677536, position = 122
bfs_local = 176.225647, position = 123
bfs_local = 171.471573, position = 213
bfs_local = 168.271423, position = 215
bfs_local = 162.154968, position = 216
bfs_local = 154.559036, position = 217
bfs_local = 147.802399, position = 218
bfs_local = 147.409470, position = 219
bfs_local = 136.467148, position = 276
bfs_local = 119.478745, position = 277
bfs_local = 111.248001, position = 278
bfs_local = 97.767136, position = 279
bfs_local = 91.186790, position = 280
bfs_local = 88.461464, position = 281
bfs_local = 81.444992, position = 282
bfs_local = 76.351196, position = 283
bfs_local = 70.627625, position = 284
bfs_local = 66.497856, position = 285
bfs_local = 64.691162, position = 286
bfs_local = 63.258091, position = 287
bfs_local = 58.124992, position = 288
bfs_local = 51.965195, position = 289
bfs_local = 49.853256, position = 290
bfs_local = 48.226753, position = 295
bfs_local = 47.366440, position = 296
bfs_local = 41.476273, position = 297
bfs_local = 35.048958, position = 298
bfs_local = 34.222851, position = 299
bfs_local = 30.823175, position = 301
bfs_local = 29.411608, position = 302
bfs_local = 28.527399, position = 303
bfs_local = 28.290102, position = 750
bfs_local = 27.358423, position = 751
bfs_local = 27.046579, position = 10387
bfs_local = 24.315945, position = 10388
bfs_local = 23.930904, position = 10389
bfs_local = 23.117802, position = 23825
bfs_local = 22.581512, position = 41362
bfs_local = 20.043903, position = 41363
bfs_local = 18.409355, position = 41364
bfs_local = 17.944241, position = 41961
bfs_local = 16.710615, position = 148876
bfs_local = 16.135900, position = 148877
bfs_local = 14.907098, position = 340701
bfs_local = 14.845065, position = 730917
bfs_local = 14.230558, position = 756562
bsf = 3.77234
position = 756562
DTW Calculation = 0.37%
Count of DTW = 3669
Time for calculation lower bounding = 80.95%
Time for initializing arrays with start and end segment values = 0.000%
Time for finding the best subsequence = 18.57%
Runtime of reading 4.58
Total Runtime of finding the best subsequence 38.84


count of threads = 10

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bfs_local = 210.589447, position = 299964
bfs_local = 178.367203, position = 99990
bfs_local = 171.264465, position = 99988
bfs_local = 51.730286, position = 899892
bfs_local = 50.668819, position = 800045
bfs_local = 47.796062, position = 400139
bfs_local = 47.407501, position = 400141
bfs_local = 46.352119, position = 400140
bfs_local = 41.891052, position = 900149
bfs_local = 29.046331, position = 900152
bfs_local = 24.792389, position = 900156
bfs_local = 22.190273, position = 900157
bfs_local = 21.482826, position = 300319
bfs_local = 21.116850, position = 301551
bfs_local = 17.759609, position = 301548
bfs_local = 16.877314, position = 301549
bfs_local = 16.316532, position = 811033
bfs_local = 14.845065, position = 730917
bfs_local = 14.230558, position = 756562
bsf = 3.77234
position = 756562
DTW Calculation = 0.30%
Count of DTW = 3032
Time for calculation lower bounding = 95.40%
Time for initializing arrays with start and end segment values = 0.001%
Time for finding the best subsequence = 4.00%
Runtime of reading 4.65
Total Runtime of finding the best subsequence 3.32


count of threads = 30

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bfs_local = 208.206009, position = 299970
bfs_local = 137.329971, position = 33332
bfs_local = 88.028908, position = 199980
bfs_local = 48.398388, position = 466622
bfs_local = 47.400845, position = 666602
bfs_local = 43.532913, position = 666601
bfs_local = 40.528076, position = 666600
bfs_local = 36.174305, position = 333456
bfs_local = 35.898132, position = 333453
bfs_local = 33.745243, position = 333455
bfs_local = 25.949881, position = 633451
bfs_local = 23.794403, position = 633453
bfs_local = 22.190273, position = 900157
bfs_local = 21.482826, position = 300319
bfs_local = 21.116850, position = 301551
bfs_local = 18.882217, position = 301550
bfs_local = 17.759609, position = 301548
bfs_local = 16.877314, position = 301549
bfs_local = 16.483076, position = 968401
bfs_local = 14.907098, position = 340701
bfs_local = 14.799088, position = 756563
bfs_local = 14.230558, position = 756562
bsf = 3.77234
position = 756562
DTW Calculation = 0.33%
Count of DTW = 3298
Time for calculation lower bounding = 89.41%
Time for initializing arrays with start and end segment values = 0.004%
Time for finding the best subsequence = 10.00%
Runtime of reading 4.51
Total Runtime of finding the best subsequence 1.21


count of threads = 60

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bfs_local = 164.440369, position = 399960
bfs_local = 70.441849, position = 383295
bfs_local = 69.436127, position = 566612
bfs_local = 58.797066, position = 583277
bfs_local = 31.404686, position = 449955
bfs_local = 26.566319, position = 516754
bfs_local = 25.316599, position = 633452
bfs_local = 24.995846, position = 900155
bfs_local = 23.134346, position = 900158
bfs_local = 22.190273, position = 900157
bfs_local = 22.122601, position = 217225
bfs_local = 20.420355, position = 217226
bfs_local = 19.389399, position = 217229
bfs_local = 18.613129, position = 217228
bfs_local = 17.759609, position = 301548
bfs_local = 16.877314, position = 301549
bfs_local = 16.483076, position = 968401
bfs_local = 15.747613, position = 756561
bfs_local = 14.230558, position = 756562
bsf = 3.77234
position = 756562
DTW Calculation = 0.36%
Count of DTW = 3554
Time for calculation lower bounding = 77.81%
Time for initializing arrays with start and end segment values = 0.008%
Time for finding the best subsequence = 21.66%
Runtime of reading 4.50
Total Runtime of finding the best subsequence 0.76


count of threads = 120

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bfs_local = 147.790009, position = 541646
bfs_local = 114.691742, position = 491649
bfs_local = 87.320419, position = 516647
bfs_local = 86.418480, position = 524980
bfs_local = 67.913048, position = 408317
bfs_local = 63.917747, position = 841634
bfs_local = 63.221676, position = 566645
bfs_local = 45.882893, position = 224991
bfs_local = 43.490498, position = 224992
bfs_local = 40.436356, position = 224993
bfs_local = 40.163605, position = 224995
bfs_local = 39.960804, position = 558363
bfs_local = 27.594410, position = 924983
bfs_local = 27.397968, position = 516753
bfs_local = 22.190273, position = 900157
bfs_local = 18.243155, position = 391983
bfs_local = 15.458848, position = 391984
bfs_local = 14.799088, position = 756563
bfs_local = 14.230558, position = 756562
bsf = 3.77234
position = 756562
DTW Calculation = 0.40%
Count of DTW = 4040
Time for calculation lower bounding = 64.88%
Time for initializing arrays with start and end segment values = 0.010%
Time for finding the best subsequence = 34.69%
Runtime of reading 4.50
Total Runtime of finding the best subsequence 0.66


count of threads = 180

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bfs_local = 133.599091, position = 755481
bfs_local = 63.040024, position = 361077
bfs_local = 40.528076, position = 666600
bfs_local = 16.823044, position = 811032
bfs_local = 16.549961, position = 811034
bfs_local = 16.316532, position = 811033
bfs_local = 15.934624, position = 756564
bfs_local = 14.230558, position = 756562
bsf = 3.77234
position = 756562
DTW Calculation = 0.31%
Count of DTW = 3131
Time for calculation lower bounding = 59.71%
Time for initializing arrays with start and end segment values = 0.008%
Time for finding the best subsequence = 39.95%
Runtime of reading 4.50
Total Runtime of finding the best subsequence 0.82


count of threads = 240

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bfs_local = 227.113602, position = 479207
bfs_local = 172.833862, position = 112509
bfs_local = 138.467972, position = 654220
bfs_local = 130.337967, position = 720891
bfs_local = 79.486183, position = 779229
bfs_local = 64.873360, position = 120843
bfs_local = 42.013649, position = 429203
bfs_local = 39.613194, position = 145846
bfs_local = 34.703053, position = 429201
bfs_local = 30.142466, position = 516749
bfs_local = 28.355940, position = 516750
bfs_local = 25.316599, position = 633452
bfs_local = 21.961109, position = 591881
bfs_local = 18.971350, position = 287661
bfs_local = 18.293655, position = 287662
bfs_local = 15.458848, position = 391984
bfs_local = 14.845065, position = 730917
bfs_local = 14.799088, position = 756563
bfs_local = 14.230558, position = 756562
bsf = 3.77234
position = 756562
DTW Calculation = 0.49%
Count of DTW = 4898
Time for calculation lower bounding = 61.01%
Time for initializing arrays with start and end segment values = 0.009%
Time for finding the best subsequence = 38.67%
Runtime of reading 4.51
Total Runtime of finding the best subsequence 0.86


number of a  series of experiments = 2
count of threads = 1

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bfs_local = 272.069519, position = 66
bfs_local = 261.471954, position = 68
bfs_local = 260.237152, position = 69
bfs_local = 259.988586, position = 72
bfs_local = 258.978485, position = 74
bfs_local = 257.658508, position = 75
bfs_local = 246.786926, position = 109
bfs_local = 243.525406, position = 110
bfs_local = 231.208115, position = 111
bfs_local = 225.559830, position = 112
bfs_local = 222.185684, position = 113
bfs_local = 216.420135, position = 114
bfs_local = 208.122055, position = 115
bfs_local = 203.365707, position = 116
bfs_local = 192.613907, position = 117
bfs_local = 178.490372, position = 118
bfs_local = 176.677536, position = 122
bfs_local = 176.225647, position = 123
bfs_local = 171.471573, position = 213
bfs_local = 168.271423, position = 215
bfs_local = 162.154968, position = 216
bfs_local = 154.559036, position = 217
bfs_local = 147.802399, position = 218
bfs_local = 147.409470, position = 219
bfs_local = 136.467148, position = 276
bfs_local = 119.478745, position = 277
bfs_local = 111.248001, position = 278
bfs_local = 97.767136, position = 279
bfs_local = 91.186790, position = 280
bfs_local = 88.461464, position = 281
bfs_local = 81.444992, position = 282
bfs_local = 76.351196, position = 283
bfs_local = 70.627625, position = 284
bfs_local = 66.497856, position = 285
bfs_local = 64.691162, position = 286
bfs_local = 63.258091, position = 287
bfs_local = 58.124992, position = 288
bfs_local = 51.965195, position = 289
bfs_local = 49.853256, position = 290
bfs_local = 48.226753, position = 295
bfs_local = 47.366440, position = 296
bfs_local = 41.476273, position = 297
bfs_local = 35.048958, position = 298
bfs_local = 34.222851, position = 299
bfs_local = 30.823175, position = 301
bfs_local = 29.411608, position = 302
bfs_local = 28.527399, position = 303
bfs_local = 28.290102, position = 750
bfs_local = 27.358423, position = 751
bfs_local = 27.046579, position = 10387
bfs_local = 24.315945, position = 10388
bfs_local = 23.930904, position = 10389
bfs_local = 23.117802, position = 23825
bfs_local = 22.581512, position = 41362
bfs_local = 20.043903, position = 41363
bfs_local = 18.409355, position = 41364
bfs_local = 17.944241, position = 41961
bfs_local = 16.710615, position = 148876
bfs_local = 16.135900, position = 148877
bfs_local = 14.907098, position = 340701
bfs_local = 14.845065, position = 730917
bfs_local = 14.230558, position = 756562
bsf = 3.77234
position = 756562
DTW Calculation = 0.37%
Count of DTW = 3669
Time for calculation lower bounding = 81.05%
Time for initializing arrays with start and end segment values = 0.000%
Time for finding the best subsequence = 18.47%
Runtime of reading 4.57
Total Runtime of finding the best subsequence 38.79


count of threads = 10

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bfs_local = 210.589447, position = 299964
bfs_local = 112.032097, position = 199977
bfs_local = 51.730286, position = 899892
bfs_local = 50.668819, position = 800045
bfs_local = 47.796062, position = 400139
bfs_local = 47.407501, position = 400141
bfs_local = 46.352119, position = 400140
bfs_local = 24.792389, position = 900156
bfs_local = 22.190273, position = 900157
bfs_local = 21.482826, position = 300319
bfs_local = 21.116850, position = 301551
bfs_local = 17.759609, position = 301548
bfs_local = 16.877314, position = 301549
bfs_local = 16.316532, position = 811033
bfs_local = 14.845065, position = 730917
bfs_local = 14.230558, position = 756562
bsf = 3.77234
position = 756562
DTW Calculation = 0.30%
Count of DTW = 3032
Time for calculation lower bounding = 95.44%
Time for initializing arrays with start and end segment values = 0.001%
Time for finding the best subsequence = 3.96%
Runtime of reading 4.50
Total Runtime of finding the best subsequence 3.36


count of threads = 30

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bfs_local = 208.206009, position = 299970
bfs_local = 135.493073, position = 33333
bfs_local = 46.565601, position = 466621
bfs_local = 43.532913, position = 666601
bfs_local = 40.528076, position = 666600
bfs_local = 35.898132, position = 333453
bfs_local = 33.745243, position = 333455
bfs_local = 25.949881, position = 633451
bfs_local = 23.794403, position = 633453
bfs_local = 23.495186, position = 300321
bfs_local = 22.190273, position = 900157
bfs_local = 21.482826, position = 300319
bfs_local = 21.116850, position = 301551
bfs_local = 17.759609, position = 301548
bfs_local = 16.877314, position = 301549
bfs_local = 16.483076, position = 968401
bfs_local = 14.907098, position = 340701
bfs_local = 14.799088, position = 756563
bfs_local = 14.230558, position = 756562
bsf = 3.77234
position = 756562
DTW Calculation = 0.33%
Count of DTW = 3298
Time for calculation lower bounding = 87.84%
Time for initializing arrays with start and end segment values = 0.004%
Time for finding the best subsequence = 11.58%
Runtime of reading 4.51
Total Runtime of finding the best subsequence 1.23


count of threads = 60

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bfs_local = 88.028908, position = 199980
bfs_local = 70.441849, position = 383295
bfs_local = 69.436127, position = 566612
bfs_local = 58.797066, position = 583277
bfs_local = 31.404686, position = 449955
bfs_local = 26.566319, position = 516754
bfs_local = 25.316599, position = 633452
bfs_local = 24.995846, position = 900155
bfs_local = 23.134346, position = 900158
bfs_local = 22.190273, position = 900157
bfs_local = 22.122601, position = 217225
bfs_local = 20.420355, position = 217226
bfs_local = 19.389399, position = 217229
bfs_local = 18.613129, position = 217228
bfs_local = 17.759609, position = 301548
bfs_local = 16.877314, position = 301549
bfs_local = 16.483076, position = 968401
bfs_local = 15.747613, position = 756561
bfs_local = 14.230558, position = 756562
bsf = 3.77234
position = 756562
DTW Calculation = 0.36%
Count of DTW = 3554
Time for calculation lower bounding = 77.59%
Time for initializing arrays with start and end segment values = 0.009%
Time for finding the best subsequence = 21.86%
Runtime of reading 4.58
Total Runtime of finding the best subsequence 0.75


count of threads = 120

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bfs_local = 116.024490, position = 491648
bfs_local = 114.691742, position = 491649
bfs_local = 64.218323, position = 674975
bfs_local = 63.917747, position = 841634
bfs_local = 63.487434, position = 683308
bfs_local = 53.518360, position = 674973
bfs_local = 53.400757, position = 674974
bfs_local = 43.490498, position = 224992
bfs_local = 40.436356, position = 224993
bfs_local = 39.156898, position = 683318
bfs_local = 27.594410, position = 924983
bfs_local = 24.995846, position = 900155
bfs_local = 24.792389, position = 900156
bfs_local = 22.190273, position = 900157
bfs_local = 15.458848, position = 391984
bfs_local = 14.799088, position = 756563
bfs_local = 14.230558, position = 756562
bsf = 3.77234
position = 756562
DTW Calculation = 0.40%
Count of DTW = 4040
Time for calculation lower bounding = 66.46%
Time for initializing arrays with start and end segment values = 0.009%
Time for finding the best subsequence = 33.11%
Runtime of reading 4.69
Total Runtime of finding the best subsequence 0.67


count of threads = 180

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bfs_local = 196.339386, position = 299972
bfs_local = 120.509644, position = 277751
bfs_local = 57.057896, position = 855470
bfs_local = 45.462509, position = 449957
bfs_local = 16.823044, position = 811032
bfs_local = 16.549961, position = 811034
bfs_local = 16.316532, position = 811033
bfs_local = 15.934624, position = 756564
bfs_local = 14.230558, position = 756562
bsf = 3.77234
position = 756562
DTW Calculation = 0.31%
Count of DTW = 3131
Time for calculation lower bounding = 57.43%
Time for initializing arrays with start and end segment values = 0.008%
Time for finding the best subsequence = 42.25%
Runtime of reading 4.50
Total Runtime of finding the best subsequence 0.82


count of threads = 240

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bfs_local = 164.614624, position = 304193
bfs_local = 103.898125, position = 308360
bfs_local = 79.486183, position = 779229
bfs_local = 51.569679, position = 220852
bfs_local = 42.013649, position = 429203
bfs_local = 37.998222, position = 145847
bfs_local = 34.703053, position = 429201
bfs_local = 30.142466, position = 516749
bfs_local = 25.505444, position = 587607
bfs_local = 25.316599, position = 633452
bfs_local = 23.134346, position = 900158
bfs_local = 21.961109, position = 591881
bfs_local = 18.971350, position = 287661
bfs_local = 18.293655, position = 287662
bfs_local = 15.458848, position = 391984
bfs_local = 14.845065, position = 730917
bfs_local = 14.799088, position = 756563
bfs_local = 14.230558, position = 756562
bsf = 3.77234
position = 756562
DTW Calculation = 0.49%
Count of DTW = 4898
Time for calculation lower bounding = 57.70%
Time for initializing arrays with start and end segment values = 0.010%
Time for finding the best subsequence = 41.96%
Runtime of reading 4.50
Total Runtime of finding the best subsequence 0.88


