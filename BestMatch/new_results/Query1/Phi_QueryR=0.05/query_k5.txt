number of a  series of experiments = 0
count of threads = 1

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bfs_local = 272.069519, position = 66
bfs_local = 261.471954, position = 68
bfs_local = 260.237152, position = 69
bfs_local = 259.988586, position = 72
bfs_local = 258.978485, position = 74
bfs_local = 257.658508, position = 75
bfs_local = 246.786926, position = 109
bfs_local = 243.525406, position = 110
bfs_local = 231.208115, position = 111
bfs_local = 225.559830, position = 112
bfs_local = 222.185684, position = 113
bfs_local = 216.420135, position = 114
bfs_local = 208.122055, position = 115
bfs_local = 203.365707, position = 116
bfs_local = 192.613907, position = 117
bfs_local = 178.490372, position = 118
bfs_local = 176.677536, position = 122
bfs_local = 176.225647, position = 123
bfs_local = 171.471573, position = 213
bfs_local = 168.271423, position = 215
bfs_local = 162.154968, position = 216
bfs_local = 154.559036, position = 217
bfs_local = 147.802399, position = 218
bfs_local = 147.409470, position = 219
bfs_local = 136.467148, position = 276
bfs_local = 119.478745, position = 277
bfs_local = 111.248001, position = 278
bfs_local = 97.767136, position = 279
bfs_local = 91.186790, position = 280
bfs_local = 88.461464, position = 281
bfs_local = 81.444992, position = 282
bfs_local = 76.351196, position = 283
bfs_local = 70.627625, position = 284
bfs_local = 66.497856, position = 285
bfs_local = 64.691162, position = 286
bfs_local = 63.258091, position = 287
bfs_local = 58.124992, position = 288
bfs_local = 51.965195, position = 289
bfs_local = 49.853256, position = 290
bfs_local = 48.226753, position = 295
bfs_local = 47.366440, position = 296
bfs_local = 41.476273, position = 297
bfs_local = 35.048958, position = 298
bfs_local = 34.222851, position = 299
bfs_local = 30.823175, position = 301
bfs_local = 29.411608, position = 302
bfs_local = 28.527399, position = 303
bfs_local = 28.290102, position = 750
bfs_local = 27.358423, position = 751
bfs_local = 27.046579, position = 10387
bfs_local = 24.315945, position = 10388
bfs_local = 23.930904, position = 10389
bfs_local = 23.117802, position = 23825
bfs_local = 22.581512, position = 41362
bfs_local = 20.043903, position = 41363
bfs_local = 18.409355, position = 41364
bfs_local = 17.944241, position = 41961
bfs_local = 16.710615, position = 148876
bfs_local = 16.135900, position = 148877
bfs_local = 14.907098, position = 340701
bfs_local = 14.845065, position = 730917
bfs_local = 14.230558, position = 756562
bsf = 3.77234
position = 756562
DTW Calculation = 0.37%
Count of DTW = 3669
Time for calculation lower bounding = 87.36%
Time for initializing arrays with start and end segment values = 0.000%
Time for finding the best subsequence = 12.12%
Runtime of reading 4.58
Total Runtime of finding the best subsequence 36.02


count of threads = 10

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bfs_local = 237.109283, position = 499941
bfs_local = 171.264465, position = 99988
bfs_local = 122.000877, position = 199976
bfs_local = 102.840683, position = 199978
bfs_local = 88.028908, position = 199980
bfs_local = 56.890251, position = 899893
bfs_local = 51.730286, position = 899892
bfs_local = 50.668819, position = 800045
bfs_local = 46.352119, position = 400140
bfs_local = 45.065697, position = 900161
bfs_local = 24.792389, position = 900156
bfs_local = 22.190273, position = 900157
bfs_local = 21.482826, position = 300319
bfs_local = 21.116850, position = 301551
bfs_local = 20.214142, position = 301547
bfs_local = 17.759609, position = 301548
bfs_local = 16.877314, position = 301549
bfs_local = 16.823044, position = 811032
bfs_local = 16.316532, position = 811033
bfs_local = 14.845065, position = 730917
bfs_local = 14.230558, position = 756562
bsf = 3.77234
position = 756562
DTW Calculation = 0.31%
Count of DTW = 3103
Time for calculation lower bounding = 96.16%
Time for initializing arrays with start and end segment values = 0.001%
Time for finding the best subsequence = 3.23%
Runtime of reading 4.61
Total Runtime of finding the best subsequence 3.29


count of threads = 30

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bfs_local = 40.528076, position = 666600
bfs_local = 39.233807, position = 333452
bfs_local = 33.745243, position = 333455
bfs_local = 29.624767, position = 566834
bfs_local = 25.901678, position = 900154
bfs_local = 25.316599, position = 633452
bfs_local = 23.134346, position = 900158
bfs_local = 22.190273, position = 900157
bfs_local = 21.482826, position = 300319
bfs_local = 20.214142, position = 301547
bfs_local = 18.882217, position = 301550
bfs_local = 17.759609, position = 301548
bfs_local = 16.877314, position = 301549
bfs_local = 16.483076, position = 968401
bfs_local = 14.907098, position = 340701
bfs_local = 14.230558, position = 756562
bsf = 3.77234
position = 756562
DTW Calculation = 0.34%
Count of DTW = 3424
Time for calculation lower bounding = 90.08%
Time for initializing arrays with start and end segment values = 0.004%
Time for finding the best subsequence = 9.32%
Runtime of reading 4.50
Total Runtime of finding the best subsequence 1.21


count of threads = 60

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bfs_local = 248.053787, position = 166650
bfs_local = 74.190262, position = 383296
bfs_local = 69.436127, position = 566612
bfs_local = 52.488773, position = 466623
bfs_local = 45.462509, position = 449957
bfs_local = 40.528076, position = 666600
bfs_local = 37.858158, position = 449956
bfs_local = 31.404686, position = 449955
bfs_local = 29.562687, position = 633455
bfs_local = 27.397968, position = 516753
bfs_local = 25.949881, position = 633451
bfs_local = 25.857635, position = 633454
bfs_local = 23.794403, position = 633453
bfs_local = 21.482826, position = 300319
bfs_local = 20.420355, position = 217226
bfs_local = 19.825230, position = 217227
bfs_local = 18.613129, position = 217228
bfs_local = 17.759609, position = 301548
bfs_local = 16.483076, position = 968401
bfs_local = 15.934624, position = 756564
bfs_local = 14.907098, position = 340701
bfs_local = 14.230558, position = 756562
bsf = 3.77234
position = 756562
DTW Calculation = 0.39%
Count of DTW = 3892
Time for calculation lower bounding = 77.37%
Time for initializing arrays with start and end segment values = 0.008%
Time for finding the best subsequence = 22.10%
Runtime of reading 4.51
Total Runtime of finding the best subsequence 0.75


count of threads = 120

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bfs_local = 82.157654, position = 524981
bfs_local = 65.062119, position = 566646
bfs_local = 40.436356, position = 224993
bfs_local = 38.827389, position = 224994
bfs_local = 37.481529, position = 508415
bfs_local = 28.173296, position = 516752
bfs_local = 27.559216, position = 516751
bfs_local = 27.419325, position = 516755
bfs_local = 27.397968, position = 516753
bfs_local = 26.566319, position = 516754
bfs_local = 25.316599, position = 633452
bfs_local = 21.776529, position = 41959
bfs_local = 17.944241, position = 41961
bfs_local = 15.458848, position = 391984
bfs_local = 14.845065, position = 730917
bfs_local = 14.230558, position = 756562
bsf = 3.77234
position = 756562
DTW Calculation = 0.45%
Count of DTW = 4493
Time for calculation lower bounding = 61.33%
Time for initializing arrays with start and end segment values = 0.008%
Time for finding the best subsequence = 38.29%
Runtime of reading 4.50
Total Runtime of finding the best subsequence 0.73


count of threads = 180

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bfs_local = 57.717033, position = 855471
bfs_local = 45.462509, position = 449957
bfs_local = 19.123877, position = 811031
bfs_local = 16.549961, position = 811034
bfs_local = 16.316532, position = 811033
bfs_local = 14.230558, position = 756562
bsf = 3.77234
position = 756562
DTW Calculation = 0.34%
Count of DTW = 3390
Time for calculation lower bounding = 60.15%
Time for initializing arrays with start and end segment values = 0.008%
Time for finding the best subsequence = 39.50%
Runtime of reading 4.56
Total Runtime of finding the best subsequence 0.77


count of threads = 240

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bfs_local = 180.053787, position = 516710
bfs_local = 105.457802, position = 791731
bfs_local = 102.022018, position = 508377
bfs_local = 34.703053, position = 429201
bfs_local = 33.745243, position = 333455
bfs_local = 25.759672, position = 620966
bfs_local = 25.316599, position = 633452
bfs_local = 23.134346, position = 900158
bfs_local = 22.190273, position = 900157
bfs_local = 18.293655, position = 287662
bfs_local = 15.458848, position = 391984
bfs_local = 14.230558, position = 756562
bsf = 3.77234
position = 756562
DTW Calculation = 0.57%
Count of DTW = 5730
Time for calculation lower bounding = 60.63%
Time for initializing arrays with start and end segment values = 0.012%
Time for finding the best subsequence = 39.01%
Runtime of reading 4.50
Total Runtime of finding the best subsequence 0.84


number of a  series of experiments = 1
count of threads = 1

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bfs_local = 272.069519, position = 66
bfs_local = 261.471954, position = 68
bfs_local = 260.237152, position = 69
bfs_local = 259.988586, position = 72
bfs_local = 258.978485, position = 74
bfs_local = 257.658508, position = 75
bfs_local = 246.786926, position = 109
bfs_local = 243.525406, position = 110
bfs_local = 231.208115, position = 111
bfs_local = 225.559830, position = 112
bfs_local = 222.185684, position = 113
bfs_local = 216.420135, position = 114
bfs_local = 208.122055, position = 115
bfs_local = 203.365707, position = 116
bfs_local = 192.613907, position = 117
bfs_local = 178.490372, position = 118
bfs_local = 176.677536, position = 122
bfs_local = 176.225647, position = 123
bfs_local = 171.471573, position = 213
bfs_local = 168.271423, position = 215
bfs_local = 162.154968, position = 216
bfs_local = 154.559036, position = 217
bfs_local = 147.802399, position = 218
bfs_local = 147.409470, position = 219
bfs_local = 136.467148, position = 276
bfs_local = 119.478745, position = 277
bfs_local = 111.248001, position = 278
bfs_local = 97.767136, position = 279
bfs_local = 91.186790, position = 280
bfs_local = 88.461464, position = 281
bfs_local = 81.444992, position = 282
bfs_local = 76.351196, position = 283
bfs_local = 70.627625, position = 284
bfs_local = 66.497856, position = 285
bfs_local = 64.691162, position = 286
bfs_local = 63.258091, position = 287
bfs_local = 58.124992, position = 288
bfs_local = 51.965195, position = 289
bfs_local = 49.853256, position = 290
bfs_local = 48.226753, position = 295
bfs_local = 47.366440, position = 296
bfs_local = 41.476273, position = 297
bfs_local = 35.048958, position = 298
bfs_local = 34.222851, position = 299
bfs_local = 30.823175, position = 301
bfs_local = 29.411608, position = 302
bfs_local = 28.527399, position = 303
bfs_local = 28.290102, position = 750
bfs_local = 27.358423, position = 751
bfs_local = 27.046579, position = 10387
bfs_local = 24.315945, position = 10388
bfs_local = 23.930904, position = 10389
bfs_local = 23.117802, position = 23825
bfs_local = 22.581512, position = 41362
bfs_local = 20.043903, position = 41363
bfs_local = 18.409355, position = 41364
bfs_local = 17.944241, position = 41961
bfs_local = 16.710615, position = 148876
bfs_local = 16.135900, position = 148877
bfs_local = 14.907098, position = 340701
bfs_local = 14.845065, position = 730917
bfs_local = 14.230558, position = 756562
bsf = 3.77234
position = 756562
DTW Calculation = 0.37%
Count of DTW = 3669
Time for calculation lower bounding = 87.33%
Time for initializing arrays with start and end segment values = 0.000%
Time for finding the best subsequence = 12.14%
Runtime of reading 4.50
Total Runtime of finding the best subsequence 36.04


count of threads = 10

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bfs_local = 257.877655, position = 499940
bfs_local = 171.264465, position = 99988
bfs_local = 122.000877, position = 199976
bfs_local = 88.028908, position = 199980
bfs_local = 51.730286, position = 899892
bfs_local = 50.668819, position = 800045
bfs_local = 46.352119, position = 400140
bfs_local = 24.792389, position = 900156
bfs_local = 22.190273, position = 900157
bfs_local = 21.482826, position = 300319
bfs_local = 21.116850, position = 301551
bfs_local = 20.214142, position = 301547
bfs_local = 17.759609, position = 301548
bfs_local = 16.877314, position = 301549
bfs_local = 16.823044, position = 811032
bfs_local = 16.316532, position = 811033
bfs_local = 14.845065, position = 730917
bfs_local = 14.230558, position = 756562
bsf = 3.77234
position = 756562
DTW Calculation = 0.31%
Count of DTW = 3103
Time for calculation lower bounding = 95.89%
Time for initializing arrays with start and end segment values = 0.001%
Time for finding the best subsequence = 3.50%
Runtime of reading 4.56
Total Runtime of finding the best subsequence 3.30


count of threads = 30

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bfs_local = 40.528076, position = 666600
bfs_local = 39.233807, position = 333452
bfs_local = 33.745243, position = 333455
bfs_local = 29.624767, position = 566834
bfs_local = 25.901678, position = 900154
bfs_local = 25.857635, position = 633454
bfs_local = 25.316599, position = 633452
bfs_local = 23.134346, position = 900158
bfs_local = 22.190273, position = 900157
bfs_local = 21.482826, position = 300319
bfs_local = 20.214142, position = 301547
bfs_local = 18.882217, position = 301550
bfs_local = 17.759609, position = 301548
bfs_local = 16.877314, position = 301549
bfs_local = 16.483076, position = 968401
bfs_local = 14.907098, position = 340701
bfs_local = 14.230558, position = 756562
bsf = 3.77234
position = 756562
DTW Calculation = 0.34%
Count of DTW = 3424
Time for calculation lower bounding = 90.01%
Time for initializing arrays with start and end segment values = 0.004%
Time for finding the best subsequence = 9.39%
Runtime of reading 4.61
Total Runtime of finding the best subsequence 1.20


count of threads = 60

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bfs_local = 189.569000, position = 83325
bfs_local = 74.190262, position = 383296
bfs_local = 69.436127, position = 566612
bfs_local = 52.488773, position = 466623
bfs_local = 40.528076, position = 666600
bfs_local = 37.858158, position = 449956
bfs_local = 31.404686, position = 449955
bfs_local = 29.562687, position = 633455
bfs_local = 27.397968, position = 516753
bfs_local = 25.949881, position = 633451
bfs_local = 23.794403, position = 633453
bfs_local = 21.482826, position = 300319
bfs_local = 20.420355, position = 217226
bfs_local = 19.825230, position = 217227
bfs_local = 18.613129, position = 217228
bfs_local = 17.759609, position = 301548
bfs_local = 16.483076, position = 968401
bfs_local = 15.934624, position = 756564
bfs_local = 14.907098, position = 340701
bfs_local = 14.230558, position = 756562
bsf = 3.77234
position = 756562
DTW Calculation = 0.39%
Count of DTW = 3892
Time for calculation lower bounding = 77.76%
Time for initializing arrays with start and end segment values = 0.008%
Time for finding the best subsequence = 21.69%
Runtime of reading 4.50
Total Runtime of finding the best subsequence 0.75


count of threads = 120

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bfs_local = 65.062119, position = 566646
bfs_local = 58.522182, position = 841633
bfs_local = 53.518360, position = 674973
bfs_local = 45.882893, position = 224991
bfs_local = 38.827389, position = 224994
bfs_local = 32.973480, position = 16727
bfs_local = 27.559216, position = 516751
bfs_local = 27.419325, position = 516755
bfs_local = 27.397968, position = 516753
bfs_local = 26.566319, position = 516754
bfs_local = 25.515665, position = 591882
bfs_local = 25.316599, position = 633452
bfs_local = 21.776529, position = 41959
bfs_local = 17.944241, position = 41961
bfs_local = 15.458848, position = 391984
bfs_local = 14.845065, position = 730917
bfs_local = 14.230558, position = 756562
bsf = 3.77234
position = 756562
DTW Calculation = 0.45%
Count of DTW = 4493
Time for calculation lower bounding = 65.04%
Time for initializing arrays with start and end segment values = 0.010%
Time for finding the best subsequence = 34.55%
Runtime of reading 4.50
Total Runtime of finding the best subsequence 0.69


count of threads = 180

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bfs_local = 85.313087, position = 383298
bfs_local = 81.663582, position = 405518
bfs_local = 79.348236, position = 355523
bfs_local = 65.108597, position = 361078
bfs_local = 62.351185, position = 622163
bfs_local = 52.488773, position = 466623
bfs_local = 43.532913, position = 666601
bfs_local = 40.528076, position = 666600
bfs_local = 16.823044, position = 811032
bfs_local = 16.549961, position = 811034
bfs_local = 16.316532, position = 811033
bfs_local = 14.230558, position = 756562
bsf = 3.77234
position = 756562
DTW Calculation = 0.34%
Count of DTW = 3390
Time for calculation lower bounding = 58.88%
Time for initializing arrays with start and end segment values = 0.008%
Time for finding the best subsequence = 40.77%
Runtime of reading 4.56
Total Runtime of finding the best subsequence 0.79


count of threads = 240

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bfs_local = 180.053787, position = 516710
bfs_local = 132.882584, position = 529211
bfs_local = 88.716949, position = 920910
bfs_local = 74.726822, position = 366697
bfs_local = 68.412209, position = 558380
bfs_local = 64.873360, position = 120843
bfs_local = 51.569679, position = 220852
bfs_local = 38.323048, position = 429202
bfs_local = 37.998222, position = 145847
bfs_local = 34.703053, position = 429201
bfs_local = 25.924377, position = 620965
bfs_local = 25.527267, position = 754245
bfs_local = 25.316599, position = 633452
bfs_local = 23.134346, position = 900158
bfs_local = 22.190273, position = 900157
bfs_local = 20.269180, position = 129595
bfs_local = 19.846710, position = 391985
bfs_local = 19.389399, position = 217229
bfs_local = 18.293655, position = 287662
bfs_local = 15.458848, position = 391984
bfs_local = 14.230558, position = 756562
bsf = 3.77234
position = 756562
DTW Calculation = 0.57%
Count of DTW = 5730
Time for calculation lower bounding = 59.47%
Time for initializing arrays with start and end segment values = 0.012%
Time for finding the best subsequence = 40.20%
Runtime of reading 4.50
Total Runtime of finding the best subsequence 0.85


number of a  series of experiments = 2
count of threads = 1

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bfs_local = 272.069519, position = 66
bfs_local = 261.471954, position = 68
bfs_local = 260.237152, position = 69
bfs_local = 259.988586, position = 72
bfs_local = 258.978485, position = 74
bfs_local = 257.658508, position = 75
bfs_local = 246.786926, position = 109
bfs_local = 243.525406, position = 110
bfs_local = 231.208115, position = 111
bfs_local = 225.559830, position = 112
bfs_local = 222.185684, position = 113
bfs_local = 216.420135, position = 114
bfs_local = 208.122055, position = 115
bfs_local = 203.365707, position = 116
bfs_local = 192.613907, position = 117
bfs_local = 178.490372, position = 118
bfs_local = 176.677536, position = 122
bfs_local = 176.225647, position = 123
bfs_local = 171.471573, position = 213
bfs_local = 168.271423, position = 215
bfs_local = 162.154968, position = 216
bfs_local = 154.559036, position = 217
bfs_local = 147.802399, position = 218
bfs_local = 147.409470, position = 219
bfs_local = 136.467148, position = 276
bfs_local = 119.478745, position = 277
bfs_local = 111.248001, position = 278
bfs_local = 97.767136, position = 279
bfs_local = 91.186790, position = 280
bfs_local = 88.461464, position = 281
bfs_local = 81.444992, position = 282
bfs_local = 76.351196, position = 283
bfs_local = 70.627625, position = 284
bfs_local = 66.497856, position = 285
bfs_local = 64.691162, position = 286
bfs_local = 63.258091, position = 287
bfs_local = 58.124992, position = 288
bfs_local = 51.965195, position = 289
bfs_local = 49.853256, position = 290
bfs_local = 48.226753, position = 295
bfs_local = 47.366440, position = 296
bfs_local = 41.476273, position = 297
bfs_local = 35.048958, position = 298
bfs_local = 34.222851, position = 299
bfs_local = 30.823175, position = 301
bfs_local = 29.411608, position = 302
bfs_local = 28.527399, position = 303
bfs_local = 28.290102, position = 750
bfs_local = 27.358423, position = 751
bfs_local = 27.046579, position = 10387
bfs_local = 24.315945, position = 10388
bfs_local = 23.930904, position = 10389
bfs_local = 23.117802, position = 23825
bfs_local = 22.581512, position = 41362
bfs_local = 20.043903, position = 41363
bfs_local = 18.409355, position = 41364
bfs_local = 17.944241, position = 41961
bfs_local = 16.710615, position = 148876
bfs_local = 16.135900, position = 148877
bfs_local = 14.907098, position = 340701
bfs_local = 14.845065, position = 730917
bfs_local = 14.230558, position = 756562
bsf = 3.77234
position = 756562
DTW Calculation = 0.37%
Count of DTW = 3669
Time for calculation lower bounding = 87.36%
Time for initializing arrays with start and end segment values = 0.000%
Time for finding the best subsequence = 12.12%
Runtime of reading 4.56
Total Runtime of finding the best subsequence 35.98


count of threads = 10

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bfs_local = 237.109283, position = 499941
bfs_local = 225.258545, position = 499942
bfs_local = 171.264465, position = 99988
bfs_local = 122.000877, position = 199976
bfs_local = 88.028908, position = 199980
bfs_local = 56.890251, position = 899893
bfs_local = 51.730286, position = 899892
bfs_local = 50.668819, position = 800045
bfs_local = 46.352119, position = 400140
bfs_local = 26.195553, position = 900153
bfs_local = 24.792389, position = 900156
bfs_local = 22.190273, position = 900157
bfs_local = 21.482826, position = 300319
bfs_local = 21.116850, position = 301551
bfs_local = 20.214142, position = 301547
bfs_local = 17.759609, position = 301548
bfs_local = 16.877314, position = 301549
bfs_local = 16.823044, position = 811032
bfs_local = 16.316532, position = 811033
bfs_local = 14.845065, position = 730917
bfs_local = 14.230558, position = 756562
bsf = 3.77234
position = 756562
DTW Calculation = 0.31%
Count of DTW = 3103
Time for calculation lower bounding = 96.15%
Time for initializing arrays with start and end segment values = 0.001%
Time for finding the best subsequence = 3.24%
Runtime of reading 4.58
Total Runtime of finding the best subsequence 3.29


count of threads = 30

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bfs_local = 145.277740, position = 733262
bfs_local = 55.829483, position = 666603
bfs_local = 40.528076, position = 666600
bfs_local = 39.233807, position = 333452
bfs_local = 33.745243, position = 333455
bfs_local = 32.277740, position = 566837
bfs_local = 25.901678, position = 900154
bfs_local = 25.857635, position = 633454
bfs_local = 25.316599, position = 633452
bfs_local = 23.134346, position = 900158
bfs_local = 22.190273, position = 900157
bfs_local = 21.482826, position = 300319
bfs_local = 18.882217, position = 301550
bfs_local = 17.759609, position = 301548
bfs_local = 16.877314, position = 301549
bfs_local = 16.483076, position = 968401
bfs_local = 14.907098, position = 340701
bfs_local = 14.230558, position = 756562
bsf = 3.77234
position = 756562
DTW Calculation = 0.34%
Count of DTW = 3424
Time for calculation lower bounding = 89.82%
Time for initializing arrays with start and end segment values = 0.006%
Time for finding the best subsequence = 9.58%
Runtime of reading 4.57
Total Runtime of finding the best subsequence 1.21


count of threads = 60

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bfs_local = 189.569000, position = 83325
bfs_local = 74.190262, position = 383296
bfs_local = 69.436127, position = 566612
bfs_local = 66.540260, position = 566614
bfs_local = 58.797066, position = 583277
bfs_local = 52.488773, position = 466623
bfs_local = 40.528076, position = 666600
bfs_local = 37.858158, position = 449956
bfs_local = 31.404686, position = 449955
bfs_local = 29.562687, position = 633455
bfs_local = 27.397968, position = 516753
bfs_local = 25.949881, position = 633451
bfs_local = 23.794403, position = 633453
bfs_local = 21.482826, position = 300319
bfs_local = 20.420355, position = 217226
bfs_local = 19.825230, position = 217227
bfs_local = 18.613129, position = 217228
bfs_local = 17.759609, position = 301548
bfs_local = 16.483076, position = 968401
bfs_local = 15.934624, position = 756564
bfs_local = 14.907098, position = 340701
bfs_local = 14.230558, position = 756562
bsf = 3.77234
position = 756562
DTW Calculation = 0.39%
Count of DTW = 3892
Time for calculation lower bounding = 77.86%
Time for initializing arrays with start and end segment values = 0.008%
Time for finding the best subsequence = 21.61%
Runtime of reading 4.50
Total Runtime of finding the best subsequence 0.75


count of threads = 120

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bfs_local = 82.157654, position = 524981
bfs_local = 69.374268, position = 983296
bfs_local = 66.477348, position = 408319
bfs_local = 65.062119, position = 566646
bfs_local = 58.522182, position = 841633
bfs_local = 53.518360, position = 674973
bfs_local = 40.436356, position = 224993
bfs_local = 40.163605, position = 224995
bfs_local = 38.827389, position = 224994
bfs_local = 27.559216, position = 516751
bfs_local = 27.419325, position = 516755
bfs_local = 27.397968, position = 516753
bfs_local = 26.566319, position = 516754
bfs_local = 25.316599, position = 633452
bfs_local = 24.399836, position = 300317
bfs_local = 21.776529, position = 41959
bfs_local = 17.944241, position = 41961
bfs_local = 15.458848, position = 391984
bfs_local = 14.845065, position = 730917
bfs_local = 14.230558, position = 756562
bsf = 3.77234
position = 756562
DTW Calculation = 0.45%
Count of DTW = 4493
Time for calculation lower bounding = 61.46%
Time for initializing arrays with start and end segment values = 0.009%
Time for finding the best subsequence = 38.16%
Runtime of reading 4.57
Total Runtime of finding the best subsequence 0.72


count of threads = 180

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bfs_local = 90.117584, position = 644381
bfs_local = 60.094303, position = 488844
bfs_local = 45.462509, position = 449957
bfs_local = 43.532913, position = 666601
bfs_local = 16.316532, position = 811033
bfs_local = 14.230558, position = 756562
bsf = 3.77234
position = 756562
DTW Calculation = 0.34%
Count of DTW = 3390
Time for calculation lower bounding = 60.67%
Time for initializing arrays with start and end segment values = 0.008%
Time for finding the best subsequence = 38.98%
Runtime of reading 4.51
Total Runtime of finding the best subsequence 0.78


count of threads = 240

START:	Read query
FINISH:	Read query

START:	Read time series and fill matrix of subsequences
FINISH:	Read time series and fill matrix of subsequences
bfs_local = 180.053787, position = 516710
bfs_local = 82.302261, position = 104175
bfs_local = 51.569679, position = 220852
bfs_local = 48.709499, position = 220851
bfs_local = 37.998222, position = 145847
bfs_local = 34.703053, position = 429201
bfs_local = 33.561455, position = 891832
bfs_local = 25.316599, position = 633452
bfs_local = 23.134346, position = 900158
bfs_local = 22.190273, position = 900157
bfs_local = 19.846710, position = 391985
bfs_local = 18.971350, position = 287661
bfs_local = 18.293655, position = 287662
bfs_local = 15.458848, position = 391984
bfs_local = 14.230558, position = 756562
bsf = 3.77234
position = 756562
DTW Calculation = 0.57%
Count of DTW = 5730
Time for calculation lower bounding = 60.52%
Time for initializing arrays with start and end segment values = 0.009%
Time for finding the best subsequence = 39.14%
Runtime of reading 4.50
Total Runtime of finding the best subsequence 0.84


